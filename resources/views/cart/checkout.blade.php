@extends('frontend.layout_page')

@section('someCSS')
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/product_styles.css')}}">
  <style>
    .cart_item_name {
      margin-left: 0%;
    }
    .alamat_name {
      font-size: 12px;
      font-weight: 500;
      color: rgba(0, 0, 0, 0.82);
    }
    .alamat_location {
      font-size: 12px;
      font-weight: 400;
      color: rgba(0,0,0,0.5);
    }
    .telp_padding{
      padding-top: 15px;
    }
    .float_right{
      float: right;
    }
    .cart_padding {
        padding-right: 15px;
    }
    .width_100{
        width: 100%;
    }
    .margin_0{
      margin-top: 0px;
    }
    .cart_button_checkout{
      font-size: 14px;
    }
    .cart_item_image {
      width: 65px;
      height: 65px;
      margin-right: 10px;
    }
    .price{
      font-size: 15px;
      color: #0e8ce4;
    }
    .qty{
      font-size: 12px;
      font-weight: 400;
    }
    .name{
      font-size: 13px;
    }
    .no_padding{
      padding-left: 0px;
      padding-right: 0px;
      padding-top: 10px;
    }
    .list-dropdown{
      padding-left: 5px;
      padding-right: 5px;
    }
    .menu-dropdown{
      font-size: 12px;
    }
    .padding-top-10{
      padding-top: 10px;
    }
    .order_total_title{
      line-height: 25px;
    }
    .order_total_amount{
      line-height: 25px;
    }
    .margin-left-0{
        margin-left: 0px;
    }
    .padding-top-30{
      padding-top: 30px;
    }
    .margin-top-15{
      margin-top: 15px;
    }
  </style>
@endsection

@section('someJS')
  <script src="{{asset('js/product_custom.js')}}"></script>
@endsection

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="cart_container">
            <div class="cart_title">Checkout</div>
            <div class="row col-md-12">
                <div class="col-md-7">
                  <div class="cart_items">
                    <h5 class="cart_item_title">
                      <i class="fas fa-map-marker-alt"></i> Alamat Pengiriman
                      <span class="float_right"><a href="#"><i class="fas fa-edit"></i> Ganti Alamat</a></span>
                    </h5>
                    <ul class="cart_list">
                      <li class="cart_item clearfix">
                        <div class="d-flex flex-md-row flex-column justify-content-between">
                          <div class="cart_item_name cart_info_col width_100">
                            <div class="alamat_name">{{$address->receiver}}<span class="alamat_location"> {{$address->name}}</span></div>
                            <div class="alamat_location telp_padding">{{$address->phoneno}}</div>
                            <div class="alamat_location">{{$address->address}}</div>

                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="cart_items">
                    <h5 class="cart_item_title"><i class="far fa-list-alt"></i> Ringkasan Belanja</h5>
                    <ul class="cart_list">

                      <li class="cart_item clearfix cart_padding">
                         @forelse($cartItems as $cartItem)
                        <div class="order_total_content text-md-right">
                          <div class="order_total_title" style="float:left">{{$cartItem->name}}</div>

                          <input type="hidden" class="product_id" name="product_id[]" value="{{$cartItem->id}}">
                          <input type="hidden" name="ongkir[]" value="">

                          <div class="order_total_amount">Rp {{$cartItem->price}}</div>
                        </div>
                        @empty
                         @endforelse
                        <!-- <div class="order_total_content text-md-right">
                          <div class="order_total_title" style="float:left">Total Ongkos Kirim</div>
                          <div class="order_total_amount">Rp 18.000</div>
                        </div> -->
                        <div class="order_total_content text-md-right padding-top-30">
                          <div class="order_total_amount margin-left-0" style="float:left">Total Tagihan</div>
                          <div class="order_total_amount">Rp {{Cart::subtotal()}}</div>
                        </div>

                        <div class="d-flex flex-md-row flex-column justify-content-between">
                          <div class="cart_item_name cart_info_col width_100">
                            <div class="cart_buttons margin-top-15">
                              <button id="pay-button" class="button cart_button_checkout width_100">Bayar</button>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-7">
                  <div class="cart_items margin_0 width_100">
                    <h5 class="cart_item_title"><i class="fas fa-map-marker-alt"></i> Kurir Pengiriman</h5>
                    @forelse($cartItems as $cartItem)
                    <ul class="cart_list">
                      <li class="cart_item clearfix cart_padding">
                        <div class="d-flex flex-md-row flex-column justify-content-between">
                          <div class="cart_item_name cart_info_col width_100">
                            <div class="alamat_location">Penjual : <span class="alamat_name"> {{$cartItem->shop()->first()->name}}</span></div>
                            <div class="alamat_location">Kota Jakarta Selatan</div>
                            <div class="row">

                              <div class="col-md-8">

                                <div class="col-md-12 clearfix no_padding">
                                  <div class="cart_item_image"><img src="images/shopping_cart.jpg" alt=""></div>
                                  <div class="cart_item_name cart_info_col">
                                    <div class="alamat_name cart_item_title name">{{$cartItem->name}}</div>
                                    <div class="alamat_name cart_item_title price">Rp {{number_format($cartItem->price)}}</div>
                                    <div class="alamat_name cart_item_title qty"><span>Jumlah : {{($cartItem->qty)}} Barang</span><span class="float_right">Berat : (1.5 kg)</span></div>
                                  </div>
                                </div>

                                <input type="hidden" name="from" value="{{$cartItem->shop->first()->shop_city}}">
                                <input type="hidden" name="to" value="{{$address->city}}">
                                <!-- <div class="col-md-12 clearfix no_padding">
                                  <div class="cart_item_image"><img src="images/shopping_cart.jpg" alt=""></div>
                                  <div class="cart_item_name cart_info_col">
                                    <div class="alamat_name cart_item_title name">Laptop Macbook Air 2017</div>
                                    <div class="alamat_name cart_item_title price">Rp 17.000.000</div>
                                    <div class="alamat_name cart_item_title qty"><span>Jumlah : 1 Barang</span><span class="float_right">Berat : (1.5 kg)</span></div>
                                  </div>
                                </div>
                              </div> -->
                              <div class="col-md-4">
                                <div class="cart_item_name cart_info_col">
                                  <div class="cart_item_title">Pilih Durasi :<br></div>
                                  <div class="dropdown padding-top-10">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                                      Durasi
                                    </button>
                                    <div  class="dropdown-menu menu-dropdown" aria-labelledby="dropdownMenuButton" id="dropdown-list" style="font-size: 12px;">
                                      <!-- <a class="dropdown-item list-dropdown" href="#"><span>Next Day (1 hari)</span><span class="float_right">Rp 18.000</span></a>
                                      <a class="dropdown-item list-dropdown" href="#"><span>Reguler (2-4 hari)</span><span>Rp 8.000 - Rp 10.000</span></a>
                                      <a class="dropdown-item list-dropdown" href="#"><span>Ekonomi (2 hari)</span><span>Rp 5.000</span></a> -->
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                    @empty
                    @endforelse
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <form id="payment-form" method="post" action="snapfinish">
      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
      <input type="hidden" name="result_type" id="result-type" value=""></div>
      <input type="hidden" name="result_data" id="result-data" value=""></div>
    </form>
    <script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key="SB-Mid-client-KnCJM26FYe8X8XQz"></script>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

$('#dropdownMenuButton').on('click',function (event) {

    $.ajax({
			url: "{{ url('/testongkir') }}",
			dataType:"json",
			type: "POST",
			data:{
				origin : '501',
				destination: '114',
        weight: '1000',
        courier: 'jne',
				_method:"post",
				_token : '{{ csrf_token() }}'
			}
		}).done(function(data){
      if(data.status.code == '200'){
        var strArr = data.results[0].costs;
        console.log(strArr);
        var name = data.results[0].code;
				for(i=0; i < strArr.length; i++){
          $('#dropdown-list').append('<a class="dropdown-item list-dropdown"><span>'+name.toUpperCase()+' '+strArr[i].service+' ('+strArr[i].cost[0].etd+' hari)</span><span class="float_right">Rp '+(parseInt(strArr[i].cost[0].value)+1000).format()+'</span></a>');
        }
      }else{
        alert('NOK');
      }
    });
});
$(document).ready(function() {
  // body...


  $('#pay-button').on('click',function (event) {

      event.preventDefault();
      $(this).attr("disabled", "disabled");

      // var product = $('.product_id').map(function(idx, elem) {
      //   return $(elem).val();
      // }).get();
  $('.product_id').each(function(){
         var prod = $(this).val();

  });
   console.log(prod);
    $.ajax({

      url: '/snaptoken',
      data:{product:product},
      cache: false,
      success: function(data) {
        //location = data;
        console.log('token = '+data);

        var resultType = document.getElementById('result-type');
        var resultData = document.getElementById('result-data');
        function changeResult(type,data){
          $("#result-type").val(type);
          $("#result-data").val(JSON.stringify(data));
          //resultType.innerHTML = type;
          //resultData.innerHTML = JSON.stringify(data);
        }
        snap.pay(data, {

          onSuccess: function(result){
            changeResult('success', result);
            console.log(result.status_message);
            console.log(result);
            $("#payment-form").submit();
          },
          onPending: function(result){
            changeResult('pending', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          },
          onError: function(result){
            changeResult('error', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          }
        });
      }
    });
  });
  });
</script>
    @endsection
@section('someJS')

@endsection
