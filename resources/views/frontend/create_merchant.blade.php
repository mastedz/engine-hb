@extends('frontend.layout_page')

@section('someCSS')
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/product_styles.css')}}">
  <style>
    .float_right{
      float: right;
    }
    .cart_padding {
      padding-right: 35px;
    }
    .width_100{
        width: 100%;
    }
    .cart_button_checkout{
      font-size: 14px;
    }
    .margin-top-15{
      margin-top: 15px;
    }

    .align-center{
      text-align: center;
      color : #8a8a8a;
    }
    .float-left{
      float: left;
    }

    .strike {
        display: block;
        text-align: center;
        overflow: hidden;
        white-space: nowrap;
    }

    .strike > span {
        position: relative;
        display: inline-block;
    }

    .strike > span:before,
    .strike > span:after {
        content: "";
        position: absolute;
        top: 50%;
        width: 9999px;
        height: 1px;
        background: #8a8a8a;
    }

    .strike > span:before {
        right: 100%;
        margin-right: 15px;
    }

    .strike > span:after {
        left: 100%;
        margin-left: 15px;
    }
    .cart_item {
        padding: 40px;
    }

    .google-button {
      width: 100%;
      height: 40px;
      border-width: 0;
      background: white;
      color: #737373;
      border-radius: 5px;
      white-space: nowrap;
      box-shadow: 1px 1px 0px 1px rgba(0,0,0,0.05);
      transition-property: background-color, box-shadow;
      transition-duration: 150ms;
      transition-timing-function: ease-in-out;
      padding: 0;

      &:focus,
      &:hover {
        box-shadow: 1px 4px 5px 1px rgba(0,0,0,0.1);
      }

      &:active {
        background-color: #e5e5e5;
        box-shadow: none;
        transition-duration: 10ms;
      }
    }

    .google-button__icon {
      display: inline-block;
      vertical-align: middle;
      margin: 8px 0 8px 8px;
      width: 18px;
      height: 18px;
      box-sizing: border-box;
    }

    .google-button__icon--plus {
      width: 27px;
    }

    .google-button__text {
      display: inline-block;
      vertical-align: middle;
      padding: 0 24px;
      font-size: 14px;
      font-weight: bold;
      font-family: 'Roboto',arial,sans-serif;
    }

    // Boilerplate stuff

    html, body {
      height: 100%;
    }

    body {
      background-color: #f0f0f0;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    button ~ button {
      margin-left: 20px;
    }
    .cart_item_image {
        width: 100%;
        height: 50%;
    }
    .characteristics{
        padding-top: 0px;
    }
  </style>
@endsection

@section('someJS')
  <script src="{{asset('js/product_custom.js')}}"></script>
@endsection

@section('content')
    <div class="characteristics" style="background: #fff;">
      <div class="row">
        <div class="col-lg-12">
          <div class="cart_container">
            <div class="row col-md-12">
                <div class="col-md-7">
                  <div class="cart_item_image"><img src="https://previews.123rf.com/images/bloomua/bloomua1505/bloomua150500104/39953191-thin-line-flat-design-of-online-shopping-checkout-buying-store-goods-by-pos-terminal-sell-mass-marke.jpg" alt=""></div>
                </div>
                <div class="col-md-5">
                  <div class="cart_items">
                    <ul class="cart_list">
                      <li class="cart_item clearfix cart_padding">
                        <div class="order_total_content text-md-right">
                          <h4 class="align-center">Hai, {{ Auth::user()->name }}</h4>
                          <h6 class="cart_item_title align-center">Selamat datang di hariBelanja, ayo buat merchant sekarang!</h6>
                          <form id="login-form"method="POST" role="form" action="{{ url('add/merchant')  }}" style="color: #8a8a8a;">
                            {{ csrf_field() }}
                            <div class="form-group">
                              <label for="exampleInputEmail1" class="float-left">Nama Merchant</label>
                              <input type="text" class="form-control" id="name" placeholder="Nama Merchant" name="name">
                              <p class="float-left" style="color: darksalmon;font-size: 12px;">*Maksimal 24 karakter</p>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1" style="width:100%;text-align: left;">Domain Toko</label>
                              <div class="input-group">
                                <span class="input-group-addon">www.hariBelanja.com/</span>
                                <input id="domain" type="text" class="form-control" name="domain" placeholder="Domain Toko">
                              </div>
                              <p style="text-align: left;color: darksalmon;font-size: 12px;">*Domain toko maksimal 16 karakter (huruf, angka dan tanda “-“) tanpa spasi</p>
                            </div>
                            <button type="submit" value="add" class="btn btn-primary margin-top-15 width_100">Buat Merchant Sekarang</button>
                          </form>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
