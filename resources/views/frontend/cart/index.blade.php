@extends('frontend.layouts.layout_page')

@section('someCSS')
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_responsive.css')}}">
  <style>
    .cart_item_name {
      margin-left: 3.53%;
    }
    .cart_item_text span {
      display: initial;
      width: auto;
      height: auto;
      border-radius: 50%;
      margin-right: 0px;
    }
    .add{
      width: 20px;
    }
    .sub{
      width: 20px;
    }
  </style>
@endsection

@section('content')

 <div class="cart_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="cart_container">

                        <div class="cart_title">Shopping Cart</div>
                         <form method="POST" action="/checkout" id="proceedCheckout">
                        @php
                          $total = 0;
                        @endphp
                        @forelse($cartItems as $cartItem)
                        <div class="cart_items">

                            <ul class="cart_list">
                                <li class="cart_item clearfix">
                                    <div class="cart_item_image"><img style="max-width:100px" src="{{asset('')}}images/{{json_decode($cartItem['image'])[0]}}" alt=""></div>
                                    <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                        <div class="cart_item_name cart_info_col" style="display:none;">


                                            <div class="cart_item_text"><input type="hidden" name="cart[]" value="{{$cartItem['rowId']}}" class="form-control"> </div>
                                              {{ csrf_field() }}

                                        </div>
                                        <div class="cart_item_name cart_info_col" style="max-width: 300px;">
                                            <div class="cart_item_title">Name</div>
                                            <div class="cart_item_text">{{$cartItem['name']}}</div>
                                        </div>
                                        <div class="cart_item_quantity cart_info_col">
                                            <div class="cart_item_title">Quantity</div>
                                            <div class="cart_item_text">
                                              <!-- <button type="button" id="kurang" class="sub quantity">-</button> -->
                                              <input type="number" data-rowId="{{ $cartItem['rowId'] }}" class="quantity" id="qty" data-id="{{$cartItem['id']}}" value="{{$cartItem['qty']}}" min="1" max="100" style="text-align: center;"/>
                                              <!-- <button type="button" id="tambah" class="add quantity">+</button> -->
                                            </div>
                                        </div>
                                        <div class="cart_item_price cart_info_col">
                                            <div class="cart_item_title">Price</div>
                                            <div class="cart_item_text" id="price-{{$cartItem['id']}}" data-price="{{ $cartItem['price'] - ($cartItem['price']*$cartItem['discount']/100) }}"><strike>Rp {{ number_format($cartItem['price'], 0, ',', '.') }}</strike><span style="color:red;"> Rp {{ number_format($cartItem['price'] - ($cartItem['price']*$cartItem['discount']/100), 0, ',', '.') }}</span></div>
                                        </div>
                                        <div class="cart_item_total cart_info_col">
                                            <div class="cart_item_title">Total</div>
                                            <div class="cart_item_text totalprice" id="total-{{ $cartItem['id'] }}">Rp {{ number_format(($cartItem['qty'])*($cartItem['price'] - ($cartItem['price']*$cartItem['discount']/100)), 0, ',', '.') }}</div>
                                        </div>

                                        <div class="cart_item_total cart_info_col">
                                            <div class="cart_item_title">Action</div>

                                            <div class="cart_item_text"><a class="btn btn-warning" href="{{url('/delete/'.$cartItem['rowId'])}}"><i class="fa fa-trash"></i></a> </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                            @php
                              $total += ($cartItem['qty'])*($cartItem['price'] - ($cartItem['price']*$cartItem['discount']/100));
                            @endphp
                        </div>
                         @empty
                             @endforelse
                        </form>
                        <!-- Order Total
                        <div class="order_total">
                            <div class="order_total_content text-md-right">
                                <div class="order_total_title">Order Total:</div>
                                <div class="order_total_amount">$2000</div>
                            </div>
                        </div>-->

                        <div class="cart_items">

                            <ul class="cart_list">
                                <li class="cart_item clearfix">
                                    <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between" style="width: 100%;">
                                      <h4>Grandtotal</h4>
                                      <h4 id="grantotal">Rp {{ number_format($total, 0, ',', '.') }}</h4>
                                    </div>
                                </li>
                            </ul>

                        </div>

                        <div class="cart_buttons">

                            <button id="proceed" type="submit" class="button cart_button_checkout">Lanjutkan Ke Pembayaran</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('someJS')
<script type="text/javascript">
    function rupiah(bilangan) {
      var	reverse = bilangan.toString().split('').reverse().join(''),
      ribuan 	= reverse.match(/\d{1,3}/g);
      ribuan	= ribuan.join('.').split('').reverse().join('');

      return ribuan;
    }

    $('#proceed').on('click', function(){
        $('#proceedCheckout').submit();
    });

    $('.quantity').change(function(){
      var id = $(this).attr('data-id');
			var qty =  $(this).val();
      var rowId = $(this).attr('data-rowId');;
			$.ajax({
				 url: "{{ url('/update/qty') }}",
				 data: { qty: qty, rowId : rowId }
			}).done(function(data){
        var sum = parseInt($('#price-'+id).attr('data-price')) * parseInt(qty);
        $('#total-'+id).html('Rp ' + rupiah(sum));
        jumlah();
      });
		});

    function jumlah() {
      var grantotal = 0;
      $('.totalprice').each(function(){
          grantotal += parseFloat($(this).text().split(' ')[1].replace(/\./g, ''));  // Or this.innerHTML, this.innerText
      });
      $('#grantotal').html('Rp ' + rupiah(grantotal));
    }
</script>
@endsection
