@extends('frontend.layouts.layout_page')

@section('someCSS')
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/product_styles.css')}}">
  <style>
    /*the container must be positioned relative:*/
    .custom-select {
      position: relative;
      font-family: Arial;
    }

    .custom-select select {
      display: none; /*hide original SELECT element:*/
    }

    .select-selected {
      background-color: DodgerBlue;
    }

    /*style the arrow inside the select element:*/
    .select-selected:after {
      position: absolute;
      content: "";
      top: 14px;
      right: 10px;
      width: 0;
      height: 0;
      border: 6px solid transparent;
      border-color: #fff transparent transparent transparent;
    }

    /*point the arrow upwards when the select box is open (active):*/
    .select-selected.select-arrow-active:after {
      border-color: transparent transparent #fff transparent;
      top: 7px;
    } 16px;
      border: 1px solid transparent;
      border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
      cursor: pointer;
      user-select: none;
    }

    /*style items (options):*/
    .select-items {
      position: absolute;
      background-color: DodgerBlue;
      top: 100%;
      left: 0;
      right: 0;
      z-index: 99;
    }

    /*hide the items when the select box is closed:*/
    .select-hide {
      display: none;
    }

    .select-items div:hover, .same-as-selected {
      background-color: rgba(0, 0, 0, 0.1);
    }
  </style>

  <style>
    .cart_item_name {
      margin-left: 0%;
    }
    .alamat_name {
      font-size: 12px;
      font-weight: 500;
      color: rgba(0, 0, 0, 0.82);
    }
    .alamat_location {
      font-size: 12px;
      font-weight: 400;
      color: rgba(0,0,0,0.5);
    }
    .telp_padding{
      padding-top: 15px;
    }
    .float_right{
      float: right;
    }
    .cart_padding {
        padding-right: 15px;
    }
    .width_100{
        width: 100%;
    }
    .margin_0{
      margin-top: 0px;
    }
    .cart_button_checkout{
      font-size: 14px;
    }
    .cart_item_image {
      width: 65px;
      height: 65px;
      margin-right: 10px;
    }
    .price{
      font-size: 15px;
      color: #0e8ce4;
    }
    .qty{
      font-size: 12px;
      font-weight: 400;
    }
    .name{
      font-size: 13px;
    }
    .no_padding{
      padding-left: 0px;
      padding-right: 0px;
      padding-top: 10px;
    }
    .list-dropdown{
      padding-left: 5px;
      padding-right: 5px;
    }
    .menu-dropdown{
      font-size: 12px;
    }
    .padding-top-10{
      padding-top: 10px;
    }
    .order_total_title{
      line-height: 25px;
    }
    .order_total_amount{
      line-height: 25px;
    }
    .margin-left-0{
        margin-left: 0px;
    }
    .padding-top-30{
      padding-top: 30px;
    }
    .margin-top-15{
      margin-top: 15px;
    }
  </style>
@endsection

@section('someJS')
  <script src="{{asset('js/product_custom.js')}}"></script>
@endsection

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="cart_container">
            <div class="cart_title">Checkout</div>
            <div class="row col-md-12">
                <div class="col-md-7">
                  <div class="row">
                    <div class="cart_items" style="width: 100%;">
                      <h5 class="cart_item_title">
                        <i class="fas fa-map-marker-alt"></i> Alamat Pengiriman
                        <!--<span class="float_right"><a href="#"><i class="fas fa-edit"></i> Ganti Alamat</a></span>!-->
                      </h5>
                      <ul class="cart_list">
                        <li class="cart_item clearfix">
                          <div class="d-flex flex-md-row flex-column justify-content-between">
                            <div class="cart_item_name cart_info_col width_100">
                              <div class="alamat_name">{{$address->receiver}}<span class="alamat_location"> {{$address->name}}</span></div>
                              <div class="alamat_location">{{$address->phoneno}}</div>
                              <div class="alamat_location">{{$address->address}}</div>
                              <div class="alamat_location data-destination" data-destination="{{ $address->city }}">{{ App\Helper::non()->getCity()[$address->city-1]['type'] }} {{ App\Helper::non()->getCity()[$address->city-1]['city_name'] }}, {{ App\Helper::non()->getCity()[$address->city-1]['province'] }}</div>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>

                    <div class="cart_items margin_0 width_100">
                      <h5 class="cart_item_title" style="margin-top: 10px;"><i class="fas fa-map-marker-alt"></i> Kurir Pengiriman</h5>
                      @forelse($qtyShop as $i=> $qtyShops)

                          <ul class="cart_list">
                            <li class="cart_item clearfix cart_padding">
                              <div class="d-flex flex-md-row flex-column justify-content-between">
                                <div class="cart_item_name cart_info_col width_100">
                                  @php
                                    $shop = App\Shop::whereId($qtyShops)->first();
                                  @endphp
                                  <div class="alamat_location">Penjual : <span class="alamat_name"> {{$shop->name}}</span></div>
                                  <div class="alamat_location data-origin" data-origin="{{ $shop->shop_city}}">{{ App\Helper::non()->getCity()[$shop->shop_city-1]['type'] }} {{ App\Helper::non()->getCity()[$shop->shop_city-1]['city_name'] }}</div>
                                  <div class="row">

                                    @php
                                    $delivery = json_decode($shop->delivery);
                                    @endphp

                                      <div class="col-md-12">

                                        @forelse($cartItems as $index => $cartItem)

                                          @if($qtyShops == $cartItem->shop_id)

                                            <div class="col-md-12 clearfix no_padding">
                                              <div class="cart_item_image"><img width="40px" src="{{asset('')}}images/{{json_decode($cartItem['image'])[0]}}" alt=""></div>
                                              <div class="cart_item_name cart_info_col">
                                                <div class="alamat_name cart_item_title name">{{$cartItem->name}}</div>
                                                <div class="alamat_name cart_item_title price">Rp {{ number_format(($qty[$index])*($cartItem->price - ($cartItem->price*$cartItem->discount/100)), 0, ',', '.') }}</div>
                                                <div class="alamat_name cart_item_title qty"><span>Jumlah : {{($qty[$index])}} Barang</span><span class="float_right data-weight" data-weight="{{ $cartItem->weight }}">Berat : ({{ number_format($cartItem->weight, 0, ',', '.') }} g)</span></div>
                                              </div>
                                            </div>
                                          @endif
                                        @empty
                                        @endforelse

                                        <input type="hidden" name="from" value="{{$cartItem->shop->first()->shop_city}}">
                                        <input type="hidden" name="to" value="{{$address->city}}">
                                        <!-- <div class="col-md-12 clearfix no_padding">
                                          <div class="cart_item_image"><img src="images/shopping_cart.jpg" alt=""></div>
                                          <div class="cart_item_name cart_info_col">
                                            <div class="alamat_name cart_item_title name">Laptop Macbook Air 2017</div>
                                            <div class="alamat_name cart_item_title price">Rp 17.000.000</div>
                                            <div class="alamat_name cart_item_title qty"><span>Jumlah : 1 Barang</span><span class="float_right">Berat : (1.5 kg)</span></div>
                                          </div>
                                        </div>
                                      </div> -->
                                      <div class="col-md-12">
                                        <div class="row">
                                          <div class="col-md-6"  style="padding-left: 0px;">
                                              <div class="cart_item_name cart_info_col">
                                                <select id="dropdownMenuButton" data-select="{{$i}}" class="custom-select dropdownMenuButton" style="width:100%; margin-left:0px;">
                                                    <option value="">Pilih Kurir</option>
                                                    @foreach($delivery as $key => $kurir)
                                                        <option @if($key==0) selected @endif value="{{ $kurir }}">{{ strtoupper($kurir) }}</option>
                                                    @endforeach

                                                    <!-- <option value="tiki">Tiki</option>
                                                    <option value="pos">POS</option> -->
                                                </select>
                                            </div>
                                          </div>


                                          <div class="col-md-6" style="padding-left: 0px;">
                                              <div class="cart_item_name cart_info_col">
                                                <select id="dropdown-list" data-select="{{$i}}" class="custom-select dropdown-list" style="width:100%; margin-left:0px;">
                                                      @foreach( $ongkir[$index]->results[0]->costs as $key => $value)
                                                        <option data-stat="val" value="{$ongkir[$index]->results[0]->code}},{{$value->service }},{{$value->cost[0]->value}}">{{$value->service }} ({{ $value->cost[0]->etd }}) Rp. {{$value->cost[0]->value}}</option>
                                                      @endforeach
                                                </select>
                                            </div>
                                          </div>
                                        </div>
                                          <!-- <div class="cart_item_title">Pilih Durasi :<br></div>
                                          <div class="dropdown padding-top-10">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                                              Durasi
                                            </button>
                                            <div  class="dropdown-menu menu-dropdown" aria-labelledby="dropdownMenuButton" id="dropdown-list" style="font-size: 12px;"> -->
                                              <!-- <a class="dropdown-item list-dropdown" href="#"><span>Next Day (1 hari)</span><span class="float_right">Rp 18.000</span></a>
                                              <a class="dropdown-item list-dropdown" href="#"><span>Reguler (2-4 hari)</span><span>Rp 8.000 - Rp 10.000</span></a>
                                              <a class="dropdown-item list-dropdown" href="#"><span>Ekonomi (2 hari)</span><span>Rp 5.000</span></a> -->
                                            <!-- </div>
                                          </div> -->

                                      </div>

                                    </div>
                                </div>
                              </div>
                            </li>
                          </ul>
                      @empty
                      @endforelse
                    </div>
                  </div>
                </div>

                <div class="col-md-5">
                  <div class="cart_items">
                    <h5 class="cart_item_title"><i class="far fa-list-alt"></i> Ringkasan Belanja</h5>
                    <ul class="cart_list">

                      <li class="cart_item clearfix cart_padding">
                        @php
                        $item_total = 0;
                        $ongkir_total=0;
                        @endphp

                        @forelse($qtyShop as $index => $qtyShops)
                          @forelse($cartItems as $index => $cartItem)
                            @if($qtyShops == $cartItem->shop_id)
                              <div class="order_total_content text-md-right">
                                <div class="order_total_title" style="float:left">{{ str_limit($cartItem->name, $limit = 30, $end = '...')}}</div>
                                <div class="order_total_amount product-price">Rp {{ number_format(($cartItem->price  - ($cartItem->price * $cartItem->discount /100)) * $qty[$index], 0, ',', '.') }}</div>
                              </div>
                              @php
                              $ongkir_total += $ongkir[$index]->{'results'}[0]->{'costs'}[0]->{'cost'}[0]->{'value'};
                              $item_total += ($cartItem->price - ($cartItem->price * $cartItem->discount /100)) * $qty[$index];
                              @endphp


                                <input type="hidden" class="total_products" value="{{ $item_total }}">
                                <input type="hidden" class="product_id" name="product_id[]" value="{{$cartItem->id}}">
                                <input type="hidden" name="ongkir[]" value="{{ $ongkir[$index]->{'results'}[0]->{'code'} }},{{ $ongkir[$index]->{'results'}[0]->{'costs'}[0]->{'service'} }},{{ $ongkir[$index]->{'results'}[0]->{'costs'}[0]->{'cost'}[0]->{'value'} }}" class="ongkir-{{$cartItem->id}} costs">
                                  @if ($loop->last)
                                <div class="order_total_content text-md-right">
                                  <div class="order_total_title t-ongkir-{{$cartItem->id}}" style="float:left; ">Ongkir</div>


                                  <div class="order_total_amount t-o d-ongkir-{{$cartItem->id}}"></div>
                                </div>
                              @endif
                            @endif
                          @empty
                          @endforelse
                        @empty
                        @endforelse

                        <div class="order_total_content text-md-right padding-top-30">


                        </div>
                        <div class="order_total_content text-md-right">
                          <div class="order_total_amount margin-left-0" style="float:left">Total Tagihan</div>
                          <div class="order_total_amount final">Rp {{ $item_total}}</div>
                        </div>

                        <div class="d-flex flex-md-row flex-column justify-content-between">
                          <div class="cart_item_name cart_info_col width_100">
                            <div class="cart_buttons margin-top-15">
                              <button id="pay-button"  class="btn btn-primary width_100">Bayar</button>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <form id="payment-form" method="post" action="snapfinish">
        <input type="hidden" name="service" id="service" value="">
      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
      <input type="hidden" name="result_type" id="result-type" value=""></div>
      <input type="hidden" name="result_data" id="result-data" value=""></div>
    </form>
    <script type="text/javascript"
            src="https://app.midtrans.com/snap/snap.js"
            data-client-key="{{env('MIDTRANS_CLIENT_KEY')}}"></script>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
function addCommas(numberString) {
    var resultString = numberString + '',
        x = resultString.split('.'),
        x1 = x[0],
        x2 = x.length > 1 ? '.' + x[1] : '',
        rgxp = /(\d+)(\d{3})/;

    while (rgxp.test(x1)) {
        x1 = x1.replace(rgxp, '$1' + '.' + '$2');
    }

    return x1 + x2;
}
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};
$(".dropdown-list").change(function() {
    var pr = $(this).data('select'),
        before = $('.d-ongkir').text().split(' ');
    $('.ongkir-' + pr).val('');
    $('.ongkir-' + pr).val($(this).val());
    console.log(pr);
    if ($(this).val().split('&')[0] == 'tiki®') {
        // console.log('tiki');
        var s = parseInt($(this).val().split(',')[1]);
    } else {
        var s = parseInt($(this).val().split(',')[2]);
    }
    if (before == "") {

        $('.t-o').text("");
        $('.d-ongkir-' + pr).text("Rp " + (addCommas(s)));
        $('.t-ongkir-' + pr).css("display", "");
        // $('.d-ongkir').text("Rp "+parseInt($(this).val())+parseInt(before));
    } else {
        console.log('not nan');
        var after = parseInt(before[1]);
        $('.d-ongkir-' + pr).text("");
        $('.t-ongkir-' + pr).css("display", "");
        $('.d-ongkir-' + pr).text("Rp " + (addCommas(s + after)));
        $('#pay-button').prop("disabled", false);

    }

    sum();

});

function sum() {
    var total_products = $('.total_products').val();
    var sum = 0;
    $('.dropdown-list > option:selected').each(function() {
      if($(this).val() == ''){
       return;
      }
      sum += parseInt($(this).val().split(',')[2]);
    });
    console.log('sum='+sum );
    $('.t-o').text('Rp ' +addCommas(sum));
    var fin = 0;
    $('.product-price').each(function() {


      fin += parseInt($(this).text().split(' ')[1].split('.').join(''));
    });

    console.log($('.t-o').text(), $('.final').text(), fin, sum);
    $('.final').text();
    // $('.final').text("Rp "+ (addCommas(sum+parseInt(fin))));
    $('.final').text("Rp " + (addCommas(sum + parseInt(fin))));

}
$('document').ready(function() {
    sum();

});
// $('#dropdownMenuButton').on('click',function (event) {
$(".dropdownMenuButton").change(function() {
  var el = $(this).nextAll('.cart_item');

    var kurir = $(this).children("option:selected").val(),
        id = $(this).data('select');
        console.log(id);
    var origin = $('.data-origin').attr('data-origin');
    var destination = $('.data-destination').attr('data-destination');
    var weight = $('.data-weight').data('weight');
    console.log(el.find('.dropdown-list'));
    $.ajax({
        url: "{{ url('/getongkir') }}",
        dataType: "json",
        type: "POST",
        data: {
            origin: origin,
            originType: 'city',
            destination: destination,
            destinationType: 'subdistrict',
            weight: weight,
            courier: kurir,
            _method: "post",
            _token: '{{ csrf_token() }}'
        }
    }).done(function(data) {
        if (data.status.code == '200') {
            var strArr = data.results[0].costs;
            var name = data.results[0].code;
            $('.dropdown-list[data-select="' + id + '"]').find('option[data-stat="val"]', ).remove();
            $('.dropdown-list[data-select="' + id + '"]').append('<option data-stat="val" value=""><span>Pilih Service</span></option>');
            for (i = 0; i < strArr.length; i++) {
                $('.dropdown-list[data-select="' + id + '"]').append('<option data-stat="val" value="' + kurir + ',' + strArr[i].service + ',' + strArr[i].cost[0].value + '"><span>' + strArr[i].service + ' (' + strArr[i].cost[0].etd + ' hari)</span><span> Rp ' + (parseInt(strArr[i].cost[0].value)).format() + '</span></option>');
            }
        } else {

        }
    });
});

// body...
function getOngkir(){

}


$('#pay-button').on('click', function(event) {


    // $(this).attr("disabled", "disabled");

    var product = $('.product_id').map(function(idx, elem) {
        return $(elem).val();
    }).get();
    var costs = $('.costs').map(function(idx, elem) {
        return $(elem).val();
    }).get();
    // var product = $('.product_id').serialize();
    console.log(product);
    $.ajax({

        url: '/snaptoken',
        type: "POST",
        data: {
            product: product,
            cost: costs,
            _method: "post",
            _token: '{{ csrf_token() }}'
        },

        success: function(ret) {
            //location = data;

            var data = ret.token;
            console.log(ret);

            var resultType = document.getElementById('result-type');
            var resultData = document.getElementById('result-data');

            function changeResult(type, data) {
                $("#service").val(ret.service);
                $("#result-type").val(type);
                $("#result-data").val(JSON.stringify(data));
                //resultType.innerHTML = type;
                //resultData.innerHTML = JSON.stringify(data);
            }
            snap.pay(data, {

                onSuccess: function(result) {
                    changeResult('success', result);
                    console.log(result.status_message);
                    console.log(result);
                    $("#payment-form").submit();
                },
                onPending: function(result) {
                    changeResult('pending', result);
                    console.log(result.status_message);
                    $("#payment-form").submit();
                },
                onError: function(result) {
                    changeResult('error', result);
                    console.log(result.status_message);
                    $("#payment-form").submit();
                }
            });
        }
    });
});


</script>
    @endsection
@section('someJS')

@endsection
