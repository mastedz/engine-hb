@extends('frontend.layouts.layout_page')
@section('title')
  hariBelanja - Reservasi
@endsection

@section('someCSS')
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/shop_styles.css">

<link rel="stylesheet" type="text/css" href="{{asset('')}}plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<style type="text/css">
    .right-inner-addon {
  position: relative;
}

.right-inner-addon input {
  padding-right: 30px;
}

.right-inner-addon i {
    z-index: 99;
  position: absolute;
  left: 0px;
  padding: 10px 12px;
  pointer-events: none;
}
.product_image img{
  max-height: 120px;
}
.form-control {
    color: #495057 !important;
}
.button {
    display: inline-block;
    background: #0e8ce4;
    border-radius: 5px;
    height: 48px;
    -webkit-transition: all 200ms ease;
    -moz-transition: all 200ms ease;
    -ms-transition: all 200ms ease;
    -o-transition: all 200ms ease;
    transition: all 200ms ease;
}
.cart_button_checkout {
    display: inline-block;
    border: none;
    font-size: 18px;
    font-weight: 400;
    line-height: 48px;
    color: #FFFFFF;
    padding-left: 35px;
    padding-right: 35px;
    outline: none;
    cursor: pointer;
    vertical-align: top;
    height: auto !important;
}
.reservasi{
    width: 100%;
}
</style>
@endsection
@section('content')
    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{asset('')}}images/shop_background.jpg"></div>
        <div class="home_overlay"></div>

        <div class="home_content d-flex flex-column align-items-center justify-content-center">
            <h2 class="home_title">Reservasi</h2>
        </div>
    </div>
    <div class="shop">
        <div class="container">
            <div class="row">
              <div class="reservasi">
                <div class="select col-md-12">
                  <div class="col-md-12">
                    <div class="col-md-12 order_total_content" style="padding-top:20px;">
                      {{ @csrf_field()}}

                      @foreach($cartItems as $cartItem)
                      <input type="hidden" class="product_id" name="product_id[]" value="{{$cartItem->id}}">
                      <input type="hidden" class="costs" value="jne,OKE,0">
                      @endforeach
                       <div class="form-group row">
                         <label for="inputEmail3" class="col-sm-12 col-form-label">Nama</label>
                         <div class="col-sm-12">
                           <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Product" value="{{ Auth::user()->name}}">
                         </div>
                       </div>
                       <div class="form-group row">
                         <label for="inputEmail3" class="col-sm-12 col-form-label">Kontak</label>
                         <div class="col-sm-12">
                           <input type="text" name="phone" class="form-control" id="phone" placeholder="Kontak yang dapan dihubungi">
                         </div>
                       </div>
                       <div class="form-group row">
                         <label for="inputEmail3" class="col-sm-12 col-form-label">Lokasi <span data-toggle="tooltip" data-placement="right" title="Lokasi yang akan di datangi oleh jasa / artis"><i class="fas fa-info-circle"></i></span></label>
                         <div class="col-sm-12">
                           <textarea type="text" name="lokasi" class="form-control" id="lokasi" placeholder="Lokasi yang dituju"></textarea>
                         </div>
                       </div>
                    </div>
                  </div>
                </div>
                <div class="order_total_content text-md-right margin-top-15" style="padding-right: 12%;padding-left: 15px; float:right;">
                  <div class="contact_form_button">
                    <button id="pay-button" class="btn btn-primary cart_button_checkout" value="Simpan">SIMPAN</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
  </div>
  <form id="payment-form" method="post" action="snapfinish">
     <input type="hidden" name="service" id="service" value="">
   <input type="hidden" name="_token" value="{!! csrf_token() !!}">
   <input type="hidden" name="result_type" id="result-type" value=""></div>
   <input type="hidden" name="result_data" id="result-data" value=""></div>
 </form>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script type="text/javascript"
         src="https://app.midtrans.com/snap/snap.js"
         data-client-key="{{env('MIDTRANS_CLIENT_KEY')}}"></script>
         <script type="text/javascript">
         $('#pay-button').on('click', function(event) {


             // $(this).attr("disabled", "disabled");

             var product = $('.product_id').map(function(idx, elem) {
                 return $(elem).val();
             }).get();
             var costs = $('.costs').map(function(idx, elem) {
                 return $(elem).val();
             }).get();
             // var product = $('.product_id').serialize();
             console.log(product);
             $.ajax({

                 url: '/snaptoken',
                 type: "POST",
                 data: {
                     product: product,
                     cost: costs,
                     _method: "post",
                     _token: '{{ csrf_token() }}'
                 },

                 success: function(ret) {
                     //location = data;

                     var data = ret.token;
                     console.log(ret);

                     var resultType = document.getElementById('result-type');
                     var resultData = document.getElementById('result-data');

                     function changeResult(type, data) {
                         $("#service").val(ret.service);
                         $("#result-type").val(type);
                         $("#result-data").val(JSON.stringify(data));
                         //resultType.innerHTML = type;
                         //resultData.innerHTML = JSON.stringify(data);
                     }
                     snap.pay(data, {

                         onSuccess: function(result) {
                             changeResult('success', result);
                             console.log(result.status_message);
                             console.log(result);
                             $("#payment-form").submit();
                         },
                         onPending: function(result) {
                             changeResult('pending', result);
                             console.log(result.status_message);
                             $("#payment-form").submit();
                         },
                         onError: function(result) {
                             changeResult('error', result);
                             console.log(result.status_message);
                             $("#payment-form").submit();
                         }
                     });
                 }
             });
         });
         </script>
@endsection
