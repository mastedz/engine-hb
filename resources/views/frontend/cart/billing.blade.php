@extends('frontend.layouts.layout_page')

@section('someCSS')
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/product_styles.css')}}">
  <style>
    .cart_item_name {
      margin-left: 0%;
    }
    .alamat_name {
      font-size: 12px;
      font-weight: 500;
      color: rgba(0, 0, 0, 0.82);
    }
    .alamat_location {
      font-size: 12px;
      font-weight: 400;
      color: rgba(0,0,0,0.5);
    }
    .telp_padding{
      padding-top: 15px;
    }
    .float_right{
      float: right;
    }
    .cart_padding {
        padding-right: 15px;
    }
    .width_100{
        width: 100%;
    }
    .margin_0{
      margin-top: 0px;
    }
    .cart_button_checkout{
      font-size: 14px;
    }
    .cart_item_image {
      width: 65px;
      height: 65px;
      margin-right: 10px;
    }
    .price{
      font-size: 15px;
      color: #0e8ce4;
    }
    .qty{
      font-size: 12px;
      font-weight: 400;
    }
    .name{
      font-size: 13px;
    }
    .no_padding{
      padding-left: 0px;
      padding-right: 0px;
      padding-top: 10px;
    }
    .list-dropdown{
      padding-left: 5px;
      padding-right: 5px;
    }
    .menu-dropdown{
      font-size: 12px;
    }
    .padding-top-10{
      padding-top: 10px;
    }
    .order_total_title{
      line-height: 25px;
    }
    .order_total_amount{
      line-height: 25px;
    }
    .margin-left-0{
        margin-left: 0px;
    }
    .padding-top-30{
      padding-top: 30px;
    }
    .margin-top-15{
      margin-top: 15px;
    }
  </style>
@endsection

@section('someJS')
  <script src="{{asset('js/product_custom.js')}}"></script>
@endsection

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="cart_container">
            <div class="cart_title">Checkout</div>
            <div class="row col-md-12">
                <div class="col-md-7">
                  <div class="cart_items">
                    <h5 class="cart_item_title">
                      <i class="fas fa-map-marker-alt"></i> Alamat Pengiriman
                      <span class="float_right"><a href="#"><i class="fas fa-edit"></i> Ganti Alamat</a></span>
                    </h5>
                    <ul class="cart_list">
                      <li class="cart_item clearfix">
                        <div class="d-flex flex-md-row flex-column justify-content-between">
                          <div class="cart_item_name cart_info_col width_100">
                            <div class="alamat_name">Rani Dewi<span class="alamat_location"> (Kantor)</span></div>
                            <div class="alamat_location telp_padding">082245326737</div>
                            <div class="alamat_location">Menara Bidakara 1 Lt. 12, Jl. Jend. Gatot Subroto Kav 71-73, RT.1/RW.1</div>
                            <div class="alamat_location">Menteng Dalam</div>
                            <div class="alamat_location">Tebet, Jakarta, Daerah Khusus Ibukota Jakarta 12870</div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="cart_items">
                    <h5 class="cart_item_title"><i class="far fa-list-alt"></i> Ringkasan Belanja</h5>
                    <ul class="cart_list">
                      <li class="cart_item clearfix cart_padding">
                        <div class="order_total_content text-md-right">
                          <div class="order_total_title" style="float:left">Name</div>
                          <div class="order_total_amount">Rp 30.000</div>
                        </div>
                        <!-- <div class="order_total_content text-md-right">
                          <div class="order_total_title" style="float:left">Total Ongkos Kirim</div>
                          <div class="order_total_amount">Rp 18.000</div>
                        </div> -->
                        <div class="order_total_content text-md-right padding-top-30">
                          <div class="order_total_amount margin-left-0" style="float:left">Total Tagihan</div>
                          <div class="order_total_amount">Rp 30.000</div>
                        </div>
                        <div class="d-flex flex-md-row flex-column justify-content-between">
                          <div class="cart_item_name cart_info_col width_100">
                            <div class="cart_buttons margin-top-15">
                              <a href="{{route('checkout.shipping')}}" class="button cart_button_checkout width_100">Bayar</a>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-7">
                  <div class="cart_items margin_0 width_100">
                    <h5 class="cart_item_title"><i class="fas fa-map-marker-alt"></i> Kurir Pengiriman</h5>
                    <ul class="cart_list">
                      <li class="cart_item clearfix cart_padding">
                        <div class="d-flex flex-md-row flex-column justify-content-between">
                          <div class="cart_item_name cart_info_col width_100">
                            <div class="alamat_location">Penjual : <span class="alamat_name"> Rani Store</span></div>
                            <div class="alamat_location">Kota Jakarta Selatan</div>
                            <div class="row">
                              <div class="col-md-8">
                                <div class="col-md-12 clearfix no_padding">
                                  <div class="cart_item_image"><img src="images/shopping_cart.jpg" alt=""></div>
                                  <div class="cart_item_name cart_info_col">
                                    <div class="alamat_name cart_item_title name">Buku</div>
                                    <div class="alamat_name cart_item_title price">Rp 30.000</div>
                                    <div class="alamat_name cart_item_title qty"><span>Jumlah : 10 Barang</span><span class="float_right">Berat : (1.5 kg)</span></div>
                                  </div>
                                </div>
                                <!-- <div class="col-md-12 clearfix no_padding">
                                  <div class="cart_item_image"><img src="images/shopping_cart.jpg" alt=""></div>
                                  <div class="cart_item_name cart_info_col">
                                    <div class="alamat_name cart_item_title name">Laptop Macbook Air 2017</div>
                                    <div class="alamat_name cart_item_title price">Rp 17.000.000</div>
                                    <div class="alamat_name cart_item_title qty"><span>Jumlah : 1 Barang</span><span class="float_right">Berat : (1.5 kg)</span></div>
                                  </div>
                                </div>
                              </div> -->
                              <div class="col-md-4">
                                <div class="cart_item_name cart_info_col">
                                  <div class="cart_item_title">Pilih Durasi :<br></div>
                                  <div class="dropdown padding-top-10">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Durasi
                                    </button>
                                    <div class="dropdown-menu menu-dropdown" aria-labelledby="dropdownMenuButton">
                                      <a class="dropdown-item list-dropdown" href="#"><span>Next Day (1 hari)</span><span class="float_right">Rp 18.000</span></a>
                                      <a class="dropdown-item list-dropdown" href="#"><span>Reguler (2-4 hari)</span><span>Rp 8.000 - Rp 10.000</span></a>
                                      <a class="dropdown-item list-dropdown" href="#"><span>Ekonomi (2 hari)</span><span>Rp 5.000</span></a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
