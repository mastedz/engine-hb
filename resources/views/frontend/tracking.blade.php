@extends('frontend.layouts.layout_page')
@section('title')
  hariBelanja - Tracking
@endsection

@section('tracking')
    color: #007bff;
@endsection

@section('someCSS')
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/shop_styles.css">

<link rel="stylesheet" type="text/css" href="{{asset('')}}plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<style type="text/css">
    .right-inner-addon {
  position: relative;
}

.right-inner-addon input {
  padding-right: 30px;
}

.right-inner-addon i {
    z-index: 99;
  position: absolute;
  left: 0px;
  padding: 10px 12px;
  pointer-events: none;
}
.product_image img{
  max-height: 120px;
}
</style>
@endsection
@section('content')
    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{asset('')}}images/shop_background.jpg"></div>
        <div class="home_overlay"></div>

        <div class="home_content d-flex flex-column align-items-center justify-content-center">
            <h2 class="home_title">Tracking</h2>
        </div>
    </div>
    <div class="shop">
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                <h2>Tracking Order</h2>
              </div>
              <div class="col-md-12">
                <p>Klik link dan input resi</p>
              </div>
              <table class="table table-hover table-fixed">
                <thead>
                  <tr>
                    <th style="width: 50%;">Kurir</th>
                    <th style="width: 50%;">Link</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td>JNE</td>
                      <td><a href="https://www.jne.co.id/en/tracking/trace">Link</a></td>
                    </tr>
                    <tr>
                      <td>TIKI</td>
                      <td><a href="https://www.tiki.id/id/tracking">Link</a></td>
                    </tr>
                    <tr>
                      <td>POS</td>
                      <td><a href="https://www.posindonesia.co.id/id/tracking">Link</a></td>
                    </tr>
                </tbody>
              </table>
            </div>
        </div>
    </div>
  </div>
@endsection
