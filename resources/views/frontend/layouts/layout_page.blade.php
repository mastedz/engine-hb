<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <title>
      @yield('title')
    </title>
    <meta charset="UTF-8">
    @yield('someCSS')
    @include('frontend.include.css')

</head>
<body>
  <div class='notifications top-right'></div>

  <div class="super_container">
    @include('frontend.include.header_page')
    @yield('content')

    @include('frontend.include.footer_page')
  </div>
  <div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
      <div class="cd-user-modal-container"> <!-- this is the container wrapper -->
        <ul class="cd-switcher">
          <li><a href="#0">Sign in</a></li>
          <li><a href="#0">New account</a></li>
        </ul>

        <div id="cd-login"> <!-- log in form -->
          <form class="cd-form" id="user-login" method="post" action="/attempt">
            <p id="notif-result" style="display:none;">Email atau passwaord salah!</p>
            <p class="fieldset">
              <label class="image-replace cd-email" for="signin-email">E-mail</label>
              <input name="email" class="full-width has-padding has-border" id="signin-email" type="email" placeholder="E-mail" required>
              <span class="cd-error-message">Error message here!</span>
            </p>
            {{ csrf_field() }}
            <p class="fieldset">
              <label class="image-replace cd-password" for="signin-password">Password</label>
              <input name="password" class="full-width has-padding has-border" id="signin-password" type="password"  placeholder="Password" required>
              <a href="#0" class="hide-password">Hide</a>
              <span class="cd-error-message">Error message here!</span>
            </p>

            <p class="fieldset">
              <input type="checkbox" id="remember-me" checked {{ old('remember') ? 'checked' : ''}} >
              <label for="remember-me">Remember me</label>
            </p>

            <p class="fieldset">
              <input class="full-width" type="submit" value="Login" id="btn-login">
            </p>
          </form>

          <p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>
          <div class="footer_social text-center" style="margin-top: 0px;">
            <ul>
              <li><a href="{{ url('/auth/facebook') }}"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="{{ url('/auth/google') }}"><i class="fab fa-google"></i></a></li>
            </ul><br>
          </div>
          <!-- <a href="#0" class="cd-close-form">Close</a> -->
        </div> <!-- cd-login -->

        <div id="cd-signup"> <!-- sign up form -->
          <form class="cd-form" id="user-signup" method="post" action="/registration">
            <p class="fieldset">
              <label class="image-replace cd-username" for="signup-username">Username</label>
              <input name="name" class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username" required>
              <span class="cd-error-message">Error message here!</span>
            </p>

            <p class="fieldset">
              <label class="image-replace cd-email" for="signup-email">E-mail</label>
              <input name="email" class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail" required>
              <span class="cd-error-message">Error message here!</span>
            </p>
             {{ csrf_field() }}
            <p class="fieldset">
              <label class="image-replace cd-password" for="signup-password">Password</label>
              <input name="password" class="full-width has-padding has-border" id="signup-password" type="password"  placeholder="Password" required>
              <a href="#0" class="hide-password">Hide</a>
              <span class="cd-error-message">Error message here!</span>
            </p>

            <p class="fieldset">
              <input type="checkbox" id="accept-terms" {{ old('remember') ? 'checked' : ''}} required>
              <label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
            </p>

            <p class="fieldset">
              <input class="full-width has-padding" type="submit" value="Create account" id="btn-create">
            </p>
          </form>

          <div class="footer_social text-center" style="margin-top: 0px;">
            <ul>
              <li><a href="{{ url('/auth/facebook') }}"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="{{ url('/auth/google') }}"><i class="fab fa-google"></i></a></li>
            </ul><br>
          </div>

          <!-- <a href="#0" class="cd-close-form">Close</a> -->
        </div> <!-- cd-signup -->

        <div id="cd-reset-password"> <!-- reset password form -->
          <p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

          <form class="cd-form" id="user-signup">
            <p class="fieldset">
              <label class="image-replace cd-email" for="reset-email">E-mail</label>
              <input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
              <span class="cd-error-message">Error message here!</span>
            </p>

            <p class="fieldset">
              <input class="full-width has-padding" type="submit" value="Reset password">
            </p>
          </form>

          <p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p>
        </div> <!-- cd-reset-password -->
        <a href="#0" class="cd-close-form">Close</a>
      </div> <!-- cd-user-modal-container -->
    </div> <!-- cd-user-modal -->
  <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('styles/bootstrap4/popper.js')}}"></script>
  <script src="{{asset('styles/bootstrap4/bootstrap.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/TweenMax.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/TimelineMax.min.js')}}"></script>
  <script src="{{asset('plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/animation.gsap.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/ScrollToPlugin.min.js')}}"></script>
  <script src="{{asset('plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
  <script src="{{asset('plugins/easing/easing.js')}}"></script>
<script src="{{ asset('vendors/pnotify/dist/pnotify.js') }}"></script>
  <script src="{{ asset('vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
<script src="{{ asset('vendors/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{asset('')}}plugins/parallax-js-master/parallax.min.js"></script>
 <script src="{{ mix('js/front.js') }}"></script>


  <script type="text/javascript">
    var CREATE_LOGIN_SUCCESS_LINK= "{{url('/')}}";
    var CREATE_SIGNUP_SUCCESS_LINK="{{url('/')}}";

    </script>
  @yield('someJS')
