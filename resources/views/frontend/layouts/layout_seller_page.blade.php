<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <title>
      @yield('title')
    </title>
    <meta charset="UTF-8">
    @yield('someCSS')
    @include('frontend.include.css')

</head>
<body>
  <div class='notifications top-right'></div>

  <div class="super_container">
    <header class="header">

      <!-- Top Bar -->

      <div class="top_bar">
        <div class="container">
          <div class="row">
            <div class="col flex-row">
              <ul class="standard_dropdown top_bar_dropdown">
                <li>
                  <a href="#">hariBelanjaSeller<i class="fas fa-chevron-down"></i></a>
                </li>
              </ul>
            </div>
            <div class="col d-flex flex-row">
              <div class="top_bar_content ml-auto">
                <div class="top_bar_menu">
                  <ul class="standard_dropdown top_bar_dropdown">
                    <li>
                      <a href="#">+ Notifikasi<i class="fas fa-chevron-down"></i></a>
                    </li>
                  </ul>
                </div>
                <div class="top_bar_user">
                  @if(\Auth::user())
                      <div class="user_icon main-nav"><img src="{{asset('')}}images/user.svg" alt=""></div>
                      <div><a href="{{ url('/biodata') }}">{{Auth::user()->name}}</a></div>
                      <div><a href="{{ url('/logout') }}">Logout</a></div>
                  @else
                      <div class="user_icon main-nav"><img src="{{asset('')}}images/user.svg" alt=""></div>
                      <div><a class="cd-signup" href="#0">Register</a></div>
                      <div><a class="cd-signin" href="#0">Sign in</a></div>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Header Main -->

      <div class="header_main">
        <div class="container">
          <div class="row">

            <!-- Logo -->
            <div class="col-lg-2 col-sm-3 col-3 order-1">
              <div class="logo_container">
                <div class="logo"><a href="/">hariBelanja</a></div>
              </div>
            </div>

            <!-- Search -->
            <div class="col-lg-6 col-12 order-lg-2 order-3 text-lg-left text-right">
              <div class="header_search">
                <div class="header_search_content">
                  <div class="header_search_form_container">
                    <form action="{{route('frontend.search')}}" method="post" class="header_search_form clearfix">
                      <input type="search" name="q" required="required" class="header_search_input" placeholder="Search for products...">

                       {{ csrf_field() }}


                      <button type="submit" class="header_search_button trans_300" value="Submit"><img src="{{asset('')}}images/search.png" alt=""></button>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <!-- Wishlist -->
            <div class="col-lg-4 col-9 order-lg-3 order-2 text-lg-left text-right">
              <div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
                <!-- <div class="wishlist d-flex flex-row align-items-center justify-content-end">
                  <div class="wishlist_icon"><img src="images/heart.png" alt=""></div>
                  <div class="wishlist_content">
                    <div class="wishlist_text"><a href="#">Wishlist</a></div>
                    <div class="wishlist_count">15</div>
                  </div>
                </div> -->

                <!-- Cart -->
                <div class="cart">
                  <div class="cart_container d-flex flex-row align-items-center justify-content-end">
                    <div class="cart_icon">
                      <img src="{{asset('')}}images/cart.png" alt="">
                      <div class="cart_count"><span>{{\Cart::count()}}</span></div>
                    </div>
                    <div class="cart_content">
                      <div class="cart_text">
                        @if(Auth::user())
                        <a @if(\Cart::count()>0) href="/cart" @else href="#" @endif >Cart</a>
                        @else
                        <a class="cd-signin" href="#">Cart</a>
                        @endif

                      </div>
                      <div class="cart_price">{{\Cart::total()}}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Main Navigation -->

      <nav class="main_nav">
        <div class="container">
          <div class="row">
            <div class="col">

              <div class="main_nav_content d-flex flex-row">

                <!-- Categories Menu -->

                <div class="cat_menu_container">
                  <div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
                    <div class="cat_burger"><span></span><span></span><span></span></div>
                    <div class="cat_menu_text">categories</div>
                  </div>

                  <ul class="cat_menu">
                    @if(App\Category::count())
                            @foreach(App\Category::where('id_parent', "")->with('child')->limit(20)->get() as $category)

                              <li class="hassubs">
                                  <a href="{{route('collection.subproduct',$category->slug)}}">{{$category->title}}<i @if($category->id_parent != '') class="fas fa-chevron-right" @endif></i></a>
                                  <ul >

                                      @foreach($category->child as $child)
                                        <li><a href="{{route('collection.subproduct',$child->slug)}}">{{$child->title}}</a></li>
                                      @endforeach
                                  </ul>
                              </li>
                            @endforeach
                    @endif
                  </ul>
                </div>

                <!-- Main Nav Menu -->

                <div class="main_nav_menu ml-left">
                  <ul class="standard_dropdown main_nav_dropdown">
                    <li><a href="#">Home<i class="fas fa-chevron-down"></i></a></li>
                    <li class="hassubs">
                      <a href="#">Semua Promo<i class="fas fa-chevron-down"></i></a>
                      <ul>
                        <li>
                          <a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
                          <ul>
                            <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                            <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                            <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                          </ul>
                        </li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                      </ul>
                    </li>
                    <!--<li class="hassubs">
                      <a href="#">Featured Brands<i class="fas fa-chevron-down"></i></a>
                      <ul>
                        <li>
                          <a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
                          <ul>
                            <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                            <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                            <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                          </ul>
                        </li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                      </ul>
                    </li>
                    <li class="hassubs">
                      <a href="#">Pages<i class="fas fa-chevron-down"></i></a>
                      <ul>
                        <li><a href="shop.html">Shop<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="product.html">Product<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="blog.html">Blog<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="blog_single.html">Blog Post<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="regular.html">Regular Post<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="cart.html">Cart<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="contact.html">Contact<i class="fas fa-chevron-down"></i></a></li>
                      </ul>
                    </li>-->
                    <li><a href="blog.html">Mulai Jualan<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="contact.html">Lacak Pesanan<i class="fas fa-chevron-down"></i></a></li>
                    <!--<li><a href="blog.html">Pusat Bantuan<i class="fas fa-chevron-down"></i></a></li>-->
                  </ul>
                </div>

                <!-- Menu Trigger -->

                <div class="menu_trigger_container ml-auto">
                  <div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
                    <div class="menu_burger">
                      <div class="menu_trigger_text">menu</div>
                      <div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </nav>

      <!-- Menu -->

      <div class="page_menu">
        <div class="container">
          <div class="row">
            <div class="col">

              <div class="page_menu_content">

                <div class="page_menu_search">
                  <form action="#">
                    <input type="search" required="required" class="page_menu_search_input" placeholder="Search for products...">
                  </form>
                </div>
                <ul class="page_menu_nav">
                  <li class="page_menu_item has-children">
                    <a href="#">Language<i class="fa fa-angle-down"></i></a>
                    <ul class="page_menu_selection">
                      <li><a href="#">English<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Italian<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Spanish<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Japanese<i class="fa fa-angle-down"></i></a></li>
                    </ul>
                  </li>
                  <li class="page_menu_item has-children">
                    <a href="#">Currency<i class="fa fa-angle-down"></i></a>
                    <ul class="page_menu_selection">
                      <li><a href="#">US Dollar<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">EUR Euro<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">GBP British Pound<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">JPY Japanese Yen<i class="fa fa-angle-down"></i></a></li>
                    </ul>
                  </li>
                  <li class="page_menu_item">
                    <a href="#">Home<i class="fa fa-angle-down"></i></a>
                  </li>
                  <li class="page_menu_item has-children">
                    <a href="#">Super Deals<i class="fa fa-angle-down"></i></a>
                    <ul class="page_menu_selection">
                      <li><a href="#">Super Deals<i class="fa fa-angle-down"></i></a></li>
                      <li class="page_menu_item has-children">
                        <a href="#">Menu Item<i class="fa fa-angle-down"></i></a>
                        <ul class="page_menu_selection">
                          <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                          <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                          <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                          <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                        </ul>
                      </li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                    </ul>
                  </li>
                  <li class="page_menu_item has-children">
                    <a href="#">Featured Brands<i class="fa fa-angle-down"></i></a>
                    <ul class="page_menu_selection">
                      <li><a href="#">Featured Brands<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                    </ul>
                  </li>
                  <li class="page_menu_item has-children">
                    <a href="#">Trending Styles<i class="fa fa-angle-down"></i></a>
                    <ul class="page_menu_selection">
                      <li><a href="#">Trending Styles<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                    </ul>
                  </li>
                  <li class="page_menu_item"><a href="blog.html">blog<i class="fa fa-angle-down"></i></a></li>
                  <li class="page_menu_item"><a href="contact.html">contact<i class="fa fa-angle-down"></i></a></li>
                </ul>

                <div class="menu_contact">
                  <div class="menu_contact_item"><div class="menu_contact_icon"><img src="images/phone_white.png" alt=""></div>082245326737</div>
                  <div class="menu_contact_item"><div class="menu_contact_icon"><img src="images/mail_white.png" alt=""></div><a href="mailto:fastsales@gmail.com">haribelanja@gmail.com</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    @yield('content')

    @include('frontend.include.footer_page')
  </div>
  <div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
      <div class="cd-user-modal-container"> <!-- this is the container wrapper -->
        <ul class="cd-switcher">
          <li><a href="#0">Sign in</a></li>
          <li><a href="#0">New account</a></li>
        </ul>

        <div id="cd-login"> <!-- log in form -->
          <form class="cd-form" id="user-login" method="post" action="/attempt">
            <p id="notif-result" style="display:none;">Email atau passwaord salah!</p>
            <p class="fieldset">
              <label class="image-replace cd-email" for="signin-email">E-mail</label>
              <input name="email" class="full-width has-padding has-border" id="signin-email" type="email" placeholder="E-mail" required>
              <span class="cd-error-message">Error message here!</span>
            </p>
            {{ csrf_field() }}
            <p class="fieldset">
              <label class="image-replace cd-password" for="signin-password">Password</label>
              <input name="password" class="full-width has-padding has-border" id="signin-password" type="password"  placeholder="Password" required>
              <a href="#0" class="hide-password">Hide</a>
              <span class="cd-error-message">Error message here!</span>
            </p>

            <p class="fieldset">
              <input type="checkbox" id="remember-me" checked {{ old('remember') ? 'checked' : ''}} >
              <label for="remember-me">Remember me</label>
            </p>

            <p class="fieldset">
              <input class="full-width" type="submit" value="Login" id="btn-login">
            </p>
          </form>

          <p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>
          <!-- <a href="#0" class="cd-close-form">Close</a> -->
        </div> <!-- cd-login -->

        <div id="cd-signup"> <!-- sign up form -->
          <form class="cd-form" id="user-signup" method="post" action="/registration">
            <p class="fieldset">
              <label class="image-replace cd-username" for="signup-username">Username</label>
              <input name="name" class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username" required>
              <span class="cd-error-message">Error message here!</span>
            </p>

            <p class="fieldset">
              <label class="image-replace cd-email" for="signup-email">E-mail</label>
              <input name="email" class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail" required>
              <span class="cd-error-message">Error message here!</span>
            </p>
             {{ csrf_field() }}
            <p class="fieldset">
              <label class="image-replace cd-password" for="signup-password">Password</label>
              <input name="password" class="full-width has-padding has-border" id="signup-password" type="password"  placeholder="Password" required>
              <a href="#0" class="hide-password">Hide</a>
              <span class="cd-error-message">Error message here!</span>
            </p>

            <p class="fieldset">
              <input type="checkbox" id="accept-terms" {{ old('remember') ? 'checked' : ''}} required>
              <label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
            </p>

            <p class="fieldset">
              <input class="full-width has-padding" type="submit" value="Create account" id="btn-create">
            </p>
          </form>

          <!-- <a href="#0" class="cd-close-form">Close</a> -->
        </div> <!-- cd-signup -->

        <div id="cd-reset-password"> <!-- reset password form -->
          <p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

          <form class="cd-form" id="user-signup">
            <p class="fieldset">
              <label class="image-replace cd-email" for="reset-email">E-mail</label>
              <input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
              <span class="cd-error-message">Error message here!</span>
            </p>

            <p class="fieldset">
              <input class="full-width has-padding" type="submit" value="Reset password">
            </p>
          </form>

          <p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p>
        </div> <!-- cd-reset-password -->
        <a href="#0" class="cd-close-form">Close</a>
      </div> <!-- cd-user-modal-container -->
    </div> <!-- cd-user-modal -->
  <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('styles/bootstrap4/popper.js')}}"></script>
  <script src="{{asset('styles/bootstrap4/bootstrap.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/TweenMax.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/TimelineMax.min.js')}}"></script>
  <script src="{{asset('plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/animation.gsap.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/ScrollToPlugin.min.js')}}"></script>
  <script src="{{asset('plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
  <script src="{{asset('plugins/easing/easing.js')}}"></script>
<script src="{{ asset('vendors/pnotify/dist/pnotify.js') }}"></script>
  <script src="{{ asset('vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
<script src="{{ asset('vendors/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{asset('')}}plugins/parallax-js-master/parallax.min.js"></script>
 <script src="{{ mix('js/front.js') }}"></script>


  <script type="text/javascript">
    var CREATE_LOGIN_SUCCESS_LINK= "{{url('/')}}";
    var CREATE_SIGNUP_SUCCESS_LINK="{{url('/')}}";

    </script>
  @yield('someJS')
