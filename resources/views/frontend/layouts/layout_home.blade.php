<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <title>
      @yield('title')
    </title>
    <meta charset="UTF-8">
    @yield('someCSS')

    @include('frontend.include.css')
</head>
<body>
  <div class="super_container">
    @include('frontend.include.header_page')
    @yield('content')

    @include('frontend.include.footer_page')
  </div>


  

  <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('styles/bootstrap4/popper.js')}}"></script>
  <script src="{{asset('styles/bootstrap4/bootstrap.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/TweenMax.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/TimelineMax.min.js')}}"></script>
  <script src="{{asset('plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/animation.gsap.min.js')}}"></script>
  <script src="{{asset('plugins/greensock/ScrollToPlugin.min.js')}}"></script>
  <script src="{{asset('plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
  <script src="{{asset('plugins/slick-1.8.0/slick.js')}}"></script>
  <script src="{{asset('plugins/easing/easing.js')}}"></script>
  <script src="{{asset('js/custom.js?version=1')}}"></script>
@yield('someJS')
</body>

</html>
