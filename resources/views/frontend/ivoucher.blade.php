@extends('frontend.layouts.layout_page')
@section('title')
  hariBelanja - i Voucher
@endsection

@section('ivoucher')
  color: #007bff;
@endsection

@section('someCSS')
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/shop_styles.css">

<link rel="stylesheet" type="text/css" href="{{asset('')}}plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<style type="text/css">
    .right-inner-addon {
  position: relative;
}

.right-inner-addon input {
  padding-right: 30px;
}

.right-inner-addon i {
    z-index: 99;
  position: absolute;
  left: 0px;
  padding: 10px 12px;
  pointer-events: none;
}
.product_image img{
  max-height: 120px;
}
.single_post_title {
    font-size: 30px;
    font-weight: 500;
}
ul {
    list-style-type: disc !important;
    padding-left:1em !important;
    margin-left:1em;
    color: #828282;
    padding-bottom: 1em;
}
.single_post_ul{
  font-size: 20px;
}
</style>
@endsection
@section('content')
    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{asset('')}}images/shop_background.jpg"></div>
        <div class="home_overlay"></div>

        <div class="home_content d-flex flex-column align-items-center justify-content-center">
            <h2 class="home_title">i-Voucher</h2>
        </div>
    </div>
    <div class="shop" style="text-align: justify;">
        <div class="container">
            <div class="row">
              <div class="col-lg-8 offset-lg-2">
                <div class="single_post_title text-center">Sylva Pay</div>
                  <div class="single_post_text">
                    <p><strong>Sylva Pay</strong> adalah system pembayaran produk elektronik dari PT Sylva Dinamika Utama, perusahaan yang didirikan di Kota Jakarta dan merupakan perusahaan yang selalu konsisten memberikan layanan yang terbaik kepada customer, bergerak di bidang pemasaran produk voucher elektronik dan multibiller yang sudah berpengalaman dalam menangani  ribuan transaksi dengan jumlah mitra-mitra yang sudah tersebar diseluruh nusantara.</p>
                    <p><strong>Sylva Pay</strong> adalah suatu layanan produk elektronik bekerjasama dengan mitra-mitra master dealer operator Telco, Tiketing, e-voucher multibiller dan juga sudah bekerjasama dengan mitra distributor serta komunitas-kominitas etnik marga, almamater sekolah tinggi dan kemitraan lainya  baik integrasi system secara H2H (Host to Host) dan P2H (Point to Host) di Seluruh wilayah Indonesia.</p>
                    <p>Adapun layanan produk elektronik yang kami tawarkan antara lain:</p>
                    <ul class="single_post_text">
                        <li>Billing PT. Telekomunikasi Indonesia group</li>
                        <li>Billing dan e-voucher PT Telekomunikasi Seluler Indonesia, Tbk (Telkomsel)</li>
                        <li>Billing dan e-voucher PT Indosat Tbk</li>
                        <li>Billing dan e-voucher PT XL Axiata Tbk</li>
                        <li>Billing dan e-voucher PT SmartFren Telecom</li>
                        <li>Billing dan e-voucher THREE</li>
                        <li>Billing Multibiller lainya (PLN, BPJS, Tiket Transportasi, Hotel, PDAM dll)</li>
                    </ul>
                    <p><strong>Sylva Pay</strong> selain dapat digunakan untuk layanan isi ulang pulsa elektrik, saldo Sylva Pay anda juga dapat digunakan untuk bertransaksi PEMBAYARAN ONLINE (PPOB), PLN PRABAYAR & PASCABAYAR, Voucher Game Online, PDAM, TELKOM, SPEEDY, INDIHOME, TV KABEL, INDOVISION, BIGTV, , ADIRA, FIF, BAF, WOM, MULTIFINANCE, ASURANSI, Tiket Pesawat, Tiket Kereta, Tiket PELNI Voucher Hotel, Paket Wisata dll.</p>
                    <p><strong>Sylva Pay</strong> dikembangkan oleh team professional yang sudah mempunyai pengalaman dalam layanan penjualan produk elektronik dengan memiliki kantor pusat berlokasi di Jalan Swasembada Timur X No.54 F Jakarta Utara. Jumlah karyawan yang bekerja sebanyak 100 orang yang tersebar dibeberapa area, terdiri dari admin, customer service, dan marketing. Jumlah member sudah mencapai +/- 30.000 mitra distribusi dan ritel dengan rata-rata transaksi mencapai +-250.000 transaksi/hari.</p>
                    <div class="single_post_title text-center">Pendaftaran</div>
                    <p>Tempat Belanja PULSA dan LOKET Pembayaran Tagihan (HALO, XL, BPJS, TELKOM, SPEEDY, PDAM, ORANGE TV, INDOVISION, WOM, ADIRA, FIF, BAF, PLN) "TERMURAH Se INDONESIA"</p>
                    <ul class="single_post_text">
                        <li>Silahkan Download Aplikasi Sylva payment di Andorid / Play store</li>
                        <li>Kemudian Lakukan registrasi melalui Aplikasi Android</li>
                        <li>Link ke Play Store…</li>
                        <li>Bagi Server Pulsa yang ingin Host to Host bisa chat ke ID telegram CS di ………</li>
                        <li>(confirmasi ke team development systel PT. Sylva dhi Tektrans)</li>
                    </ul>
                    <div class="single_post_title text-center">Harga Produk</div>
                    <p>Daftar Harga Master Update Realtime</p>
                    <ul class="single_post_text">
                        <li>Link Web Page</li>
                        <li>Harga H2H</li>
                        <li>Harga Retail Nasional</li>
                    </ul>
                    <div class="single_post_title text-center">Dokumen Tehnis H2H</div>
                    <p><a href="{{ asset('/storage/doc/Dokumentasi API HTTP-GET Center Sylva.pdf') }}">Download dokumen teknis H2H (API)</a></p>
                    <div class="single_post_title text-center">Keunggulan</div>
                    <ul class="single_post_ul">
                        <li>BISNIS MODEL YANG MENARIK
                            <p>Salah satu keunggulan SYLVA PAY yang menjadi prioritas adalah bisnis model yang menarik dan kecukupan produk yang dapat dipakai oleh para mitra. Dengan melakukan kerjasama dengan berbagai operator seluler, kami berusaha untuk mendapatkan harga pulsa terbaik sehingga kami dapat memberikan kepada seluruh agen-agen kami harga yang bersaing. Untuk mengecek harga lengkap dan terbaru silahkan klik menu Produk atau langsung cek di Web Report.</p>
                        </li>
                        <li>PELAYANAN PELANGGAN
                            <p>Dengan sangat memperhatikan kualitas dalam segi pelayanan, SYLVA PAY benar-benar sangat berkomitmen untuk menjaga kepercayaan seluruh pelanggannya agar tercipta kenyamanan dalam setiap melakukan transaksi. Kemudahan setiap pengecekan transaksi dengan membuka semua layanan, dan web reporting adalah salah satu bentuk pelayanan yang ada di SYLVA PAY selama 24 jam non-stop tanpa henti walaupun pada hari libur & hari raya akan tetap melayani anda.</p>
                        </li>
                        <li>DUKUNGAN HARDWARE & SOFTWARE
                            <p>Penggunaan perangkat baik lunak (software) dan keras (hardware) sebagai penunjang layanan yang sangat vital yaitu hardware yang dipakai berada pada Data Center serta softwarenya benar-benar sangat kami perhatikan dengan pertimbangan dari sisi kualitas dan keamanan serta serta kepuasan para pelanggan dan team Sylva itu sendiri. Penggunaan hardware dan software yang dipakai SYLVA PAY saat ini bisa dikatakan sebagai spesifikasi yang cukup tinggi untuk menjaga kestabilan transaksi pengisian pulsa elektrik hingga sektor pelayanan pelanggan. Dengan bertransaksi di SYLVA PAY dapat dirasakan perbedaan dengan para distributor pulsa elektrik lainnya, kecepatan transaksi menjadi andalan kami dalam menjaga transaksi seluruh mitra-mitra kami. Setiap bertransaksi kami kirimkan laporan transaksi berhasil lengkap dengan Voucher Serial Number(VSN) yang berguna sebagai referensi bila menghubungi CS operator yang bersangkutan.</p>
                        </li>
                        <li>SDM BERKUALITAS
                            <p>Pintu utama dalam menghadirkan pelayanan terbaik tidak luput dari peran kerja dari sumber dayanya itu sendiri, tanpa sumber daya yang terbaik dijamin akan sulit menawarkan pelayanan yang baik. Oleh karena itu, SYLVA PAY memilih dengan cermat putra dan putri bangsa yang terbaik dan sesuai dengan keahlian pada bidangnya masing-masing dan juga tidak mengesampingkan sisi kualitas pendidikan serta rasa tanggung jawab.</p>
                          </li>
                        </li>
                        <li>ADMINISTRASI CEPAT & TEPAT
                            <p>Kami tawarkan kepada seluruh agen-agen kami yaitu kecepatan dalam administrasi seperti melakukan deposit dan transaksi refund. Anda tidak perlu lagi melakukan SMS konfirmasi deposit anda, cukup anda menunggu beberapa detik saja saldo andapun sudah masuk dan siap untuk digunakan. Begitu pula dengan transaksi yang kami nyatakan tidak berhasil akan segera kami refund tanpa menunggu anda untuk komplain transaksi, laporan refund pun sampai kepada anda tanpa sebelumnya melakukan komplain. Jadi intinya anda hanya duduk santai saja, dan menerima keuntungan.</p>
                          </li>
                        </li>
                        <li>PENDAFTARAN GRATIS
                            <p>Ada banyak distributor pulsa elektrik yang mengenakan biaya pendaftaran kepada calon membernya, biaya pendaftarannya pun tidak main-main ada yang berkisar 50.000 sampai 5.000.000, jumlah tersebut sudah kami anggap sangat banyak bila ingin menjadi agen pulsa elektrik. Hanya di BONAFIT PAYMENT yang membuka pendaftaran agen pulsa elektrik secara cuma-cuma tanpa biaya sepeserpun, dan kami juga tidak menetapkan target kepada anda kapan anda melakukan deposit serta juga tidak ada masa aktif dari keanggotaan anda, dijamin keanggotaan anda akan tersimpan aktif dalam database kami. Itulah point-point alasan kenapa harus memilih SYLVA PAY. Bila sekarang anda telah berminat bergabung bersama kami, silahkan klik link berikut ini : <a href="#">PENDAFTARAN MEMBER SYLVA PAY.</a></p>
                          </li>
                        </li>
                    </ul>
                    <div class="single_post_title text-center">Hubungi Kami</div>
                    <p>Alamat Sylva Payment</p>
                    <p>No HP / Telp CS Sylva Payment</p>
                    <table class="table table-hover table-fixed">
                      <thead>
                        <tr>
                          <th>Produk</th>
                          <th>Denom</th>
                          <th>Keterangan</th>
                          <th>Harga</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td>Indosat</td>
                            <td>5</td>
                            <td>Indosat Regular 5000</td>
                            <td>5.950</td>
                          </tr>
                          <tr>
                            <td>Indosat</td>
                            <td>10</td>
                            <td>Indosat Regular 10000</td>
                            <td>10.950</td>
                          </tr>
                          <tr>
                            <td>Indosat</td>
                            <td>20</td>
                            <td>Indosat Regular 20000</td>
                            <td>20.375</td>
                          </tr>
                          <tr>
                            <td>Simpati/AS</td>
                            <td>5</td>
                            <td>Simpati/AS Regular 5000</td>
                            <td>5.600</td>
                          </tr>
                          <tr>
                            <td>Simpati/AS</td>
                            <td>10</td>
                            <td>Simpati/AS Regular 10000</td>
                            <td>10.600</td>
                          </tr>
                          <tr>
                            <td>Simpati/AS</td>
                            <td>20</td>
                            <td>Simpati/AS Regular 20000</td>
                            <td>20.250</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
  </div>
@endsection
