@extends('frontend.layouts.layout_page')
@section('someCSS')
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/product_styles.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/product_responsive.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/qna.css">
<style type="text/css">
section{
    display: none;
}
.rvm-shipping {
    position: relative;
}
.rvm-shipping-input-holder {
    width: 85%;
    position: relative;
}
.rvm-shipping--district {
    float: left;
    width: 50%;
    padding: 8px 10px;
    border-top-left-radius: 3px;
    border-bottom-left-radius: 3px;
    border-color: #e0e0e0;
    border-style: solid;
    border-width: 1px 0 1px 1px;
    box-sizing: border-box;
}
.rvm-shipping-title {
    font-size: 11px;
    color: rgba(0,0,0,.54);
    font-weight: 600;
}
.rvm-shipping--postal {
    float: left;
    width: 30%;
    padding: 8px 10px;
    border-color: #e0e0e0;
    border-style: solid;
    border-width: 1px;
    position: relative;
    box-sizing: border-box;
}
.rvm-shipping--weight {
    float: left;
    width: 20%;
    height: auto;
    padding: 8px;
    background-color: #fafafa;
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px;
    border-color: #e0e0e0;
    border-style: solid;
    border-width: 1px 1px 1px 0;
    box-sizing: border-box;
}
.rvm-shipping-result {
    display: none;
    position: relative;
    border: 1px solid #e0e0e0;
    margin-top: 10px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
.rvm-shipping-result__btn-expand {
    display: none;
    text-align: -webkit-center;
    border-style: solid;
    border-width: 0 1px 1px;
    border-color: #e0e0e0;
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    padding: 8px 0;
    position: absolute;
    width: 100%;
    cursor: pointer;
    box-sizing: border-box;
    text-align: -moz-center;
}
.rvm-shipping-alert {
    margin-top: 6px;
    font-size: 12px;
    line-height: 1.42;
    color: #d0021b;
    z-index: 1;
}
.rvm-shipping--calc {
    position: absolute;
    top: 0;
    right: 0;
    width: 10%;
    line-height: 60px;
    color: #42b549;
    padding: 25px 20px;
    cursor: pointer;
    font-size: 17px;
}
* {
    -webkit-font-smoothing: antialiased;
}
.select2-hidden-accessible {
    border: 0!important;
    clip: rect(0 0 0 0)!important;
    height: 1px!important;
    margin: -1px!important;
    overflow: hidden!important;
    padding: 0!important;
    position: absolute!important;
    width: 1px!important;
}
.select2-container {
    font-size: 13px;
    box-sizing: border-box;
    display: inline-block;
    margin: 0;
    position: relative;
    vertical-align: middle;
    color: rgba(0,0,0,.54);
}
.svg_icon__carret_up {
    /* background-image: url(data:image/svg+xml;charset=utf8;base64,IDxzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMu…E4OC4xODggMCAuMzc1LjE4OEwxOSAyMC43NXEuMTg4LjE4OC4xODguMzc1eicvPjwvc3ZnPg==); */
}

.order_info {
    margin-top: 20px;
}
.product_price {
    font-size: 30px !important;
    font-weight: 500 !important;
    margin-top: 0px !important;
    color: #0e8ce4;
}
.button_container {
    margin-top: 20px !important;
    margin-bottom: 20px !important;
}
.single_product {
    padding-top: 50px !important;
    padding-bottom: 140px;
}
.test div {
display: inline-block;

width: 100%;
*display: inline;
*zoom:1;
*width: 16.6%;
}
</style>
@endsection
@section('content')
    <div class="single_product">
        <div class="container">
            <div class="row">

                <!-- Images -->
                <div class="col-lg-2 order-lg-1 order-2">
                    <ul class="image_list">
                      @if($product_edit->is_artist == 1)
                      <li ><img src="{{$product_edit->image}}" alt=""></li>
                      @else

                    @if($product_edit->image)
                        @foreach(json_decode($product_edit->image) as $img)
                        <li data-image="{{asset('')}}images/{{$img}}"><img src="{{asset('')}}images/{{$img}}" alt=""></li>


                        @endforeach
                    @else
                    <li data-image="https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwifpdnNi4LiAhWLdXAKHcReBJkQjRx6BAgBEAU&url=%2Furl%3Fsa%3Di%26source%3Dimages%26cd%3D%26ved%3D%26url%3Dhttps%253A%252F%252Fwww.brother.ca%252Fen%252Fp%252FSA375%26psig%3DAOvVaw13D_WmoC4L7ZbAc6FsF_Gw%26ust%3D1557066690097191&psig=AOvVaw13D_WmoC4L7ZbAc6FsF_Gw&ust=1557066690097191"><img src="https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwifpdnNi4LiAhWLdXAKHcReBJkQjRx6BAgBEAU&url=%2Furl%3Fsa%3Di%26source%3Dimages%26cd%3D%26ved%3D%26url%3Dhttps%253A%252F%252Fwww.brother.ca%252Fen%252Fp%252FSA375%26psig%3DAOvVaw13D_WmoC4L7ZbAc6FsF_Gw%26ust%3D1557066690097191&psig=AOvVaw13D_WmoC4L7ZbAc6FsF_Gw&ust=1557066690097191" alt=""></li>
                    @endif
                    @endif
                    </ul>
                </div>

                <!-- Selected Image -->

                <div class="col-lg-5 order-lg-2 order-1">
                    @if($product_edit->is_artist == 0)
                    <div class="image_selected"><img src="{{asset('')}}images/{{json_decode($product_edit->image)[0]}}" alt=""></div>
                    @else
                    <div class="image_selected"><iframe width="420" height="315" src="https://www.youtube.com/embed/{{$product_edit->url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                    @endif
                </div>

                <!-- Description -->

                @php
                  if(Auth::user()){
                    $shop = App\Shop::whereUserId(Auth::user()->id)->first(['id']);
                    if($shop){
                        $id = App\Shop::whereUserId(Auth::user()->id)->first(['id'])->id;
                    }
                  }
                  $shop_vendor = App\Shop::whereId($product_edit->shop_id)->first();
                @endphp
                <div class="col-lg-5 order-3">
                    <div class="product_description">
                      <div class="test">
                          <a href="">
                            <div class="padding" style="width: 15%;"><img src="{{ $shop_vendor->image ? asset('/images/'.$shop_vendor->image) : 'https://cdn2.iconfinder.com/data/icons/online-shopping-flat-round/550/store-512.png'}}" alt="" style="width: 40px;height: 40px;border-radius:50%;"></div>
                            <div class="padding" style="width: 60%;"><p>{{ $shop_vendor->name }}</p></div>
                          </a>
                      </div>
                        <div class="product_category">{{$product_edit->category()->first()->title}}</div>
                        <div class="product_name">{{$product_edit->name}}</div>
                        <div class="product_price">Rp {{ number_format($product_edit->price, 0, ',', '.') }}</div>
                        <div class="rating_r rating_r_{{$product_edit->reputation}} product_rating"><i></i><i></i><i></i><i></i><i></i></div>

                        <div class="order_info d-flex flex-row">
                            <form action="#">
                                <div class="clearfix" style="z-index: 1000;display:none;">

                                    <!-- Product Quantity -->
                                    <div class="product_quantity clearfix">
                                        <span>Quantity: </span>
                                        <input id="quantity_input" type="text" pattern="[0-9]*" value="1">
                                        <div class="quantity_buttons">
                                            <div id="quantity_inc_button" class="quantity_inc quantity_control"><i class="fas fa-chevron-up"></i></div>
                                            <div id="quantity_dec_button" class="quantity_dec quantity_control"><i class="fas fa-chevron-down"></i></div>
                                        </div>
                                    </div>

                                    <!-- Product Color -->
                                    <ul class="product_color">
                                        <li>
                                            <span>Color: </span>
                                            <div class="color_mark_container"><div id="selected_color" class="color_mark"></div></div>
                                            <div class="color_dropdown_button"><i class="fas fa-chevron-down"></i></div>

                                            <ul class="color_list">
                                                <li><div class="color_mark" style="background: #999999;"></div></li>
                                                <li><div class="color_mark" style="background: #b19c83;"></div></li>
                                                <li><div class="color_mark" style="background: #000000;"></div></li>
                                            </ul>
                                        </li>
                                    </ul>

                                </div>
                                @if($product_edit->is_artist == 0)
                                  @if($product_edit->shop_id == @$id)
                                    <div class="button_container">
                                        <button  type="button" class="button cart_button"><a href="{{ url('products/'.$product_edit->slug.'/edit') }}">Update</a></button>
                                        <div class="product_fav"><i class="fas fa-heart"></i></div>
                                    </div>
                                  @else
                                  <div class="button_container">
                                      <button  type="button" class="button cart_button"><a href="{{url('buynow', $product_edit->id)}}">Beli</a></button>
                                      <div class="product_fav"><i class="fas fa-heart"></i></div>
                                  </div>
                                  @endif
                                @else
                                  @if($product_edit->shop_id == @$id)
                                    <div class="button_container">
                                        <button  type="button" class="button cart_button"><a href="{{ url('products/'.$product_edit->slug.'/edit') }}">Update</a></button>
                                        <div class="product_fav"><i class="fas fa-heart"></i></div>
                                    </div>
                                  @else
                                    <div class="button_container">
                                        <button  type="button" class="button cart_button"><a href="{{url('buynow', $product_edit->id)}}">Reservasi</a></button>
                                        <div class="product_fav"><i class="fas fa-heart"></i></div>
                                    </div>
                                  @endif
                                @endif
                                @if($product_edit->is_artist == 0)
                                <div class="product_price" style="font-size: 17px;margin-bottom:10px;">Estimasi ongkos kirim</div>
                                @if($districts == null)
                                  <div class="rvm-shipping-result active" style="display: block;width:90%;">
                                    <table class="table table-striped" style="margin-bottom: 0px;">
                                      <tbody>
                                        <tr>
                                          <td style="width: 33%">Provinsi</td>
                                          <td style="width: 30%">Kota</td>
                                          <td>Kecamatan</td>
                                        </tr>
                                        <tr>
                                          <td style="white-space:normal;">
                                            <select id="edit-province" class="custom-select" style="width:100%; margin-left:0px;">
                                                <option value="">Provinsi</option>
                                                @foreach($provinces as $provinsi)
                                                    <option data-province="{{$provinsi['province_id']}}" value="{{$provinsi['province_id']}}">{{$provinsi['province']}}</option>
                                                @endforeach
                                            </select>
                                          </td>
                                          <td style="white-space:normal;">
                                            <select id="edit-city" class="custom-select" style="width:100%; margin-left:0px;">
                                                <option value="">Kota</option>
                                            </select>
                                          </td>
                                          <td style="white-space:normal;">
                                            <select id="edit-district" class="custom-select" style="width:100%; margin-left:0px;">
                                                <option value="">Kecamatan</option>
                                            </select>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                @endif

                                <div class="relative mt-20">
                                <div class="rvm-shipping-result active" style="display: block;width:90%;">
                                  <table class="table table-striped" style="margin-bottom: 0px;">
                                    <tbody>
                                      <tr>
                                        <td>Pilih Kurir</td>
                                        @if($districts != null)
                                          <td>Kecamatan</td>
                                        @endif
                                        <td>Berat</td>
                                      </tr>
                                      <tr>
                                        <td style="white-space:normal;">
                                          <select id="kurir" class="custom-select" style="width:100%; margin-left:0px;">
                                              <option value="">Pilih Kurir</option>
                                              @foreach($delivery as $kurir)
                                                  <option value="{{ $kurir }}">{{ strtoupper($kurir) }}</option>
                                              @endforeach
                                          </select>
                                        </td>
                                        @if($districts != null)
                                          <td style="white-space:normal;">
                                              <select id="districtDes" class="custom-select" style="width:100%; margin-left:0px;">
                                                <option value="">Kecamatan</option>
                                                @foreach($districts as $district)
                                                    <option value="{{ $district->district }}">{{ $district->district }}</option>
                                                @endforeach
                                            </select>
                                          </td>
                                        @endif
                                        <td style="white-space:normal;padding-top: 20px;"><span id="weight">{{$product_edit->weight}}</span> gr</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <div class="rvm-shipping--calc" id="btn-hitung">Hitung</div>
                                <div class="rvm-shipping mt-10" id="show-result">
                                    <div class="rvm-shipping-input-holder clearfix" style="width: 100%;">
                                        <div class="clearfix"></div>
                                        <div class="rvm-shipping-result active" style="display: block;">
                                          <table class="table table-striped">
                                            <thead>
                                              <td>Service</td>
                                              <td>Estimasi</td>
                                              <td>Ongkir</td>
                                            </thead>
                                            <tbody id="tbody">

                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="rvm-shipping-alert"></div>
                                    </div>
                                </div>
                            </div>
                              @endif
                            </form>
                        </div>
                    </div>

                </div>

            </div>
            <br>
            <main>

  <input id="tab1" type="radio" name="tabs" checked>
  <label for="tab1">Informasi Produk</label>

  <input id="tab2" type="radio" name="tabs">
  <label for="tab2">Review Produk</label>

  <input id="tab3" type="radio" name="tabs">
  <label for="tab3">Diskusi Produk</label>


  <section id="content1">
    <p>
     {{$product_edit->description}}
    </p>

  </section>

  <section id="content2">
    <p>
      Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
    </p>
    <p>
      Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
    </p>
  </section>

  <section id="content3">
   <div style="height: 40%;
    overflow-y: auto;
    overflow: auto;" class="ask"></div>
   <hr>
   @if(\Auth::user())
    <div class="actionBox">
          <form class="form-inline" id="postQuestion" role="form">
            <div class="form-group">
                <input type="hidden" id="uid"  value="{{\Auth::user()->id}}" data-id="{{$product_edit->id}}">
                <textarea class="form-control" id="question" type="text" placeholder="Ajukan Pertanyaan" ></textarea>
            </div>
            <div class="form-group">
                <button style="margin-top: 27px;" id="submit" class="btn btn-primary">Kirim</button>
            </div>
        </form>
    </div>
    @endif
  </section>


  <section id="content4">
    <p>
      Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
    </p>
    <p>
      Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
    </p>
  </section>

</main>
        </div>

    </div>

    <!-- Recently Viewed -->

    <div class="viewed">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="viewed_title_container">
                        <h3 class="viewed_title">Barang Terkait</h3>
                        <div class="viewed_nav_container">
                            <div class="viewed_nav viewed_prev"><i class="fas fa-chevron-left"></i></div>
                            <div class="viewed_nav viewed_next"><i class="fas fa-chevron-right"></i></div>
                        </div>
                    </div>

                    <div class="viewed_slider_container">

                        <!-- Recently Viewed Slider -->

                        <div class="owl-carousel owl-theme viewed_slider">

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_1.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$225<span>$300</span></div>
                                        <div class="viewed_name"><a href="#">Beoplay H7</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_2.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$379</div>
                                        <div class="viewed_name"><a href="#">LUNA Smartphone</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_3.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$225</div>
                                        <div class="viewed_name"><a href="#">Samsung J730F...</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item is_new d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_4.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$379</div>
                                        <div class="viewed_name"><a href="#">Huawei MediaPad...</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_5.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$225<span>$300</span></div>
                                        <div class="viewed_name"><a href="#">Sony PS4 Slim</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_6.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$375</div>
                                        <div class="viewed_name"><a href="#">Speedlink...</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('someJS')

<script type="text/javascript">
$(document).ready(function(){
  initImage();
})
  Number.prototype.format = function(n, x) {
      var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
      return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
  };
  function initImage()
	{
		var images = $('.image_list li');
		var selected = $('.image_selected img');

		images.each(function()
		{
			var image = $(this);
			image.on('click', function()
			{
				var imagePath = new String(image.data('image'));
				selected.attr('src', imagePath);
			});
		});
	}
  $('#edit-province').on('change', function() {
    var province = $(this).children("option:selected").val()
    $.ajax({
      url: "{{ url('/getCity') }}",
      dataType:"json",
      type: "GET",
      data:{
        province:province
      }
    }).done(function(ret) {
        $('#edit-city').empty();
      for(i=0; i < ret.length; i++){

        $('#edit-city').append('<option data-stat="val" value="'+ret[i].city_id+'">'+ret[i].type+" " + ret[i].city_name+'</option>');
      }
    });
  });

  $('#edit-city').on('change', function() {
    var city = $(this).children("option:selected").val()
    $.ajax({
      url: "{{ url('/getDistrict') }}",
      dataType:"json",
      type: "GET",
      data:{
        city:city
      }
    }).done(function(ret) {
        $('#edit-district').empty();
      for(i=0; i < ret.length; i++){

        $('#edit-district').append('<option data-stat="val" value="'+ret[i].subdistrict_id+'">'+ret[i].subdistrict_name+'</option>');
      }
    });
  });

  $('#btn-hitung').on('click', function() {
    var checkAlamat = '{{ $districts }}';

    var kurir = $('#kurir').val();
    var destination = $('#districtDes').val();
    var origin = '{{$shopDistrict}}';
    var weight = $('#weight').html();

    var district = $('#edit-district').val();

    if(checkAlamat == ''){
      if(kurir != '' && district != ''){
        // alert('go');
        $.ajax({
    			url: "{{ url('/getongkir') }}",
    			dataType:"json",
    			type: "POST",
    			data:{
    				origin : origin,
            originType : 'subdistrict',
    				destination: district,
            destinationType : 'subdistrict',
            weight: weight,
            courier: kurir,
    				_method:"post",
    				_token : '{{ csrf_token() }}'
    			}
    		}).done(function(data){
          if(data.status.code == '200'){
            $('#show-result').show();
            var strArr = data.results[0].costs;
            var name = data.results[0].code;
            $('#tbody').empty();
    				for(i=0; i < strArr.length; i++){
              $('#tbody').append('<tr><td>'+strArr[i].service+'</td><td>'+strArr[i].cost[0].etd+' hari</td><td><strong>Rp '+(parseInt(strArr[i].cost[0].value)).format()+'</strong></td></tr>');
              // <tr>
              //   <td colspan="3" style="white-space:normal;">Perkiraan tiba dihitung sejak pesanan dikirim</td>
              // </tr>
            }
          }else{
            alert('Data tidak ditemukan!');
          }
        });
      }else{
        alert('Data tidak lengkap!');
      }
    }else{
      if(kurir != '' && origin != ''){
        $.ajax({
    			url: "{{ url('/getongkir') }}",
    			dataType:"json",
    			type: "POST",
    			data:{
    				origin : origin,
            originType : 'subdistrict',
    				destination: destination,
            destinationType : 'subdistrict',
            weight: weight,
            courier: kurir,
    				_method:"post",
    				_token : '{{ csrf_token() }}'
    			}
    		}).done(function(data){
          if(data.status.code == '200'){
            $('#show-result').show();
            var strArr = data.results[0].costs;
            var name = data.results[0].code;
            $('#tbody').empty();
    				for(i=0; i < strArr.length; i++){
              $('#tbody').append('<tr><td>'+strArr[i].service+'</td><td>'+strArr[i].cost[0].etd+' hari</td><td><strong>Rp '+(parseInt(strArr[i].cost[0].value)).format()+'</strong></td></tr>');
              // <tr>
              //   <td colspan="3" style="white-space:normal;">Perkiraan tiba dihitung sejak pesanan dikirim</td>
              // </tr>
            }
          }else{
            alert('Data tidak ditemukan!');
          }
        });
      }else{
        alert('Isi data dengan lengkap!');
      }
    }
  });

    $('#tab3').on('click', function() {

        LoadChat('{{$product_edit->id}}');
    });
  function LoadChat(id){
        $('.ask').load("/ask/"+id, function(){
            $('#postComment').on('submit', function(){

            var comment = $('#comment').val(),
             UID = $('#uid').val(),
            TID = $('#uid').data("id");
            $.ajax({
        url: "{!! url('/postDiscuss') !!}",
        dataType: "json",
        type: "POST",
        data:{
          comment: comment,
          TID:TID,
           UID: UID,
          _method:"post",
          _token : '{{ csrf_token() }}'
        }
      }).done(function(data){
                    if(data.success == true){
                        // alert('oke');
                        LoadChat();

                    }else{
                       alert('failed');
                    }
            });
        });
    });
}
$('#postQuestion').on('submit', function(){

        var question = $('#question').val(),
        UID = $('#uid').val(),
        PID = $('#uid').data("id");
    $.ajax({
        url: "{!! url('/postQuestion') !!}",
        dataType: "json",
        type: "POST",
        data:{
          question: question,
            PID:PID,
           UID: UID,
          _method:"post",
          _token : '{{ csrf_token() }}'
        }
      }).done(function(data){
                    if(data.success == true){

                        LoadChat();
                        $('#question').val("");
                    }else{
                       alert('failed');
                    }
            });
        });
$(function () {
  $('#show-result').hide();
   $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]').on('click', function(event) {
        var $panel = $(this).closest('.panel-google-plus');
            $comment = $panel.find('.panel-google-plus-comment');

        $comment.find('.btn:first-child').addClass('disabled');
        $comment.find('textarea').val('');

        $panel.toggleClass('panel-google-plus-show-comment');

        if ($panel.hasClass('panel-google-plus-show-comment')) {
            $comment.find('textarea').focus();
        }
   });
   $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function(event) {
        var $comment = $(this).closest('.panel-google-plus-comment');

        $comment.find('button[type="submit"]').addClass('disabled');
        if ($(this).val().length >= 1) {
            $comment.find('button[type="submit"]').removeClass('disabled');
        }
   });
});
</script>
@endsection
