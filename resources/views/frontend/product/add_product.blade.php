@section('title')
hariBelanja - Upload Barang
@endsection


@section('product_add')
hover-active
@endsection

@section('name_div')
  Tambah Produk
@endsection

@extends('frontend.layouts.layout_profile')

@section('css')
<style>
  /* BUTTON checked */
  .containerbtn {
      display: block;
      position: relative;
      padding-left: 35px;
      margin-bottom: 12px;
      cursor: pointer;
      font-size: 22px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    /* Hide the browser's default radio button */
    .containerbtn input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark {
      position: absolute;
      top: 7px;
      left: 0;
      height: 25px;
      width: 25px;
      background-color: #eee;
      border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .containerbtn:hover input ~ .checkmark {
      background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .containerbtn input:checked ~ .checkmark {
      background-color: #2196F3;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
      content: "";
      position: absolute;
      display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .containerbtn input:checked ~ .checkmark:after {
      display: block;
    }

    /* Style the indicator (dot/circle) */
    .containerbtn .checkmark:after {
     	top: 9px;
    	left: 9px;
    	width: 8px;
    	height: 8px;
    	border-radius: 50%;
    	background: white;
    }
    .hover-active{
      color:black;
    }
  </style>

  <style>
    .ticker{
      display: table;
      box-sizing: border-box;
      position: relative;
      background-color: rgba(14, 140, 228, 0.07);
      width: 100%;
      border-width: 1px;
      border-style: solid;
      border-image: initial;
      border-radius: 8px;
      overflow: hidden;
      border-color: rgb(14, 140, 228);
    }
    .css-1es387d::before {
        content: "";
        display: inline-block;
        position: absolute;
        height: 100%;
        top: 0px;
        left: 0px;
        border-left: 3px solid;
        border-color: rgb(14, 140, 228)
    }
    .padding-ticker{
      padding: 10px 10px 2px 25px;
    }
    .select{
      margin: 0px;
      color: #727577 !important;
    }
    .btn-foto {
        text-align: center;
        position: relative;
        overflow: hidden;
    }
    .btn-foto input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        cursor: inherit;
        display: block;
    }
    .span-jasa{
      margin-left: 10px;
      font-size: 15px;
    }
    .form-control {
      color: #727577 !important;
    }
  progress, progress[role] {
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  border: none;
  background-size: auto;
  height: 20px;
  width: 100%;
}

.bar::-webkit-progress-value {
  background: #0e8ce4;
}
  </style>
@endsection

@section('content_div')
  <div class="ticker css-1es387d">
    <div class="padding-ticker" style="font-size: 12px;">
      <p>Sebelum menambahkan produk, pastikan produk tersebut sudah sesuai dengan syarat dan ketentuan hariBelanja. Semua produk yang melanggar syarat dan ketentuan akan dinon-aktifkan oleh tim kami.</p>
    </div>
  </div>
  <div class="cart_items" style="width:100%;margin-top:10px;">
    <ul class="cart_list">
      <li class="cart_item clearfix cart_padding">
        <div class="row" style="padding-right: 15px;padding-left: 15px;">
          <form action="{{ route('products.store')}}" method="POST" id="product_form" form-type="upload" enctype="multipart/form-data">
<<<<<<< HEAD
<<<<<<< HEAD
           
=======
            <h3>Progress</h3><label class="percent"></label>
            <progress class="bar" max="100" value="75">
               <strong>Skill Level: 75%</strong>
             </progress>
>>>>>>> 1abc130155c0ecab2e64d7d29b7f069f5dd03223
=======
>>>>>>> 4852fec912453e2b6aa17ea22c54cf8faedd11db
            <div class="row" style="padding-right: 15px;padding-left: 15px;">
              <div class="col-md-12 order_total_content">
                <div class="list-name name-title">Upload Barang</div>
                <div class="name-title" style="color:#727577b0;font-size:12px">Untuk gambar optimal gunakan ukuran minimum 700 x 700 px</div>
              </div>
              <div class="col-md-4">
                <ul class="cart_list" style="background:#e0e0e082;">
                  <li class="cart_item clearfix cart_padding">
                    <div class="order_total_content">
                      <div id="preview1"  style="width: 100%;height:128px;">

                      </div>
                      <div class="cart_buttons margin-top-15">
                        <span class="button cart_button_checkout width_100 btn-foto">
                            Browse <input class="upload" data-id="1" type="file" name="foto[]">
                        </span>
                      </div>
                      <div>
                        <p class="desc-foto">Besar file maksimum: 10 Megabytes <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>

              <div class="col-md-4">
                <ul class="cart_list" style="background:#e0e0e082;">
                  <li class="cart_item clearfix cart_padding">
                    <div class="order_total_content">
                      <div id="preview2"  style="width: 100%;height:128px;">

                      </div>
                      <div class="cart_buttons margin-top-15">
                        <span class="button cart_button_checkout width_100 btn-foto">
                            Browse <input class="upload" data-id="2" type="file" name="foto[]">
                        </span>
                      </div>
                      <div>
                        <p class="desc-foto">Besar file maksimum: 10 Megabytes <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>

              <div class="col-md-4">
                <ul class="cart_list" style="background:#e0e0e082;">
                  <li class="cart_item clearfix cart_padding">
                    <div class="order_total_content">
                      <div id="preview3"  style="width: 100%;height:128px;">

                      </div>
                      <div class="cart_buttons margin-top-15">
                        <span class="button cart_button_checkout width_100 btn-foto">
                            Browse <input class="upload" data-id="3" type="file" name="foto[]">
                        </span>
                      </div>
                      <div>
                        <p class="desc-foto">Besar file maksimum: 10 Megabytes <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <br>
              </div>
            <div class="row select">
              <div class="col-md-12">
                <div class="col-md-12 order_total_content" style="padding-top:20px;">
                  {{ @csrf_field()}}
                   <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Product</label>
                     <div class="col-sm-10">
                       <input type="text" name="nama" class="form-control" id="inputEmail3" placeholder="Nama Product">
                     </div>
                   </div>
                   <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 col-form-label">Deskripsi</label>
                     <div class="col-sm-10">
                       <textarea type="text" name="description" class="form-control" id="inputEmail3" placeholder="Deskripsi barang"></textarea>
                     </div>
                   </div>
                   <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 col-form-label">Kategori</label>
                     <div class="col-sm-4">
                       <select name="category" class="form-control select" id="category">
                         <option value="">Kategori</option>

                         @foreach(App\Category::where('id_parent', "")->get() as $key => $value)

                         <option class="{{$value->id_category}}" value="{{$value->id_category}}">{{$value->title}}</option>
                         @endforeach
                       </select>

                     </div>
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Sub Kategori</label>
                    <div class="col-sm-4">
                       <select name="subcategory" class="form-control select" id="subcategory">
                         <option value="">Sub Kategori</option>

                         @foreach(App\Category::where('id_parent','!=', "")->get() as $key => $value)

                       

                         <option class="{{$value->id_parent}}" value="{{$value->id_category}}">{{$value->title}}</option>
                         @endforeach
                       </select>

                     </div>
                   </div>
                   <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 col-form-label">Harga</label>
                     <div class="col-sm-10">
                       <input name="harga" type="text" class="form-control" id="inputEmail3" placeholder="Harga">
                     </div>
                   </div>
                   <div class="form-group row">
                     <label for="exampleRadios1" class="col-sm-2 col-form-label">Kondisi Barang</label>
                     <div class="col-sm-10">
                       <div class="form-check">
                         <label class="containerbtn" for="exampleRadios1"><span class="span-jasa">Baru</span>
                           <input class="form-check-input" type="radio" name="kondisi" id="exampleRadios1" value="1" checked>
                           <span class="checkmark"></span>
                         </label>
                       </div>
                       <div class="form-check">
                         <label class="containerbtn" for="exampleRadios2"><span class="span-jasa">Bekas</span>
                           <input class="form-check-input" type="radio" name="kondisi" id="exampleRadios2" value="0">
                           <span class="checkmark"></span>
                         </label>
                       </div>
                     </div>
                   </div>

                   <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 col-form-label">Asuransi</label>
                     <div class="col-sm-10">
                       <div class="form-check">
                         <label class="containerbtn" for="exampleRadios3"><span class="span-jasa">Ya</span>
                           <input class="form-check-input" type="radio" name="asuransi" id="exampleRadios3" value="1" checked>
                           <span class="checkmark"></span>
                         </label>
                       </div>
                       <div class="form-check">
                         <label class="containerbtn" for="exampleRadios4"><span class="span-jasa">Tidak</span>
                           <input class="form-check-input" type="radio" name="asuransi" id="exampleRadios4" value="0">
                           <span class="checkmark"></span>
                         </label>
                       </div>
                     </div>
                   </div>
                   <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 col-form-label">Berat</label>
                     <div class="col-sm-10">
                       <input type="number" name="weight" class="form-control" id="inputEmail3" placeholder="Berat">
                     </div>
                   </div>
                   <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 col-form-label">Diskon</label>
                     <div class="col-sm-10">
                       <input type="number" name="discount" class="form-control select" value="0" id="inputEmail3" placeholder="Diskon Barang">
                     </div>
                   </div>
                   <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 col-form-label">Jumlah Barang</label>
                     <div class="col-sm-10">
                       <input type="number" name="qty" class="form-control select" value="1" id="inputEmail3" placeholder="Jumlah Barang">
                     </div>
                   </div>
                </div>
              </div>
            </div>
            <div class="order_total_content text-md-right margin-top-15" style="padding-right: 15px;padding-left: 15px;">
              <div class="contact_form_button">
                <input type="submit" class="button cart_button_checkout" value="Simpan">
              </div>
            </div>
            </form>
          </div>
        </li>
      </ul>
    </div>
@endsection
@section('someJS')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.min.js"></script>
<script type="text/javascript">
 $(document).ready(function(){
   var msg = "{{ Session::has('msg') }}";
   if(msg == 1){
     var message = "{{ Session::get('msg') }}";
     alert(message);
   }

      $("#subcategory").chainedTo("#category");

    $(".upload").on('change',function(){
      console.log($(this).data('id'));
      var filereader = new FileReader();
      var $img=jQuery.parseHTML("<img style='width: 100%;height:128px;' src=''>");
      filereader.onload = function(){
          $img[0].src=this.result;
      };
      filereader.readAsDataURL(this.files[0]);
       $("#preview" + $(this).data("id")).empty();
      $("#preview" + $(this).data("id")).append($img);
    });
  });
</script>
@endsection
