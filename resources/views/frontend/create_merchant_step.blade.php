@extends('frontend.layout_page')

@section('someCSS')
  <!-- <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/product_styles.css')}}">
  <style>
    .float_right{
      float: right;
    }
    .cart_padding {
      padding-right: 35px;
    }
    .width_100{
        width: 100%;
    }
    .cart_button_checkout{
      font-size: 14px;
    }
    .margin-top-15{
      margin-top: 15px;
    }

    .align-center{
      text-align: center;
      color : #8a8a8a;
    }
    .float-left{
      float: left;
    }
    .cart_item {
        padding: 40px;
    }

    .cart_item_image {
        width: 100%;
        height: 50%;
    }
    .characteristics{
        padding-top: 0px;
    }
  </style> -->
  <style>

  body {
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
    font-weight: 300;
    color: #888;
    line-height: 30px;
    text-align: center;
  }

  strong { font-weight: 500; }

  a, a:hover, a:focus {
  color: #0e8ce4;
  text-decoration: none;
    -o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
  }

  h1, h2 {
  margin-top: 10px;
  font-size: 38px;
    font-weight: 100;
    color: #555;
    line-height: 50px;
  }

  h3 {
  font-size: 22px;
    font-weight: 300;
    color: #555;
    line-height: 30px;
  }

  h4 {
  font-size: 18px;
    font-weight: 300;
    color: #555;
    line-height: 26px;
  }

  img { max-width: 100%; }

  ::-moz-selection { background: #0e8ce4; color: #fff; text-shadow: none; }
  ::selection { background: #0e8ce4; color: #fff; text-shadow: none; }


  /***** Top menu *****/

  .navbar {
  padding-top: 10px;
  background: #333;
  background: rgba(51, 51, 51, 0.3);
  border: 0;
  -o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
  }
  .navbar.navbar-no-bg { background: none; }

  ul.navbar-nav {
  font-size: 16px;
  color: #fff;
  }

  .navbar-inverse ul.navbar-nav li { padding-top: 8px; padding-bottom: 8px; }

  .navbar-inverse ul.navbar-nav li .li-text { opacity: 0.8; }

  .navbar-inverse ul.navbar-nav li a { display: inline; padding: 0; color: #fff; }
  .navbar-inverse ul.navbar-nav li a:hover { color: #fff; opacity: 1; border-bottom: 1px dotted #fff; }
  .navbar-inverse ul.navbar-nav li a:focus { color: #fff; outline: 0; opacity: 1; border-bottom: 1px dotted #fff; }

  .navbar-inverse ul.navbar-nav li .li-social a {
  margin: 0 5px;
  font-size: 28px;
  vertical-align: middle;
  }
  .navbar-inverse ul.navbar-nav li .li-social a:hover,
  .navbar-inverse ul.navbar-nav li .li-social a:focus { border: 0; color: #0e8ce4; }

  .navbar-brand {
  width: 162px;
  background: url(../img/logo.png) left center no-repeat;
  text-indent: -99999px;
  }


  /***** Top content *****/

  .top-content { padding: 40px 0 170px 0; }

  .top-content .text { color: #fff; }
  .top-content .text h1 { color: #fff; }
  .top-content .description { margin: 20px 0 10px 0; }
  .top-content .description p { opacity: 0.8; }
  .top-content .description a { color: #fff; }
  .top-content .description a:hover,
  .top-content .description a:focus { border-bottom: 1px dotted #fff; }

  .form-box { padding-top: 40px; }

  .f1 {
  padding: 25px; background: #fff;
  -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;
  }
  .f1 h3 { margin-top: 0; margin-bottom: 5px; text-transform: uppercase; }

  .f1-steps { overflow: hidden; position: relative; margin-top: 20px; }

  .f1-progress { position: absolute; top: 24px; left: 0; width: 100%; height: 1px; background: #ddd; }
  .f1-progress-line { position: absolute; top: 0; left: 0; height: 1px; background: #1A237E; }

  .f1-step { position: relative; float: left; width: 25%; padding: 0 5px; }

  .f1-step-icon {
  display: inline-block; width: 40px; height: 40px; margin-top: 4px; background: #ddd;
  font-size: 16px; color: #fff; line-height: 40px;
  -moz-border-radius: 50%; -webkit-border-radius: 50%; border-radius: 50%;
  }
  .f1-step.activated .f1-step-icon {
  background: #fff; border: 1px solid #0e8ce4; color: #0e8ce4; line-height: 38px;
  }
  .f1-step.active .f1-step-icon {
  width: 48px; height: 48px; margin-top: 0; background: #0e8ce4; font-size: 22px; line-height: 48px;
  }

  .f1-step p { color: #ccc; }
  .f1-step.activated p { color: #0e8ce4; }
  .f1-step.active p { color: #0e8ce4; }

  .f1 fieldset { display: none; text-align: left; }

  .f1-buttons { text-align: right; }

  .f1 .input-error { border-color: #0e8ce4; }



  /***** Media queries *****/

  @media (min-width: 992px) and (max-width: 1199px) {}

  @media (min-width: 768px) and (max-width: 991px) {}

  @media (max-width: 767px) {

  .navbar { padding-top: 0; }
  .navbar.navbar-no-bg { background: #333; background: rgba(51, 51, 51, 0.9); }
  .navbar-brand { height: 60px; margin-left: 15px; }
  .navbar-collapse { border: 0; }
  .navbar-toggle { margin-top: 12px; }

  .top-content { padding: 40px 0 110px 0; }

  }

  @media (max-width: 415px) {

  h1, h2 { font-size: 32px; }

  .f1 { padding-bottom: 20px; }
  .f1-buttons button { margin-bottom: 5px; }

  }


  /* Retina-ize images/icons */

  @media
  only screen and (-webkit-min-device-pixel-ratio: 2),
  only screen and (   min--moz-device-pixel-ratio: 2),
  only screen and (     -o-min-device-pixel-ratio: 2/1),
  only screen and (        min-device-pixel-ratio: 2),
  only screen and (                min-resolution: 192dpi),
  only screen and (                min-resolution: 2dppx) {

  /* logo */
    .navbar-brand {
      background-image: url(../img/logo@2x.png) !important; background-repeat: no-repeat !important; background-size: 162px 36px !important;
    }

  }

  </style>

  <style>
    /* BUTTON checked */
    .containerbtn {
      display: block;
      position: relative;
      padding-left: 35px;
      margin-bottom: 0px;
      cursor: pointer;
      font-size: 22px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    /* Hide the browser's default checkbox */
    .containerbtn input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      height: 0;
      width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
      position: absolute;
      top: 20px;
      left: 0;
      height: 25px;
      width: 25px;
      background-color: #eee;
    }

    /* On mouse-over, add a grey background color */
    .containerbtn:hover input ~ .checkmark {
      background-color: #ccc;
    }

    /* When the checkbox is checked, add a blue background */
    .containerbtn input:checked ~ .checkmark {
      background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
      content: "";
      position: absolute;
      display: none;
    }

    /* Show the checkmark when checked */
    .containerbtn input:checked ~ .checkmark:after {
      display: block;
    }

    /* Style the checkmark/indicator */
    .containerbtn .checkmark:after {
      left: 9px;
      top: 5px;
      width: 5px;
      height: 10px;
      border: solid white;
      border-width: 0 3px 3px 0;
      -webkit-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      transform: rotate(45deg);
    }
    </style>

  <style>
  .form-control {
      color: #0e8ce4;
    }
  .div-step{
    text-align: center;
    margin-top: 15px;
    margin-bottom: 30px;
  }
  .div-border{
    padding: 10px;
    border: solid 1px #e8e8e8;
    box-shadow: 0px 1px 5px rgba(0,0,0,0.1);
  }
  .form-check{
    margin-bottom: 0px !important;
  }
  .span-jasa{
    margin-left: 10px;
    font-size: 15px;
  }
  </style>
@endsection

@section('someJS')
  <script src="{{asset('js/product_custom.js')}}"></script>
    <script src="{{asset('js/public_assets_build_js_jquery.backstretch.min.js')}}"></script>
  <script>

  function scroll_to_class(element_class, removed_height) {
  var scroll_to = $(element_class).offset().top - removed_height;
  if($(window).scrollTop() != scroll_to) {
    $('html, body').stop().animate({scrollTop: scroll_to}, 0);
  }
  }

  function bar_progress(progress_line_object, direction) {
  var number_of_steps = progress_line_object.data('number-of-steps');
  var now_value = progress_line_object.data('now-value');
  var new_value = 0;
  if(direction == 'right') {
    new_value = now_value + ( 100 / number_of_steps );
  }
  else if(direction == 'left') {
    new_value = now_value - ( 100 / number_of_steps );
  }
  progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
  }

$(document).ready(function () {

/*
Fullscreen background
*/
$.backstretch("assets/img/backgrounds/1.jpg");

$('#top-navbar-1').on('shown.bs.collapse', function(){
$.backstretch("resize");
});
$('#top-navbar-1').on('hidden.bs.collapse', function(){
$.backstretch("resize");
});

/*
Form
*/
$('.f1 fieldset:first').fadeIn('slow');

$('.f1 input[type="text"], .f1 input[type="password"], .f1 textarea').on('focus', function() {
$(this).removeClass('input-error');
});

// next step
$('.f1 .btn-next').on('click', function() {
var parent_fieldset = $(this).parents('fieldset');
var next_step = true;
// navigation steps / progress steps
var current_active_step = $(this).parents('.f1').find('.f1-step.active');
var progress_line = $(this).parents('.f1').find('.f1-progress-line');

// fields validation
parent_fieldset.find('input[type="text"], input[type="password"], textarea').each(function() {
if( $(this).val() == "" ) {
  $(this).addClass('input-error');
  next_step = false;
}
else {
  $(this).removeClass('input-error');
}
});
// fields validation

if( next_step ) {
parent_fieldset.fadeOut(400, function() {
  // change icons
  current_active_step.removeClass('active').addClass('activated').next().addClass('active');
  // progress bar
  bar_progress(progress_line, 'right');
  // show next step
  $(this).next().fadeIn();
  // scroll window to beginning of the form
  scroll_to_class( $('.f1'), 20 );
});
}

});

// previous step
$('.f1 .btn-previous').on('click', function() {
// navigation steps / progress steps
var current_active_step = $(this).parents('.f1').find('.f1-step.active');
var progress_line = $(this).parents('.f1').find('.f1-progress-line');

$(this).parents('fieldset').fadeOut(400, function() {
// change icons
current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
// progress bar
bar_progress(progress_line, 'left');
// show previous step
$(this).prev().fadeIn();
// scroll window to beginning of the form
scroll_to_class( $('.f1'), 20 );
});
});

// submit
$('.f1').on('submit', function(e) {

// fields validation
$(this).find('input[type="text"], input[type="password"], textarea').each(function() {
if( $(this).val() == "" ) {
  e.preventDefault();
  $(this).addClass('input-error');
}
else {
  $(this).removeClass('input-error');
}
});
// fields validation

});


});

  </script>
@endsection

@section('content')
<!DOCTYPE html>
        <div >
            <div class="container">
                <div>
                    <div class="col-sm-12 form-box">
                        <form role="form" action="{{ url('/add/merchant/step') }}" method="post" enctype="multipart/form-data" class="f1">
                          {{ csrf_field() }}
                          <input type="hidden" name="id" value="{{ $id }}">
                            <div class="f1-steps">
                                <div class="f1-progress">
                                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                                </div>
                                <div class="f1-step active">
                                    <div class="f1-step-icon">1</div>
                                    <p>Validasi Merchant</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon">2</div>
                                    <p>Lokasi Pengiriman</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon">3</div>
                                    <p>Layanan Jasa Pengiriman</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon">4</div>
                                    <p>Approve</p>
                                </div>
                            </div>

                            <!-- <fieldset>
                                <div class="div-step">
                                  <h4>Informasi Merchant</h4>
                                  <p>Lengkapi ringkasan informasi merchant.<br>Tampilan merchant yang menarik buat pembeli tertarik.</p>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">Deskripsi</label>
                                    <textarea name="description" placeholder="Deskripsi Merchant" class="f1-about-yourself form-control" id="description" required></textarea>
                                </div>
                                <div class="form-group">
                                  <p>Logo</p>
                                    <label class="sr-only">Logo</label>
                                    <input type="file" name="logo" class="f1-pict-name form-control" id="logo" required>
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset> -->

                            <fieldset>
                                <div class="div-step">
                                  <h4>Validasi Merchant</h4>
                                  <p>Lengkapi data merchant untuk validasi.</p>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">Deskripsi</label>
                                    <textarea name="description" placeholder="Deskripsi Merchant" class="f1-about-yourself form-control" id="description" required></textarea>
                                </div>
                                <div class="form-group">
                                  <p>Logo</p>
                                    <label class="sr-only">Logo</label>
                                    <input type="file" name="logo" class="f1-pict-name form-control" id="logo" required>
                                </div>
                                <div class="form-group">
                                  <p>Product</p>
                                    <label class="sr-only">Product</label>
                                    <input type="file" name="product" class="f1-pict-name form-control" id="product">
                                </div>
                                <div class="form-group">
                                  <p>KTP</p>
                                    <label class="sr-only">KTP</label>
                                    <input type="file" name="ktp" class="f1-pict-name form-control" id="ktp">
                                </div>
                                <div class="form-group">
                                  <p>Foto selfie bersama KTP</p>
                                    <label class="sr-only">NPWP</label>
                                    <input type="file" name="selfie" class="f1-pict-name form-control" id="selfie">
                                </div>
                                <div class="form-group">
                                  <p>NPWP</p>
                                    <label class="sr-only">NPWP</label>
                                    <input type="file" name="npwp" class="f1-pict-name form-control" id="npwp">
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="div-step">
                                  <h4>Lokasi Pengiriman</h4>
                                  <p>Lengkapi ringkasan informasi merchant.<br>Tampilan merchant yang menarik buat pembeli tertarik.</p>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">Alamat Pengiriman</label>
                                    <!-- <input type="text" name="city" placeholder="Alamat Pengiriman City" class="f1-first-name form-control" id="city"> -->
                                    <select id="city" class="f1-last-name form-control" required="" style="color: #828282 !important;margin-left: 0px;" name="city">
                                      <optgroup label="Pilih">
                                          <option value="Pilih">Pilih</option>
                                      </optgroup>

                                      @foreach($provinces as $provinsi)

                                      <optgroup label="{{$provinsi['province']}}">
                                          @foreach(\App\Helper::getCity($provinsi['province_id']) as $city)
                                            <option data-province="{{$provinsi['province_id']}}" value="{{$provinsi['province_id']}}&{{$city['city_id']}}">{{$city['city_name']}}</option>
                                          @endforeach
                                      </optgroup>
                                      @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">Kode Pos</label>
                                    <input type="text" name="zipcode" placeholder="Kode Pos" class="f1-last-name form-control" id="zipcode">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">Alamat Pickup</label>
                                    <textarea name="address" placeholder="Alamat Pickup" class="f1-about-yourself form-control" id="address"></textarea>
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="div-step">
                                  <h4>Layanan Jasa Pengiriman</h4>
                                  <p>Aktifkan layanan jasa pengiriman yang ingin Anda Gunakan.<br>Berikan pilihan yang luas untuk pelanggan Anda.</p>
                                </div>
                                <div class="row">
                                  <div class="col-md-1"></div>
                                  <div class="col-md-5">
                                    <div class="div-border">
                                      <div class="form-check">
                                        <label class="containerbtn"><img src="https://s3-ap-southeast-1.amazonaws.com/asset1.gotomalls.com/uploads/retailers/logo/L43yLOvWvm-612DF-1269-jne-1529458956_1.jpg" alt="" width="60" height="60"><span class="span-jasa">Aktifkan JNE</span>
                                          <input type="checkbox" class="form-check-input" value="jne" name="jasa[]">
                                          <span class="checkmark"></span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="div-border">
                                      <div class="form-check">
                                        <label class="containerbtn"><img src="https://1.bp.blogspot.com/-jV9NSilzZuQ/WcDMH2zTC9I/AAAAAAAAAaQ/B1qXHKjE1s8tvEiYZ4KSr7bbQ6zconmXgCLcBGAs/s400/logo_j%2526t.png" alt="" width="60" height="60"><span class="span-jasa">Aktifkan J&T</span>
                                          <input type="checkbox" class="form-check-input" value="jnt" name="jasa[]">
                                          <span class="checkmark"></span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="f1-buttons">
                                    <button type="submit" class="btn btn-next">Approve</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>

            </div>
        </div>


        <!-- Javascript -->
        <!-- <script src="{{asset('/assets')}}/vendors/jquery/dist/jquery.min.js"></script>
        <script src="{{asset('/assets')}}/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="{{asset('/assets')}}/build/js/jquery.backstretch.min.js"></script>
        <script src="{{asset('/assets')}}/build/js/retina-1.1.0.min.js"></script> -->
        <!-- <script src="{{asset('/assets')}}/build/js/scripts.js"></script> -->

    <!-- <div class="characteristics" style="background: #fff;">
      <div class="row">
        <div class="col-lg-12">
          <div class="cart_container">
            <div class="row col-md-12">
                <div class="col-md-7">
                  <div class="cart_item_image"><img src="https://previews.123rf.com/images/bloomua/bloomua1505/bloomua150500104/39953191-thin-line-flat-design-of-online-shopping-checkout-buying-store-goods-by-pos-terminal-sell-mass-marke.jpg" alt=""></div>
                </div>
                <div class="col-md-5">
                  <div class="cart_items">
                    <ul class="cart_list">
                      <li class="cart_item clearfix cart_padding">
                        <div class="order_total_content text-md-right">
                          <h4 class="align-center">Hai, {{ Auth::user()->name }}</h4>
                          <h6 class="cart_item_title align-center">Selamat datang di hariBelanja, ayo buat merchant sekarang!</h6>
                          <form id="login-form"method="POST" role="form" action="{{ url('/login')  }}" style="color: #8a8a8a;">
                            <div class="form-group">
                              <label for="exampleInputEmail1" class="float-left">Nama Merchant</label>
                              <input type="text" class="form-control" id="name" placeholder="Nama Merchant" name="name">
                              <p class="float-left" style="color: darksalmon;font-size: 12px;">*Maksimal 24 karakter</p>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1" style="width:100%;text-align: left;">Domain Toko</label>
                              <div class="input-group">
                                <span class="input-group-addon">www.hariBelanja.com/</span>
                                <input id="domain" type="text" class="form-control" name="domain" placeholder="Domain Toko">
                              </div>
                              <p style="text-align: left;color: darksalmon;font-size: 12px;">*Domain toko maksimal 16 karakter (huruf, angka dan tanda “-“) tanpa spasi</p>
                            </div>
                            <button type="submit" value="login" class="btn btn-primary margin-top-15 width_100">Buat Merchant Sekarang</button>
                          </form>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->

@endsection
