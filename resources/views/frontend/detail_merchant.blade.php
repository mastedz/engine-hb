@extends('frontend.layout_home')
@section('someCSS')
    <link rel="stylesheet" type="text/css" href="{{asset('')}}styles/shop_styles.css">
    <link rel="stylesheet" type="text/css" href="{{asset('')}}styles/shop_responsive.css">
    <link rel="stylesheet" type="text/css" href="{{asset('styles/product_styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('styles/product_responsive.css') }}">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Open+Sans:400italic);
      blockquote{
        font-size: 12px;
        width: 100%;
        font-family: Open Sans;
        font-style: italic;
        color: #555555;
        padding: 1.2em 30px 1.2em 40px;
        border-left: 8px solid #0e8ce4;
        line-height: 1.6;
        position: relative;
        background: #ededed94;
      }

      blockquote::before{
      font-family:Arial;
      content: "\201C";
      color:#0e8ce4;
      font-size:4em;
      position: absolute;
      left: 10px;
      top:-10px;
      }

      blockquote::after{
      content: '';
      }

      blockquote span{
      display:block;
      color:#333333;
      font-style: normal;
      font-weight: bold;
      margin-top:1em;
      }
    </style>
    <style>
    .clearfix:before,
    .clearfix:after {
    	content: " ";
    	display: table;
    }
    a {
      color: #ccc;
      text-decoration: none;
      outline: none;
      }

      /*Fun begins*/
      .tab_container {
      padding-top: 70px;
      position: relative;
      }

      input, section {
      clear: both;
      padding-top: 10px;
      display: none;
      }

      label {
      font-weight: 500;
      font-size: 14px;;
      display: block;
      float: left;
      width: 20%;
      padding: 1.5em;
      color: #757575;
      cursor: pointer;
      text-decoration: none;
      text-align: center;
      background: #f0f0f0;
      }

      #tab1:checked ~ #content1,
      #tab2:checked ~ #content2,
      #tab3:checked ~ #content3,
      #tab4:checked ~ #content4,
      #tab5:checked ~ #content5 {
      display: block;
      padding: 20px;
      background: #fff;
      color: #999;
      border-bottom: 2px solid #f0f0f0;
      }

      .tab_container .tab-content p,
      .tab_container .tab-content h3 {
      -webkit-animation: fadeInScale 0.7s ease-in-out;
      -moz-animation: fadeInScale 0.7s ease-in-out;
      animation: fadeInScale 0.7s ease-in-out;
      }
      .tab_container .tab-content h3  {
      text-align: center;
      }

      .tab_container [id^="tab"]:checked + label {
      background: #fff;
      box-shadow: inset 0 3px #0e8ce4;
      color: #0e8ce4;
      }

      .tab_container [id^="tab"]:checked + label .fa {
      color: #0e8ce4;
      }

      label .fa {
      font-size: 1.3em;
      margin: 0 0.4em 0 0;
      }

      /*Media query*/
      @media only screen and (max-width: 930px) {
      label span {
        font-size: 14px;
      }
      label .fa {
        font-size: 14px;
      }
      }

      @media only screen and (max-width: 768px) {
      label span {
        display: none;
      }

      label .fa {
        font-size: 16px;
      }

      .tab_container {
        width: 98%;
      }
      }

      /*Content Animation*/
      @keyframes fadeInScale {
      0% {
        transform: scale(0.9);
        opacity: 0;
      }

      100% {
        transform: scale(1);
        opacity: 1;
      }
      }
    </style>
    <style>
      .image_selected {
        height: auto;
      }
      .btn-chat{
        font-size: unset;
        display: inline-block;
        white-space: nowrap;
        float: right;
        vertical-align: middle;
        text-align: center;
        cursor: pointer;
        background-color: rgb(255, 255, 255);
        color: rgba(0, 0, 0, 0.54);
        background-image: none;
        margin: 0px 5px;
        border-width: 1px;
        border-style: solid;
        border-color: rgba(0, 0, 0, 0.12);
        border-image: initial;
        transition: all 0.3s ease 0s;
        border-radius: 3px;
        padding: 0px 14px;
      }
      .color-font{
        color: rgba(0, 0, 0, 0.54) !important;
      }
      .btn-follow{
        background-color: rgb(43, 168, 255);
        color: #ffff;
      }
      .checked {
        color: orange;
      }
      .product_text{
        margin-top: 0px;
      }
      .margin-1{
        margin: 0px -1px;
        background: whitesmoke;
      }
      .section {
          display: none;
          border: solid 1px #e8e8e8;
          box-shadow: 0px 1px 5px rgba(0,0,0,0.1);
      }
      .diskusi{
          max-height: 85px;
      }
    </style>
@endsection
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-lg-2 order-lg-2 order-1">
        <div class="image_selected"><img src="{{ asset('/storage/images/'.$shop->image) }}" alt=""></div>
      </div>
      <div class="col-lg-10 order-3">
        <div class="product_description">
          <!-- <div class="product_category">Laptops</div> -->
          <div class="product_name" style="color: #0e8ce4;">{{ $shop->name }}</div>
          <hr>
          <!-- <div class="rating_r rating_r_4 product_rating"><i></i><i></i><i></i><i></i><i></i></div> -->
          <div class="row">
            <div class="col-lg-6 order-3">
              <div><i class="fas fa-map-marker-alt color-font"></i>&nbsp;&nbsp;{{ $city }}, {{ $province }}</div>
              <div><i class="fas fa-clock color-font"></i> 1 menit yang lalu</div>
            </div><div class="col-lg-6 order-3">
              <button type="button" class="button btn-chat btn-follow btn-primary"><i class="fas fa-plus"></i> Follow</button>
              <button type="button" class="button btn-chat btn-primary"><i class="fas fa-comments"></i> Chat Penjual</button>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-lg-6 order-3">
              <div class="product_text"><blockquote>{{ $shop->description }}</blockquote></div>
            </div>
            <div class="col-lg-6 order-3">
              <div class="btn-chat margin-1" style="padding: 9px 14px;">
                <span style="font-weight: 700;color: #0e8ce4;">500</span>
                <br>Followers
              </div>
              <div class="btn-chat margin-1" style="padding: 9px 14px;">
                <span style="font-weight: 700;color: #0e8ce4;">19</span>
                <br>Produk Terjual
              </div>
              <div class="btn-chat margin-1" style="padding: 9px 14px;">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
                <br>Rating
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 order-3" style="padding-left:0px;margin-top:20px">
        <div class="tab_container">
    			<input id="tab1" type="radio" name="tabs">
    			<label for="tab1"><span>Produk</span></label>

    			<input id="tab2" type="radio" name="tabs" checked>
    			<label for="tab2"><span>Diskusi Produk</span></label>

    			<input id="tab3" type="radio" name="tabs">
    			<label for="tab3"><span>Ulasan</span></label>

    			<input id="tab4" type="radio" name="tabs">
    			<label for="tab4"><span>Informasi Merchant</span></label>

    			<section id="content1" class="tab-content section">
            <div class="row">
                <div class="col-lg-3">
                    <!-- Shop Sidebar -->
                    <div>
                        <div class="sidebar_section">
                            <div class="sidebar_title">Categories</div>
                            <ul class="sidebar_categories">
                                @if(App\Category::count())
                                            @foreach(App\Category::all() as $category)
                                <li><a href="{{route('collection.show',$category->id)}}">{{$category->name}}</a></li>
                                            @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="sidebar_section filter_by_section">
                            <form id="form" method="get" action="{{url('/filter')}}">
                            <div class="sidebar_title">Filter </div>
                            <div class="sidebar_subtitle">Harga</div>

                              <div class="right-inner-addon">
                                <i class="icon-search">Rp</i>
                                <input type="text" name="start" style="text-align: right;" class="form-control col-md-8" placeholder="Dari" />

                            </div>
                            <br>
                            <div class="right-inner-addon">
                                <i class="icon-search">Rp</i>
                                <input type="text" name="end" style="text-align: right;" class="form-control col-md-8" placeholder="Sampai" />

                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Filter</button>
                            </form>
                        </div>

                    </div>

                </div>

                <div class="col-lg-9">

                    <!-- Shop Content -->

                    <div class="shop_content">
                        <div class="shop_bar clearfix">
                            <div class="shop_product_count">
                              @if($products != null)
                                <span>{{$products->total()}}</span> products found
                              @else
                                product tidak ada
                              @endif
                            </div>
                            <div class="shop_sorting">
                                <span>Urutkan :</span>
                                <ul>
                                    <li>
                                        <span class="sorting_text">Harga<i class="fas fa-chevron-down"></span></i>
                                        <ul>
                                            <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Harga Tertinggi</li>
                                            <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Harga Terendah</li>
                                       <!--      <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>price</li> -->
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="product_grid">
                            <div class="product_grid_border"></div>
                            <div class="row" style="margin-right: -4px;"> <!--border-left: solid 2px #e5e5e5;!-->
                            @if($products != null)
                              @forelse($products as $product)
                                @php
                                if(!is_null($product->discount)){
                                $price = $product->price - ($product->price * ($product->discount / 100));
                                }else{
                                $price = $product->price;
                                }
                                $pict = json_decode($product->image);
                                @endphp
                               <div class="col-md-3 col-sm-6">
                              <!-- Product Item -->
                              @if(!is_null($product->discount))
                              <div class="product_item discount">
                                  @else
                                  <div class="product_item ">
                                      @endif
                                  <div class="product_border"></div>
                                  <div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{asset('')}}images/{{$pict[0]}}" alt=""></div>
                                  <div class="product_content">
                                      <div class="product_price">Rp.{{number_format($price)}}
                                          @if(!is_null($product->discount))
                                              <span>{{$product->price}}</span>
                                          @endif
                                      </div>
                                      <div class="product_name"><div><a href="{{route('frontend.detail', $product->id)}}" tabindex="0">{{$product->name}}</a></div></div>
                                  </div>
                                  <div class="product_fav"><i class="fas fa-heart"></i></div>
                                  <ul class="product_marks" style="display: block" >
                                      @if(!is_null($product->discount))
                                      <li class="product_mark product_discount">-{{$product->discount}}%</li>
                                      @endif
                                      <li class="product_mark product_new">new</li>
                                  </ul>
                                  <a href="{{url('buynow', $product->id)}}" class="btn btn-primary">Keranjang</a>
                              </div>
                          </div>
                              @empty
                              @endforelse
                          @endif


                        </div>
                        </div>

                        <!-- Shop Page Navigation -->

                        @if($products != null)
                          {{$products->links()}}
                        @endif
                    </div>
                </div>
            </div>
    			</section>

    			<section id="content2" class="tab-content">
            <div class="row section diskusi">
              <div class="col-lg-2">
                <div class="image_selected" style="border: none;box-shadow: none;"><img src="{{ asset('/storage/images/'.$shop->image) }}" alt="" width="50px"></div>
              </div>
              <div class="col-lg-10">
                  <div class="product_description" style="padding: 20px;">
                    <p>Mac Book Pro<br><span style="color: #0e8ce4;font-weight: 600;">Rp 90.000</span></p>
                  </div>
              </div>
            </div>
            <div class="row section diskusi">
              <div class="col-lg-2">
                <div class="image_selected" style="border: none;box-shadow: none;"><img src="{{ asset('/storage/images/'.$shop->image) }}" alt="" width="50px"></div>
              </div>
              <div class="col-lg-10">
                  <div class="product_description" style="padding: 20px;">
                    <p>Sri Wahyu Ningsih - <span style="font-size:11px;">3 jam yang lalu</span><br>
                      <span>Warna Hitam ready Kak? Terima kasih!</span>
                    </p>
                  </div>
              </div>
            </div>
            <div class="row section diskusi"  style="background:#e8e8e86b;">
              <div class="col-lg-1"></div>
              <div class="col-lg-2">
                <div class="image_selected" style="border: none;box-shadow: none;"><img src="{{ asset('/storage/images/'.$shop->image) }}" alt="" width="50px"></div>
              </div>
              <div class="col-lg-9">
                  <div class="product_description" style="padding: 20px;">
                    <input placeholder="Isi komentar disini">
                  </div>
              </div>
            </div>
            <div class="col-lg-12 section" style="display:block;">

            </div>
    			</section>

    			<section id="content3" class="tab-content section">
    				<h3>Headline 3</h3>
    		      	<p>Tab 3 content</p>
    			</section>

    			<section id="content4" class="tab-content section">
    				<h3>Headline 4</h3>
    		      	<p>tab 4 Content</p>
    			</section>
    		</div>
      </div>
    </div>
  </div>

    @endsection
    @section('someJS')
<script type="text/javascript">


        var $form = $( "#form" );
        var $input = $form.find( "input" );

        $input.on( "keyup", function( event ) {


            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
            if ( selection !== '' ) {
                return;
            }

            // When the arrow keys are pressed, abort.
            if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                return;
            }


            var $this = $( this );

            // Get the value.
            var input = $this.val();

            var input = input.replace(/[\D\s\._\-]+/g, "");
            input = input ? parseInt( input, 10 ) : 0;

            $this.val( function() {
                return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
            } );
        } );


        $form.on( "submit", function( event ) {

            var $this = $( this );
            var arr = $this.serializeArray();

            for (var i = 0; i < arr.length; i++) {
                    arr[i].value = arr[i].value.replace(/[($)\s\._\-]+/g, ''); // Sanitize the values.
            };

            console.log( arr );

            event.preventDefault();
        });


</script>
@endsection
