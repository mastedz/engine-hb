@section('title')
hariBelanja - My Merchant
@endsection

@section('my_merchant')
hover-active
@endsection

@extends('frontend.layouts.layout_profile')

@section('css')
  <!-- <link rel="stylesheet" href="select2.css">
  <link rel="stylesheet" href="select2-bootstrap.css"> -->
  <style>
    /* BUTTON checked */
    .containerbtn {
      display: block;
      position: relative;
      padding-left: 35px;
      margin-bottom: 0px;
      cursor: pointer;
      font-size: 22px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    /* Hide the browser's default checkbox */
    .containerbtn input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      height: 0;
      width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
      position: absolute;
      top: 20px;
      left: 0;
      height: 25px;
      width: 25px;
      background-color: #eee;
    }

    /* On mouse-over, add a grey background color */
    .containerbtn:hover input ~ .checkmark {
      background-color: #ccc;
    }

    /* When the checkbox is checked, add a blue background */
    .containerbtn input:checked ~ .checkmark {
      background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
      content: "";
      position: absolute;
      display: none;
    }

    /* Show the checkmark when checked */
    .containerbtn input:checked ~ .checkmark:after {
      display: block;
    }

    /* Style the checkmark/indicator */
    .containerbtn .checkmark:after {
      left: 9px;
      top: 5px;
      width: 5px;
      height: 10px;
      border: solid white;
      border-width: 0 3px 3px 0;
      -webkit-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      transform: rotate(45deg);
    }
    </style>
  <style>
    .p-width{
      width: 100px;
      padding-top: 11px;
    }
    .padding-top{
      padding-top: 15px;
    }
    .margin-left{
      margin-left: 0px;
    }
    .btn-foto {
        text-align: center;
        position: relative;
        overflow: hidden;
    }
    .btn-foto input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        cursor: inherit;
        display: block;
    }
    .input_field{
      color: #828282 !important;
    }
  </style>
@endsection

@section('someJS')
  <script>
  $(document).ready(function(){

     $(".upload").on('change',function(){
       console.log($(this).data('id'));
       var filereader = new FileReader();
       var $img=jQuery.parseHTML("<img style='width: 100%;height:128px;' src=''>");
       filereader.onload = function(){
           $img[0].src=this.result;
       };
       filereader.readAsDataURL(this.files[0]);
        $("#preview" + $(this).data("id")).empty();
       $("#preview" + $(this).data("id")).append($img);
     });

   });

    $('#edit-province').on('change', function() {
      var province = $(this).children("option:selected").val()
      $.ajax({
        url: "{{ url('/getCity') }}",
        dataType:"json",
        type: "GET",
        data:{
          province:province
        }
      }).done(function(ret) {
          $('#edit-city').empty();
        for(i=0; i < ret.length; i++){

          $('#edit-city').append('<option data-stat="val" value="'+ret[i].city_id+'">'+ret[i].type+" " + ret[i].city_name+'</option>');
        }
      })
    });

    $('#edit-city').on('change', function() {
      var city = $(this).children("option:selected").val()
      $.ajax({
  			url: "{{ url('/getDistrict') }}",
  			dataType:"json",
  			type: "GET",
  			data:{
  				city:city
  			}
  		}).done(function(ret) {
          $('#edit-district').empty();
        for(i=0; i < ret.length; i++){

          $('#edit-district').append('<option data-stat="val" value="'+ret[i].subdistrict_id+'">'+ret[i].subdistrict_name+'</option>');
        }
      })
    });
  </script>
@endsection

@section('content_div')

  <div class="row" style="padding-right: 15px;padding-left: 15px;">
    <div class="col-md-12 order_total_content">
      <div class="list-name name-title">&nbsp;My Merchant</div>
    </div>
    <!-- <form role="form" action="{{ url('/update/profile') }}" method="post" enctype="multipart/form-data" class="f1"> -->
    <div class="col-md-4">
      <form action="{{ route('merchant.edit', $shop->shop_slug) }}" method="POST" id="contact_form" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input hidden value="{{ $shop->id }}" name="id">
      <ul class="cart_list" style="background:#e0e0e082;">
        <li class="cart_item clearfix cart_padding">
          <div class="order_total_content">
            <div>
              @if(!empty($shop->image))

                <img src="{{ asset('/storage/images/'.$shop->image) }}" alt="HTML5 Icon" style="width: 100%;height:auto;">

              @else
              <div id="preview1"  style="width: 100%;height:128px;">

              </div>
              @endif
            </div>
            <div class="cart_buttons margin-top-15">
              <span class="button cart_button_checkout width_100 btn-foto">
                  Browse <input class="upload" data-id="1" type="file" name="image">
              </span>
            </div>
            <div>
              <p class="desc-foto">Besar file maksimum: 10 Megabytes <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col-md-8">
      <!-- <form action="#" id="contact_form"> -->
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <p class="p-width">Nama</p>
          <input type="text" id="contact_form_name" class="contact_form_name input_field width_100" required="required" data-error="Name is required." value="{{ $shop->name }}" name="name">
        </div>
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <p class="p-width">Slug</p>
          <input type="text" id="contact_form_email" class="contact_form_email input_field width_100" required="required" data-error="Email is required." value="{{ $shop->shop_slug }}" disabled name="shop_slug">
        </div>
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <p class="p-width">Provinsi</p>
          <select name="province" id="edit-province" class="form-control full-width has-padding has-border margin-left" required="" style="color: #828282 !important;">
            <option value="Pilih" disabled>Pilih</option>
            @foreach($provinces as $provinsi)
              <option data-province="{{$provinsi['province_id']}}" value="{{$provinsi['province_id']}}" {{ $shop->province == $provinsi['province_id'] ? 'selected' : '' }}>{{$provinsi['province']}}</option>
            @endforeach
          </select>
        </div>
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <p class="p-width">Kota</p>
          <select id="edit-city" class="form-control full-width has-padding has-border margin-left" required="" style="color: #828282 !important;" name="city">
            <option value="Pilih" disabled>Pilih</option>
            @foreach($cities as $city)
              <option data-city="{{$city['city_id']}}" value="{{$city['city_id']}}" {{ $shop->shop_city == $city['city_id'] ? 'selected' : '' }}>{{$city['type']}} {{$city['city_name']}}</option>
            @endforeach
          </select>
        </div>
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <p class="p-width">Kecamatan</p>
          <select id="edit-district" class="form-control full-width has-padding has-border margin-left" required="" style="color: #828282 !important;" name="district">
            <option value="Pilih" disabled>Pilih</option>
            @foreach($districts as $district)
              <option data-city="{{$district['subdistrict_id']}}" value="{{$district['subdistrict_id']}}" {{ $shop->district == $district['subdistrict_id'] ? 'selected' : '' }}>{{$district['subdistrict_name']}}</option>
            @endforeach
          </select>
        </div>
      <!-- </form> -->
    </div>
    <div class="col-md-12" style="padding-top: 20px;">
      <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
        <p class="p-width">Address</p>
        <textarea type="text" id="contact_form_phone" class="contact_form_phone input_field width_100 padding-top" name="address">{{ $shop->shop_address }}</textarea>
      </div>
      <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
        <p class="p-width">Deskripsi</p>
        <textarea type="text" id="contact_form_phone" class="contact_form_phone input_field width_100 padding-top" name="description">{{ $shop->description }}</textarea>
      </div>
      <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
        <p class="p-width">Jasa Kurir</p>
        <fieldset style="width: 100%;">
            <div class="row">
              <div class="col-md-4">
                <div class="div-border">
                  <div class="form-check">
                    <label class="containerbtn"><img src="https://s3-ap-southeast-1.amazonaws.com/asset1.gotomalls.com/uploads/retailers/logo/L43yLOvWvm-612DF-1269-jne-1529458956_1.jpg" alt="" width="60" height="60">
                      <input type="checkbox" class="form-check-input" value="jne" name="jasa[]" {{ in_array('jne', $delivery) ? 'checked="checked"' : '' }}>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="div-border">
                  <div class="form-check">
                    <label class="containerbtn"><img src="https://img.solopos.com/thumb/posts/2018/04/18/911266/pt-pos-indonesia.jpg?w=600&h=400" alt="" height="60">
                      <input type="checkbox" class="form-check-input" value="pos" name="jasa[]" {{ in_array('pos', $delivery) ? 'checked="checked"' : '' }}>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="div-border">
                  <div class="form-check">
                    <label class="containerbtn"><img src="https://apollo-singapore.akamaized.net/v1/files/i2wa3d8nut35-ID/image;s=966x691;olx-st/_1_.jpg" alt="" height="60">
                      <input type="checkbox" class="form-check-input" value="tiki" name="jasa[]" {{ in_array('tiki', $delivery) ? 'checked="checked"' : '' }}>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
        </fieldset>
      </div>
    </div>
  </div>
  <div class="order_total_content text-md-right margin-top-15" style="padding-right: 15px;padding-left: 15px;">
    <div class="contact_form_button">
      <button type="submit" class="button cart_button_checkout">Simpan</button>
    </div>
  </div>
</form>
@endsection
