@section('title')
hariBelanja - Konfirmasi Resi
@endsection


@section('product_add')
hover-active
@endsection

@section('name_div')
  Tambah Produk
@endsection

@extends('frontend.layouts.layout_profile')

@section('css')
<style>
  /* BUTTON checked */
  .containerbtn {
      display: block;
      position: relative;
      padding-left: 35px;
      margin-bottom: 12px;
      cursor: pointer;
      font-size: 22px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    /* Hide the browser's default radio button */
    .containerbtn input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark {
      position: absolute;
      top: 7px;
      left: 0;
      height: 25px;
      width: 25px;
      background-color: #eee;
      border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .containerbtn:hover input ~ .checkmark {
      background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .containerbtn input:checked ~ .checkmark {
      background-color: #2196F3;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
      content: "";
      position: absolute;
      display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .containerbtn input:checked ~ .checkmark:after {
      display: block;
    }

    /* Style the indicator (dot/circle) */
    .containerbtn .checkmark:after {
     	top: 9px;
    	left: 9px;
    	width: 8px;
    	height: 8px;
    	border-radius: 50%;
    	background: white;
    }
    .hover-active{
      color:black;
    }
  </style>

  <style>
    .ticker{
      display: table;
      box-sizing: border-box;
      position: relative;
      background-color: rgba(14, 140, 228, 0.07);
      width: 100%;
      border-width: 1px;
      border-style: solid;
      border-image: initial;
      border-radius: 8px;
      overflow: hidden;
      border-color: rgb(14, 140, 228);
    }
    .css-1es387d::before {
        content: "";
        display: inline-block;
        position: absolute;
        height: 100%;
        top: 0px;
        left: 0px;
        border-left: 3px solid;
        border-color: rgb(14, 140, 228)
    }
    .padding-ticker{
      padding: 10px 10px 2px 25px;
    }
    .select{
      margin: 0px;
      color: #727577 !important;
    }
    .btn-foto {
        text-align: center;
        position: relative;
        overflow: hidden;
    }
    .btn-foto input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        cursor: inherit;
        display: block;
    }
    .span-jasa{
      margin-left: 10px;
      font-size: 15px;
    }
    .form-control {
      color: #727577 !important;
    }
  progress, progress[role] {
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  border: none;
  background-size: auto;
  height: 20px;
  width: 100%;
}

.bar::-webkit-progress-value {
  background: #0e8ce4;
}
  .offset-1 {
      margin-left: 2.333333% !important;
  }
  </style>
@endsection

@section('content_div')
  <div class="ticker css-1es387d">
    <div class="padding-ticker" style="font-size: 12px;">
      <p>Merchant harus konfirmasi resi setelah barang dikirimkan. Merchant harus menginputkan channel kurir yang dipakai dan nomor resi yang telah didapatkan.</p>
    </div>
  </div>
    <div class="row" style="padding-right: 15px;padding-left: 15px;">
      <form action="/acceptOrder" method="get" >
        <div class="row select">
          <div class="col-md-12">
            <div class="col-md-12 order_total_content" style="padding-top:20px;">

               <div class="form-group row">
                 <label for="inputEmail3" class="col-sm-12 col-form-label">Nomor Resi</label>
                 <div class="col-sm-12 offset-1">
                   <input type="hidden" name="status" value="1">
                   <input type="hidden" name="id" value="{{$delivery['id']}}">
                   <input type="text" name="resi" class="form-control" id="inputEmail3" placeholder="Nomor Resi">
                 </div>
               </div>
            </div>
          </div>
        </div>
        <div class="order_total_content text-md-right margin-top-15" style="padding-right: 15px;padding-left: 15px;">
          <div class="contact_form_button">
            <input type="submit" class="button cart_button_checkout" value="Simpan">
          </div>
        </div>
        </form>
      </div>
    </div>
@endsection
