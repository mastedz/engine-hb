@extends('frontend.layouts.layout_page')
@section('title')
hariBelanja - Detail Merchant
@endsection
@section('someCSS')

<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/product_responsive.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/qna.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/shop_styles.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<style type="text/css">
section{
    display: none;
}
.rvm-shipping {
    position: relative;
}
.rvm-shipping-input-holder {
    width: 85%;
    position: relative;
}
.rvm-shipping--district {
    float: left;
    width: 50%;
    padding: 8px 10px;
    border-top-left-radius: 3px;
    border-bottom-left-radius: 3px;
    border-color: #e0e0e0;
    border-style: solid;
    border-width: 1px 0 1px 1px;
    box-sizing: border-box;
}
.rvm-shipping-title {
    font-size: 11px;
    color: rgba(0,0,0,.54);
    font-weight: 600;
}
.rvm-shipping--postal {
    float: left;
    width: 30%;
    padding: 8px 10px;
    border-color: #e0e0e0;
    border-style: solid;
    border-width: 1px;
    position: relative;
    box-sizing: border-box;
}
.rvm-shipping--weight {
    float: left;
    width: 20%;
    height: auto;
    padding: 8px;
    background-color: #fafafa;
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px;
    border-color: #e0e0e0;
    border-style: solid;
    border-width: 1px 1px 1px 0;
    box-sizing: border-box;
}
.rvm-shipping-result {
    display: none;
    position: relative;
    border: 1px solid #e0e0e0;
    margin-top: 10px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
.rvm-shipping-result__btn-expand {
    display: none;
    text-align: -webkit-center;
    border-style: solid;
    border-width: 0 1px 1px;
    border-color: #e0e0e0;
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    padding: 8px 0;
    position: absolute;
    width: 100%;
    cursor: pointer;
    box-sizing: border-box;
    text-align: -moz-center;
}
.rvm-shipping-alert {
    margin-top: 6px;
    font-size: 12px;
    line-height: 1.42;
    color: #d0021b;
    z-index: 1;
}
.rvm-shipping--calc {
    position: absolute;
    top: 0;
    right: 0;
    width: 10%;
    line-height: 60px;
    color: #42b549;
    padding: 25px 20px;
    cursor: pointer;
    font-size: 17px;
}
* {
    -webkit-font-smoothing: antialiased;
}
.select2-hidden-accessible {
    border: 0!important;
    clip: rect(0 0 0 0)!important;
    height: 1px!important;
    margin: -1px!important;
    overflow: hidden!important;
    padding: 0!important;
    position: absolute!important;
    width: 1px!important;
}
.select2-container {
    font-size: 13px;
    box-sizing: border-box;
    display: inline-block;
    margin: 0;
    position: relative;
    vertical-align: middle;
    color: rgba(0,0,0,.54);
}
.svg_icon__carret_up {
    /* background-image: url(data:image/svg+xml;charset=utf8;base64,IDxzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMu…E4OC4xODggMCAuMzc1LjE4OEwxOSAyMC43NXEuMTg4LjE4OC4xODguMzc1eicvPjwvc3ZnPg==); */
}

.order_info {
    margin-top: 20px;
}
.product_price {
    font-size: 30px !important;
    font-weight: 500 !important;
    margin-top: 0px !important;
    color: #0e8ce4;
}
.button_container {
    margin-top: 20px !important;
    margin-bottom: 20px !important;
}
.single_product {
    padding-top: 50px !important;
    padding-bottom: 140px;
}
.product_image img{
  max-height: 120px !important;
}
.product_price {
  font-size: 16px !important;
  color: #DF3B3B !important;
}
.product_name {
  color: #000000 !important;
}
.product_item {
  padding-top: 0px !important;
}
.shop_content {
  padding-top: 0px !important;
}
.char_item{
  padding: 0px 0px 0px 5px !important;
}

</style>
@endsection
@section('content')
    <div class="single_product">
        <div class="container">
            <div class="row">
                <!-- Images -->
                <div class="col-lg-2 order-lg-1 order-2">
                    <ul class="image_list">
                      <li ><img src="https://ecs7.tokopedia.net/img/cache/100-square/default_picture_user/default_toped-12.jpg" alt="Foto Merchant">
                      <div class="rating_r product_rating"><i></i><i></i><i></i><i></i><i></i></div>
                      <b>Nama Merchant</b>
                      <p style="color: #696464; font-size:80%;">Deskripsi Merchant Lorem Ipsum Dolor Sit Amet </p></li>
                      <li><br><br>
                        <div class="sidebar_title">Categories</div>
              							<ul class="sidebar_categories">
              								<li><a href="#">Computers & Laptops</a></li>
              								<li><a href="#">Cameras & Photos</a></li>
              								<li><a href="#">Hardware</a></li>
              								<li><a href="#">Smartphones & Tablets</a></li>
              								<li><a href="#">TV & Audio</a></li>
              								<li><a href="#">Gadgets</a></li>
              								<li><a href="#">Car Electronics</a></li>
              								<li><a href="#">Video Games & Consoles</a></li>
              								<li><a href="#">Accessories</a></li>
              							</ul>
                      </li>
                    </ul>
                </div>

                <!-- Selected Image -->
                <div class="col-lg-10 order-lg-2 order-1">
                  <p>Product yang dijual</p>
                  <div class="shop_content">

                        <div class="product_grid_border"></div>
                        <div class="col-md-3 col-sm-6">
                        <!-- Product Item -->

                        <div class="product_item discount">
                            <div class="product_item">
                            <div class="product_border"></div>

                                <div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="https://ecs7.tokopedia.net/img/cache/100-square/default_picture_user/default_toped-12.jpg" alt="Product Image"></div>

                            <div class="product_content">
                                <div class="product_price">Rp.{{number_format(500000)}}
                                        <span>400.000</span>
                                </div>
                                <div class="product_name">Lorem Ipsum</div>
                            </div>

                            <div class="product_fav"><i class="fas fa-heart"></i></div>
                            <ul class="product_marks" style="display: block" >
                                <li class="product_mark product_discount">-100%</li>
                                <li class="product_mark product_new">new</li>
                            </ul>
                        </div>
                    </div>
                    </div>
                    <!-- Shop Page Navigation -->
                </div>
                <!-- Description -->
            </div>
        </div>
    </div>



@endsection
