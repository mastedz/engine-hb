@section('title')
hariBelanja - Profile
@endsection

@extends('frontend.layout_page')

@section('someCSS')
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/product_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/contact_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/contact_responsive.css')}}">

  <style>
    .cart_item_name {
      margin-left: 0%;
    }
    .alamat_name {
      font-size: 12px;
      font-weight: 500;
      color: rgba(0, 0, 0, 0.82);
    }
    .alamat_location {
      font-size: 12px;
      font-weight: 400;
      color: rgba(0,0,0,0.5);
    }
    .telp_padding{
      padding-top: 15px;
    }
    .float_right{
      float: right;
    }
    .cart_padding {
        padding-right: 15px;
    }
    .width_100{
        width: 100%;
    }
    .margin_0{
      margin-top: 0px;
    }
    .cart_button_checkout{
      font-size: 14px;
    }
    .cart_item_image {
      width: 65px;
      height: 65px;
      margin-right: 10px;
    }
    .price{
      font-size: 15px;
      color: #0e8ce4;
    }
    .qty{
      font-size: 12px;
      font-weight: 400;
    }
    .name{
      font-size: 13px;
    }
    .no_padding{
      padding-left: 0px;
      padding-right: 0px;
      padding-top: 10px;
    }
    .list-dropdown{
      padding-left: 5px;
      padding-right: 5px;
    }
    .menu-dropdown{
      font-size: 12px;
    }
    .padding-top-10{
      padding-top: 10px;
    }
    .order_total_title{
      line-height: 25px;
    }
    .order_total_amount{
      line-height: 25px;
    }
    .margin-left-0{
        margin-left: 0px;
    }
    .padding-top-30{
      padding-top: 30px;
    }
    .margin-top-15{
      margin-top: 15px;
    }

    .shop_sidebar {
        width: auto;
        transform: translateX(0px);
    }
    .sidebar_categories {
        margin-top: 0px;
    }
    .list-name{
      font-weight: 500;
      font-size: 15px;
      color: #0e8ce4;\;
    }
    .margin-top-botton-10{
      margin-top: 10px;
      margin-bottom: 10px;
    }
    .cart_item_title{
      font-size: 16px;
      color: rgb(14, 140, 228);
    }
    .cart_items {
      margin-top: 39px;
    }
    .name-title {
      color: #727577;
      margin-bottom: 10px;
    }
    .btn-foto{
      background: #b5b6b7ed;
    }
    .desc-foto{
      font-size: 11px;
      line-height: 1.4;
    }
  </style>
@endsection

@section('someJS')
  <script src="{{asset('js/product_custom.js')}}"></script>
@endsection

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="cart_container">
            <div class="row col-md-12">
              <div class="col-md-3">
                <div class="cart_items">
                  <ul class="cart_list">
                    <li class="cart_item clearfix">
                      <div class="d-flex flex-md-row flex-column justify-content-between">
                        <div class="cart_item_name cart_info_col width_100 padding-top-10">
                          <div class="shop_sidebar">
                						<div class="sidebar_section">
                              <p class="list-name">Kotak Masuk</p>
                							<ul class="sidebar_categories">
                								<li><a href="#">Diskusi</a></li>
                								<li><a href="#">Ulasan</a></li>
                							</ul>
                              <p class="list-name margin-top-botton-10">Pembelian</p>
                							<ul class="sidebar_categories">
                								<li><a href="#">Menunggu Pembayaran</a></li>
                								<li><a href="#">Daftar Transaksi</a></li>
                							</ul>
                              <p class="list-name margin-top-botton-10">Profile Saya</p>
                							<ul class="sidebar_categories">
                								<li><a href="#">Biodata Diri</a></li>
                  							<li><a href="#">Alamat</a></li>
                							</ul>
                						</div>
                					</div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-md-9">
                <div class="cart_items">
                  <h5 class="cart_item_title"><i class="far fa-user"></i> Rani Dewi Aristhania</h5>
                  <ul class="cart_list">
                    <li class="cart_item clearfix cart_padding">
                      <div class="row" style="padding-right: 15px;padding-left: 15px;">
                        <div class="col-md-12 order_total_content">
                          <div class="list-name name-title">&nbsp;Ubah Biodata Diri</div>
                        </div>
                        <div class="col-md-4">
                          <ul class="cart_list" style="background:#e0e0e082;">
                            <li class="cart_item clearfix cart_padding">
                              <div class="order_total_content">
                                <div>
                                  <img src="https://www.w3schools.com/html/pic_trulli.jpg" alt="HTML5 Icon" style="width: 100%;height:128px;">
                                </div>
                                <div class="cart_buttons margin-top-15">
                                  <button type="button" class="button cart_button_checkout width_100 btn-foto">Pilih Foto</button>
                                </div>
                                <div>
                                  <p class="desc-foto">Besar file: maksimum 10.000.000 bytes (10 Megabytes) <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-8">
                          <form action="#" id="contact_form">
              							<div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
              								<input type="text" id="contact_form_name" class="contact_form_name input_field width_100" placeholder="Isi Nama" required="required" data-error="Name is required.">
              							</div>
                            <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
              								<input type="text" id="contact_form_email" class="contact_form_email input_field width_100" placeholder="Isi Email" required="required" data-error="Email is required." value="anggrekaristhania@gmail.com" disabled>
              							</div>
                            <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
              								<input type="text" id="contact_form_phone" class="contact_form_phone input_field width_100" placeholder="Isi Nomor Telpon">
              							</div>
              						</form>
                        </div>
                      </div>
                      <div class="order_total_content text-md-right margin-top-15" style="padding-right: 15px;padding-left: 15px;">
                        <div class="contact_form_button">
                          <button type="button" class="button cart_button_checkout float-left" style="background:#b5b6b785;"><i class="fas fa-key"></i> Ubah Password</button>
                          <button type="button" class="button cart_button_checkout">Simpan</button>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
