@extends('frontend.layouts.layout_page')
@section('title')
hariBelanja - {{$categories->title}}
@endsection
@section('someCSS')
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/shop_styles.css">

<link rel="stylesheet" type="text/css" href="{{asset('')}}plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<style type="text/css">
    .right-inner-addon {
  position: relative;
}

.right-inner-addon input {
  padding-right: 30px;
}

.right-inner-addon i {
    z-index: 99;
  position: absolute;
  left: 0px;
  padding: 10px 12px;
  pointer-events: none;
}
.product_image img{
  max-height: 120px;
}
</style>
@endsection
@section('content')
<div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{asset('')}}images/shop_background.jpg"></div>
        <div class="home_overlay"></div>

        <div class="home_content d-flex flex-column align-items-center justify-content-center">
            <h2 class="home_title">{{$categories->title}}</h2>
        </div>
    </div>
    <div class="shop">
        <div class="container">
            <div class="row">{{--
                <div class="col-lg-3">

                    <!-- Shop Sidebar -->

                        <div class="sidebar_section filter_by_section">
                            <form method="get" action="{{url('/filter')}}">
                            <div class="sidebar_title">Filter </div>
                            <div class="sidebar_subtitle">Harga</div>

                              <div class="right-inner-addon">
                                <i class="icon-search">Rp</i>
                                <input type="text" name="start" style="text-align: right;" class="form-control col-md-8" placeholder="Dari" />

                            </div>
                            <br>
                            <div class="right-inner-addon">
                                <i class="icon-search">Rp</i>
                                <input type="text" name="end" style="text-align: right;" class="form-control col-md-8" placeholder="Sampai" />

                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Filter</button>
                            </form>
                        </div>

                    </div>

                </div>
                --}}
                <div class="col-lg-12">

                    <!-- Shop Content -->
                    {{--
                    <div class="shop_content">
                        <div class="shop_bar clearfix">
                            <div class="shop_product_count"><span>{{$products->total()}}</span> products found</div>
                            <div class="shop_sorting">
                                <span>Urutkan :</span>
                                <ul>
                                    <li>
                                        <span class="sorting_text">Harga<i class="fas fa-chevron-down"></span></i>
                                        <ul>
                                            <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Harga Tertinggi</li>
                                            <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Harga Terendah</li>
                                       <!--      <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>price</li> -->
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        --}}
                        <div class="product_grid">
                            <div class="product_grid_border"></div>
                            <div class="row">
                            @forelse($products as $product)
                            @php
                            if(!is_null($product->discount)){
                            $price = $product->price - ($product->price * ($product->discount / 100));
                            }else{
                            $price = $product->price;
                            }
                            $pict = json_decode($product->image);
                            @endphp
                             <div class="col-md-3 col-sm-6">
                            <!-- Product Item -->
                            @if(!is_null($product->discount))
                            <div class="product_item discount">
                                @else
                                <div class="product_item ">
                                    @endif
                                <div class="product_border"></div>
                                <a href="{{route('frontend.detail', $product->slug)}}" tabindex="0">
                                    @if($product->is_artist == 1)
                                    <div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{$product->image}}" alt=""></div>
                                    @else
                                    <div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{asset('')}}images/{{$pict[0]}}" alt=""></div>
                                    @endif
                                </a>
                                <div class="product_content">
                                    <div class="product_price">Rp.{{number_format($price)}}
                                          @if($product->discount !== 0)
                                            <span>{{$product->price}}</span>
                                        @endif
                                    </div>
                                    <div class="product_name"><div><a href="{{route('frontend.detail', $product->slug)}}" tabindex="0">{{mb_strimwidth($product->name, 0, 25, "...")}}</a></div></div>
                                    <div class="product_name text-left"><div><p><i class="fas fa-map-marker-alt"></i> {{ App\Helper::non()->getCity()[$product->shop->shop_city-1]['type'] }} {{ App\Helper::non()->getCity()[$product->shop->shop_city-1]['city_name'] }}</p></div></div>
                                </div>

                                <div class="product_fav"><i class="fas fa-heart"></i></div>
                                <ul class="product_marks" style="display: block" >
                                    @if($product->discount !== 0)
                                    <li class="product_mark
                                     product_discount">-{{$product->discount}}%</li>
                                    @endif
                                    <li class="product_mark product_new">new</li>

                                </ul>


                            </div>
                        </div>
                            @empty
                            @endforelse


                        </div>
                        </div>

                        <!-- Shop Page Navigation -->

                        {{$products->links()}}

                    </div>


                </div>

            </div>
        </div>

    </div>
    <div class="brands">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="brands_slider_container">
                        <h2>{{$categories->meta_title}}</h2>
                        <p>{!!$categories->description!!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('someJS')
<script type="text/javascript">


        var $form = $( "#form" );
        var $input = $form.find( "input" );

        $input.on( "keyup", function( event ) {


            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
            if ( selection !== '' ) {
                return;
            }

            // When the arrow keys are pressed, abort.
            if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                return;
            }


            var $this = $( this );

            // Get the value.
            var input = $this.val();

            var input = input.replace(/[\D\s\._\-]+/g, "");
                    input = input ? parseInt( input, 10 ) : 0;

                    $this.val( function() {
                        return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
                    } );
        } );

        /**
         * ==================================
         * When Form Submitted
         * ==================================
         */
        $form.on( "submit", function( event ) {

            var $this = $( this );
            var arr = $this.serializeArray();

            for (var i = 0; i < arr.length; i++) {
                    arr[i].value = arr[i].value.replace(/[($)\s\._\-]+/g, ''); // Sanitize the values.
            };

            console.log( arr );

            event.preventDefault();
        });




</script>
@endsection
