<header class="header">

  <!-- Top Bar -->

  <div class="top_bar" style="height: 80px !important;">
    <div class="container">
      <div class="row">
        <div class="col d-flex flex-row">

          <div class="top_bar_content" style="padding-top:17px;width:50%">
            <div class="top_bar_menu" style="width: 39%;">
              <div><a href="">Dowload hariBELanja App <i class="fab fa-google-play"></i></a></div>
              <!-- <div><span>Follow Us on <a href="#"><i class="fab fa-facebook-f"></i></a><a href="#"> <i class="fab fa-instagram"></i></a></span></div> -->
              <div class="row" style="margin-top: 5px;">
                <div class="col-md-6" style="padding-right: 0px;">
                  <p>Follow Us on</p>
                </div>
                <div class="col-md-6" style="padding-left: 0px;">
                  <div class="footer_social" style="margin-top:0px">
                    <ul>
                      <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                      <li><a href="https://www.instagram.com/hari.belanja/"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="top_bar_content ml-auto">
            <div class="top_bar_menu">
								<ul class="standard_dropdown top_bar_dropdown">
									<li>
									  <a href="#"><i class="fas fa-headset"></i> Help Center</a>
                  </li>
                </ul>
            </div>
          <!-- </div>? -->
            <!--<div class="top_bar_menu">
              <ul class="standard_dropdown top_bar_dropdown">
                <li>
                  <a href="#">+ Notifikasi<i class="fas fa-chevron-down"></i></a>
                </li>
              </ul>
            </div>!-->
              @if(\Auth::user())
            @php
            $user = Auth::user();
            $orders['waiting_payment'] = \App\Orders::whereHas('orderItems', function ($query) {
                      $query->where('delivery_status', 0);

                    })->with('orderItems')->where('payment_status', '=', 'pending')->where('user_id', Auth::user()->id)->get();

            $orders['packed'] = \App\Orders::whereHas('orderItems', function ($query) {
                      $query->where('delivery_status', 1);

                    })->with('orderItems')->where('user_id', Auth::user()->id)->get();

            $orders['on_delivery'] = \App\Orders::whereHas('orderItems', function ($query) {
                      $query->where('delivery_status', 2);

                    })->with('orderItems')->where('user_id', Auth::user()->id)->get();

            $orders['delivered'] = \App\Orders::whereHas('orderItems', function ($query) {
                      $query->where('delivery_status', 3);

                    })->with('orderItems')->where('user_id', Auth::user()->id)->get();


                $notif = count($orders['delivered']) + count($orders['on_delivery']) + count(  $orders['packed']) + count($orders['waiting_payment']);
                @endphp
            <div class="top_bar_menu">
								<ul class="standard_dropdown top_bar_dropdown">
									<li>
									  <a href="#">+ Notifikasi <span class="badge badge-primary">@if($notif){{$notif}}@endif</span><i class="fas fa-chevron-down"></i></a>
										<ul>
											<li><a href="pembelian/waiting_payment">Menunggu Pembayaran <span class="badge badge-primary">@if(count($orders['waiting_payment'])) {{count($orders['waiting_payment'])}}@endif</span></a> </li>
                      @if(App\Shop::whereUserId(Auth::user()->id)->first())
                        @if(App\Shop::whereUserId(Auth::user()->id)->first()->status == '1')
											<li><a href="{{url('list/sales/confirmation')}}">Menunggu Konfirmasi <span class="badge badge-primary"></span></a></li>
                        @endif
                      @endif
											<li><a href="{{url('/pembelian/packed')}}">Pesanan Diproses <span class="badge badge-primary">@if(count($orders['packed'])) {{count($orders['packed'])}}@endif</span></a></li>
                      <li><a href="{{url('/pembelian/on_delivery')}}">Sedang Dikirim <span class="badge badge-primary">@if(count($orders['on_delivery'])) {{count($orders['on_delivery'])}}@endif</span></a></li>
                      <li><a href="{{url('/pembelian/delivered')}}">Sampai Tujuan <span class="badge badge-primary">@if(count($orders['delivered'])) {{count($orders['delivered'])}}@endif</span></a></li>
                      <li><a href="#">Diskusi Product <span class="badge badge-primary">1</span></a></li>
										</ul>
									</li>


								</ul>
							</div>
                @endif
            <div class="top_bar_user" style="padding-top: 9px;">
              @if(\Auth::user())
                  <div class="user_icon main-nav"><img src="{{asset('')}}images/user.svg" alt=""></div>
                  <div><a href="{{ url('/biodata') }}">{{Auth::user()->name}}</a></div>
                  <div><a href="{{ url('/logout') }}">Logout</a></div>
              @else
                  <div class="user_icon main-nav"><img src="{{asset('')}}images/user.svg" alt=""></div>
                  <div><a class="cd-signup" href="#0">Register</a></div>
                  <div><a class="cd-signin" href="#0">Sign in</a></div>
                  <!--<div><a class="cd-signup" href="#0">Help Center</a></div>-->
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Header Main -->

  <div class="header_main">
    <div class="container">
      <div class="row">

        <!-- Logo -->
        <div class="col-lg-4 col-sm-3 col-3 order-1">
          <div class="logo_container">
            <div class="logo"><a href="/"><img width="250px"src="{{asset('')}}images/logo.jpg"></a></div>
          </div>
        </div>

        <!-- Search -->
        <div class="col-lg-5 col-12 order-lg-2 order-3 text-lg-left text-right">
          <div class="header_search">
            <div class="header_search_content">
              <div class="header_search_form_container">
                <form action="{{route('frontend.search')}}" method="post" class="header_search_form clearfix">
                  <input type="search" name="q" required="required" class="header_search_input" placeholder="Search for products...">

                   {{ csrf_field() }}


                  <button type="submit" class="header_search_button trans_300" value="Submit"><img src="{{asset('')}}images/search.png" alt=""></button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Wishlist -->
        <div class="col-lg-2 col-8 order-lg-3 order-2 text-lg-left text-right">
          <div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
            <!-- <div class="wishlist d-flex flex-row align-items-center justify-content-end">
              <div class="wishlist_icon"><img src="images/heart.png" alt=""></div>
              <div class="wishlist_content">
                <div class="wishlist_text"><a href="#">Wishlist</a></div>
                <div class="wishlist_count">15</div>
              </div>
            </div> -->

            <!-- Cart -->
            <div class="cart">
              <div class="cart_container d-flex flex-row align-items-center justify-content-end">
                <div class="cart_icon">
                  <img src="{{asset('')}}images/cart.png" alt="">
                  <div class="cart_count"><span>{{\Cart::count()}}</span></div>
                </div>
                <div class="cart_content">
                  <div class="cart_text">
                    @if(Auth::user())
                    <a @if(\Cart::count()>0) href="/cart" @else href="#" @endif >Cart</a>
                    @else
                    <a class="cd-signin" href="#">Cart</a>
                    @endif

                  </div>
                  <div class="cart_price"><span>Rp </span>{{ str_replace(",",".",substr(\Cart::total(),0,-3)) }}</div>
                </div>
                <div class="cart_content">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-1 col-1 order-lg-3 order-2 text-lg-left text-right" style="padding-top: 5%">
          <button style="border-radius:12px;background:#0E8CE4;width:100px">Indonesia</button>
          <button style="border-radius:12px;width:100px">English</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Main Navigation -->

  <nav class="main_nav">
    <div class="container">
      <div class="row">
        <div class="col">

          <div class="main_nav_content d-flex flex-row">

            <!-- Categories Menu -->

            <div class="cat_menu_container">
              <div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
                <div class="cat_burger"><span></span><span></span><span></span></div>
                <div class="cat_menu_text">mitra</div>
              </div>

              <ul class="cat_menu">
                @if(App\Category::count())
                        @foreach(App\Category::where('id_parent', "")->with('child')->limit(20)->get() as $category)

                          <li class="hassubs">
                              <a href="{{route('collection.subproduct',$category->slug)}}">{{$category->title}}<i @if($category->id_parent != '') class="fas fa-chevron-right" @endif></i></a>
                              <ul >

                                  @foreach($category->child as $child)
                                    <li><a href="{{route('collection.subproduct',$child->slug)}}">{{$child->title}}</a></li>
                                  @endforeach
                              </ul>
                          </li>
                        @endforeach
                @endif
              </ul>
            </div>

            <!-- Main Nav Menu -->

            <div class="main_nav_menu ml-left">
              <ul class="standard_dropdown main_nav_dropdown">
                <li><a href="#" style="@yield('pakaian')">Pakaian<i class="fas fa-chevron-down"></i></a></li>
                <li><a href="#" style="@yield('pertanian')">Pertanian<i class="fas fa-chevron-down"></i></a></li>
                <!-- <li class="hassubs">
                  <a href="#">Pakaian<i class="fas fa-chevron-down"></i></a>
                  <ul>
                    <li>
                      <a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
                      <ul>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                      </ul>
                    </li>
                    <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                  </ul>
                </li> -->
                <!--<li class="hassubs">
                  <a href="#">Featured Brands<i class="fas fa-chevron-down"></i></a>
                  <ul>
                    <li>
                      <a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
                      <ul>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                      </ul>
                    </li>
                    <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                  </ul>
                </li>
                <li class="hassubs">
                  <a href="#">Pages<i class="fas fa-chevron-down"></i></a>
                  <ul>
                    <li><a href="shop.html">Shop<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="product.html">Product<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="blog.html">Blog<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="blog_single.html">Blog Post<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="regular.html">Regular Post<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="cart.html">Cart<i class="fas fa-chevron-down"></i></a></li>
                    <li><a href="contact.html">Contact<i class="fas fa-chevron-down"></i></a></li>
                  </ul>
                </li>-->
                <li><a href="{{ url('ivoucher') }}" style="@yield('ivoucher')">i-Voucher<i class="fas fa-chevron-down"></i></a></li>
                <li><a href="#" style="@yield('kesenian')">Kesenian<i class="fas fa-chevron-down"></i></a></li>
                <!-- <li><a href="{{ asset('/storage/doc/Tata Cara Penggunaan Haribelanja.pdf') }}" download="Tata Cara Penggunaan Haribelanja">Dokumentasi<i class="fas fa-chevron-down"></i></a></li> -->
              </ul>
            </div>

            <!-- Menu Trigger -->

            <div class="menu_trigger_container ml-auto">
              <div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
                <div class="menu_burger">
                  <div class="menu_trigger_text">menu</div>
                  <div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </nav>

  <!-- Menu -->

  <div class="page_menu">
    <div class="container">
      <div class="row">
        <div class="col">

          <div class="page_menu_content">

            <!--<div class="page_menu_search">
              <form action="#">
                <input type="search" required="required" class="page_menu_search_input" placeholder="Search for products...">
              </form>
            </div>-->
            <ul class="page_menu_nav">
              <li class="page_menu_item"><a class="cd-signin" href="#0">Sign in<i class="fa fa-angle-down"></i></a></li>
              <li class="page_menu_item"><a class="cd-signup" href="#0">Register<i class="fa fa-angle-down"></i></a></li>
              <!--<li class="page_menu_item has-children">
                <a href="#">Language<i class="fa fa-angle-down"></i></a>
                <ul class="page_menu_selection">
                  <li><a href="#">English<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Italian<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Spanish<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Japanese<i class="fa fa-angle-down"></i></a></li>
                </ul>
              </li>
              <li class="page_menu_item has-children">
                <a href="#">Currency<i class="fa fa-angle-down"></i></a>
                <ul class="page_menu_selection">
                  <li><a href="#">US Dollar<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">EUR Euro<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">GBP British Pound<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">JPY Japanese Yen<i class="fa fa-angle-down"></i></a></li>
                </ul>
              </li>
              <li class="page_menu_item">
                <a href="#">Home<i class="fa fa-angle-down"></i></a>
              </li>
              <li class="page_menu_item has-children">
                <a href="#">Super Deals<i class="fa fa-angle-down"></i></a>
                <ul class="page_menu_selection">
                  <li><a href="#">Super Deals<i class="fa fa-angle-down"></i></a></li>
                  <li class="page_menu_item has-children">
                    <a href="#">Menu Item<i class="fa fa-angle-down"></i></a>
                    <ul class="page_menu_selection">
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                      <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                    </ul>
                  </li>
                  <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                </ul>
              </li>
              <li class="page_menu_item has-children">
                <a href="#">Featured Brands<i class="fa fa-angle-down"></i></a>
                <ul class="page_menu_selection">
                  <li><a href="#">Featured Brands<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                </ul>
              </li>
              <li class="page_menu_item has-children">
                <a href="#">Trending Styles<i class="fa fa-angle-down"></i></a>
                <ul class="page_menu_selection">
                  <li><a href="#">Trending Styles<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                  <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                </ul>
              </li>
              <li class="page_menu_item"><a href="blog.html">blog<i class="fa fa-angle-down"></i></a></li>
              <li class="page_menu_item"><a href="contact.html">contact<i class="fa fa-angle-down"></i></a></li>-->
            </ul>

            <div class="menu_contact">
              <div class="menu_contact_item"><div class="menu_contact_icon"><img src="images/phone_white.png" alt=""></div>082245326737</div>
              <div class="menu_contact_item"><div class="menu_contact_icon"><img src="images/mail_white.png" alt=""></div><a href="mailto:fastsales@gmail.com">haribelanja@gmail.com</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</header>
