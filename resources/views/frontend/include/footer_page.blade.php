<footer class="footer">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 footer_col">
        <div class="footer_column footer_contact">
          <div class="logo_container">
            <div class="logo"><a href="#">hariBELanja</a></div>
            <div>
              <img src="images/silva_logo.jpg" alt="" style="height: 150px;width: 150px;">
            </div>
          </div>
          <div class="footer_title">Punya Pertanyaan? Hubungi</div>
          @php
          $footer = json_decode(\App\Settings::where('option', 'footer_contact')->first()->value);

          @endphp

          <div class="footer_phone">{{$footer->contact}}</div>
          <div class="footer_contact_text">
            <p>{{$footer->address}}</p>

          </div>
          <!-- <div class="footer_social">
            <ul>
              <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="#"><i class="fab fa-twitter"></i></a></li>
              <li><a href="#"><i class="fab fa-youtube"></i></a></li>
              <li><a href="#"><i class="fab fa-google"></i></a></li>
              <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
            </ul>
          </div> -->
        </div>
      </div>

      <div class="col-lg-2 offset-lg-1">
        <div class="footer_column">
          <div class="footer_title">Tentang hariBelanja</div>
          <ul class="footer_list">
            <li><a href="#">Pakaian</a></li>
            <li><a href="#">Pertanian</a></li>
            <li><a href="{{ url('ivoucher') }}">i-Voucher</a></li>
            <li><a href="#">Kesenian</a></li>
            <!-- <li><a href="#">Keuntungan Jualan</a></li> -->
          </ul>
        </div>
      </div>

      <div class="col-lg-2">
        <div class="footer_column">
          <div class="footer_title">Customer Care</div>
          <ul class="footer_list">
            <li><a href="{{ asset('/storage/doc/Tata Cara Penggunaan Haribelanja.pdf') }}">Dokumentasi</a></li>
            <li><a href="{{ url('tracking') }}">Tracking Pesanan</a></li>
            <li><a href="#">FAQs</a></li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</footer>
