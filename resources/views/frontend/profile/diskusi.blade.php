@section('title')
hariBelanja - Diskusi
@endsection

@section('diskusi')
hover-active
@endsection

@section('css')

<style>

body {
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  font-weight: 300;
  color: #888;
  line-height: 30px;
  text-align: center;
}

.char_item {
    padding-left: 33% !important;
}

@media only screen and (max-width: 991px){
  .char_item {
      padding-left: 33% !important;
  }
}

.char_content{
  width: 100%;
}
.char_item{
  padding-left: 0px !important;
  text-align: center;
}

</style>
@endsection

@section('someJS')
  <script type="text/javascript">
  $( document ).ready(function() {
    $('.details').hide();
  });
    $(function(){
      $('#metode').hide();

      $('#diskusi-saya').off().on('click', function(){
        window.location.href ="{{ url('/discussion/out') }}";
      });
      $('#diskusi-masuk').off().on('click', function(){
        window.location.href ="{{ url('/discussion/in') }}";
      });
    });
  </script>
@endsection

@extends('frontend.layouts.layout_profile')

@section('content_div')


    <div class="characteristics">
      <div class="container">
        <div class="row">

          <!-- Char. Item -->
          <div class="col-lg-6 col-md-6 char_col" id="diskusi-saya">

            <div class="char_item d-flex flex-row align-items-center justify-content-start">
              <div class="char_content">
                <div class="char_title">Diskusi Saya <i class="fas fa-sign-out-alt"></i><span class="badge badge-info" id="countConfirmation"></span></div>
                <p style="font-size:12px;margin-bottom: 0px;">Diskusi pada merchant lain</p>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 char_col" id="diskusi-masuk">

            <div class="char_item d-flex flex-row align-items-center justify-content-start">
              <div class="char_content">
                <div class="char_title">Diskusi Produk Saya <i class="far fa-comment"></i></div>
                <p style="font-size:12px;margin-bottom: 0px;">Diskusi dari customer</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  <!-- </div>? -->
@endsection
