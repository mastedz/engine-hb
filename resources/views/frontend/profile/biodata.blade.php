@section('title')
hariBelanja - Biodata
@endsection

@section('biodata')
hover-active
@endsection

@extends('frontend.layouts.layout_profile')

@section('content_div')

  <div class="row" style="padding-right: 15px;padding-left: 15px;">
    <div class="col-md-12 order_total_content" id="title-biodata">
      <div class="list-name name-title">&nbsp;Ubah Biodata Diri</div>
    </div>
    <!-- <form role="form" action="{{ url('/update/profile') }}" method="post" enctype="multipart/form-data" class="f1"> -->
    <div class="col-md-4" id="input-image">

      <form role="form" action="{{ url('/update/profile') }}" method="post" enctype="multipart/form-data" class="f1">

        {{ csrf_field() }}
      <ul class="cart_list" style="background:#e0e0e082;">
        <li class="cart_item clearfix cart_padding">
          <div class="order_total_content">
            @if(!empty($user->image))

              <img src="{{ asset('/storage/images/'.$user->image) }}" alt="HTML5 Icon" style="width: 100%;height:auto;">

            @else
              <div id="preview1"  style="width: 100%;height:128px;"></div>
            @endif
            <div class="cart_buttons margin-top-15">
              <button type="button" class="button cart_button_checkout width_100 btn-foto" onclick="document.getElementById('getFile').click()">Pilih Foto</button>
              <input name="image" class="upload" data-id="1" type='file' id="getFile" style="display:none">
            </div>
            <div>
              <p class="desc-foto">Besar file maksimum: 10 Megabytes <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col-md-8"  id="input-data">
      <!-- <form action="#" id="contact_form"> -->
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <input type="text" id="contact_form_name" class="contact_form_name input_field width_100" placeholder="Isi Nama" required="required" data-error="Name is required." value="{{ $user->name }}" name="name">
        </div>
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <input type="text" id="contact_form_email" class="contact_form_email input_field width_100" placeholder="Isi Email" required="required" data-error="Email is required." value="{{ $user->email }}" disabled name="email">
        </div>
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <input type="text" id="contact_form_phone" class="contact_form_phone input_field width_100" placeholder="Isi Nomor Telpon" name="phone" value="{{ $user->phone }}">
        </div>
      <!-- </form> -->
    </div>
  </div>
  <div class="order_total_content text-md-right margin-top-15" style="padding-right: 15px;padding-left: 15px;"  id="btn-submit">
    <div class="contact_form_button">
      <button id="btn-change-pass" type="button" class="button cart_button_checkout float-left" style="background:#b5b6b785;color:#0e8ce4;"><i class="fas fa-key"></i> Ubah Password</button>
      <button type="submit" class="button cart_button_checkout">Simpan</button>
    </div>
  </div>
</form>

<div class="col-md-12 order_total_content" id="title-change-pass">
  <div class="list-name name-title">&nbsp;Ubah Password</div>
</div>
<div class="col-md-12" id="input-change-pass">
  <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
    <input type="password" id="pass-old" class="contact_form_name input_field width_100" placeholder="Password Lama" required="required" data-error="Data is required." name="pass-old">
  </div>
  <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
    <input type="password" id="pass-new" class="contact_form_email input_field width_100" placeholder="Password Baru" required="required" data-error="Data is required." name="pass-new">
  </div>
</div>
<div class="order_total_content text-md-right margin-top-15" style="padding-right: 15px;padding-left: 15px;"  id="btn-reset">
  <div class="contact_form_button">
    <button id="btn-reset-pass" type="submit" class="button cart_button_checkout">Simpan</button>
  </div>
</div>
@endsection
@section('someJS')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.min.js"></script>
<script type="text/javascript">
 $(document).ready(function(){
    $('#input-change-pass').hide();
    $('#btn-reset').hide();
    $('#title-change-pass').hide();

    $(".upload").on('change',function(){
      console.log($(this).data('id'));
      var filereader = new FileReader();
      var $img=jQuery.parseHTML("<img style='width: 100%;height:128px;' src=''>");
      filereader.onload = function(){
          $img[0].src=this.result;
      };
      filereader.readAsDataURL(this.files[0]);
       $("#preview" + $(this).data("id")).empty();
      $("#preview" + $(this).data("id")).append($img);
    });
  });

  $('#btn-change-pass').click(function() {
      $('#input-image').hide();
      $('#input-data').hide();
      $('#btn-submit').hide();
      $('#title-biodata').hide();
      $('#btn-reset').show();
      $('#title-change-pass').show();
      $('#input-change-pass').show();
  });

  $('#btn-reset').click(function() {
    var lama = $('#pass-old').val();
    var baru = $('#pass-new').val();

    // var lama = "{{ Illuminate\Support\Facades\Hash::make("+$('#pass-old').val()+") }}";
    // var baru = "{{ Illuminate\Support\Facades\Hash::make("+$('#pass-new').val()+") }}";;
    if(lama != '' && baru != ''){
      $.ajax({
          url: "{{ url('reset/password') }}",
          type: 'POST',
          data: {
              lama    : lama,
              baru    : baru,
              _method:"post",
          _token : '{{ csrf_token() }}'
          }
        }).done(function(data){
          console.log(data);
          if(data == '00'){
            alert('Password berhasil diganti!');
          }else{
            alert('Password gagal diganti!');
          }
          location.reload();
        });
    }else{
      alert('Data tidak lengkap!');
    }
  });
</script>
@endsection
