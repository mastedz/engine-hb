@section('title')
hariBelanja - Alamat
@endsection

@section('alamat')
hover-active
@endsection

@extends('frontend.layouts.layout_profile')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <style>
    /*
      loader
    */

    #loader {
          bottom: 0;
          height: 175px;
          left: 0;
          margin: auto;
          position: absolute;
          right: 0;
          top: 0;
          width: 175px;
      }
      #loader .dot {
          bottom: 0;
          height: 100%;
          left: 0;
          margin: auto;
          position: absolute;
          right: 0;
          top: 0;
          width: 87.5px;
      }
      #loader .dot::before {
          border-radius: 100%;
          content: "";
          height: 87.5px;
          left: 0;
          position: absolute;
          right: 0;
          top: 0;
          transform: scale(0);
          width: 87.5px;
      }
      #loader .dot:nth-child(7n+1) {
          transform: rotate(45deg);
      }
      #loader .dot:nth-child(7n+1)::before {
          animation: 0.8s linear 0.1s normal none infinite running load;
          background: #00ff80 none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+2) {
          transform: rotate(90deg);
      }
      #loader .dot:nth-child(7n+2)::before {
          animation: 0.8s linear 0.2s normal none infinite running load;
          background: #00ffea none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+3) {
          transform: rotate(135deg);
      }
      #loader .dot:nth-child(7n+3)::before {
          animation: 0.8s linear 0.3s normal none infinite running load;
          background: #00aaff none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+4) {
          transform: rotate(180deg);
      }
      #loader .dot:nth-child(7n+4)::before {
          animation: 0.8s linear 0.4s normal none infinite running load;
          background: #0040ff none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+5) {
          transform: rotate(225deg);
      }
      #loader .dot:nth-child(7n+5)::before {
          animation: 0.8s linear 0.5s normal none infinite running load;
          background: #2a00ff none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+6) {
          transform: rotate(270deg);
      }
      #loader .dot:nth-child(7n+6)::before {
          animation: 0.8s linear 0.6s normal none infinite running load;
          background: #9500ff none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+7) {
          transform: rotate(315deg);
      }
      #loader .dot:nth-child(7n+7)::before {
          animation: 0.8s linear 0.7s normal none infinite running load;
          background: magenta none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+8) {
          transform: rotate(360deg);
      }
      #loader .dot:nth-child(7n+8)::before {
          animation: 0.8s linear 0.8s normal none infinite running load;
          background: #ff0095 none repeat scroll 0 0;
      }
      #loader .lading {
          background-image: url("");
          background-position: 50% 50%;
          background-repeat: no-repeat;
          bottom: -40px;
          height: 20px;
          left: 0;
          position: absolute;
          right: 0;
          width: 180px;
      }
      @keyframes load {
      100% {
          opacity: 0;
          transform: scale(1);
      }
      }
      @keyframes load {
      100% {
          opacity: 0;
          transform: scale(1);
      }
      }
    </style>
    <style>
      /* The container */
      .container-radio {
      display: block;
      position: relative;
      padding-left: 0px;
      margin-bottom: 12px;
      cursor: pointer;
      font-size: 22px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      }

      /* Hide the browser's default radio button */
      .container-radio input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      }

      /* Create a custom radio button */
      .checkmark {
      position: absolute;
      top: 0;
      left: 0;
      height: 15px;
      width: 15px;
      background-color: #eee;
      border-radius: 50%;
      }

      /* On mouse-over, add a grey background color */
      .container-radio:hover input ~ .checkmark {
      background-color: #ccc;
      }

      /* When the radio button is checked, add a blue background */
      .container-radio input:checked ~ .checkmark {
      background-color: #2196F3;
      }

      /* Create the indicator (the dot/circle - hidden when not checked) */
      .checkmark:after {
      content: "";
      position: absolute;
      display: none;
      }

      /* Show the indicator (dot/circle) when checked */
      .container-radio input:checked ~ .checkmark:after {
      display: block;
      }

      /* Style the indicator (dot/circle) */
      .container-radio .checkmark:after {
        top: 4px;
        left: 4px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
      }
    </style>

  <style>
    .urutkan{
      margin-right: 20px;
      padding-top: 14px;
    }
    .btn-urutkan{
      margin: 2px 0px 3px 0px;
      background: #e6e6e67a;
      border-radius: 5px !important;
      color: #7f8484;
    }
    .table{
      margin-top: 15px;
      font-size: 12px;
      color: #7f8484;
    }
    .border-btn{
      border-color: #929292;
      font-size: 12px;
      background: #ffff;
    }
    .cd-form input.has-border{
      padding-left: 20px !important;
    }
    .input-border{
      padding: 16px 20px 16px 20px;
      width: 100%;
      border: 1px solid #d2d8d8;
    }
    .input-select{
      color: #828282 !important;
      padding: 16px 20px 16px 20px !important;
      height: auto !important;
      margin-left: 0px !important;
    }
  </style>


@endsection

@section('content_div')

  <div class="row" style="padding-right: 15px;padding-left: 15px;">
    <div class="col-md-12 order_total_content">
      <div class="list-name name-title">&nbsp;Daftar Alamat</div>
      <div class="order_total_content text-md-right margin-top-15">
        <div class="contact_form_button">
          <!-- <button type="button" class="button cart_button_checkout float-left"><i class="fas fa-plus"></i> Tambah Alamat</button> -->
          <a class="cd-add button cart_button_checkout float-left" href="#0"><i class="fas fa-plus"></i> Tambah Alamat</a>
        </div>
      </div>
      <!--Table-->
      <table class="table table-hover table-fixed">

        <!--Table head-->
        <thead>
          <tr>
            <th></th>
            <th>Penerima</th>
            <th>Alamat Pengirim</th>
            <th>Daerah Penerima</th>
            <th>Action</th>
          </tr>
        </thead>
        <!--Table head-->

        <!--Table body-->
        <tbody>
          @forelse($addresses as $address)
            <tr>
              <th scope="row">
                <label class="container-radio">
                    <input type="radio"
                    @if($address->is_primary == '1')
                      {{ 'checked="checked"' }}
                    @endif
                    name="radio" data-id="{{ $address->id }}">
                    <span class="checkmark"></span>
                </label>
              </th>
              <td><b>{{ $address->receiver }}</b><br>{{ $address->phoneno}}</td>
              <td><b>{{ $address->name }}</b><br>{{ $address->address }}</td>
              <td>{{ $cities[$address->city]['type'] }} {{ $cities[$address->city-1]['city_name'] }},<br> {{ $cities[$address->city-1]['province'] }}</td>
              <td>
                <button type="button" class="btn btn-default btn-sm btn-urutkan border-btn opeditedit" data-id="{{ $address->id }}"><i class="fas fa-edit"></i> Ubah</button>&nbsp;&nbsp;
                <button type="button" class="btn btn-default btn-sm btn-urutkan border-btn opendelete" data-id="{{ $address->id }}"><i class="fas fa-trash-alt"></i> Hapus</button>
              </td>
            </tr>
            @empty
          @endforelse
          <!-- <tr>
            <th scope="row">
              <label class="container-radio">
                <input type="radio" name="radio">
                <span class="checkmark"></span>
              </label>
            </th>
            <td><b>Cuk Gengs</b><br>082245326737</td>
            <td><b>Kantor</b><br>JL. Tabanas No. 8a/9</td>
            <td>DKI Jakarta,<br> Kota Administrasi Jakarta Selatan,<br> Pancoran 12870<br>Indonesia</td>
            <td><a href="#"  class="btn btn-default btn-sm btn-urutkan border-btn"><i class="fas fa-edit"></i> Ubah</a>&nbsp;&nbsp;<a href="#"  class="btn btn-default btn-sm btn-urutkan border-btn"><i class="fas fa-trash-alt"></i> Hapus</a></td>
          </tr> -->
        </tbody>
        <!--Table body-->

      </table>
      <!--Table-->
    </div>
  </div>
    <div class="cd-user-modal cd-user-add"> <!-- this is the entire modal form, including the background -->
  			<div class="cd-user-modal-container"> <!-- this is the container wrapper -->
  				<ul class="cd-switcher" style="text-align: center;">
  					<a href="#0">Alamat</a>
  				</ul>

  				<div id="modal-edit"> <!-- sign up form -->
  					<form id="add-address" method="post" action="/store" class="cd-form">
              <p class="p-margin-button-0">Nama Alamat</p>
  						<p class="fieldset margin-button-5">
  							<input name="addressName" class="full-width has-padding has-border" id="edit-address-name" type="text" placeholder="Nama Alamat" required>
  						</p>
              <p>Contoh: Alamat Rumah, Kantor, Apartemen, Dropship</p>

              {{ csrf_field() }}
              <p class="p-margin-button-0">Nama Penerima</p>
  						<p class="fieldset margin-button-5">
  							<input name="receiverName" class="full-width has-padding has-border" id="edit-receiver-name" type="text" placeholder="Nama Penerima" required>
  						</p>

              <p class="p-margin-button-0">Nomor HP</p>
  						<p class="fieldset margin-button-5">
  							<!-- <input class="full-width has-padding has-border" id="signup-phone" type="text" placeholder="08XXXX" required> -->
                {!! Form::text('phone', null, ['id'=>'edit-phone', 'class' => 'full-width has-padding has-border', 'required', 'placeholder'=>'08XXXX']) !!}
                <p>Angka [0-9], Contoh: 081234567890</p>
  						</p>

              <!-- <p class="p-margin-button-0">Provinsi</p>
  						<p class="fieldset margin-button-5">
                {!!Form::select('province',$provinces,null, ['id'=>'province','class' => 'form-control full-width has-padding has-border margin-left', 'required', 'style' => 'color: #828282 !important;']) !!}
  						</p> -->

              <p class="p-margin-button-0">Provinsi</p>
  						<p class="fieldset margin-button-5">
                <select name="province" id="edit-province" class="form-control full-width has-padding has-border margin-left input-select" required="" style="color: #828282 !important;" name="city">
                  <optgroup label="Pilih">
                      <option value="Pilih">Pilih</option>
                  </optgroup>

                  @foreach($provinces as $provinsi)



                        <option data-province="{{$provinsi['province_id']}}" value="{{$provinsi['province_id']}}">{{$provinsi['province']}}</option>


                  @endforeach
                </select>
              </p>
              <p class="p-margin-button-0">Kota</p>
  						<p class="fieldset margin-button-5">
                <select name="city" id="edit-city" class="form-control full-width has-padding has-border margin-left input-select" required="" style="color: #828282 !important;" name="city">


                      <option value="Pilih">Pilih</option>

                </select>
              </p>
              <p class="p-margin-button-0">Kecamatan</p>
  						<p class="fieldset margin-button-5">
                <select name="district" id="edit-district" class="form-control full-width has-padding has-border margin-left input-select" required="" style="color: #828282 !important;" name="city">

                      <option value="Pilih">Pilih</option>

                </select>
              </p>

              <p class="p-margin-button-0">Kode Pos</p>
  						<p class="fieldset margin-button-5">
  							<!-- <input class="full-width has-padding has-border" id="signup-phone" type="text" placeholder="08XXXX" required> -->
                {!! Form::text('zipcode', null, ['id'=>'edit-zipcode', 'class' => 'full-width has-padding has-border', 'required', 'placeholder'=>'65XXXX']) !!}
  						</p>

              <p class="p-margin-button-0">Alamat Lengkap</p>
  						<p class="fieldset margin-button-5">
  							<!-- <input class="full-width has-padding has-border" id="signup-phone" type="text" placeholder="08XXXX" required> -->
                {!! Form::textarea('address', null, ['id'=>'edit-address', 'rows' => 3, 'class' => 'full-width has-padding has-border input-border', 'required', 'placeholder'=>'Alamat Lengkap']) !!}
  						</p>
              {!! Form::hidden('id', null, ['id'=>'edit-id', 'class' => 'full-width has-padding has-border', 'required']) !!}
  						<p class="fieldset">
  							<input class="full-width has-padding" data-id="" type="submit" value="Simpan" id="btn-simpan">
  						</p>
  					</form>

  					<!-- <a href="#0" class="cd-close-form">Close</a> -->
  				</div> <!-- cd-add -->
  				<a href="#0" class="cd-close-form">Close</a>
  			</div> <!-- cd-user-modal-container -->
  		</div> <!-- cd-user-modal -->

    <!-- <div id="loader">
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="lading"></div>
		</div> -->
@endsection

@section('someJS')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" type="text/javascript"></script>

		<script type="text/javascript">
			$( document ).ready(function() {
        $('#loader').hide();
        $("#disease").select2({
             allowClear:true,
             placeholder: 'Search for a disease'
        });

			var $form_modal = $('.cd-user-add'),
				$form_login = $form_modal.find('#cd-login'),
				$form_signup = $form_modal.find('#cd-edit'),
				$form_forgot_password = $form_modal.find('#cd-reset-password'),
				$form_modal_tab = $('.cd-switcher'),
				$tab_login = $form_modal_tab.children('li').eq(0).children('a'),
				$tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
				$forgot_password_link = $form_login.find('.cd-form-bottom-message a'),
				$back_to_login_link = $form_forgot_password.find('.cd-form-bottom-message a'),
				$main_nav = $('.main-nav');

			//open modal
			$('.cd-add').on('click', function(event){


					// on mobile open the submenu
				$form_modal.addClass('is-visible');
        $('#update-address').trigger("reset");
        $('#update-address').attr('id', 'add-address');
				$('#btn-simpan').data('id', 'add');
				signup_selected();


			});
			$('.cd-signin').on('click', function(event){


					// on mobile open the submenu
				$form_modal.addClass('is-visible');
				login_selected();


			});


			//close modal
			$('.cd-user-modal').on('click', function(event){
				if( $(event.target).is($form_modal) || $(event.target).is('.cd-close-form') ) {
					$form_modal.removeClass('is-visible');
				}
			});
			//close modal when clicking the esc keyboard button
			$(document).keyup(function(event){
					if(event.which=='27'){
						$form_modal.removeClass('is-visible');
					}
				});

			//switch from a tab to another
			$form_modal_tab.on('click', function(event) {
				event.preventDefault();
				( $(event.target).is( $tab_login ) ) ? login_selected() : signup_selected();
			});

			//hide or show password
			$('.hide-password').on('click', function(){
				var $this= $(this),
					$password_field = $this.prev('input');

				( 'password' == $password_field.attr('type') ) ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
				( 'Hide' == $this.text() ) ? $this.text('Show') : $this.text('Hide');
				//focus and move cursor to the end of input field
				$password_field.putCursorAtEnd();
			});

			//show forgot-password form
			$forgot_password_link.on('click', function(event){
				event.preventDefault();
				forgot_password_selected();
			});

			//back to login from the forgot-password form
			$back_to_login_link.on('click', function(event){
				event.preventDefault();
				login_selected();
			});

			function login_selected(){
				$form_login.addClass('is-selected');
				$form_signup.removeClass('is-selected');
				$form_forgot_password.removeClass('is-selected');
				$tab_login.addClass('selected');
				$tab_signup.removeClass('selected');
			}

			function signup_selected(){
				$form_login.removeClass('is-selected');
				$form_signup.addClass('is-selected');
				$form_forgot_password.removeClass('is-selected');
				$tab_login.removeClass('selected');
				$tab_signup.addClass('selected');
			}

			function forgot_password_selected(){
				$form_login.removeClass('is-selected');
				$form_signup.removeClass('is-selected');
				$form_forgot_password.addClass('is-selected');
			}

			//REMOVE THIS - it's just to show error messages
			$form_login.find('input[type="submit"]').on('click', function(event){
				event.preventDefault();
				$form_login.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
			});
			$form_signup.find('input[type="submit"]').on('click', function(event){
				event.preventDefault();
				$form_signup.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
			});

			// $('#btn-simpan').on('click', function(){
			// 	if ($(this).data('id')=='add') {
			// 		console.log("hai");
			// 		addData();
			// 	}else{
			// 		updateData();
			// 	}
      //
      //
			// });
			//IE9 placeholder fallback
			//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html


		});


		//credits https://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
		jQuery.fn.putCursorAtEnd = function() {
			return this.each(function() {
					// If this function exists...
					if (this.setSelectionRange) {
							// ... then use it (Doesn't work in IE)
							// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
							var len = $(this).val().length * 2;
							this.setSelectionRange(len, len);
					} else {
						// ... otherwise replace the contents with itself
						// (Doesn't work in Google Chrome)
							$(this).val($(this).val());
					}
			});
		};

		//ajax add address
	// 	function addData() {

 //      $('#loader').show();
 //      // var addressName  = $('#address-name').val();
	// 		// var receiverName = $('#receiver-name').val();
	// 		// var phone        = $('#phone').val();
 //      // var city         = $('#city').val();
	// 		// var zipcode      = $('#zipcode').val();
 //      // var address      = $('#address').val();

 //      var addressName  = $('#edit-address-name').val();
	// 		var receiverName = $('#edit-receiver-name').val();
	// 		var phone        = $('#edit-phone').val();
 //      var city         = $('#edit-city').val();
	// 		var zipcode      = $('#edit-zipcode').val();
 //      var address      = $('#edit-address').val();

 //      // alert('addressName : '+addressName+', receiverName : '+receiverName+', phone : '+phone+', city : '+city+', zipcode : '+zipcode+', address : '+address);

 //      if(addressName != '' && receiverName != '' && phone != '' && city != '' && zipcode != '' && address != ''){
 //        $.ajax({
 //          url: "{!! url('/store') !!}",
 //          dataType: "json",
 //          type: "POST",
 //          data:{
 //            addressName: addressName,
 //  					receiverName: receiverName,
 //  					phone: phone,
 //            city: city,
 //            zipcode: zipcode,
 //  					address: address,
 //            _method:"post",
 //            _token : '{{ csrf_token() }}'
 //          }
 //        }).done(function(data){
 //          $('#loader').hide();
 //  				if(data.resultCode == '00'){
 //  					alert(data.resultDesc);
 //            window.location.reload();
 //            $('#loader').show();
 //  				}else{
 //  					alert(data.resultDesc);
 //            window.location.reload();
 //            $('#loader').show();
 //  				}
 //  			});
 //      }else{
 //        alert('data belum lengkap');
 //      }
	// }

 //    //edit address
    $('.opeditedit').off().on('click', function(){
      var id = $(this).attr('data-id');
      $.ajax({
        url: "{!! url('/alamat/detail') !!}",
        dataType: "json",
        type: "POST",
        data:{
          id: id,
          _method:"post",
          _token : '{{ csrf_token() }}'
        }
      }).done(function(data){
      	console.log(data);
          $('.cd-user-add').addClass('is-visible');
          $('#btn-simpan').data('id', 'update');
          $('#edit-address-name').val(data.name);
					$('#edit-receiver-name').val(data.receiver);
					$('#edit-phone').val(data.phoneno);
					$('#edit-city').empty().append('<option data-stat="val" value="'+data.city+'">'+data.city_name+'</option>');
          $('#edit-province').val(data.province_id);
          $('#edit-district').empty().append('<option data-stat="val" value="'+data.subdistrict_id+'">'+data.subdistrict_name+'</option>');
					$('#edit-zipcode').val(data.postcode);
					$('#edit-address').val(data.address);
					$('#edit-id').val(data.id);
          $('#add-address').attr('action', '/alamat/update');
          $('#add-address').attr('id', 'update-address');
      });
    });

 //    //update
 //    function updateData(){
 //      var addressName  = $('#edit-address-name').val();
	// 		var receiverName = $('#edit-receiver-name').val();
	// 		var phone        = $('#edit-phone').val();
 //      var city         = $('#edit-city').val();
	// 		var zipcode      = $('#edit-zipcode').val();
 //      var address      = $('#edit-address').val();
 //      var id           = $('#edit-id').val();

 //      // alert(addressName+receiverName+phone+city+zipcode+address+id);

 //      $.ajax({
 //        url: "{!! url('/alamat/update') !!}",
 //        dataType: "json",
 //        type: "POST",
 //        data:{
 //          id: id,
 //          addressName: addressName,
 //          receiverName: receiverName,
 //          phone: phone,
 //          city: city,
 //          zipcode: zipcode,
 //          address: address,
 //          _method:"post",
 //          _token : '{{ csrf_token() }}'
 //        }
 //      }).done(function(data){
 //      	console.log(data);
 //        if(data.resultCode == '00'){
 //          alert(data.resultDesc);
 //          window.location.reload();
 //        }else{
 //          alert(data.resultDesc);
 //          window.location.reload();
 //        }
 //      });
 //   }
 //   function resetInput(argument) {
 //   	 var addressName  = $('#edit-address-name').val(""),
	// receiverName = $('#edit-receiver-name').val(""),
	//  phone        = $('#edit-phone').val(""),
 //     city         = $('#edit-city').val(""),
	//  zipcode      = $('#edit-zipcode').val(""),
 //     address      = $('#edit-address').val(""),
 //     id           = $('#edit-id').val("");

 //   }
    $('#edit-province').on('change', function() {
      var province = $(this).children("option:selected").val()
      $.ajax({
  			url: "{{ url('/getCity') }}",
  			dataType:"json",
  			type: "GET",
  			data:{
  				province:province
  			}
  		}).done(function(ret) {
          $('#edit-city').empty();
        for(i=0; i < ret.length; i++){

          $('#edit-city').append('<option data-stat="val" value="'+ret[i].city_id+'">'+ret[i].type+" " + ret[i].city_name+'</option>');
        }
      })
    })
    $('#edit-city').on('change', function() {
      var city = $(this).children("option:selected").val()
      $.ajax({
  			url: "{{ url('/getDistrict') }}",
  			dataType:"json",
  			type: "GET",
  			data:{
  				city:city
  			}
  		}).done(function(ret) {
          $('#edit-district').empty();
        for(i=0; i < ret.length; i++){

          $('#edit-district').append('<option data-stat="val" value="'+ret[i].subdistrict_id+'">'+ret[i].subdistrict_name+'</option>');
        }
      })
    })
    //delete
    $('.opendelete').off().on('click', function(){
      var id = $(this).attr('data-id');
      $.ajax({
        url: "{!! url('/alamat/delete/') !!}"+'/'+id,
        dataType: "json",
        type: "GET",
      }).done(function(data){
      	console.log(data);
        if(data.resultCode == '00'){
          alert('alamat berhasil di delete');
          window.location.reload();
        }else{
          alert(data.resultDesc);
          window.location.reload();
        }

      });
    });

    $('input[name="radio"]').change(function() {
      var id = $(this).attr("data-id");
      $.ajax({
          url: "{{ url('/address/change') }}",
          type: 'POST',
          data: {
              id    : id,
              _method:"post",
          _token : '{{ csrf_token() }}'
          }
        }).done(function(data){
          console.log(data);
          if(data == '00'){
            alert('Alamat berhasil diganti!');
          }else{
            alert('Alamat gagal diganti!');
          }
        });
    });
		</script>
@endsection
