@section('title')
hariBelanja - Diskusi
@endsection

@section('diskusi')
hover-active
@endsection

@section('css')

<style>

body {
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  font-weight: 300;
  color: #888;
  line-height: 30px;
  text-align: center;
}

.char_item {
    padding-left: 33% !important;
}

@media only screen and (max-width: 991px){
  .char_item {
      padding-left: 33% !important;
  }
}

.char_content{
  width: 100%;
}
.char_item{
  padding-left: 5% !important;
}
.padding-cart{
  height: auto !important;
  padding-top: 2% !important;
  padding-bottom: 2% !important;
}

.characteristics {
    padding-top: 30px !important;
    padding-bottom: 30px !important;
}

</style>
@endsection

@section('someJS')
  <script type="text/javascript">
  $( document ).ready(function() {
    $('.details').hide();
  });
    $(function(){
      });
    });
  </script>
@endsection

@extends('frontend.layouts.layout_profile')

@section('content_div')


    <div class="characteristics">
      <div class="container">
        <div class="row">

          <!-- Char. Item -->
          <div class="col-lg-12 col-md-12 char_col">

            <div class="char_item d-flex flex-row align-items-center justify-content-start padding-cart">
              <div class="char_content row">
                <div class="col-md-1"><img src="http://placekitten.com/40/40"></div>
                <div class="col-md-11">
                  <div class="char_title">Popcron Garret</div>
                  <p style="font-size:12px;margin-bottom: 0px;color:#007bff;">Rp 50.000</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 char_col">

            <div class="char_item d-flex flex-row align-items-center justify-content-start padding-cart">
              <div class="char_content row">
                <div class="col-md-1"><img src="http://placekitten.com/40/40"></div>
                <div class="col-md-11">
                  <div class="char_title">Rani Dewi Aristhania <span style="font-size:12px;font-weight: 100;">- 25 Juli pukul 21:46</span></div>
                  <p style="margin-bottom: 0px;">sudah sama bubble warp gan?</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 char_col">

            <div class="char_item d-flex flex-row align-items-center justify-content-start padding-cart">
              <div class="char_content row">

                <div class="col-md-1"></div>
                <div class="col-md-1 padding-cart"><img src="http://placekitten.com/40/40"></div>
                <div class="col-md-10 padding-cart">
                  <div class="char_title">proSkil17 <span class="badge badge-info">Penjual</span><span style="font-size:12px;font-weight: 100;">- 25 Juli pukul 21:52</span></div>
                  <p style="margin-bottom: 0px;">sudah gan</p>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-1 padding-cart"><img src="http://placekitten.com/40/40"></div>
                <div class="col-md-10 padding-cart">
                  <div class="char_title">Rani Dewi Aristhania <span style="font-size:12px;font-weight: 100;">- 25 Juli pukul 21:59</span></div>
                  <p style="margin-bottom: 0px;">oke nanti saya beli gan</p>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-1 padding-cart"><img src="http://placekitten.com/40/40"></div>
                <div class="col-md-10 padding-cart">
                  <input type="text" class="contact_form_name input_field width_100" placeholder="Isi komentar disini...">
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  <!-- </div>? -->
@endsection
