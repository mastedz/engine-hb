@section('title')
hariBelanja - Invoice
@endsection

@section('invoice')
hover-active
@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('styles/invoice.css')}}">
<style>

body {
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  font-weight: 300;
  color: #888;
  line-height: 30px;
  text-align: center;
}

strong { font-weight: 500; }


</style>

@endsection

@section('someJS')
  <script type="text/javascript">
  $( document ).ready(function() {
    $('.details').hide();
  });
  </script>
@endsection

@extends('frontend.layouts.layout_profile')

@section('content_div')

    <div class="characteristics">
      @forelse($data as $index => $value)

      <div class="container">
        <div class="row">
          <div class="col-md-12 order_total_content" id="title-biodata">
            <div class="list-name name-title">&nbsp;My Invoice</div>
            <div class="name-title hapusinvoice"><a href="#">Batalkan Transaksi</a></div>
          </div>
          <!-- Char. Item -->
          <br><br>
          <div class="col-md-9">
            <!-- <form action="#" id="contact_form">
              <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between"> -->
                <p class="p-width">Kode Pembelian : <b>{{$value->transaction_id}}</b>  | Tanggal Pembelian : <b>{{$value->transaction_time}}</b></p>
                <!--<input type="text" id="contact_form_name" class="contact_form_name input_field width_100" required="required" data-error="Name is required."  name="name">
              </div>-->
              <div class="col-md-12 reminder">
                <center><p class="p-width"><i class="fas fa-exclamation-circle"></i>&nbsp;Bayar Sebelum {{\Carbon\Carbon::parse($value->transaction_time)->addDays(1)}}</p><center>
              </div>
              <br>

              <p class="p-width">&nbsp;&nbsp;Metode Pembayaran: {{strtoupper(str_replace("_", " ", $value->payment_type))}} </p>
              <p class="p-width">&nbsp;&nbsp;Nomor Pembayaran: @if(isset($value->va_numbers[0]->va_number)){{$value->va_numbers[0]->va_number}} @else {{$value->biller_code}} @endif</p>
              <p class="p-width">&nbsp;&nbsp;Total Harga: <span class="totharga">{{$value->gross_amount}}</span></p>

        </div>
        <div class="col-md-3">
            <img class="invoice-bank" width="300px"style="margin-bottom: 35px;margin-top: 35px;" src="https://upload.wikimedia.org/wikipedia/id/thumb/e/e0/BCA_logo.svg/1280px-BCA_logo.svg.png">
            <button class="button carabayar"><i class="fa fa-question-circle-o" aria-hidden="true"></i>Cara Pembayaran</button>
        </div>
        <div class="col-md-12">


        </div>

      </div>
      @empty
      <div class="container">
        <div class="row">
          <div class="col-md-12 order_total_content" id="title-biodata">
            <h2>Belum Ada Pesanan</h2>
          </div>
        </div>
      </div>
      @endforelse
    </div>


  <!-- </div>? -->
@endsection
