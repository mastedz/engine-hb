@section('title')
hariBelanja - Metode Bayar
@endsection

@section('')
hover-active
@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('styles/invoice.css')}}">
<style>

body {
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  font-weight: 300;
  color: #888;
  line-height: 30px;
  text-align: center;
}

.guide {
  padding: 2px;
  border-bottom: solid #F9F6F2;
  margin: auto;
}

strong { font-weight: 500; }

.sprite {
    background-image: url('https://i.ibb.co/xfgwYb3/sprite-6ef6641704245013927165f1efc378c72d7f07323b0661f4fd4b4739593655b9.png');
    background-size: 160px 430px;
    background-repeat: no-repeat;
    display: block;
}

.sprite.credit-card-full {
    background-position: -120px -312px;
    width: 40px;
    height: 41px;
}
.sprite.bca {
    background-position: 0px -232px;
    width: 40px;
    height: 40px;
}
.sprite.mandiri {
    background-position: -40px -232px;
    width: 40px;
    height: 40px;
}
.sprite.bni {
    background-position: -120px -232px;
    width: 40px;
    height: 40px;
}
.sprite.permata {
    background-position: -80px -232px;
    width: 40px;
    height: 40px;
}
.sprite.bank-transfer {
    background-position: 0px -392px;
    width: 40px;
    height: 40px;
}
.sprite.gopay {
    background-position: -120px -72px;
    width: 40px;
    height: 40px;
}
.sprite.bca-klikbca {
    background-position: 0px -192px;
    width: 40px;
    height: 40px;
}
.sprite.mandiri-clickpay {
    background-position: -40px -112px;
    width: 40px;
    height: 40px;
}
.sprite.cimb-clicks {
    background-position: -80px -32px;
    width: 40px;
    height: 40px;
}
.sprite.danamon-online {
    background-position: -80px -392px;
    width: 40px;
    height: 40px;
}
.sprite.bri-epay {
    background-position: 0px -112px;
    width: 40px;
    height: 40px;
}
.sprite.linepay-mandiri {
    background-position: -120px -272px;
    width: 40px;
    height: 40px;
}
.sprite.indomaret {
    background-position: -40px -72px;
    width: 40px;
    height: 40px;
}
.sprite.alfamart {
    background-position: -80px -72px;
    width: 40px;
    height: 40px;
}
.sprite.akulaku {
    background-position: -120px -392px;
    width: 40px;
    height: 40px;
}
.sprite.bca-klikpay {
    background-position: -80px -152px;
    width: 40px;
    height: 40px;
}
.grey{
  color: #555555ed;
}


</style>

@endsection

@section('someJS')
  <script type="text/javascript">
  $( document ).ready(function() {
    $(".myBtn").click(function(){
        var id = $(this).attr('data-id');
        var url = "{!! url('/getguide') !!}"+"/"+id;
        $.ajax({
          url: url,
          type: "GET",
        }).done(function(data){
          var response = jQuery.parseJSON(data);
          // alert(response.app.length);
          // alert(response.app[0]);
          // alert(response.channel);
          var app = '';
          var desc = '';
          $('#channel').empty().append(response.channel);

          if(response.app.length > 1){
            app += '<li class="nav-item"><a class="nav-link active" href="#app0" role="tab" data-toggle="tab">'+response.app[0]+'</a></li>';
            for(i=1;i<response.app.length;i++){
            app += '<li class="nav-item"><a class="nav-link" href="#app'+i+'" role="tab" data-toggle="tab">'+response.app[i]+'</a></li>';
            }

            desc += '<div role="tabpanel" class="tab-pane fade active show" id="app0">'+response.desc[0]+'</div>';
            for(i=1;i<response.desc.length;i++){
                desc += '<div role="tabpanel" class="tab-pane fade" id="app'+i+'">'+response.desc[i]+'</div>';
            }
            $('#app').show();
            $('#app').empty().append(app);
          }else{
            $('#app').hide();
            desc = response.desc[0];
          }
          $('#myModal0').modal("show");
          $('#desc').empty().append(desc);
        });
    });
  });

  </script>
@endsection

@extends('frontend.layouts.layout_profile')

@section('content_div')

  <!-- Modal -->


    <div class="characteristics">
      <div class="container">
        <div class="row">
          <div class="col-md-12 order_total_content" id="title-biodata">
            <div class="list-name name-title">&nbsp;Payment Channel</div>
          </div>
          <!-- Char. Item -->
          <br><br>
          <div class="col-md-12" style="font-size:12px;color:#828282;">
            <!-- <div class="row details">
              <div class="col-md-2">
                <span class="credit-card-full sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">Credit Card</b><br>Pay With Visa, MasterCard, JCB, or Amex</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div> -->
            <div class="row myBtn" data-id="0">
              <div class="col-md-2">
                <span class="bca sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">BCA</b><br>Pay from BCA ATMs or internet banking</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="1">
              <div class="col-md-2">
                <span class="mandiri sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">Mandiri</b><br>Pay from Mandiri ATMs or internet banking</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="2">
              <div class="col-md-2">
                <span class="bni sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">BNI</b><br>Pay from BNI ATMs or internet banking</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="3">
              <div class="col-md-2">
                <span class="permata sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">Permata</b><br>Pay from Permata ATMs or internet banking</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="4">
              <div class="col-md-2">
                <span class="bank-transfer sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">Other Bank</b><br>Pay from other Bank ATMs</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="5">
              <div class="col-md-2">
                <span class="gopay sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">GO-PAY</b><br>Pay with your GO-PAY wallet</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="6">
              <div class="col-md-2">
                <span class="bca-klikbca sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">KlikBCA</b><br>Pay with BCA KlikPay</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="7">
              <div class="col-md-2">
                <span class="bca-klikpay sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">BCA KlikPay</b><br>Pay with BCA KlikPay</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="8">
              <div class="col-md-2">
                <span class="mandiri-clickpay sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">Mandiri Clickpay</b><br>Pay through your Mandiri Clickpay account</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="9">
              <div class="col-md-2">
                <span class="cimb-clicks sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">CIMB Clicks</b><br>Pay using CIMB Clicks account</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="10">
              <div class="col-md-2">
                <span class="danamon-online sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">Danamon Online Banking</b><br>Pay with your Danamon Online Banking account</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="11">
              <div class="col-md-2">
                <span class="bri-epay sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">e-Pay BRI</b><br>Pay using e-Pay BRI account</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="12">
              <div class="col-md-2">
                <span class="linepay-mandiri sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">LINE Pay e-cash | mandiri e-cash</b><br>Pay with your LINE Pay e-cash | mandiri e-cash</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="13">
              <div class="col-md-2">
                <span class="indomaret sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">Indomaret</b><br>Pay from Indomaret</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="14">
              <div class="col-md-2">
                <span class="alfamart sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">Alfamart</b><br>Pay from an Alfamart, Alfamidi or Dan+Dan outlet</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
            <div class="row myBtn" data-id="15">
              <div class="col-md-2">
                <span class="akulaku sprite"></span>
              </div>
              <div class="col-md-9">
                <b class="grey">Other Bank</b><br>Pay with your Akulaku account</p>
              </div>
              <div class="col-md-1">
                <b>></b>
              </div>
            </div>
          </div>

          <div class="modal fade" id="myModal0" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                  <h4 class="modal-title">Cara Bayar Dengan <span id="channel"></span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <ul class="nav nav-tabs" role="tablist" id="app">
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content" id="desc">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

                </div>
              </div>
            </div>


      </div>
    </div>


  <!-- </div>? -->
@endsection
