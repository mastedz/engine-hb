<!DOCTYPE html>
<!-- saved from url=(0058)https://payment.tokopedia.com/pms/howtopay_v2?id=445661899 -->
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Konfirmasi Pembayaran</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{asset('plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('styles/info_invoice.css?v=0.0.1')}}">


</head>

<body>
    <header class="app-header">
        <div class="app-header-content">
            <a class="app-header-close" href="/">
                <i class="sprite-small sprite-back va-middle"></i>
            </a>
            <div class="app-title">Cara Pembayaran</div>
        </div>
    </header>

    <div class="page" id="maincontent">
        <div class="checkout-detail">
            <div class="checkout-detail__content pl-16 pr-16 pt-24">
                <p class="fs-18 mb-8">SEGERA LAKUKAN PEMBAYARAN SEBELUM:</p>
                <div class="bold fs-16">21 Jul 2019, 18:14 WIB</div>
            </div>
            <div class="checkout-detail__content pl-16 pr-16 pt-24">

                <p class="fs-14 mb-8">Transfer pembayaran ke nomor Virtual Account :</p>

                <div class="payment-content">
                    <div class="box-media">
                        <div class="box-left pr-11">
                            <i class="sprite-normal"><img width="200"src="https://upload.wikimedia.org/wikipedia/id/thumb/e/e0/BCA_logo.svg/1280px-BCA_logo.svg.png"></i>
                        </div>
                        <div class="box-right va-middle p-0">

                            <p class="mt-10 mb-8 fs-14">Nomor Virtual Accounts</p>

                            <p class="fs-18 mb-0 bold" id="payment-code" data-text="No. Rekening berhasil disalin" data-copy="80777085854079798">80777085854079798</p>
                        </div>
                    </div>

                    <button onclick="copyToClipboard(&#39;payment-code&#39;)" class="get-code mt-18 fs-12 p-0">Salin no. Rek</button>

                </div>
            </div>
            <div class="pl-16 pr-16 pt-24">
                <hr class="border-payment m-0">
            </div>
            <div class="checkout-detail__content pl-16 pr-16 pt-24">
                <p class="fs-14 mb-8">
                    Jumlah yang harus dibayar :
                </p>

                <p class="payment-sum fs-18 orange mb-0" id="payment-sum" data-text="Jumlah pembayaran berhasil disalin" data-copy="10941320">
                    <strong>Rp 10.941.320</strong>
                </p>

                <button onclick="copyToClipboard(&#39;payment-sum&#39;)" class="get-code mt-24 fs-12 p-0">Salin Jumlah</button>

            </div>

            <div class="pl-16 pr-16 pt-16">
                <hr class="border-payment m-0">
            </div>

            <div class="popup-action">
                <p class="popup-action__text mb-0">
                </p>
            </div>
        </div>

        <div class="w-accordion" style="margin:0;">
            <div class="w-accordion-menu">ATM BCA
                <div class="pull-right"><i class="fas fa-chevron-down"></i></div>
            </div>
            <div class="w-accordion-content" style="display: none;">
                <ol>
                    <li class="mt-10 mb-10">Masukkan Kartu ATM BCA &amp; PIN</li>
                    <li class="mt-10 mb-10">Pilih menu Transaksi Lainnya &gt; Transfer &gt; ke Rekening BCA Virtual Account</li>
                    <li class="mt-10 mb-10">Masukkan 5 angka kode perusahaan untuk Tokopedia (80777) dan Nomor HP yang terdaftar di akun Tokopedia Anda (Contoh: 80777085854079798)</li>
                    <li class="mt-10 mb-10">Di halaman konfirmasi, pastikan detil pembayaran sudah sesuai seperti No VA, Nama, Perus/Produk dan Total Tagihan</li>
                    <li class="mt-10 mb-10">Masukkan Jumlah Transfer sesuai dengan Total Tagihan</li>
                    <li class="mt-10 mb-10">Ikuti instruksi untuk menyelesaikan transaksi</li>
                    <li class="mt-10 mb-10">Simpan struk transaksi sebagai bukti pembayaran</li>
                </ol>
            </div>
            <div class="w-accordion-menu">m-BCA (BCA mobile)
                <div class="pull-right"><i class="fas fa-chevron-down"></i></div>
            </div>
            <div class="w-accordion-content" style="display: none;">
                <ol>
                    <li class="mt-10 mb-10">Lakukan log in pada aplikasi BCA Mobile</li>
                    <li class="mt-10 mb-10">Pilih menu m-BCA, kemudian masukkan kode akses m-BCA</li>
                    <li class="mt-10 mb-10">Pilih m-Transfer &gt; BCA Virtual Account</li>
                    <li class="mt-10 mb-10">Pilih dari Daftar Transfer, atau masukkan 5 angka kode perusahaan untuk Tokopedia (80777) dan Nomor HP yang terdaftar di akun Tokopedia Anda (Contoh: 80777085854079798)</li>
                    <li class="mt-10 mb-10">Masukkan pin m-BCA</li>
                    <li class="mt-10 mb-10">Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran</li>
                </ol>
            </div>
            <div class="w-accordion-menu">Internet Banking BCA
                <div class="pull-right"><i class="fas fa-chevron-down"></i></div>
            </div>
            <div class="w-accordion-content" style="display: none;">
                <ol>
                    <li class="mt-10 mb-10">Login pada alamat Internet Banking BCA (<a href="https://klikbca.com/" target="_blank">https://klikbca.com</a>)</li>
                    <li class="mt-10 mb-10">Pilih menu Pembayaran Tagihan &gt; Pembayaran &gt; BCA Virtual Account</li>
                    <li class="mt-10 mb-10">Pada kolom kode bayar, masukkan 5 angka kode perusahaan untuk Tokopedia (80777) dan Nomor HP yang terdaftar di akun Tokopedia Anda (Contoh: 80777085854079798)</li>
                    <li class="mt-10 mb-10">Di halaman konfirmasi, pastikan detil pembayaran sudah sesuai seperti Nomor BCA Virtual Account, Nama Pelanggan dan Jumlah Pembayaran</li>
                    <li class="mt-10 mb-10">Masukkan <span class="italic">password</span> dan mToken</li>
                    <li class="mt-10 mb-10">Cetak/simpan struk pembayaran BCA Virtual Account sebagai bukti pembayaran</li>
                </ol>
            </div>
            <div class="w-accordion-menu">Kantor Bank BCA
                <div class="pull-right"><i class="fas fa-chevron-up"></i></div>
            </div>
            <div class="w-accordion-content" style="display: block;">
                <ol>
                    <li class="mt-10 mb-10">Ambil nomor antrian transaksi Teller dan isi slip setoran</li>
                    <li class="mt-10 mb-10">Serahkan slip dan jumlah setoran kepada Teller BCA</li>
                    <li class="mt-10 mb-10">Teller BCA akan melakukan validasi transaksi</li>
                    <li class="mt-10 mb-10">Simpan slip setoran hasil validasi sebagai bukti pembayaran</li>
                </ol>
            </div>
        </div>
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
        <script type="text/javascript">
        $('.w-accordion-menu').on('click', function(event) {
            event.preventDefault();
            $('.w-accordion-content').slideUp();
            $('.w-accordion-menu').children().children('i').removeClass('fas fa-chevron-up').addClass('fas fa-chevron-down');
            if ($(this).next().is(':visible')) {
                $(this).children().children('i').removeClass('fas fa-chevron-up').addClass('fas fa-chevron-down');
            } else {
                $(this).next().slideDown();
                $(this).children().children('i').removeClass('fas fa-chevron-down').addClass('fas fa-chevron-up');
            }
        });
        $('.w-accordion-menu').first().click();
        </script>

    </div>

</body>

</html>
