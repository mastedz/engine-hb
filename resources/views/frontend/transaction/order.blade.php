@section('title')
hariBelanja - Transaksi Saya
@endsection

@section('daftar_trx')
hover-active
@endsection

@section('css')

<style>

body {
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  font-weight: 300;
  color: #888;
  line-height: 30px;
  text-align: center;
}

strong { font-weight: 500; }

a, a:hover, a:focus {
color: #0e8ce4;
text-decoration: none;
  -o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
}

h1, h2 {
margin-top: 10px;
font-size: 38px;
  font-weight: 100;
  color: #555;
  line-height: 50px;
}

h3 {
font-size: 22px;
  font-weight: 300;
  color: #555;
  line-height: 30px;
}

h4 {
font-size: 18px;
  font-weight: 300;
  color: #555;
  line-height: 26px;
}

img { max-width: 100%; }

::-moz-selection { background: #0e8ce4; color: #fff; text-shadow: none; }
::selection { background: #0e8ce4; color: #fff; text-shadow: none; }


/***** Top menu *****/

.navbar {
padding-top: 10px;
background: #333;
background: rgba(51, 51, 51, 0.3);
border: 0;
-o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;
}
.navbar.navbar-no-bg { background: none; }

ul.navbar-nav {
font-size: 16px;
color: #fff;
}

.navbar-inverse ul.navbar-nav li { padding-top: 8px; padding-bottom: 8px; }

.navbar-inverse ul.navbar-nav li .li-text { opacity: 0.8; }

.navbar-inverse ul.navbar-nav li a { display: inline; padding: 0; color: #fff; }
.navbar-inverse ul.navbar-nav li a:hover { color: #fff; opacity: 1; border-bottom: 1px dotted #fff; }
.navbar-inverse ul.navbar-nav li a:focus { color: #fff; outline: 0; opacity: 1; border-bottom: 1px dotted #fff; }

.navbar-inverse ul.navbar-nav li .li-social a {
margin: 0 5px;
font-size: 28px;
vertical-align: middle;
}
.navbar-inverse ul.navbar-nav li .li-social a:hover,
.navbar-inverse ul.navbar-nav li .li-social a:focus { border: 0; color: #0e8ce4; }

.navbar-brand {
width: 162px;
background: url(../img/logo.png) left center no-repeat;
text-indent: -99999px;
}


/***** Top content *****/

.top-content { padding: 40px 0 170px 0; }

.top-content .text { color: #fff; }
.top-content .text h1 { color: #fff; }
.top-content .description { margin: 20px 0 10px 0; }
.top-content .description p { opacity: 0.8; }
.top-content .description a { color: #fff; }
.top-content .description a:hover,
.top-content .description a:focus { border-bottom: 1px dotted #fff; }

.form-box { padding-top: 40px; }

.f1 {
padding: 25px; background: #fff;
-moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;
}
.f1 h3 { margin-top: 0; margin-bottom: 5px; text-transform: uppercase; }

.f1-steps { overflow: hidden; position: relative; margin-top: 20px;text-align: center;}

.f1-progress { position: absolute; top: 24px; left: 0; width: 100%; height: 1px; background: #ddd; }
.f1-progress-line { position: absolute; top: 0; left: 0; height: 1px; background: #1A237E; }

.f1-step { position: relative; float: left; width: 20%; padding: 0 5px; }

.f1-step-icon {
display: inline-block; width: 40px; height: 40px; margin-top: 4px; background: #ddd;
font-size: 16px; color: #fff; line-height: 40px;
-moz-border-radius: 50%; -webkit-border-radius: 50%; border-radius: 50%;text-align: center;
}
.f1-step.activated .f1-step-icon {
background: #fff; border: 1px solid #0e8ce4; color: #0e8ce4; line-height: 38px;
}
.f1-step.active .f1-step-icon {
width: 48px; height: 48px; margin-top: 0; background: #0e8ce4; font-size: 22px; line-height: 48px;
}

.f1-step p { color: #ccc; }
.f1-step.activated p { color: #0e8ce4; }
.f1-step.active p { color: #0e8ce4; }

.f1 fieldset { display: none; text-align: left; }

.f1-buttons { text-align: right; }

.f1 .input-error { border-color: #0e8ce4; }



/***** Media queries *****/

@media (min-width: 992px) and (max-width: 1199px) {}

@media (min-width: 768px) and (max-width: 991px) {}

@media (max-width: 767px) {

.navbar { padding-top: 0; }
.navbar.navbar-no-bg { background: #333; background: rgba(51, 51, 51, 0.9); }
.navbar-brand { height: 60px; margin-left: 15px; }
.navbar-collapse { border: 0; }
.navbar-toggle { margin-top: 12px; }

.top-content { padding: 40px 0 110px 0; }

}

@media (max-width: 415px) {

h1, h2 { font-size: 32px; }

.f1 { padding-bottom: 20px; }
.f1-buttons button { margin-bottom: 5px; }

}


/* Retina-ize images/icons */

@media
only screen and (-webkit-min-device-pixel-ratio: 2),
only screen and (   min--moz-device-pixel-ratio: 2),
only screen and (     -o-min-device-pixel-ratio: 2/1),
only screen and (        min-device-pixel-ratio: 2),
only screen and (                min-resolution: 192dpi),
only screen and (                min-resolution: 2dppx) {

/* logo */
  .navbar-brand {
    background-image: url(../img/logo@2x.png) !important; background-repeat: no-repeat !important; background-size: 162px 36px !important;
  }

}

</style>

<style>
.clearfix:before,
.clearfix:after {
  content: " ";
  display: table;
}
a {
  color: #ccc;
  text-decoration: none;
  outline: none;
  }

  /*Fun begins*/
  .tab_container {
  padding-top: 0px;
  position: relative;
  }

  input, section {
  clear: both;
  padding-top: 10px;
  display: none;
  }

  label {
  font-weight: 500;
  font-size: 14px;;
  display: block;
  float: left;
  width: 20%;
  padding: 1.5em;
  color: #757575;
  cursor: pointer;
  text-decoration: none;
  text-align: center;
  background: #f0f0f0;
  }

  #tab1:checked ~ #content1,
  #tab2:checked ~ #content2,
  #tab3:checked ~ #content3,
  #tab4:checked ~ #content4,
  #tab5:checked ~ #content5 {
  display: block;
  background: #fff;
  color: #999;
  }

  .tab_container .tab-content p,
  .tab_container .tab-content h3 {
  -webkit-animation: fadeInScale 0.7s ease-in-out;
  -moz-animation: fadeInScale 0.7s ease-in-out;
  animation: fadeInScale 0.7s ease-in-out;
  }
  .tab_container .tab-content h3  {
  text-align: center;
  }

  .tab_container [id^="tab"]:checked + label {
  background: #fff;
  box-shadow: inset 0 3px #0e8ce4;
  color: #0e8ce4;
  }

  .tab_container [id^="tab"]:checked + label .fa {
  color: #0e8ce4;
  }

  label .fa {
  font-size: 1.3em;
  margin: 0 0.4em 0 0;
  }

  /*Media query*/
  @media only screen and (max-width: 930px) {
  label span {
    font-size: 14px;
  }
  label .fa {
    font-size: 14px;
  }
  }

  @media only screen and (max-width: 768px) {
  label span {
    display: none;
  }

  label .fa {
    font-size: 16px;
  }

  .tab_container {
    width: 98%;
  }
  }

  /*Content Animation*/
  @keyframes fadeInScale {
  0% {
    transform: scale(0.9);
    opacity: 0;
  }

  100% {
    transform: scale(1);
    opacity: 1;
  }
  }

  .test div {
  /* The vertical alignment magic */
  display: inline-block;
  /*vertical-align: middle;*/

  width: 100%; /* Needs width for inline-block */

  /* Fix for IE <= 7 */
  *display: inline;
  *zoom:1;
  *width: 16.6%;
}

/* Change number of boxes per row based on viewport width */
@media (min-width: 300px) {
  .test div {
    width: 50%;
  }
}
@media all and (min-width: 500px) {
  .test div {
    width: 33.3333333%;
  }
}
@media all and (min-width: 700px) {
  .test div {
    width: 16.6666667%;
  }
}
</style>
<style>
  .transaction{
    display: block;
    padding: 20px 20px 0px 20px;
    background: #fff;
    color: #999;
    border: 1px solid #e6e6e6bf;
    margin-bottom: 15px;
  }
  .btn-link-detail:hover {
      color: #00a6fe !important;
      text-decoration: none !important;
      background-color: transparent;
      border-color: #00a6fe !important;
  }
  .time-step{
    font-size: 11px;
    color: #a5a0a0;
  }
  .shopee-border-delivery {
      height: .1875rem;
      width: 100%;
      background-position-x: -1.875rem;
      background-size: 7.25rem .1875rem;
      background-image: repeating-linear-gradient(45deg,#6fa6d6,#6fa6d6 33px,transparent 0,transparent 41px,#f18d9b 0,#f18d9b 74px,transparent 0,transparent 82px);
  }
  a:not([href]):not([tabindex]):hover {
      color: #0e8ce4 !important;
  }
</style>
@endsection

@section('someJS')
  <script type="text/javascript">
  $( document ).ready(function() {
    $('.details').hide();
  });
    $(function(){
      $('#metode').hide();

      $('.btn-link-detail').off().on('click', function(){
          var trxId = $(this).attr('data-id');
          window.location.href ="{{ url('/order/detail/') }}"+"/"+trxId;
      });

      $('.btn-link').off().on('click', function(){
        var trxId = $(this).attr('data-id');
        $.ajax({
          url: "{!! url('/trx/detail') !!}",
          dataType: "json",
          type: "POST",
          data:{
            trxId: trxId,
            _method:"post",
            _token : '{{ csrf_token() }}'
          }
        }).done(function(data){
          console.log(data);
          $('#grand-total').append(data.total);
          $('#buyer_name').append(data.buyer_name);
          $('#address_name').append(data.address_name);
          $('#phone').append(data.phone);
          $('#address').append(data.address);
          $('#city').append(data.city);
          $('#province').append(data.province);
          $('#postcode').append(data.postcode);

          if(data.id != ''){
            var id = data.id;
            $('#subtotal').empty().append('Rp '+$('#subtotal-' + trxId).val());
            $('#output').empty();
            for(var i=0;i < data.order_items['length'];i++){
              json = JSON.parse(data.order_items[i].image);
               $('#output').append('<tr id="abu">'
                 +'<td>'
                   +'<div style="padding-left:0px;">'
                     +'<img src="{{asset("")}}images/'+json[0]+'" alt="" style="height: 60px;">'
                   +'</div>'
                 +'</td>'
                 +'<td>'
                   +'<p>'+data.order_items[i].name+'</p>'
                 +'</td>'
                 +'<td>'
                   +'<div style="padding-left:5px; padding-right:30px">'
                     +'<p>Rp.&nbsp;'+data.order_items[i].pivot.total+'<br>X'+data.order_items[i].pivot.qty+' </p>'
                   +'</div>'
                 +'</td>'
               +'</tr>');
               if(data.order_items[i].pivot.delivery_status == 0){
               }
             }
          }

          if(data.delivered == 1){
            //activekan di buat & dibayar
            $("#order-dibuat").addClass("active");
            $("#order-dibayar").addClass("active");

            $("#no-pesanan").append('NO. PESANAN 190426021556SMW');
            $('#status-delivered').append('DIKEMAS');
          }else if (data.delivered == 2) {
            //activekan di buat & dibayar &dikirim
            $("#order-dibuat").addClass("active");
            $("#order-dibayar").addClass("active");
            $("#order-dikirim").addClass("active");
            $("#no-pesanan").append('NO. RESI 190426021556SMW');
            $('#status-delivered').append('DIKIRIM');
          }else if (data.payment_status == 'done') {
            //activekan di buat & dibayar - selesai
            $("#order-dibuat").addClass("active");
            $("#order-dibayar").addClass("active");
            $("#order-dikirim").addClass("active");
            $("#order-diterima").addClass("active");
            $("#order-selesai").addClass("active");

            $("#no-pesanan").append('NO. RESI 190426021556SMW');
            $('#status-delivered').append('SELESAI');
            $('#metode').show();
            $('#metode-pay').append('Indomaret');
          }
        });
        $('.details').show();
        $('#list-trx').hide();
      });

      $('#btn-kembali').off().on('click', function(){
        //menghapus / mengkosongkan element yang di append -> tidak tumpuk
        $('#no-pesanan').empty();
        $('#status-delivered').empty();
        $('#metode-pay').empty();
        $('#grand-total').empty();
        $('#buyer_name').empty();
        $('#address_name').empty();
        $('#phone').empty();
        $('#address').empty();
        $('#city').empty();
        $('#province').empty();
        $('#postcode').empty();

        $('.details').hide();
        $('#list-trx').show();
      });
    });
  </script>
@endsection

@extends('frontend.layouts.layout_profile')

@section('content_div')




    <div class="col-lg-12" style="padding-left:0px;" id="list-trx">
          <!--for mulai sini!-->
          @forelse($orders as $key => $value)
            <div class="transaction">
              <div class="test" style="border-bottom: 1px solid #e6e6e6bf;">
                  <div class="padding" style="display:inline"><img src="https://cdn2.iconfinder.com/data/icons/online-shopping-flat-round/550/store-512.png" alt="" style="width: 25px;height: 25px;"></div>
                  <div class="padding"><p>rani-shops</p></div>
                  <div class="padding" style="text-align:right;width: 75%;"><p style="color: #0e8ce4;"></p></div>
              </div>
              <div class="col-md-12" style="padding: 20px 0px 20px 0px;">
                <div class="row" style="margin-left:0px;">
                  <style type="text/css">
                    table tr#abu  {background-color:#f2f2f2;}
                    table td{border:solid 5px;
                             border-right-color:transparent;
                             border-bottom-color:#ffffff;
                             border-left-color:transparent}}
                  </style>
                  <table class="table">
                  @php
                  $total_canceled = 0;
                  @endphp
                  @foreach($value->orderItems as $item)
                  @if(isset($canceled))
                    @if($item->pivot->delivery_status !== 4 )
                      @php

                      continue;
                      @endphp
                    @endif
                  @endif
                  <tr id="abu">
                    <td>
                  <div style="padding-left:0px;">

                    <img src="{{asset('')}}images/{{json_decode($item->image)[0]}}" alt="" style="height: 60px;">
                  </div>
                  </td>
                  <td>



                    <p>{{$item->name}}  </p>@if($item->pivot->delivery_status == 4 )<b style="color:red">CANCELED</b>@php $total_canceled += $item->price; @endphp @endif
                </td>
                <td>
                  <div style="padding-left:5px; padding-right:30px">

                    <p>Rp.&nbsp;{{number_format($item->pivot->total)}}<br>X{{$item->pivot->qty}} </p>
                  </div>
                </td>
                </tr>
                    @endforeach
                  </table>
                </div>
              </div>
              <div class="col-md-12" style="padding: 20px 20px 20px 0px;text-align:right">
                @if(isset($canceled))
                <p>Total Pesanan : <span style="color: #0e8ce4;font-size: 20px;font-weight: 500;">Rp. {{number_format($total_canceled)}}</span></p>
                @else
                <p>Total Pesanan : <span style="color: #0e8ce4;font-size: 20px;font-weight: 500;">Rp. {{number_format($value->total)}}</span></p>
                @endif
                <input hidden id="subtotal-{{$value->id}}" value="{{number_format($value->total)}}">
                <button type="button" id="btn-detail" class="btn btn-link-detail" data-id="{{$value->id}}" style="border: 1px solid #d8d8d8f2;color: #9a9a9a;font-size: 13px;">Tampilkan Rincian Transaksi</button>
              </div>
            </div>
          <!--endfor!-->
          <!--if bila gk ada trx!-->
          @empty
          <div class="transaction">
            <div style="text-align:center;background: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT17nTCmPqlIbqP8DTBnQAUdYXSIjJgFglY66qmgAnY-PGeGIWA') no-repeat center center;height: 290px;">
              <p>Belum Ada Transaksi</p>
            </div>
          </div>
          @endforelse
    </div>
  <!-- </div>? -->
@endsection
