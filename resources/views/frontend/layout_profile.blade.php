@extends('frontend.layout_page')

@section('someCSS')
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/product_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/contact_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/contact_responsive.css')}}">

  <style>
    .cart_item_name {
      margin-left: 0%;
    }
    .alamat_name {
      font-size: 12px;
      font-weight: 500;
      color: rgba(0, 0, 0, 0.82);
    }
    .alamat_location {
      font-size: 12px;
      font-weight: 400;
      color: rgba(0,0,0,0.5);
    }
    .telp_padding{
      padding-top: 15px;
    }
    .float_right{
      float: right;
    }
    .cart_padding {
        padding-right: 15px;
    }
    .width_100{
        width: 100%;
    }
    .margin_0{
      margin-top: 0px;
    }
    .cart_button_checkout{
      font-size: 14px;
    }
    .cart_item_image {
      width: 65px;
      height: 65px;
      margin-right: 10px;
    }
    .price{
      font-size: 15px;
      color: #0e8ce4;
    }
    .qty{
      font-size: 12px;
      font-weight: 400;
    }
    .name{
      font-size: 13px;
    }
    .no_padding{
      padding-left: 0px;
      padding-right: 0px;
      padding-top: 10px;
    }
    .list-dropdown{
      padding-left: 5px;
      padding-right: 5px;
    }
    .menu-dropdown{
      font-size: 12px;
    }
    .padding-top-10{
      padding-top: 10px;
    }
    .order_total_title{
      line-height: 25px;
    }
    .order_total_amount{
      line-height: 25px;
    }
    .margin-left-0{
        margin-left: 0px;
    }
    .padding-top-30{
      padding-top: 30px;
    }
    .margin-top-15{
      margin-top: 15px;
    }

    .shop_sidebar {
        width: auto;
        transform: translateX(0px);
    }
    .sidebar_categories {
        margin-top: 0px;
    }
    .list-name{
      font-weight: 500;
      font-size: 15px;
      color: #0e8ce4;\;
    }
    .margin-top-botton-10{
      margin-top: 10px;
      margin-bottom: 10px;
    }
    .cart_item_title{
      font-weight: 500;
      font-size: 16px;
      color: rgb(14, 140, 228);
    }
    .cart_items {
      margin-top: 39px;
    }
    .name-title {
      color: #727577;
      margin-bottom: 10px;
    }
    .btn-foto{
      background: #b5b6b7ed;
    }
    .desc-foto{
      font-size: 11px;
      line-height: 1.4;
    }
  	.hover-active{
  		color: #0e8ce4 !important;
  	}
    .cart_item{
      padding-right: 0px;
      padding: 15px !important;
    }
    .btn-toko{
      width: 100%;
      margin-bottom: 10px;
      text-align: center;
    }
  </style>
  @yield('css')
@endsection

@section('someJS')
  <script src="{{asset('js/product_custom.js')}}"></script>
@endsection

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="cart_container">
            <div class="row col-md-12">
              <div class="col-md-3">
                <div class="cart_items">
                  <ul class="cart_list">
                    <li class="cart_item clearfix">
                      <div class="d-flex flex-md-row flex-column justify-content-between">
                        <div class="cart_item_name cart_info_col width_100 padding-top-10">
                          <div class="shop_sidebar">
                						<div class="sidebar_section">
                              @if(empty(App\Shop::whereUserId(Auth::user()->id)->first()))
                                <a href="{{ url('create/merchant') }}" class="button cart_button_checkout btn-toko">Buat Merchant</a>
                              @endif
                              <p class="list-name">Kotak Masuk</p>
                							<ul class="sidebar_categories">
                								<li><a href="#" class="@yield('diskusi')">Diskusi</a></li>
                								<li><a href="#" class="@yield('ulasan')">Ulasan</a></li>
                							</ul>
                              <p class="list-name">Toko Saya</p>
                              <ul class="sidebar_categories">
                              @if(App\Shop::whereUserId(Auth::user()->id)->first())
                                @if(App\Shop::whereUserId(Auth::user()->id)->first()->status == '1')
                                    <li><a href="{{ route('products.create') }}" class="@yield('product_add')">Jual Barang</a></li>
                                    <li><a href="{{ url('products') }}" class="@yield('product')">Daftar Barang</a></li>
                                    <li><a href="#" class="@yield('ulasan')">Barang Terjual</a></li>
                                @else
                                    <li><span class="button cart_button_checkout" style="background:#ff9207d4;">Menunggu Konfirmasi</span></li>
                                @endif
                              @endif
                              </ul>
                              <p class="list-name margin-top-botton-10">Pembelian</p>
                							<ul class="sidebar_categories">
                								<li><a href="#" class="@yield('pending_pay')">Menunggu Pembayaran</a></li>
                								<li><a href="#" class="@yield('daftar_trx')">Daftar Transaksi</a></li>
                							</ul>
                              <p class="list-name margin-top-botton-10">Profile Saya</p>
                							<ul class="sidebar_categories">
                								<li><a href="{{ url('/biodata') }}" class="@yield('biodata')">Biodata Diri</a></li>
                  							<li><a href="{{ url('/alamat') }}" class="@yield('alamat')">Alamat</a></li>
                							</ul>
                						</div>
                					</div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-md-9">
                <div class="cart_items">
                  <ul class="cart_list">
                    <li class="cart_item clearfix cart_padding">
                      @yield('content_div')
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
