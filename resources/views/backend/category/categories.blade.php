@extends('backend.includes.header')

@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <h3 class="pull-left">{{ $parent == null ? '' : 'Subkategori dari kategori ' .$parent->title }}</h3>
              <a href="{{ route('categories.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Create</a>
              <div class="clearfix">
              </div>
            </div>
            <div class="x_content">
              <div class="">
                <form id="bulk-delete-form" action="" method="POST">
                  <input type="hidden" name="_method" value="DELETE">
                  <table class="table table-striped jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th>
                          <input type="checkbox" id="check-all" class="flat">
                        </th>
                        <th class="column-title">Title </th>
                        <th class="column-title">Slug </th>
                        <!-- <th class="column-title">Total Movie(s)</th> -->
                        <th class="column-title">Modified</th>

                        <th class="column-title no-link last" style="text-align: center;width: 30%" colspan="3"><span class="nobr">Action</span>
                        </th>
                        <th class="bulk-actions" colspan="8">
                          <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <button class="badge bg-red" id="bulk-delete-button"><i class="fa fa-trash"></i></button></a>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      @if (!count($categories))
                        <tr class="even pointer">
                          <td colspan="8">Tidak ada data</td>
                        </tr>
                      @endif
                      @foreach ($categories as $key => $category)
                        <tr class="even pointer">
                          <td class="a-center ">
                            <input type="checkbox" class="flat" name="table_records" value="{{ $category->id_category }}">
                          </td>
                          <td class=" ">{{ $category->title }}</td>
                          <td class=" ">/{{ ($category->parent_category->count()) ? $category->parent_category->first()->slug.'/' : '' }}{{ $category->slug }}</td>

                          <td class=" ">{{ $category->updated_at->diffForHumans() }}</td>
                          <td style="text-align: center;"><a href="{{ route('categories.edit', $category->id_category) }}">Edit</a></td>
                          <td style="text-align: center;"><a href="{{ route('categories.destroy', $category->id_category) }}" class="delete-menu">Delete</a></td>
                          <td  style="text-align: center;" class=" ">{!! $category->id_parent =='' ? '<a href="' . route("categories.query", $category->id_category) . '">Lihat Subkategori</a>' : '' !!}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </form>
              </div>
              {{ $categories->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-delete-menu" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Delete Category</h4>
        </div>
        <div class="modal-body">
          <p>Apakah kamu yakin ingin menghapus category ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-danger" id="delete-menu-modal-button" data-url=""><span class="fa fa-trash"></span> Delete Category</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-bulk-delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Delete Category</h4>
        </div>
        <div class="modal-body">
          <p>Apakah kamu yakin ingin menghapus category yang ditandai?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-danger" id="bulk-delete-modal-button" data-url=""><span class="fa fa-trash"></span> Delete Category</button>
        </div>
      </div>
    </div>
  </div>
  <form id="delete-menu-form" action="" method="post">
    <input type="hidden" name="_method" value="DELETE">
  </form>
  <script type="text/javascript">
    var DELETE_SUCCESS_LINK = DELETE_MENU_SUCCESS_LINK = "{{ route('categories.index') }}";
  </script>
@endsection
