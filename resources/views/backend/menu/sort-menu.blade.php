@extends('backend.includes.header')

@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{ route('admin.menu.index') }}" class="btn btn-default pull-left"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
              <a href="{{ route('admin.menu.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Create</a>
              <div class="clearfix">
              </div>
            </div>
            <div class="x_content">
              <div class="col-md-4">
                <table class="table jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th class="column-title">Title </th>
                      <th class="column-title no-link last" width="100px"><span class="nobr">Action</span>
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach ($types as $key => $value)
                      <tr>
                        <td>{{ ucwords($value) }} Menu</td>
                        <td><a href="{{ route('admin.menu.sort_form', $value) }}">Edit Sorting</a></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <div class="col-md-8">
                <h2>Sort {{ ucwords($type) }} Menu</h2>
                <div class="dd">
                  <ol class="dd-list">
                    @foreach ($menus as $key => $val)
                      @php
                        $menu = App\Menu::find($val['id']);
                      @endphp
                      <li class="dd-item" data-id="{{ $menu->id_menu }}">
                        <div class="dd-handle" data>{{ $menu->title }}</div>
                        @if (is_array(@$val['children']))
                          <ol class="dd-list">
                          @foreach ($val['children'] as $key2 => $val2)
                            @php
                              $submenu = App\Menu::find($val2['id']);
                            @endphp
                            <li class="dd-item" data-id="{{ $submenu->id_menu }}">
                              <div class="dd-handle">{{ $submenu->title }}</div>
                              @if (is_array(@$val2['children']))
                                <ol class="dd-list">
                                @foreach ($val2['children'] as $key3 => $val3)
                                  @php
                                    $submenu2 = App\Menu::find($val3['id']);
                                  @endphp
                                  <li class="dd-item" data-id="{{ $submenu2->id_menu }}">
                                    <div class="dd-handle">{{ $submenu2->title }}</div>
                                  </li>
                                @endforeach
                              </ol>
                              @endif
                            </li>
                          @endforeach
                        </ol>
                        @endif
                      </li>
                    @endforeach
                  </ol>
                </div>
              </div>
              <div class="col-md-12">
                <div class="pull-right">
                  <a href="{{ route('admin.menu.create') }}" id="save_menu_order" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Save</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <form id="delete-menu-form" action="{{ route('admin.menu.sort', $type) }}" method="post">
    <input type="hidden" name="json" value="">
  </form>
  <script type="text/javascript">
    var DELETE_MENU_SUCCESS_LINK = "{{ route('admin.menu.index') }}";
  </script>
@endsection
