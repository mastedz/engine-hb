<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            
            <li>
                <a href="/dashboard">
                    <i class="fa fa-home"></i>Dashboard</a>
            </li>
            <li>
               <a href="/merchant">
                    <i class="fa fa-shopping-bag"></i>Merchant Approval</a>
            </li>
            <li>
               <a href="/categories">
                    <i class="fa fa-list"></i>Category</a>
            </li>
            <li>
               <a href="{{ route('admin.menu.index') }}">
                    <i class="fa fa-cog"></i>Settings</a>
            </li>
            <li>
               <a href="{{ route('admin.page.index') }}">
                    <i class="fa fa-file-o"></i>Page</a>
            </li>
            
               
                
           
        </ul>
    </div>
</div>
<!-- /sidebar menu -->