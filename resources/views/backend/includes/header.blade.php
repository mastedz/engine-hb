<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="omdb-api-key" content="{{ config('OMDB_API_KEY') }}">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ env('APP_NAME') }} - @yield('title')</title>

  <!-- jquery-ui -->
  <link href="{{ asset('vendors/jquery-ui/themes/base/jquery-ui.css') }}" rel="stylesheet">
  <!-- lightbox2 -->
  <link href="{{ asset('vendors/lightbox2/dist/css/lightbox.css') }}" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
  <!-- iCheck -->
  <link href="{{ asset('vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
  @yield('css')
  <!-- bootstrap-progressbar -->
  <link href="{{ asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
  <!-- JQVMap -->
  <link href="{{ asset('vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet"/>
  <!-- bootstrap-daterangepicker -->
  <link href="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- PNotify -->
  <link href="{{ asset('vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">
  <!-- bootstrap-tokenfield -->
  <link href="{{ asset('vendors/bootstrap-tokenfield/dist/css/tokenfield-typeahead.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/bootstrap-tokenfield/dist/css/bootstrap-tokenfield.css') }}" rel="stylesheet">
  <!-- Custom Theme Style -->
  <link href=" {{ asset('back/app.css') }}" rel="stylesheet">
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="/admin" class="site_title"><img src="{{ asset('/images/logo.png') }}" alt="" style="height: 25px;"> <small>Hari Belanja</small></a>
          </div>
          <div class="clearfix"></div>
          @include('backend.includes.side-nav')
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <nav>
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  {{ Auth::user()->name }}
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">

                  <li><a href="#" id="submitLogout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  <form id="formLogout" action="{{ route('logout') }}" method="post">
                    {{ csrf_field() }}
                  </form>

                </ul>
              </li>


            </ul>
          </nav>
        </div>
      </div>
      <!-- /top navigation -->

      @yield('content')

      <!-- footer content -->
      <footer>
        <div class="pull-right">
          Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
    </div>
  </div>
  <!-- jQuery -->
  <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
  <!-- lightbox -->
  <script src="{{ asset('vendors/lightbox2/dist/js/lightbox.min.js') }}"></script>
  <!-- jquery-ui -->
  <script src="{{ asset('vendors/jquery-ui/jquery-ui.min.js') }}"></script>
  <!-- Bootstrap -->
  <script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('vendors/fastclick/lib/fastclick.js') }}"></script>
  <!-- NProgress -->
  <script src="{{ asset('vendors/nprogress/nprogress.js') }}"></script>
  <!-- Chart.js -->
  <script src="{{ asset('vendors/Chart.js/dist/Chart.min.js') }}"></script>
  <!-- gauge.js -->
  <script src="{{ asset('vendors/gauge.js/dist/gauge.min.js') }}"></script>
  <!-- jQuery Sparklines -->
  <script src="{{ asset('vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
  <!-- easy-pie-chart -->
  <script src="{{ asset('vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
  <!-- bootstrap-progressbar -->
  <script src="{{ asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
  <!-- iCheck -->
  <script src="{{ asset('vendors/iCheck/icheck.min.js') }}"></script>
  <!-- Skycons -->
  <script src="{{ asset('vendors/skycons/skycons.js') }}"></script>
  <!-- Flot -->
  <script src="{{ asset('vendors/Flot/jquery.flot.js') }}"></script>
  <script src="{{ asset('vendors/Flot/jquery.flot.pie.js') }}"></script>
  <script src="{{ asset('vendors/Flot/jquery.flot.time.js') }}"></script>
  <script src="{{ asset('vendors/Flot/jquery.flot.stack.js') }}"></script>
  <script src="{{ asset('vendors/Flot/jquery.flot.resize.js') }}"></script>
  <!-- Flot plugins -->
  <script src="{{ asset('vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
  <script src="{{ asset('vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
  <script src="{{ asset('vendors/flot.curvedlines/curvedLines.js') }}"></script>
  <!-- DateJS -->
  <script src="{{ asset('vendors/DateJS/build/date.js') }}"></script>

  <!-- JQVMap -->
  <script src="{{ asset('vendors/jqvmap/dist/jquery.vmap.js') }}"></script>
  <script src="{{ asset('vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
  <script src="{{ asset('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
  <!-- bootstrap-daterangepicker -->
  <script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

  <!-- PNotify -->
  <script src="{{ asset('vendors/pnotify/dist/pnotify.js') }}"></script>
  <script src="{{ asset('vendors/pnotify/dist/pnotify.buttons.js') }}"></script>

  <!-- tinymce -->

   <script src="{{ asset('vendors/tinymce/tinymce.min.js') }}"></script>

  <!-- Nestable -->
  <script src="{{ asset('vendors/jquery-nestable/jquery.nestable.js') }}"></script>
  <!-- Custom Theme Scripts -->

  <!-- bootstrap-tokenfield-->
  <script src="{{ asset('vendors/bootstrap-tokenfield/dist/bootstrap-tokenfield.js') }}"></script>

  <!-- ajaxq -->
  <script src="{{ asset('vendors/ajaxq/ajaxq.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.min.js"></script>
  <script src="{{ mix('js/admin.js') }}"></script>


  @yield('script')

</body>
</html>
