@extends('backend.includes.header')

@section('content')
  <div class="right_col" role="main">
    
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{ route('admin.page.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Create</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="table-responsive" style="overflow-y:hidden;">
                <form id="bulk-delete-form" action="{{ route('admin.page.bulkdelete') }}" method="POST">
                  <input type="hidden" name="_method" value="DELETE">
                  <table class="table table-striped jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th>
                          <input type="checkbox" id="check-all" class="flat">
                        </th>
                        <th class="column-title">Title </th>
                        <th class="column-title">Slug </th>
                        <th class="column-title">Author </th>
                        <th class="column-title">Modified</th>
                        <th class="column-title no-link last"  style="text-align: center;" colspan="2"><span class="nobr">Action</span>
                        </th>
                        <th class="bulk-actions" colspan="7">
                          <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <button class="badge bg-red" id="bulk-delete-button"><i class="fa fa-trash"></i></button></a>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      @if (!count($pages))
                        <tr class="even pointer">
                          <td colspan="7">Tidak ada data</td>
                        </tr>
                      @endif
                      @foreach ($pages as $key => $page)
                        <tr class="even pointer">
                          <td class="a-center ">
                            <input type="checkbox" class="flat" name="table_records" value="{{ $page->id_page }}">
                          </td>
                          <td class=" ">{{ $page->title }}</td>
                          <td class=" ">/{{ $page->slug }}</td>
                          <td class=" ">{{ ($page->user === null) ? 'Unknown' : $page->user->name }}</td>
                          <td class=" ">{{ $page->updated_at->diffForHumans() }}</td>
                          <td style="text-align: center;">
                            <a href="{{ route('admin.page.edit', $page->id_page) }}">Edit</a></td>
                          <td style="text-align: center;" class=" ">
                            <a href="{{ route('admin.page.destroy', $page->id_page) }}" class="delete-menu">Delete</a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-delete-menu" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Delete Page</h4>
        </div>
        <div class="modal-body">
          <p>Apakah kamu yakin ingin menghapus page ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-danger" id="delete-menu-modal-button" data-url=""><span class="fa fa-trash"></span> Delete Page</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-bulk-delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Delete Page</h4>
        </div>
        <div class="modal-body">
          <p>Apakah kamu yakin ingin menghapus page yang ditandai?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-danger" id="bulk-delete-modal-button" data-url=""><span class="fa fa-trash"></span> Delete Page</button>
        </div>
      </div>
    </div>
  </div>
  <form id="delete-menu-form" action="" method="post">
    <input type="hidden" name="_method" value="DELETE">
  </form>
  <script type="text/javascript">
    var DELETE_SUCCESS_LINK = DELETE_MENU_SUCCESS_LINK = "{{ route('admin.page.index') }}";
  </script>
@endsection
