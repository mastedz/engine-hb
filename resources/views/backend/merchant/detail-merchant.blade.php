@extends('backend.includes.header')

@section('content')
  <div class="right_col" role="main">

    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <h3>Deatail Merchant</h3>
              <div class="clearfix">
              </div>
            </div>


              <div class="x_content">


                    <div class="row">

                      <h4>Dokumen Lampiran</h4>

                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <a class="example-image-link" href="{{asset('storage/images/' .$files['ktp']) }}" data-lightbox="example-set" data-title="Product.">
                            <img style="width: 100%; display: block;" data-lightbox="roadtrip" src="{{asset('storage/images/' .$files['ktp']) }}" alt="image">
                          </a>
                          </div>
                          <div class="caption">
                            <p>KTP</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                              <a class="example-image-link" href="{{asset('storage/images/' .$files['npwp']) }}" data-lightbox="example-set" data-title="Npwp.">
                                <img style="width: 100%; display: block;" data-lightbox="roadtrip" src="{{asset('storage/images/' .$files['npwp']) }}" alt="image">
                            </a>
                          </div>
                          <div class="caption">
                            <p>NPWP</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                              <a class="example-image-link" href="{{asset('storage/images/' .$files['logo']) }}" data-lightbox="example-set" data-title="LOGO.">
                            <img style="width: 100%; display: block;" data-lightbox="roadtrip" src="{{asset('storage/images/' .$files['logo']) }}" alt="image">
                          </a>
                          </div>
                          <div class="caption">
                            <p>LOGO</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <a class="example-image-link" href="{{asset('storage/images/' .$files['product']) }}" data-lightbox="example-set" data-title="Product.">
                            <img style="width: 100%; display: block;" data-lightbox="roadtrip" src="{{asset('storage/images/' .$files['product']) }}" alt="image">
                          </a>
                          </div>
                          <div class="caption">
                            <p>Contoh Product</p>
                          </div>
                        </div>
                      </div>
                      </div>

                      <div class="table-responsive">
                        <div class="col-md-9">
                          <div class="col-md-6">
                            <h4>Nama Merchant:</h4>
                            <h4>{{$merchant->shop()->first()->name}}
                          </div>
                           <div class="col-md-6">
                            <h4>Alamat Merchant:</h4>
                            <h4>{{$merchant->shop()->first()->shop_address}}
                          </div>
                           <div class="col-md-6">
                            <h4>Pemilik Merchant:</h4>
                            <h4>{{$merchant->user()->first()->name}}
                          </div>
                          <div class="col-md-6" style="padding-top:25px">
                            <h2>Status Approval:</h2>
                            @if($merchant->shop->status == 0)
                            <b>Need Approve</b>
                            @elseif($merchant->shop->status == 1)
                            <b >Alredy Approve</b>
                            @else
                            <b style="color:red">Denied</b>
                            @endif
                          </div>
                          @if($merchant->shop->status == 2)
                          <div class="col-md-6" style="padding-top:25px">
                            <h2>Denial Reason:</h2>
                            <p>{{$merchant->shop()->first()->note}}</p>
                          </div>
                          @endif
                          <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top:25px">
                            <h2>Deskripsi Merchant:</h2>
                            <p>{{$merchant->shop()->first()->description}}</p>
                          </div>

                        </div>


                    </div>

              @if($merchant->shop->status == 0 || $merchant->shop->status == 2)
              <form class="text-right" action="{{url('approveMerchant')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="post">
                <input type="hidden" name="id" value="{{$merchant->shop_id}}">

                <input type="submit" name="status" value="Approve" class="btn btn-primary">


              @else
                <button type="button" disabled class="btn btn-success">Alredy Approve</button>
              @endif
              @if($merchant->shop->status != 2 )
              <button data-toggle="modal" data-target="#exampleModal" type="button" class="btn btn-danger">Deny/Block</button>
              @endif
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- MODAL -->
       <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
           <form class="" action="{{url('approveMerchant')}}" method="post">
           <div class="modal-content">
             <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Denial Reason</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
             <div class="modal-body">
              <label>Reason</label>
              <textarea class="form-control" rows="5" name="note">

              </textarea>
                {{csrf_field()}}
             </div>
             <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-danger">Submit Denial</button>
             </div>
           </div>
           <input type="hidden" name="_method" value="post">

           <input type="hidden" name="id" value="{{$merchant->shop_id}}">
           <input type="hidden" name="status" value="Deny">


         </form>

         </div>
       </div>

@endsection
@section('script')
<script>
lightbox.option({
  'resizeDuration': 500,
  'wrapAround': true
})
</script>
@endsection
