@extends('backend.includes.header')

@section('content')
<!-- page content -->
  <div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count top_tiles">
      <div class="col-md-3 col-sm-4 col-xs-6 tile">
        <span class="count_top"><i class="fa fa-user" aria-hidden="true"></i> Total User</span>
        <h2>{{$data['user']}}</h2>
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6 tile">
        <span class="count_top"><i class="fa fa-shopping-bag"></i> Total Merchant</span>
        <h2>{{$data['shops']}}</h2>
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6 tile">
        <span class="count_top red"><i class="fa fa-shopping-bag red"></i>Pending Merchant</span>
        <h2 class="red">{{$data['pending_approve']}}</h2>
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6 tile">
        <span class="count_top"><i class="fa fa-exclamation-money-bill "></i> Total Transaction</span>
        <h2 class="">{{$data['transaction']}}</h2>
      </div>

    </div>
    <canvas id="userdata-chart" style="height: 250px"></canvas>
    <!-- /top tiles -->
    <div class="row">
      <div class="col-md-12" id="user-chart" style="display: none">
        <div class="x_panel">
          <div class="row" style="margin-bottom: 10px">


        </div>
      </div>
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>User list</h2>
            <span class="pull-right"><a class="view-user-statistics" data-id="0" href="javascript:void(0)">View All User Statistics</a></span>
            <div class="clearfix">
            </div>
          </div>
          <div class="x_content">
            <div class="table-responsive">
              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                    <th class="column-title">Author </th>
                    <th class="column-title">Total Movies </th>
                    <th class="column-title">Total Series </th>
                    <th class="column-title">Total Episodes </th>
                    <th class="column-title">Total Error </th>
                    <th class="column-title">Role </th>
                    <th class="column-title">Option </th>
                  </tr>
                </thead>

                <tbody>

                    <tr class="even pointer">
                      <td class=" ">Frans</td>
                      <td class=" "><a href="">4</a> </td>
                      <td class=" "><a href="">4</a> </td>
                      <td class=" ">4</td>
                      <td class=" ">4</td>
                      <td class=" ">4</td>
                      <td><a class="view-user-statistics" data-id="" href="javascript:void(0)">View Statistics</a> </td>
                    </tr>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

    @endsection
    @section('script')
    <script type="text/javascript">
    var url = "{{url('stats')}}";
    var date = new Array();
    var count = new Array();
    var count_trans = new Array();
    var count_merchant = new Array();
    $(document).ready(function(){
      $.get(url, function(response){
        var user = response.user;
        var merchant = response.merchant;
        var transaction = response.transaction;
        user.forEach(function(data){
          date.push(data.d);

          count.push(data.c);
        });
        transaction.forEach(function(data){
          date.push(data.d);

          count_trans.push(data.c);
        });
        merchant.forEach(function(data){
          date.push(data.d);

          count_merchant.push(data.c);
        });

      console.log(count_merchant, count_trans);
    var options = {
    type: 'line',
    data: {
      labels: date,
      datasets: [
  	    {
  	      label: 'User Join Stats',
          borderColor: "#2a3f54",

        fill:false,
         pointBackgroundColor: "#2a3f54",
         pointBorderColor: "#2a3f54",
         pointHoverBackgroundColor: "#2a3f54",
         pointHoverBorderColor: "#2a3f54",
  	      data: count,
        	borderWidth: 1
      	},
        {
  	      label: 'Transaction Stats',
          borderColor: "#e74c3c",

        fill:false,
         pointBackgroundColor: "#e74c3c",
         pointBorderColor: "#e74c3c",
         pointHoverBackgroundColor: "#e74c3c",
         pointHoverBorderColor: "#e74c3c",
  	      data: count_trans,
        	borderWidth: 1
      	},
        {
  	      label: 'Merchant Join Stats',
          borderColor: "#0f3fff",

        fill:false,
         pointBackgroundColor: "#0f3fff",
         pointBorderColor: "#0f3fff",
         pointHoverBackgroundColor: "#0f3fff",
         pointHoverBorderColor: "#0f3fff",
  	      data: count_merchant,
        	borderWidth: 1
      	},

  		]
    },
    options: {
    	scales: {
      	yAxes: [{
          ticks: {
            userCallback: function(label, index, labels) {
                    // when the floored value is the same as the value we have a whole number
                    if (Math.floor(label) === label) {
                        return label;
                    }

                },
            beginAtZero: true,
  					reverse: false
          }
        }]
      }
    }
  }

  var ctx = document.getElementById('userdata-chart').getContext('2d');
  new Chart(ctx, options);
  });
  });
    </script>
    @endsection
