<html lang="en-US"><head>
    <title>
      hariBelanja - Home
    </title>
    <meta name="google-site-verification" content="B8aG5xnWNlNmH0mnfyEV-arce8R428ZJzo4XGGerqU4">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="OneTech shop project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('styles/bootstrap4/bootstrap.min.css')}}">
    <link href="{{asset('plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/slick-1.8.0/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('styles/main_styles.css?version=1')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('styles/responsive.css')}}">
    <link href="{{ asset('vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">
      <link href="{{ asset('vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/jquery-ui/themes/base/jquery-ui.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css">

    <style>
    	.ml-left {
    		margin-left: 18%!important;
    	}
    	.deals_item_name {
    		font-size: 18px;
    	}
    	.deals_item_price {
    		font-size: 20px;
    	}
    	.best_sellers {
        padding-bottom: 0px;
    }
    </style>
<style>
	.ml-left {
		margin-left: 18%!important;
	}
	.deals_item_name {
		font-size: 18px;
	}
	.deals_item_price {
		font-size: 20px;
	}
	.best_sellers {
    padding-bottom: 0px;
}
</style>

</head>

<body style="background-image:url(images/banner_background.jpg); background-repeat:no-repeat;">
  <div class="notifications top-right"></div>

  <div class="super_container">
    <header class="header">

  <!-- Top Bar -->

  <div class="top_bar">
    <div class="container">
      <div class="row">
        <div class="col d-flex flex-row">
          <div class="top_bar_content ml-auto">
            <div class="top_bar_menu">
              <ul class="standard_dropdown top_bar_dropdown">
                <li>

                </li>
              </ul>
            </div>
            <div class="top_bar_user">

                          </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Header Main -->

  <div class="header_main">
    <div class="container">
      <div class="row">

        <!-- Logo -->
        <div class="col-lg-4 col-sm-3 col-3 order-1">
          <div class="logo_container">
            <div class="logo"><a href="/"><img width="250px"src="{{asset('')}}images/bell.png"></a></div>
          </div>
        </div>

        <!-- Search -->

        <!-- Wishlist -->
        <div class="col-lg-2 col-9 order-lg-3 order-2 text-lg-left text-right">
          <div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
            <!-- <div class="wishlist d-flex flex-row align-items-center justify-content-end">
              <div class="wishlist_icon"><img src="images/heart.png" alt=""></div>
              <div class="wishlist_content">
                <div class="wishlist_text"><a href="#">Wishlist</a></div>
                <div class="wishlist_count">15</div>
              </div>
            </div> -->

            <!-- Cart -->

          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Main Navigation -->


  <!-- Menu -->

  <div class="page_menu">
    <div class="container">
      <div class="row">
        <div class="col">

          <div class="page_menu_content">

            <div class="page_menu_search">
              <form action="#">
                <input type="search" required="required" class="page_menu_search_input" placeholder="Search for products...">
              </form>
            </div>


            <div class="menu_contact">
              <div class="menu_contact_item"><div class="menu_contact_icon"><img src="images/phone_white.png" alt=""></div>082245326737</div>
              <div class="menu_contact_item"><div class="menu_contact_icon"><img src="images/mail_white.png" alt=""></div><a >haribelanja@gmail.com</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</header>


<div class="banner">
<div  class="banner_background" ></div>
<div class="container ">
  <div class="row ">

    <div style="margin-top:40px;" class="col-lg-12 offset-lg-4 ">
      <div class="banner_content">
        <h1 class="banner_text">Coming Soon</h1>

      </div>
    </div>
  </div>
</div>
</div>
	<!-- Characteristics -->

	<div style="padding-top: 142px;" class="characteristics">
		<div class="container">
			<div class="row">

				<!-- Char. Item -->
				<div class="col-lg-4 col-md-6 char_col">

					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_1.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Jasa Pengiriman</div>
							<div class="char_subtitle">Pilihan jasa pengiriman dengan jangkauan nasional</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-4 col-md-6 char_col">

					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_2.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Customer Support</div>
							<div class="char_subtitle">CS siap membantu Anda melalui e-mail dan call center (0822-4532-6737)</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-4 col-md-6 char_col">

					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_3.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Pembayaran</div>
							<div class="char_subtitle">Menyediakan metode pembayaran untuk bertransaksi</div>
						</div>
					</div>
				</div>

				<!-- Char. Item
				<div class="col-lg-3 col-md-6 char_col">

					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_4.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Pembayaran</div>
							<div class="char_subtitle">Menyediakan metode pembayaran untuk bertransaksi</div>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div>

	<!-- Deals of the week -->





  </div>




  <script type="text/javascript">
    var CREATE_LOGIN_SUCCESS_LINK= "https://haribelanja.com";
    var CREATE_SIGNUP_SUCCESS_LINK="https://haribelanja.com";

    </script>


</body></html>
