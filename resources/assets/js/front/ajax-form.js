(function ($) {
    var _sending_ajax = false;
    $.fn.ajaxForm = function(options) {

      var defaults = {
        beforeSubmit: function() {},
        success: function () {},
        error: function () {},
        beforeSend: function () {},
        uploadProgress: function () {}

      };

      var options = $.extend(defaults, options);

      $(this).on('submit', function(event) {
        event.preventDefault();
        options.beforeSubmit(event);
        if(_sending_ajax)
          return;
        _sending_ajax = true;

        var
          that = this,
          type = $(that).attr("form-type"),
          upload = type == "upload",
          data = upload ? new FormData(this) : $(this).serialize()
        ;
        $.ajax({
          url: $(that).attr('action'),
          type: $(that).attr('method'),
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: data,
          dataType: $(that).attr('data-form-type'),
          contentType: upload ? false : "application/x-www-form-urlencoded",
          processData: !upload,
          beforeSend: function() {
            $(that).find('button').attr('disabled', 'disabled');
            $('.error-explanation').each(function(index, el) {
              $(el).remove();
            });
          }
        })
        .done(function(e) {
            console.log(e);
          if(typeof e !== "undefined" && e.status == "success") {
            (new PNotify({
              title: e.title,
                text: e.message,
                type: 'info',
                styling: 'bootstrap3'
              }));
            _sending_ajax = true;
            options.success(e);
          } else {
            options.error(e);
          }
        })
        .fail(function(e) {
          var json = e.responseJSON;
          if(json != null)
          {
            var
              message = json.message,
              errors = json.errors
            ;
            (new PNotify({
                title: "Error",
                text: message,
                type: 'warning',
                styling: 'bootstrap3'
            }));

            $(that).find('button').removeAttr('disabled');
            $.each(errors, function(index, data) {
                var indexarray = index.split(".", 2);
                if( 1 in indexarray){
                  $("."+indexarray[0]).eq(parseInt(indexarray[1])).after('<div class="text-danger error-explanation"><span class="fa fa-warning"></span> ' + data + '</div>');
                }else{
                    $("input[name="+ index +"]").after('<div class="text-danger error-explanation"><span class="fa fa-warning"></span> ' + data + '</div>');
                    $("textarea[name="+ index +"]").after('<div class="text-danger error-explanation"><span class="fa fa-warning"></span> ' + data + '</div>');
                    $("select[name="+ index +"]").after('<div class="text-danger error-explanation"><span class="fa fa-warning"></span> ' + data + '</div>');
                }

            });
          }
          options.error(e);
        })
        .always(function() {
            _sending_ajax = false;
            $(that).find('button').removeAttr('disabled');
        });
      });
    };
}(jQuery));
