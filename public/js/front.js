/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/process/browser.js":
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/setimmediate/setImmediate.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js"), __webpack_require__("./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/timers-browserify/main.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var scope = (typeof global !== "undefined" && global) ||
            (typeof self !== "undefined" && self) ||
            window;
var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, scope, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, scope, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(scope, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__("./node_modules/setimmediate/setImmediate.js");
// On some exotic environments, it's not clear which object `setimmediate` was
// able to install onto.  Search each possibility in the same order as the
// `setimmediate` library.
exports.setImmediate = (typeof self !== "undefined" && self.setImmediate) ||
                       (typeof global !== "undefined" && global.setImmediate) ||
                       (this && this.setImmediate);
exports.clearImmediate = (typeof self !== "undefined" && self.clearImmediate) ||
                         (typeof global !== "undefined" && global.clearImmediate) ||
                         (this && this.clearImmediate);

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./resources/assets/js/front/ajax-form.js":
/***/ (function(module, exports) {

(function ($) {
  var _sending_ajax = false;
  $.fn.ajaxForm = function (options) {

    var defaults = {
      beforeSubmit: function beforeSubmit() {},
      success: function success() {},
      error: function error() {},
      beforeSend: function beforeSend() {},
      uploadProgress: function uploadProgress() {}

    };

    var options = $.extend(defaults, options);

    $(this).on('submit', function (event) {
      event.preventDefault();
      options.beforeSubmit(event);
      if (_sending_ajax) return;
      _sending_ajax = true;

      var that = this,
          type = $(that).attr("form-type"),
          upload = type == "upload",
          data = upload ? new FormData(this) : $(this).serialize();
      $.ajax({
        url: $(that).attr('action'),
        type: $(that).attr('method'),
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: data,
        dataType: $(that).attr('data-form-type'),
        contentType: upload ? false : "application/x-www-form-urlencoded",
        processData: !upload,
        beforeSend: function beforeSend() {
          $(that).find('button').attr('disabled', 'disabled');
          $('.error-explanation').each(function (index, el) {
            $(el).remove();
          });
        }
      }).done(function (e) {
        console.log(e);
        if (typeof e !== "undefined" && e.status == "success") {
          new PNotify({
            title: e.title,
            text: e.message,
            type: 'info',
            styling: 'bootstrap3'
          });
          _sending_ajax = true;
          options.success(e);
        } else {
          options.error(e);
        }
      }).fail(function (e) {
        var json = e.responseJSON;
        if (json != null) {
          var message = json.message,
              errors = json.errors;
          new PNotify({
            title: "Error",
            text: message,
            type: 'warning',
            styling: 'bootstrap3'
          });

          $(that).find('button').removeAttr('disabled');
          $.each(errors, function (index, data) {
            var indexarray = index.split(".", 2);
            if (1 in indexarray) {
              $("." + indexarray[0]).eq(parseInt(indexarray[1])).after('<div class="text-danger error-explanation"><span class="fa fa-warning"></span> ' + data + '</div>');
            } else {
              $("input[name=" + index + "]").after('<div class="text-danger error-explanation"><span class="fa fa-warning"></span> ' + data + '</div>');
              $("textarea[name=" + index + "]").after('<div class="text-danger error-explanation"><span class="fa fa-warning"></span> ' + data + '</div>');
              $("select[name=" + index + "]").after('<div class="text-danger error-explanation"><span class="fa fa-warning"></span> ' + data + '</div>');
            }
          });
        }
        options.error(e);
      }).always(function () {
        _sending_ajax = false;
        $(that).find('button').removeAttr('disabled');
      });
    });
  };
})(jQuery);

/***/ }),

/***/ "./resources/assets/js/front/app.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_timers__ = __webpack_require__("./node_modules/timers-browserify/main.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_timers___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_timers__);


__webpack_require__("./resources/assets/js/front/custom.js");
__webpack_require__("./resources/assets/js/front/ajax-form.js");

var AJAX_LOAD = true;

var $form_modal = $('.cd-user-modal'),
    $form_login = $form_modal.find('#cd-login'),
    $form_signup = $form_modal.find('#cd-signup'),
    $form_forgot_password = $form_modal.find('#cd-reset-password'),
    $form_modal_tab = $('.cd-switcher'),
    $tab_login = $form_modal_tab.children('li').eq(0).children('a'),
    $tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
    $forgot_password_link = $form_login.find('.cd-form-bottom-message a'),
    $back_to_login_link = $form_forgot_password.find('.cd-form-bottom-message a'),
    $main_nav = $('.main-nav');

//open modal
$('.cd-signup').on('click', function (event) {

  // on mobile open the submenu
  $form_modal.addClass('is-visible');
  signup_selected();
});
$('.cd-signin').on('click', function (event) {

  // on mobile open the submenu
  $form_modal.addClass('is-visible');
  login_selected();
});

//close modal
$('.cd-user-modal').on('click', function (event) {
  if ($(event.target).is($form_modal) || $(event.target).is('.cd-close-form')) {
    $form_modal.removeClass('is-visible');
  }
});
//close modal when clicking the esc keyboard button
$(document).keyup(function (event) {
  if (event.which == '27') {
    $form_modal.removeClass('is-visible');
  }
});

//switch from a tab to another
$form_modal_tab.on('click', function (event) {
  event.preventDefault();
  $(event.target).is($tab_login) ? login_selected() : signup_selected();
});

//hide or show password
$('.hide-password').on('click', function () {
  var $this = $(this),
      $password_field = $this.prev('input');

  'password' == $password_field.attr('type') ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
  'Hide' == $this.text() ? $this.text('Show') : $this.text('Hide');
  //focus and move cursor to the end of input field
  $password_field.putCursorAtEnd();
});

//show forgot-password form
$forgot_password_link.on('click', function (event) {
  event.preventDefault();
  forgot_password_selected();
});

//back to login from the forgot-password form
$back_to_login_link.on('click', function (event) {
  event.preventDefault();
  login_selected();
});

function login_selected() {
  $form_login.addClass('is-selected');
  $form_signup.removeClass('is-selected');
  $form_forgot_password.removeClass('is-selected');
  $tab_login.addClass('selected');
  $tab_signup.removeClass('selected');
}

function signup_selected() {
  $form_login.removeClass('is-selected');
  $form_signup.addClass('is-selected');
  $form_forgot_password.removeClass('is-selected');
  $tab_login.removeClass('selected');
  $tab_signup.addClass('selected');
}

function forgot_password_selected() {
  $form_login.removeClass('is-selected');
  $form_signup.removeClass('is-selected');
  $form_forgot_password.addClass('is-selected');
}

//REMOVE THIS - it's just to show error messages
// $form_login.find('input[type="submit"]').on('click', function(event){
//   event.preventDefault();
//   $form_login.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
// });
// $form_signup.find('input[type="submit"]').on('click', function(event){
//   event.preventDefault();
//   $form_signup.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
// });


//IE9 placeholder fallback
//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
// if(!Modernizr.input.placeholder){
//   $('[placeholder]').focus(function() {
//     var input = $(this);
//     if (input.val() == input.attr('placeholder')) {
//       input.val('');
//       }
//   }).blur(function() {
//     var input = $(this);
//       if (input.val() == '' || input.val() == input.attr('placeholder')) {
//       input.val(input.attr('placeholder'));
//       }
//   }).blur();
//   $('[placeholder]').parents('form').submit(function() {
//       $(this).find('[placeholder]').each(function() {
//       var input = $(this);
//       if (input.val() == input.attr('placeholder')) {
//         input.val('');
//       }
//       })
//   });
// }


//credits https://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
jQuery.fn.putCursorAtEnd = function () {
  return this.each(function () {
    // If this function exists...
    if (this.setSelectionRange) {
      // ... then use it (Doesn't work in IE)
      // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
      var len = $(this).val().length * 2;
      this.setSelectionRange(len, len);
    } else {
      // ... otherwise replace the contents with itself
      // (Doesn't work in Google Chrome)
      $(this).val($(this).val());
    }
  });
};

//ajax login user
// $('#btn-login').off().on('click', function(){
//   var email = $('#signin-email').val();
//   var password = $('#signin-password').val();
//   var remember = $('#accept-terms').val();
//   $.ajax({
//     url: "{!! url('/attempt') !!}",
//     dataType: "json",
//     type: "POST",
//     data:{
//       email: email,
//       password: password,
//       remember: remember,
//       _method:"post",
//       _token : '{{ csrf_token() }}'
//     }
//   }).done(function(data){
//       if(data.success == true){
//         window.location.reload();
//       }else{
//         $('#notif-result').attr("style", "color:red");
//       }
//   });
// });
$('#user-login').ajaxForm({
  success: function success() {
    Object(__WEBPACK_IMPORTED_MODULE_0_timers__["setTimeout"])(function () {
      window.location.reload();
    }, 1000);
  }
});
$('#add-address').ajaxForm({
  success: function success() {
    Object(__WEBPACK_IMPORTED_MODULE_0_timers__["setTimeout"])(function () {
      window.location.reload();
    }, 1000);
  }
});
$('#payment-form').ajaxForm({
  success: function success() {
    Object(__WEBPACK_IMPORTED_MODULE_0_timers__["setTimeout"])(function () {
      window.location = "/";
    }, 1000);
  }
});

var bar = $('.bar');
var percent = $('.percent');
console.log(bar.val());
var options = {
  beforeSend: function beforeSend() {

    var percentVal = '0';

    bar.val(percentVal);
    percent.html(percentVal);
  },
  uploadProgress: function uploadProgress(event, position, total, percentComplete) {
    var percentVal = percentComplete;
    bar.val(percentVal);
    console.log(percentVal);
    percent.html(percentVal);
  }
};
$('#product_form').ajaxForm(options);
$('#update-address').ajaxForm({
  success: function success() {
    Object(__WEBPACK_IMPORTED_MODULE_0_timers__["setTimeout"])(function () {
      window.location.reload();
    }, 1000);
  }
});
$('#user-signup').ajaxForm({
  success: function success() {
    Object(__WEBPACK_IMPORTED_MODULE_0_timers__["setTimeout"])(function () {
      window.location = CREATE_SIGNUP_SUCCESS_LINK;
    }, 1000);
  }
});

//ajax login user

/***/ }),

/***/ "./resources/assets/js/front/custom.js":
/***/ (function(module, exports) {

/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Set Header
3. Init Custom Dropdown
4. Init Page Menu
5. Init Deals Slider
6. Init Tab Lines
7. Init Tabs
8. Init Featured Slider
9. Init Favorites
10. Init ZIndex
11. Init Popular Categories Slider
12. Init Banner 2 Slider
13. Init Arrivals Slider
14. Init Arrivals Slider ZIndex
15. Init Best Sellers Slider
16. Init Trends Slider
17. Init Reviews Slider
18. Init Recently Viewed Slider
19. Init Brands Slider
20. Init Timer


******************************/

$(document).ready(function () {
	"use strict";

	/* 
 
 1. Vars and Inits
 
 */

	var menuActive = false;
	var header = $('.header');

	setHeader();

	initPageMenu();
	initDealsSlider();
	initTabLines();
	initFeaturedSlider();
	featuredSliderZIndex();
	initPopularSlider();
	initBanner2Slider();
	initFavs();
	initArrivalsSlider();
	arrivalsSliderZIndex();
	bestsellersSlider();
	initTabs();
	initTrendsSlider();
	initReviewsSlider();
	initViewedSlider();
	initBrandsSlider();
	initTimer();

	$(window).on('resize', function () {
		setHeader();
		featuredSliderZIndex();
		initTabLines();
	});

	/* 
 
 2. Set Header
 
 */

	function setHeader() {
		//To pin main nav to the top of the page when it's reached
		//uncomment the following

		// var controller = new ScrollMagic.Controller(
		// {
		// 	globalSceneOptions:
		// 	{
		// 		triggerHook: 'onLeave'
		// 	}
		// });

		// var pin = new ScrollMagic.Scene(
		// {
		// 	triggerElement: '.main_nav'
		// })
		// .setPin('.main_nav').addTo(controller);

		if (window.innerWidth > 991 && menuActive) {
			closeMenu();
		}
	}

	/* 
 
 3. Init Custom Dropdown
 
 */

	function initCustomDropdown() {
		if ($('.custom_dropdown_placeholder').length && $('.custom_list').length) {
			var placeholder = $('.custom_dropdown_placeholder');
			var list = $('.custom_list');
		}

		placeholder.on('click', function (ev) {
			if (list.hasClass('active')) {
				list.removeClass('active');
			} else {
				list.addClass('active');
			}

			$(document).one('click', function closeForm(e) {
				if ($(e.target).hasClass('clc')) {
					$(document).one('click', closeForm);
				} else {
					list.removeClass('active');
				}
			});
		});

		$('.custom_list a').on('click', function (ev) {
			ev.preventDefault();
			var index = $(this).parent().index();

			placeholder.text($(this).text()).css('opacity', '1');

			if (list.hasClass('active')) {
				list.removeClass('active');
			} else {
				list.addClass('active');
			}
		});

		$('select').on('change', function (e) {
			placeholder.text(this.value);

			$(this).animate({ width: placeholder.width() + 'px' });
		});
	}

	/* 
 
 4. Init Page Menu
 
 */

	function initPageMenu() {
		if ($('.page_menu').length && $('.page_menu_content').length) {
			var menu = $('.page_menu');
			var menuContent = $('.page_menu_content');
			var menuTrigger = $('.menu_trigger');

			//Open / close page menu
			menuTrigger.on('click', function () {
				if (!menuActive) {
					openMenu();
				} else {
					closeMenu();
				}
			});

			//Handle page menu
			if ($('.page_menu_item').length) {
				var items = $('.page_menu_item');
				items.each(function () {
					var item = $(this);
					if (item.hasClass("has-children")) {
						item.on('click', function (evt) {
							evt.preventDefault();
							evt.stopPropagation();
							var subItem = item.find('> ul');
							if (subItem.hasClass('active')) {
								subItem.toggleClass('active');
								TweenMax.to(subItem, 0.3, { height: 0 });
							} else {
								subItem.toggleClass('active');
								TweenMax.set(subItem, { height: "auto" });
								TweenMax.from(subItem, 0.3, { height: 0 });
							}
						});
					}
				});
			}
		}
	}

	function openMenu() {
		var menu = $('.page_menu');
		var menuContent = $('.page_menu_content');
		TweenMax.set(menuContent, { height: "auto" });
		TweenMax.from(menuContent, 0.3, { height: 0 });
		menuActive = true;
	}

	function closeMenu() {
		var menu = $('.page_menu');
		var menuContent = $('.page_menu_content');
		TweenMax.to(menuContent, 0.3, { height: 0 });
		menuActive = false;
	}

	/* 
 
 5. Init Deals Slider
 
 */

	function initDealsSlider() {
		if ($('.deals_slider').length) {
			var dealsSlider = $('.deals_slider');
			dealsSlider.owlCarousel({
				items: 1,
				loop: false,
				navClass: ['deals_slider_prev', 'deals_slider_next'],
				nav: false,
				dots: false,
				smartSpeed: 1200,
				margin: 30,
				autoplay: false,
				autoplayTimeout: 5000
			});

			if ($('.deals_slider_prev').length) {
				var prev = $('.deals_slider_prev');
				prev.on('click', function () {
					dealsSlider.trigger('prev.owl.carousel');
				});
			}

			if ($('.deals_slider_next').length) {
				var next = $('.deals_slider_next');
				next.on('click', function () {
					dealsSlider.trigger('next.owl.carousel');
				});
			}
		}
	}

	/* 
 
 6. Init Tab Lines
 
 */

	function initTabLines() {
		if ($('.tabs').length) {
			var tabs = $('.tabs');

			tabs.each(function () {
				var tabsItem = $(this);
				var tabsLine = tabsItem.find('.tabs_line span');
				var tabGroup = tabsItem.find('ul li');

				var posX = $(tabGroup[0]).position().left;
				tabsLine.css({ 'left': posX, 'width': $(tabGroup[0]).width() });
				tabGroup.each(function () {
					var tab = $(this);
					tab.on('click', function () {
						if (!tab.hasClass('active')) {
							tabGroup.removeClass('active');
							tab.toggleClass('active');
							var tabXPos = tab.position().left;
							var tabWidth = tab.width();
							tabsLine.css({ 'left': tabXPos, 'width': tabWidth });
						}
					});
				});
			});
		}
	}

	/* 
 
 7. Init Tabs
 
 */

	function initTabs() {
		if ($('.tabbed_container').length) {
			//Handle tabs switching

			var tabsContainers = $('.tabbed_container');
			tabsContainers.each(function () {
				var tabContainer = $(this);
				var tabs = tabContainer.find('.tabs ul li');
				var panels = tabContainer.find('.panel');
				var sliders = panels.find('.slider');

				tabs.each(function () {
					var tab = $(this);
					tab.on('click', function () {
						panels.removeClass('active');
						var tabIndex = tabs.index(this);
						$($(panels[tabIndex]).addClass('active'));
						sliders.slick("unslick");
						sliders.each(function () {
							var slider = $(this);
							// slider.slick("unslick");
							if (slider.hasClass('bestsellers_slider')) {
								initBSSlider(slider);
							}
							if (slider.hasClass('featured_slider')) {
								initFSlider(slider);
							}
							if (slider.hasClass('arrivals_slider')) {
								initASlider(slider);
							}
						});
					});
				});
			});
		}
	}

	/* 
 
 8. Init Featured Slider
 
 */

	function initFeaturedSlider() {
		if ($('.featured_slider').length) {
			var featuredSliders = $('.featured_slider');
			featuredSliders.each(function () {
				var featuredSlider = $(this);
				initFSlider(featuredSlider);
			});
		}
	}

	function initFSlider(fs) {
		var featuredSlider = fs;
		featuredSlider.on('init', function () {
			var activeItems = featuredSlider.find('.slick-slide.slick-active');
			for (var x = 0; x < activeItems.length - 1; x++) {
				var item = $(activeItems[x]);
				item.find('.border_active').removeClass('active');
				if (item.hasClass('slick-active')) {
					item.find('.border_active').addClass('active');
				}
			}
		}).on({
			afterChange: function afterChange(event, slick, current_slide_index, next_slide_index) {
				var activeItems = featuredSlider.find('.slick-slide.slick-active');
				activeItems.find('.border_active').removeClass('active');
				for (var x = 0; x < activeItems.length - 1; x++) {
					var item = $(activeItems[x]);
					item.find('.border_active').removeClass('active');
					if (item.hasClass('slick-active')) {
						item.find('.border_active').addClass('active');
					}
				}
			}
		}).slick({
			rows: 2,
			slidesToShow: 4,
			slidesToScroll: 4,
			infinite: false,
			arrows: false,
			dots: true,
			responsive: [{
				breakpoint: 768, settings: {
					rows: 2,
					slidesToShow: 3,
					slidesToScroll: 3,
					dots: true
				}
			}, {
				breakpoint: 575, settings: {
					rows: 2,
					slidesToShow: 2,
					slidesToScroll: 2,
					dots: false
				}
			}, {
				breakpoint: 480, settings: {
					rows: 1,
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false
				}
			}]
		});
	}

	/* 
 
 9. Init Favorites
 
 */

	function initFavs() {
		// Handle Favorites
		var items = document.getElementsByClassName('product_fav');
		for (var x = 0; x < items.length; x++) {
			var item = items[x];
			item.addEventListener('click', function (fn) {
				fn.target.classList.toggle('active');
			});
		}
	}

	/* 
 
 10. Init ZIndex
 
 */

	function featuredSliderZIndex() {
		// Hide slider dots on item hover
		var items = document.getElementsByClassName('featured_slider_item');

		for (var x = 0; x < items.length; x++) {
			var item = items[x];
			item.addEventListener('mouseenter', function () {
				$('.featured_slider .slick-dots').css('display', "none");
			});

			item.addEventListener('mouseleave', function () {
				$('.featured_slider .slick-dots').css('display', "block");
			});
		}
	}

	/* 
 
 11. Init Popular Categories Slider
 
 */

	function initPopularSlider() {
		if ($('.popular_categories_slider').length) {
			var popularSlider = $('.popular_categories_slider');

			popularSlider.owlCarousel({
				loop: true,
				autoplay: false,
				nav: false,
				dots: false,
				responsive: {
					0: { items: 1 },
					575: { items: 2 },
					640: { items: 3 },
					768: { items: 4 },
					991: { items: 5 }
				}
			});

			if ($('.popular_categories_prev').length) {
				var prev = $('.popular_categories_prev');
				prev.on('click', function () {
					popularSlider.trigger('prev.owl.carousel');
				});
			}

			if ($('.popular_categories_next').length) {
				var next = $('.popular_categories_next');
				next.on('click', function () {
					popularSlider.trigger('next.owl.carousel');
				});
			}
		}
	}

	/* 
 
 12. Init Banner 2 Slider
 
 */

	function initBanner2Slider() {
		if ($('.banner_2_slider').length) {
			var banner2Slider = $('.banner_2_slider');
			banner2Slider.owlCarousel({
				items: 1,
				loop: true,
				nav: false,
				dots: true,
				dotsContainer: '.banner_2_dots',
				smartSpeed: 1200
			});
		}
	}

	/* 
 
 13. Init Arrivals Slider
 
 */

	function initArrivalsSlider() {
		if ($('.arrivals_slider').length) {
			var arrivalsSliders = $('.arrivals_slider');
			arrivalsSliders.each(function () {
				var arrivalsSlider = $(this);
				initASlider(arrivalsSlider);
			});
		}
	}

	function initASlider(as) {
		var arrivalsSlider = as;
		arrivalsSlider.on('init', function () {
			var activeItems = arrivalsSlider.find('.slick-slide.slick-active');
			for (var x = 0; x < activeItems.length - 1; x++) {
				var item = $(activeItems[x]);
				item.find('.border_active').removeClass('active');
				if (item.hasClass('slick-active')) {
					item.find('.border_active').addClass('active');
				}
			}
		}).on({
			afterChange: function afterChange(event, slick, current_slide_index, next_slide_index) {
				var activeItems = arrivalsSlider.find('.slick-slide.slick-active');
				activeItems.find('.border_active').removeClass('active');
				for (var x = 0; x < activeItems.length - 1; x++) {
					var item = $(activeItems[x]);
					item.find('.border_active').removeClass('active');
					if (item.hasClass('slick-active')) {
						item.find('.border_active').addClass('active');
					}
				}
			}
		}).slick({
			rows: 2,
			slidesToShow: 5,
			slidesToScroll: 5,
			infinite: false,
			arrows: false,
			dots: true,
			responsive: [{
				breakpoint: 768, settings: {
					rows: 2,
					slidesToShow: 3,
					slidesToScroll: 3,
					dots: true
				}
			}, {
				breakpoint: 575, settings: {
					rows: 2,
					slidesToShow: 2,
					slidesToScroll: 2,
					dots: false
				}
			}, {
				breakpoint: 480, settings: {
					rows: 1,
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false
				}
			}]
		});
	}

	/* 
 
 14. Init Arrivals Slider ZIndex
 
 */

	function arrivalsSliderZIndex() {
		// Hide slider dots on item hover
		var items = document.getElementsByClassName('arrivals_slider_item');

		for (var x = 0; x < items.length; x++) {
			var item = items[x];
			item.addEventListener('mouseenter', function () {
				$('.arrivals_slider .slick-dots').css('display', "none");
			});

			item.addEventListener('mouseleave', function () {
				$('.arrivals_slider .slick-dots').css('display', "block");
			});
		}
	}

	/* 
 
 15. Init Best Sellers Slider
 
 */

	function bestsellersSlider() {
		if ($('.bestsellers_slider').length) {
			var bestsellersSliders = $('.bestsellers_slider');
			bestsellersSliders.each(function () {
				var bestsellersSlider = $(this);

				initBSSlider(bestsellersSlider);
			});
		}
	}

	function initBSSlider(bss) {
		var bestsellersSlider = bss;

		bestsellersSlider.slick({
			rows: 2,
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			arrows: false,
			dots: true,
			autoplay: true,
			autoplaySpeed: 6000,
			responsive: [{
				breakpoint: 1199, settings: {
					rows: 2,
					slidesToShow: 2,
					slidesToScroll: 2,
					dots: true
				}
			}, {
				breakpoint: 991, settings: {
					rows: 2,
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: true
				}
			}, {
				breakpoint: 575, settings: {
					rows: 1,
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false
				}
			}]
		});
	}

	/* 
 
 16. Init Trends Slider
 
 */

	function initTrendsSlider() {
		if ($('.trends_slider').length) {
			var trendsSlider = $('.trends_slider');
			trendsSlider.owlCarousel({
				loop: false,
				margin: 30,
				nav: false,
				dots: false,
				autoplayHoverPause: true,
				autoplay: false,
				responsive: {
					0: { items: 1 },
					575: { items: 2 },
					991: { items: 3 }
				}
			});

			trendsSlider.on('click', '.trends_fav', function (ev) {
				$(ev.target).toggleClass('active');
			});

			if ($('.trends_prev').length) {
				var prev = $('.trends_prev');
				prev.on('click', function () {
					trendsSlider.trigger('prev.owl.carousel');
				});
			}

			if ($('.trends_next').length) {
				var next = $('.trends_next');
				next.on('click', function () {
					trendsSlider.trigger('next.owl.carousel');
				});
			}
		}
	}

	/* 
 
 17. Init Reviews Slider
 
 */

	function initReviewsSlider() {
		if ($('.reviews_slider').length) {
			var reviewsSlider = $('.reviews_slider');

			reviewsSlider.owlCarousel({
				items: 3,
				loop: true,
				margin: 30,
				autoplay: false,
				nav: false,
				dots: true,
				dotsContainer: '.reviews_dots',
				responsive: {
					0: { items: 1 },
					768: { items: 2 },
					991: { items: 3 }
				}
			});
		}
	}

	/* 
 
 18. Init Recently Viewed Slider
 
 */

	function initViewedSlider() {
		if ($('.viewed_slider').length) {
			var viewedSlider = $('.viewed_slider');

			viewedSlider.owlCarousel({
				loop: true,
				margin: 30,
				autoplay: true,
				autoplayTimeout: 6000,
				nav: false,
				dots: false,
				responsive: {
					0: { items: 1 },
					575: { items: 2 },
					768: { items: 3 },
					991: { items: 4 },
					1199: { items: 6 }
				}
			});

			if ($('.viewed_prev').length) {
				var prev = $('.viewed_prev');
				prev.on('click', function () {
					viewedSlider.trigger('prev.owl.carousel');
				});
			}

			if ($('.viewed_next').length) {
				var next = $('.viewed_next');
				next.on('click', function () {
					viewedSlider.trigger('next.owl.carousel');
				});
			}
		}
	}

	/* 
 
 19. Init Brands Slider
 
 */

	function initBrandsSlider() {
		if ($('.brands_slider').length) {
			var brandsSlider = $('.brands_slider');

			brandsSlider.owlCarousel({
				loop: true,
				autoplay: true,
				autoplayTimeout: 5000,
				nav: false,
				dots: false,
				autoWidth: true,
				items: 8,
				margin: 42
			});

			if ($('.brands_prev').length) {
				var prev = $('.brands_prev');
				prev.on('click', function () {
					brandsSlider.trigger('prev.owl.carousel');
				});
			}

			if ($('.brands_next').length) {
				var next = $('.brands_next');
				next.on('click', function () {
					brandsSlider.trigger('next.owl.carousel');
				});
			}
		}
	}

	/* 
 
 20. Init Timer
 
 */

	function initTimer() {
		if ($('.deals_timer_box').length) {
			var timers = $('.deals_timer_box');
			timers.each(function () {
				var timer = $(this);

				var targetTime;
				var target_date;

				// Add a date to data-target-time of the .deals_timer_box
				// Format: "Feb 17, 2018"
				if (timer.data('target-time') !== "") {
					targetTime = timer.data('target-time');
					target_date = new Date(targetTime).getTime();
				} else {
					var date = new Date();
					date.setDate(date.getDate() + 2);
					target_date = date.getTime();
				}

				// variables for time units
				var days, hours, minutes, seconds;

				var h = timer.find('.deals_timer_hr');
				var m = timer.find('.deals_timer_min');
				var s = timer.find('.deals_timer_sec');

				setInterval(function () {
					// find the amount of "seconds" between now and target
					var current_date = new Date().getTime();
					var seconds_left = (target_date - current_date) / 1000;
					console.log(seconds_left);

					// do some time calculations
					days = parseInt(seconds_left / 86400);
					seconds_left = seconds_left % 86400;

					hours = parseInt(seconds_left / 3600);
					hours = hours + days * 24;
					seconds_left = seconds_left % 3600;

					minutes = parseInt(seconds_left / 60);
					seconds = parseInt(seconds_left % 60);

					if (hours.toString().length < 2) {
						hours = "0" + hours;
					}
					if (minutes.toString().length < 2) {
						minutes = "0" + minutes;
					}
					if (seconds.toString().length < 2) {
						seconds = "0" + seconds;
					}

					// display results
					h.text(hours);
					m.text(minutes);
					s.text(seconds);
				}, 1000);
			});
		}
	}
});

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/front/app.js");


/***/ })

/******/ });