var OMDB_API_KEY = $("meta[name=omdb-api-key]").attr('content');

module.exports = function(id, callback) {
  var url = "https://www.omdbapi.com";
  $.ajax({
    url: url,
    type: 'GET',
    dataType: 'json',
    data: {
      apikey: OMDB_API_KEY,
      i: id
    }
  })
  .done(function(e) {
    if(typeof callback == "function") {
      callback(e);
    } else {
      return e;
    }
  })
  .fail(function(e) {
    console.log(e);
  });
}
