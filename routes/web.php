<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/products', 'ProductsController');
Route::get('/testpro', function () {
   return view('frontend.cart.checkout');
});
Route::get('/monitorh2h', function () {
   return view('frontend.include.komodo');
});
Route::get('/invoice-info', function () {
   return view('frontend.transaction.invoice_info');
});
Route::get('/ask/{id}', function ($id) {
   return view('frontend.discussion',compact('id'));
});
Route::get('/stats', 'BackendController@getStats');
Route::get('/maintenance', function () {
   return view('maintenance');
});
// Auth::routes();
Route::resource('category', 'CategoryController', [
    'except' => ['show'],
    'names' => 'admin.category'
  ]);
Route::any('/getongkir', 'FrontendController@ongkir');
Route::get('/getCity', 'FrontendController@getCity');
Route::get('/getDistrict', 'FrontendController@getDistrict');
Route::post('/addVideo', 'ProductsController@uploadVideo')->name('products.add.video');
//Route::get('/home', 'HomeController@index');
Route::get('/empty-cart/{id}', 'CartController@destroy');
/* Frontend part of the shop */
Route::get('/', 'FrontendController@home');
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
 Route::get('/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('/home', 'FrontendController@home');
Route::get('/collection', 'FrontendController@collection')->name('frontend.collection');
//Route::get('/collection', 'FrontendController@collection');
Route::get('/topbrands', 'FrontendController@topbrands');
//Route::get('/cart', 'FrontendController@cart');
Route::get('/contact', 'FrontendController@contact');
Route::post('/attempt', 'FrontendController@attempt');
Route::post('/postDiscuss', 'FrontendController@storeDiscuss');
Route::post('/postQuestion', 'FrontendController@storeQuestion');
Route::get('/filter', 'FrontendController@filter');
Route::get('/login', 'FrontendController@login');
Route::get('/signin', 'FrontendController@signin');
Route::get('/signup', 'FrontendController@signup');
Route::get('/vendor-signup', 'FrontendController@vendorSignup');
Route::post('/registration', 'FrontendController@process_vendorSignup');
Route::get('/user/activation/{token}', 'Auth\RegisterController@userActivation');
Route::post('/process_signin', 'FrontendController@create');
Route::post('/search', 'FrontendController@search')->name('frontend.search');
Route::get('/details/{id}', 'FrontendController@details')->name('frontend.detail');
// Route::get('/category/{id}', 'FrontendController@show')->name('collection.show');
Route::get('/category/{id}', 'FrontendController@subproduct')->name('collection.subproduct');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/buynow/{id}', 'CartController@buynow')->name('cart.buy');

Route::get('shop/{id}', 'MerchantController@detail')->name('merchant.detail');

Route::resource('/cart', 'CartController');
Route::any('/update/qty', 'CartController@updateQty')->name('update.qty');
Route::resource('/address', 'AddressController');
Route::get('/checkout', 'CheckoutController@checkout')->name('frontend.checkout');
//Route::get('/checkout', 'CheckoutController@step1');


Route::get('/viewcheckout', 'FrontendController@checkout');



Route::post('/checkout', 'CheckoutController@checkout')->name('checkout');
//Route::get('/checkout', 'CheckoutController@step1');

Route::get('/snap', 'SnapController@snap');
Route::get('/delete/{id}', 'CartController@destroy');
Route::post('/snaptoken', 'SnapController@token');
Route::post('/snapfinish', 'SnapController@finish');

Route::get('/ivoucher', 'FrontendController@ivoucher')->name('frontend.ivoucher');
Route::get('/tracking', 'FrontendController@tracking')->name('frontend.tracking');

Route::group(['middleware' => 'auth'], function () {

    Route::get('shipping-info', 'CheckoutController@shipping')->name('checkout.shipping');
    Route::get('/payment', 'CheckoutController@payment')->name('checkout.payment');
    Route::post('/store-payment', 'CheckoutController@storePayment')->name('payment.store');

    Route::get('/biodata', 'FrontendController@biodata')->name('frontend.biodata');
    Route::get('/penjualan', 'FrontendController@penjualan')->name('frontend.penjualan');
    Route::get('/pembelian', 'TransactionController@pembelian')->name('frontend.pembelian');
    Route::get('/pembelian/{mode}', 'TransactionController@pembelian')->name('frontend.pembelian');
    Route::get('/my/sales', 'TransactionController@mySales')->name('frontend.mySales');
    Route::get('/list/sales/{id}', 'TransactionController@sales')->name('frontend.salesConfirmation');
    Route::post('/update/profile', 'FrontendController@profile')->name('frontend.profile');
    Route::get('/alamat', 'AddressController@alamat')->name('frontend.alamat');
    Route::post('/alamat/detail', 'AddressController@alamatDetail')->name('address.alamatDetail');
    Route::get('/alamat/delete/{id}', 'AddressController@delete')->name('address.delete');
    Route::post('/store', 'AddressController@store')->name('address.store');
    Route::post('/alamat/update', 'AddressController@update')->name('address.update');
    Route::post('/alamat/delete/{id}', 'AddressController@destroy')->name('address.destroy');
    Route::get('/discussion', 'FrontendController@discussion')->name('frontend.diskusi');
    Route::get('/discussion/{id}', 'FrontendController@discussionAkun')->name('frontend.akun');

    Route::get('/create/merchant', 'FrontendController@create_merchant')->name('frontend.create_merchant');
    Route::get('/create/step', 'FrontendController@cretae_step')->name('frontend.step');
    Route::post('/add/merchant', 'MerchantController@create')->name('merchant.add');
    Route::post('/add/merchant/step', 'MerchantController@createStep')->name('merchant.step');
    Route::get('/my/merchant/{id}', 'FrontendController@myMerchant')->name('merchant.my');
    Route::post('/merchant/{id}/edit', 'MerchantController@edit')->name('merchant.edit');
    Route::get('/shop/update/{id}', 'FrontendController@merchantUpdate')->name('merchant.update');
    Route::get('/merchant/detail', 'FrontendController@merchantDetail')->name('merchant.detail');
    Route::get('/merchant/confirmation/resi', 'FrontendController@merchantConfirmationResi')->name('merchant.confirmation');


    Route::get('/trx/sales', 'OrdersController@trxSales')->name('sales.trx');
    Route::get('/acceptOrder', 'TransactionController@transactionUpdate');
    Route::get('/denyOrder', 'TransactionController@transactionUpdate');

    //products
    Route::get('/products/add/image', 'FrontendController@productAddImage')->name('product.image');
    Route::get('/products/add/video', 'FrontendController@productAddVid')->name('product.video');
    Route::get('/products/edit/video', 'FrontendController@productEdit')->name('product.edit_video');
    Route::get('/products/choose', 'FrontendController@chooseMethod')->name('product.choose');
    Route::post('/reset/password', 'FrontendController@resetPassword')->name('profile.reset');
    Route::post('/address/change', 'AddressController@addressChange')->name('address.change');

    //my transaction
    Route::get('/trx/invoice', 'TransactionController@trxMetode');
    Route::get('/order/detail/{id}', 'OrdersController@orderDetail');
    Route::get('/trx/paymentguide', 'TransactionController@trxGuide');
    Route::any('/getguide/{id}', 'FrontendController@getGuide')->name('get.guide');
    Route::any('/trx/detail', 'TransactionController@trxDetail')->name('trx.detail');
    Route::any('/trx/detail/product', 'TransactionController@trxDetailProduct')->name('trx.detailProduct');
//    Route::get('logout', 'BackendController@getLogout')->name('frontend.signin');

    Route::get('/reservasi', 'FrontendController@reservasi');
});


    /* Backend part of the shop */
Route::get('/admin', 'BackendController@admin');


/* Admin resource controller */
Route::resource('/categories', 'CategoryController');
Route::resource('/brands', 'BrandsController');
Route::resource('/products', 'ProductsController');

Route::get('/merchant/', 'MerchantController@getMerchant');
Route::get('/merchant/{id}', 'MerchantController@detailAssigment')->name('admin.merchant.detail');
Route::post('/approveMerchant', 'MerchantController@approveShop');
Route::post('toggledeliver/{orderId}', 'OrdersController@toggledeliver')->name('toggle.deliver');
Route::resource('/attribute', 'AttributeController');
Route::resource('/attribute_set', 'AttributeSetController');
Route::get('menu/sort/{type}', 'MenuController@sort_form')->name('admin.menu.sort_form');
  Route::post('menu/sort/{type}', 'MenuController@sort')->name('admin.menu.sort');
  Route::resource('menu', 'MenuController', [
    'names' => 'admin.menu'
  ]);
Route::delete('page/bulkdelete', 'PageController@bulkdelete')->name('admin.page.bulkdelete');
  Route::get('page/search/{query?}', 'PageController@search')->name('admin.page.search');
  Route::resource('page', 'PageController', [
    'names' => 'admin.page'
  ]);
  Route::get('get_parent_menu.json', 'MenuController@parent_menu')->name('admin.menu.parent.json');
  Route::get('get_child_menu.json', 'MenuController@child_menu')->name('admin.menu.child.json');
  Route::get('get_type_menu.json', 'MenuController@type')->name('admin.menu.type');


Route::get('/users/{type?}', [
    'uses' => 'UsersController@index',
    'as' => 'users',
    'middleware' => 'roles',
    'roles' => ['Admin']
    ]);
Route::get('/manage_users', [
    'uses' => 'UsersController@manage_users',
    'as' => 'manage_users',
    'middleware' => 'roles',
    'roles' => ['Admin']
]);
Auth::routes(['verify' => true]);
Auth::routes();

//Route::get('/home', 'HomeController@index');
Route::get('test', 'Test@ajax');
Route::post('test_ajax', 'Test@test_ajax');
Route::get('admin/categories/{parent?}/{query?}', 'CategoryController@index')->name('categories.query');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
