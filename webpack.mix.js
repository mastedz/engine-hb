const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath(path.normalize('public/'));
mix.js('resources/assets/js/back/app.js', 'js/admin.js')
mix.js('resources/assets/js/front/app.js', 'js/front.js').version();

// mix.js('resources/assets/js/front/app.js', 'js/script.js')

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');
