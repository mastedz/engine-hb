<?php
namespace App;

trait Fungsi {
  	// Mereplace whitespace dan / dengan -
	public function generateAlias($url, $exception='') {
		// if url = year/**** skip url check */
		$arrUrl = explode('/',$url);
		if(count($arrUrl) == 2 ){
			if($arrUrl[0]=='year'){
				return strtolower($url);
			}
		}

		$pattern = array('/\//', '/\s+/');
		$replace = array('', '-');
		$url = preg_replace($pattern, $replace, $url);
		return strtolower(preg_replace("/[^a-z0-9\-". $exception ."]+/i", "", $url));
	}

	// format penomoran
	// contoh: 	100000 -> 100k
	// 			123321000 -> 123,3m
	public function thousandsCurrencyFormat($num)
	{
			if ($num>1000) {
					$x = round($num);
					$x_number_format = number_format($x);
					$x_array = explode(',', $x_number_format);
					$x_parts = array('k', 'm', 'b', 't');
					$x_count_parts = count($x_array) - 1;
					$x_display = $x;
					$x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
					$x_display .= $x_parts[$x_count_parts - 1];

					return $x_display;
			}

			return $num;
	}
}
