<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopAssigment extends Model
{
    protected $table='shop_assigment';
    protected $fillable=['shop_id', 'user_id', 'attach_file'];
    public function user() {

        return $this->belongsTo('App\User');

    }
    public function shop() {

        return $this->belongsTo('App\Shop');

    }
}
