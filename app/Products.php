<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    protected $table  = 'products';
    protected $primarykey = 'id';
    protected $fillable = ['name','price','qty','category_id','brand_id','image','shop_id', 'condition', 'insurance', 'weight', 'discount', 'description', 'slug', 'is_artist', 'url'];


    public function brand() {

        return $this->belongsTo('App\Brands');
    }
    public function category() {

        return $this->belongsTo('App\Category','category_id','id_category');
    }
    
    public function shop() {

        return $this->belongsTo('App\Shop');
    }

//    public function priceToCents()
//    {
//        return $this->price * 100;
//    }

}
