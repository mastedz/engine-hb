<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Category extends Model
{

    protected $table = 'category';
    protected $primaryKey = 'id_category';
    protected $fillable = [
      'title', 'slug', 'description', 'schema', 'meta_title', 'meta_description', 'meta_keyword', 'id_parent',
      'id_user'
    ];

    public function products() {

        return $this->hasMany('App\Products');
    }
    public function getParentCategoryAttribute() {
      $category = Category::where('id_category', $this->attributes['id_parent']);
      return $category;
    }
     public function getTotalProductBaseOnCategory($parent, $key)
    {
      $id_parent = Category::where('title',ucfirst(strtolower($parent)))->whereNull('id_parent')->firstOrFail()->id_category;
      $search = $key;
      $data = Products::whereHas('Category', function ($q) use($id_parent,$search ) {
          $q->whereHas('parent', function ($q) use($id_parent,$search ){
              $q->where('id_parent', $id_parent)->whereRaw("slug = ?",[$search]);
          });
      });
      return $data->count();
    }
    public function parent() {
        return $this->hasOne('App\Category', 'id_category', 'id_parent');
    }

    public function child() {
        return $this->hasMany('App\Category', 'id_parent', 'id_category');
    }

    public function user() {
      return $this->hasOne('App\User', 'id', 'id_user');
    }

    public function productCategory()
    {
        return $this->hasMany('App\MovieCategory', 'id_category', 'id_category');
    }

    public function actorKeyword()
    {
        return $this->hasMany('App\ActorKeyword', 'id_category', 'id_category');
    }

    // public function brands() {

    //     return $this->hasMany('App\Brands','category_id');
    // }
}
