<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'options';
    protected $primaryKey = 'id_option';

}
