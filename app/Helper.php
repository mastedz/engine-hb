<?php

namespace App;
use Curl;
use Cache;
class Helper
{
   function makeRequest($method, $url, $data=null, $type='')
  {
    switch (strtoupper($method)) {
      case 'POST':

      $key = env('RAJA_KEY');
      $response = Curl::to($url)
      ->withHeader('key:'.$key)

      ->withData($data)
      ->withContentType($type)
      ->asJsonResponse(true)
    ->post();
      return $response;

        break;

        case 'GET':
        $key = env('RAJA_KEY');
        $response = Curl::to($url)
        ->withHeader('key:'.$key)
        ->withContentType($type)
        ->withData($data)
        ->asJson(true)
        ->get();

        return $response;

          break;

      default:
    abort(404);
        break;
    }

  }
   public  function getProvinces($id='')
   {

      $url = 'https://pro.rajaongkir.com/api/province?id='.$id;
      $response = $this->makeRequest('GET',$url);

      return $response['rajaongkir']['results'];

   }

   public  function getCity($id='')
   {
     // dd($id);
      if (!Cache::get($id)) {
      $response = $this->makeRequest('GET','https://pro.rajaongkir.com/api/city?province='.$id);

      Cache::put($id, $response['rajaongkir']['results'], 1440);
   }
      $response= Cache::get($id);
      return $response;
   }
   public  function getDistrict($id='', $subdistrict='')
   {
     if ($subdistrict== '') {
      $response = $this->makeRequest('GET','https://pro.rajaongkir.com/api/subdistrict?city='.$id);
    }else{
      $response = $this->makeRequest('GET','https://pro.rajaongkir.com/api/subdistrict?id='.$subdistrict.'?city='.$id);
    }
      return $response['rajaongkir']['results'];
   }

   public  function getOngkir($data)
   {


      $type='application/x-www-form-urlencoded';

      $response = $this->makeRequest('POST','https://pro.rajaongkir.com/api/cost', $data, $type);

      return json_encode($response['rajaongkir']);
   }
   public static function non() {
        $a = new Helper;
        return $a;
    }
    public  function getGuide($id)
    {
        $guide = array(
                  '0' => array(
                    'channel' => 'BCA',
                    'app' => array(
                          '0' => 'ATM BCA',
                          '1' => 'Klik BCA',
                          '2' => 'm-BCA'
                        ),
                     'desc' => array(
                       '0'  => '<div class="content-table scroll"><table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 930 -->1<!-- /react-text --><!-- react-text: 931 -->.<!-- /react-text --></td><td class="table-row text-body"><span>On the main menu, choose <strong>Other Transaction</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 938 -->2<!-- /react-text --><!-- react-text: 939 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Transfer</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 946 -->3<!-- /react-text --><!-- react-text: 947 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Transfer To BCA Virtual Account</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 954 -->4<!-- /react-text --><!-- react-text: 955 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter your <strong>Payment Code</strong> (11 digits code) and press <strong>Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 962 -->5<!-- /react-text --><!-- react-text: 963 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter the full amount to be paid and press <strong>Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 970 -->6<!-- /react-text --><!-- react-text: 971 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Your payment details will appear on the payment confirmation page. If the information is correct press <strong>Yes</strong>.</span></td></tr></tbody></table></div>',
                       '1' => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 982 -->1<!-- /react-text --><!-- react-text: 983 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose Menu <strong>Fund Transfer</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 988 -->2<!-- /react-text --><!-- react-text: 989 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Transfer To BCA Virtual Account</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 994 -->3<!-- /react-text --><!-- react-text: 995 -->.<!-- /react-text --></td><td class="table-row text-body"><span><strong>Input BCA Virtual Account Number</strong> or <strong>Choose from Transfer list</strong> and click <strong>Continue</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1000 -->4<!-- /react-text --><!-- react-text: 1001 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Amount to be paid, account number and Merchant name will appear on the payment confirmation page, if the information is right click <strong>Continue</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1006 -->5<!-- /react-text --><!-- react-text: 1007 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Get your <strong>BCA token</strong> and input KEYBCA Response <strong>APPLI 1</strong> and click <strong>Submit</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1012 -->6<!-- /react-text --><!-- react-text: 1013 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Your Transaction is Done.</span></td></tr></tbody></table>',
                       '2' => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1022 -->1<!-- /react-text --><!-- react-text: 1023 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Log in to your <strong>BCA Mobile</strong> app.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1028 -->2<!-- /react-text --><!-- react-text: 1029 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>m-BCA</strong>, then input your <strong>m-BCA access code</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1034 -->3<!-- /react-text --><!-- react-text: 1035 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>m-Transfer</strong>, then choose BCA Virtual Account.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1040 -->4<!-- /react-text --><!-- react-text: 1041 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Input <strong>Virtual Account Number</strong> or choose an existing account from <strong>Daftar Transfer</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1046 -->5<!-- /react-text --><!-- react-text: 1047 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Input the <strong>payable amount</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1052 -->6<!-- /react-text --><!-- react-text: 1053 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Input your <strong>m-BCA pin</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1058 -->7<!-- /react-text --><!-- react-text: 1059 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Payment is finished. Save the notification as your payment receipt.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '1' => array(
                    'channel' => 'Mandiri',
                    'app' => array(
                          '0' => 'ATM Mandiri',
                          '1' => 'Internet Banking'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1150 -->1<!-- /react-text --><!-- react-text: 1151 -->.<!-- /react-text --></td><td class="table-row text-body"><span>On the main menu, choose <strong>Pay/Buy</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1158 -->2<!-- /react-text --><!-- react-text: 1159 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Others</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1166 -->3<!-- /react-text --><!-- react-text: 1167 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Multi Payment</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1174 -->4<!-- /react-text --><!-- react-text: 1175 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter 70012 (Midtrans company code) and press <strong>Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1182 -->5<!-- /react-text --><!-- react-text: 1183 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter your Payment Code and press <strong>Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1190 -->6<!-- /react-text --><!-- react-text: 1191 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Your payment details will appear on the payment confirmation page. If the information is correct press <strong>Yes</strong>.</span></td></tr></tbody></table>',

                       '1' => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1202 -->1<!-- /react-text --><!-- react-text: 1203 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Login to Mandiri Internet Banking (https://ib.bankmandiri.co.id/).</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1208 -->2<!-- /react-text --><!-- react-text: 1209 -->.<!-- /react-text --></td><td class="table-row text-body"><span>From the main menu choose <strong>Payment</strong>, then choose <strong>Multi Payment</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1214 -->3<!-- /react-text --><!-- react-text: 1215 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Select your account in <strong>From Account</strong>, then in <strong>Billing Name</strong> select <strong>Midtrans</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1220 -->4<!-- /react-text --><!-- react-text: 1221 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter the <strong>Payment Code</strong> and you will receive your payment details.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1226 -->5<!-- /react-text --><!-- react-text: 1227 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Confirm your payment using your Mandiri Token.</span></td></tr></tbody></table>',
                     ),
                  ),
                  '2' => array(
                    'channel' => 'BNI',
                    'app' => array(
                          '0' => 'ATM BNI',
                          '1' => 'Internet Banking',
                          '2' => 'Mobile Banking'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1320 -->1<!-- /react-text --><!-- react-text: 1321 -->.<!-- /react-text --></td><td class="table-row text-body"><span>On the main menu, choose <strong>Others</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1328 -->2<!-- /react-text --><!-- react-text: 1329 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Transfer</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1336 -->3<!-- /react-text --><!-- react-text: 1337 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Savings Account</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1344 -->4<!-- /react-text --><!-- react-text: 1345 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>To BNI Account</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1352 -->5<!-- /react-text --><!-- react-text: 1353 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter the payment account number and press <strong>Yes</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1360 -->6<!-- /react-text --><!-- react-text: 1361 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter the full amount to be paid. If the amount entered is not the same as the invoiced amount, the transaction will be declined.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1368 -->7<!-- /react-text --><!-- react-text: 1369 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Amount to be paid, account number, and merchant name will appear on the payment confirmation page. If the information is correct, press <strong>Yes</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1376 -->8<!-- /react-text --><!-- react-text: 1377 -->.<!-- /react-text --></td><td class="table-row text-body"><span>You are done.</span></td></tr></tbody></table>',

                       '1' => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1388 -->1<!-- /react-text --><!-- react-text: 1389 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Go to <strong>https://ibank.bni.co.id</strong> and then click <strong>Login</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1394 -->2<!-- /react-text --><!-- react-text: 1395 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Continue login with your <strong>User ID</strong> and <strong>Password</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1400 -->3<!-- /react-text --><!-- react-text: 1401 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Click <strong>Transfer</strong> and then <strong>Add Favorite Account</strong> and choose <strong>Antar Rekening BNI</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1406 -->4<!-- /react-text --><!-- react-text: 1407 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter account name, account number, and email and then click <strong>Continue</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1412 -->5<!-- /react-text --><!-- react-text: 1413 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Input the <strong>Authentification Code</strong> from your token and then click <strong>Continue</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1418 -->6<!-- /react-text --><!-- react-text: 1419 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Back to main menu and select <strong>Transfer</strong> and then <strong>Transfer Antar Rekening BNI</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1424 -->7<!-- /react-text --><!-- react-text: 1425 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Pick the account that you just created in the previous step as <strong>Rekening Tujuan</strong> and fill in the rest before clicking <strong>Continue</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1430 -->8<!-- /react-text --><!-- react-text: 1431 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Check whether the details are correct, if they are, please input the <strong>Authentification Code</strong> and click <strong>Continue</strong> and you are done.</span></td></tr></tbody></table>',
                       '2' => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1440 -->1<!-- /react-text --><!-- react-text: 1441 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Open the BNI Mobile Banking app and login</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1446 -->2<!-- /react-text --><!-- react-text: 1447 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose menu Transfer</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1452 -->3<!-- /react-text --><!-- react-text: 1453 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose menu Virtual Account Billing</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1458 -->4<!-- /react-text --><!-- react-text: 1459 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose the bank account you want to use</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1464 -->5<!-- /react-text --><!-- react-text: 1465 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter the 16 digits virtual account number</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1470 -->6<!-- /react-text --><!-- react-text: 1471 -->.<!-- /react-text --></td><td class="table-row text-body"><span>The billing information will appear on the payment validation page</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1476 -->7<!-- /react-text --><!-- react-text: 1477 -->.<!-- /react-text --></td><td class="table-row text-body"><span>If the information is correct, enter your password to proceed the payment</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1482 -->8<!-- /react-text --><!-- react-text: 1483 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Your transaction will be processed</span></td></tr></tbody></table>'
                     ),
                  ),
                  '3' => array(
                    'channel' => 'Permata ATM',
                    'app' => array(
                          '0' => 'ATM Permata'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1567 -->1<!-- /react-text --><!-- react-text: 1568 -->.<!-- /react-text --></td><td class="table-row text-body"><span>On the main menu, choose <strong>Other Transaction</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1575 -->2<!-- /react-text --><!-- react-text: 1576 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Payment</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1583 -->3<!-- /react-text --><!-- react-text: 1584 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Other Payment</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1591 -->4<!-- /react-text --><!-- react-text: 1592 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Virtual Account</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1599 -->5<!-- /react-text --><!-- react-text: 1600 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter 16 digits Account No. and press <strong>Correct<strong>.</strong></strong></span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1607 -->6<!-- /react-text --><!-- react-text: 1608 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Amount to be paid, account number, and merchant name will appear on the payment confirmation page. If the information is right, press <strong>Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1615 -->7<!-- /react-text --><!-- react-text: 1616 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose your payment account and press <strong>Correct</strong>.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '4' => array(
                    'channel' => 'Other Bank',
                    'app' => array(
                          '0' => 'Prima',
                          '1' => 'ATM Bersama',
                          '2' => 'Alto'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1740 -->1<!-- /react-text --><!-- react-text: 1741 -->.<!-- /react-text --></td><td class="table-row text-body"><span>On the main menu, choose <strong> Other Transaction</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1748 -->2<!-- /react-text --><!-- react-text: 1749 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Transfer</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1756 -->3<!-- /react-text --><!-- react-text: 1757 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Other Bank Account</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1764 -->4<!-- /react-text --><!-- react-text: 1765 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter <strong>009</strong> (Bank BNI code) and choose <strong>Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1772 -->5<!-- /react-text --><!-- react-text: 1773 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter the full amount to be paid. If the amount entered is not the same as the invoiced amount, the transaction will be declined.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1780 -->6<!-- /react-text --><!-- react-text: 1781 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter <span> 16 digits payment Account No. and press</span> <strong> Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1788 -->7<!-- /react-text --><!-- react-text: 1789 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Amount to be paid, account number, and merchant name will appear on the payment confirmation page. If the information is right, press <strong>Correct</strong>.</span></td></tr></tbody></table>',
                       '1' => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1827 -->1<!-- /react-text --><!-- react-text: 1828 -->.<!-- /react-text --></td><td class="table-row text-body"><span>On the main menu, choose <strong> Others</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1835 -->2<!-- /react-text --><!-- react-text: 1836 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Transfer</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1843 -->3<!-- /react-text --><!-- react-text: 1844 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Online Transfer</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1851 -->4<!-- /react-text --><!-- react-text: 1852 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter <strong>009</strong> (Bank BNI code) and 16 digits Account No. and press <strong>Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1859 -->5<!-- /react-text --><!-- react-text: 1860 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter the full amount to be paid. If the amount entered is not the same as the invoiced amount, the transaction will be declined.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1867 -->6<!-- /react-text --><!-- react-text: 1868 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Empty the transfer reference number and press <strong>Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1875 -->7<!-- /react-text --><!-- react-text: 1876 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Amount to be paid, account number, and merchant name will appear on the payment confirmation page. If the information is right, press <strong>Correct</strong>.</span></td></tr></tbody></table>',
                       '2' => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 1909 -->1<!-- /react-text --><!-- react-text: 1910 -->.<!-- /react-text --></td><td class="table-row text-body"><span>On the main menu, choose <strong> Other Transaction</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1917 -->2<!-- /react-text --><!-- react-text: 1918 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Transfer</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1925 -->3<!-- /react-text --><!-- react-text: 1926 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose <strong>Other Bank Account</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1933 -->4<!-- /react-text --><!-- react-text: 1934 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter <strong>009</strong> (Bank BNI code) and choose <strong>Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1941 -->5<!-- /react-text --><!-- react-text: 1942 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter the full amount to be paid. If the amount entered is not the same as the invoiced amount, the transaction will be declined.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1949 -->6<!-- /react-text --><!-- react-text: 1950 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter <span> 16 digits payment Account No. and press</span> <strong> Correct</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 1957 -->7<!-- /react-text --><!-- react-text: 1958 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Amount to be paid, account number, and merchant name will appear on the payment confirmation page. If the information is right, press <strong>Correct</strong>.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '5' => array(
                    'channel' => 'GO-PAY',
                    'app' => array(
                          '0' => 'GO-PAY'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body">1.</td><td class="table-row text-body"><span>Click <strong>Pay Now with GO-PAY</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body">2.</td><td class="table-row text-body"><span>Open <strong>GO-JEK</strong> app on your phone.</span></td></tr><tr><td class="table-row table-numeric text-body">3.</td><td class="table-row text-body"><span>Click <strong>Pay</strong>.</span><div class="table-images text-center"><img class="qr-instruction" alt="GO-PAY QR Instruction 1" width="460" height="320" src="//d2f3dnusg0rbp7.cloudfront.net/snap/assets/qr-instruction-1-en-1639f6a5416fa00e09ab78c4ca419a3ce19a0bbeff5c1a8df8387121a43219b8.png"></div></td></tr><tr><td class="table-row table-numeric text-body">4.</td><td class="table-row text-body"><span>Point your camera to the <strong>QR Code</strong>.</span><div class="table-images text-center"><img class="qr-instruction" alt="GO-PAY QR Instruction 2" width="460" height="320" src="//d2f3dnusg0rbp7.cloudfront.net/snap/assets/qr-instruction-2-76e4903a94594acce1954b8b037bb321ce1770d703406ba8f2d4290d368ee574.png"></div></td></tr><tr><td class="table-row table-numeric text-body">5.</td><td class="table-row text-body"><span>Check your payment details in the <strong>GO-JEK</strong> app and then tap <strong>Pay</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body">6.</td><td class="table-row text-body"><span>Your transaction is done.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '6' => array(
                    'channel' => 'KlikBCA',
                    'app' => array(
                          '0' => 'KlikBCA'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 2401 -->1<!-- /react-text --><!-- react-text: 2402 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Visit KlikBCA website <strong>www.klikbca.com</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2407 -->2<!-- /react-text --><!-- react-text: 2408 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Login using your KlikBCA’s userID.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2413 -->3<!-- /react-text --><!-- react-text: 2414 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose e-commerce Payment Menu.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2419 -->4<!-- /react-text --><!-- react-text: 2420 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose Category Others.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2425 -->5<!-- /react-text --><!-- react-text: 2426 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose Company Name.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2431 -->6<!-- /react-text --><!-- react-text: 2432 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Click Continue.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2437 -->7<!-- /react-text --><!-- react-text: 2438 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Choose the transaction that you want to pay and choose continue.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2443 -->8<!-- /react-text --><!-- react-text: 2444 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Re-confirm the payment by inputting the token key and choose submit/continue.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '7' => array(
                    'channel' => 'BCA KlikPay',
                    'app' => array(
                          '0' => 'BCA KlikPay'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 2592 -->1<!-- /react-text --><!-- react-text: 2593 -->.<!-- /react-text --></td><td class="table-row text-body"><span>You will be redirected to BCA KlikPay page once you click "Pay Now" button.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2598 -->2<!-- /react-text --><!-- react-text: 2599 -->.<!-- /react-text --></td><td class="table-row text-body"><span>After Login to your BCA Klikpay Account by entering your email address and password, it will display transaction information such as merchant name, transaction time, and amount to be paid. Choose the type of payment KlikBCA or BCA Card for the transaction.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2604 -->3<!-- /react-text --><!-- react-text: 2605 -->.<!-- /react-text --></td><td class="table-row text-body"><span>To authorize payment with BCA KlikPay, press the "send OTP" button, and you will receive an OTP (One Time Password) code sent via SMS to your mobile phone. Enter the OTP code in the fields provided.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2610 -->4<!-- /react-text --><!-- react-text: 2611 -->.<!-- /react-text --></td><td class="table-row text-body"><span>If your OTP code is correct, your payment will be processed immediately and your account balance (for KlikBCA payment type) or your BCA Card limit (for BCA Card payment type) will be reduced according to amount of transaction value.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2616 -->5<!-- /react-text --><!-- react-text: 2617 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Your transaction success status will appear on the transaction screen and you will receive a notification email.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 2622 -->6<!-- /react-text --><!-- react-text: 2623 -->.<!-- /react-text --></td><td class="table-row text-body"><span>For more information about BCA KlikPay please contact Halo BCA at 1500888 or visit http://klikbca.com/KlikPay/klikpay.html</span></td></tr></tbody></table>'
                     ),
                  ),
                  '8' => array(
                    'channel' => 'Mandiri Clickpay',
                    'app' => array(
                          '0' => 'Mandiri Clickpay'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 3382 -->1<!-- /react-text --><!-- react-text: 3383 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Activate your Mandiri token by pressing <span class="red-token">◄</span>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3388 -->2<!-- /react-text --><!-- react-text: 3389 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter your Mandiri token password.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3394 -->3<!-- /react-text --><!-- react-text: 3395 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Press 3 when it displays <strong>APPLI</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3400 -->4<!-- /react-text --><!-- react-text: 3401 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter <strong>Input 1</strong> and press <span class="red-token">◄</span> for 3 seconds.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3406 -->5<!-- /react-text --><!-- react-text: 3407 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter <strong>Input 2</strong> and press <span class="red-token">◄</span> for 3 seconds.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3412 -->6<!-- /react-text --><!-- react-text: 3413 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter <strong>Input 3</strong> and press <span class="red-token">◄</span> for 3 seconds.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3418 -->7<!-- /react-text --><!-- react-text: 3419 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Put the response code on your Mandiri token into the <strong>Type challenge response</strong> field.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '9' => array(
                    'channel' => 'CIMB Clicks',
                    'app' => array(
                          '0' => 'CIMB Clicks'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 3186 -->1<!-- /react-text --><!-- react-text: 3187 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Please make sure that you have a User ID for CIMB Clicks and have registered your mPIN before going through with the payment.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3192 -->2<!-- /react-text --><!-- react-text: 3193 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Payment via CIMB Clicks will be processed online and your CIMB bank account balance will be deducted automatically based on your total amount.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3198 -->3<!-- /react-text --><!-- react-text: 3199 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Transaction will be cancelled if payment is not completed within 2 hours.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3204 -->4<!-- /react-text --><!-- react-text: 3205 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Please make sure that there is no pop-up blocker on your browser.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '10' => array(
                    'channel' => 'Danamon Online Banking',
                    'app' => array(
                          '0' => 'Danamon Online Banking'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 3572 -->1<!-- /react-text --><!-- react-text: 3573 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter Danamon Online Banking User ID and Password and select the source of fund.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3578 -->2<!-- /react-text --><!-- react-text: 3579 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Check transaction details, enter Token Code, and click Next.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3584 -->3<!-- /react-text --><!-- react-text: 3585 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Payment confirmation will be displayed and payment is completed. Keep the merchant reference and payment reference no. and click Next to return to merchant website.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '11' => array(
                    'channel' => 'e-Pay BRI',
                    'app' => array(
                          '0' => 'e-Pay BRI'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 3738 -->1<!-- /react-text --><!-- react-text: 3739 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Please make sure that you have a User ID for BRI e-Pay and have registered for mTOKEN before going through with the payment.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3744 -->2<!-- /react-text --><!-- react-text: 3745 -->.<!-- /react-text --></td><td class="table-row text-body"><span>To obtain a User ID for BRI e-Pay, please register at the nearest BRI ATM.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3750 -->3<!-- /react-text --><!-- react-text: 3751 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Please visit the nearest BRI branch office to register the mTOKEN.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3756 -->4<!-- /react-text --><!-- react-text: 3757 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Payment via BRI e-Pay will be processed online and your BRI bank account balance will be deducted automatically based on your total amount.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3762 -->5<!-- /react-text --><!-- react-text: 3763 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Your transaction will be cancelled if payment is not completed within 2 hours.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '12' => array(
                    'channel' => 'LINE Pay e-cash | mandiri e-cash',
                    'app' => array(
                          '0' => 'LINE Pay e-cash | mandiri e-cash'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 3913 -->1<!-- /react-text --><!-- react-text: 3914 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Make sure you have registered your mobile phone number for LINE Pay e-cash | mandiri e-cash.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3919 -->2<!-- /react-text --><!-- react-text: 3920 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Click <strong>Pay Now</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3925 -->3<!-- /react-text --><!-- react-text: 3926 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter your <strong>phone number</strong> and <strong>PIN</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3931 -->4<!-- /react-text --><!-- react-text: 3932 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Please wait for an SMS from Mandiri e-cash that contains your One Time Password (OTP).</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3937 -->5<!-- /react-text --><!-- react-text: 3938 -->.<!-- /react-text --></td><td class="table-row text-body"><span><strong>Enter the 6 digit OTP</strong>, then click <strong>pay</strong> to make payment.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 3943 -->6<!-- /react-text --><!-- react-text: 3944 -->.<!-- /react-text --></td><td class="table-row text-body"><span>If you havent received the OTP or the OTP has expired, click cancel and retry your transaction.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '13' => array(
                    'channel' => 'Indomaret',
                    'app' => array(
                          '0' => 'Indomaret'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 4093 -->1<!-- /react-text --><!-- react-text: 4094 -->.<!-- /react-text --></td><td class="table-row text-body"><span>After confirming your payment, we will issue you a unique Payment Code number.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 4099 -->2<!-- /react-text --><!-- react-text: 4100 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Note down your Payment Code and total amount. Dont worry, we will also mail you a copy of this payment instructions to your email.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 4105 -->3<!-- /react-text --><!-- react-text: 4106 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Go to an Indomaret store near you and provide the cashier with the Payment Code number.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 4111 -->4<!-- /react-text --><!-- react-text: 4112 -->.<!-- /react-text --></td><td class="table-row text-body"><span>The cashier will then confirm the transaction by asking for the transaction amount and the merchant name.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 4117 -->5<!-- /react-text --><!-- react-text: 4118 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Confirm the payment with the cashier.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 4123 -->6<!-- /react-text --><!-- react-text: 4124 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Your transaction is successful! You should be receiving an email confirming your payment. Please keep your Indomaret payment receipt just in case you need help via support.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '14' => array(
                    'channel' => 'Alfamart',
                    'app' => array(
                          '0' => 'Alfamart'
                        ),
                     'desc' => array(
                       '0'  => '<span>Pay from any <strong>Alfamart</strong>, <strong>Alfamidi</strong>, or <strong>Dan+Dan</strong> outlet.</span><table class="table"><tbody><tr><td class="table-row table-numeric text-body">1.</td><td class="table-row text-body"><span>Go to an <strong>Alfamart</strong>, <strong>Alfamidi</strong>, or <strong>Dan+Dan</strong> store near you and provide the cashier with the <strong>Payment Code</strong> number.</span></td></tr><tr><td class="table-row table-numeric text-body">2.</td><td class="table-row text-body"><span>The cashier will then confirm the transaction by asking for the <strong>transaction amount</strong> and the  <strong>merchant name</strong>.</span></td></tr><tr><td class="table-row table-numeric text-body">3.</td><td class="table-row text-body"><span>Confirm the payment with the cashier to proceed payment.</span></td></tr><tr><td class="table-row table-numeric text-body">4.</td><td class="table-row text-body"><span>Your transaction is successful! You should be receiving an email confirming your payment. Please keep your payment receipt just in case you need help via support.</span></td></tr></tbody></table>'
                     ),
                  ),
                  '15' => array(
                    'channel' => 'Akulaku',
                    'app' => array(
                          '0' => 'Akulaku'
                        ),
                     'desc' => array(
                       '0'  => '<table class="table"><tbody><tr><td class="table-row table-numeric text-body"><!-- react-text: 226 -->1<!-- /react-text --><!-- react-text: 227 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Click the "Pay Now" button, then you will be directed to the Akulaku Payment Center page.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 232 -->2<!-- /react-text --><!-- react-text: 233 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Select the desired installment tenor, then Login to your Akulaku account by entering your mobile number and password.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 238 -->3<!-- /react-text --><!-- react-text: 239 -->.<!-- /react-text --></td><td class="table-row text-body"><span>Enter the verification code (OTP) that has been sent to your mobile number, then click the "Next" button.</span></td></tr><tr><td class="table-row table-numeric text-body"><!-- react-text: 244 -->4<!-- /react-text --><!-- react-text: 245 -->.<!-- /react-text --></td><td class="table-row text-body"><span>A confirmation page will be shown to you, then you can finish your transaction.</span></td></tr></tbody></table>'
                     ),
                  ),
              );
       return $guide[$id];
    }

}
