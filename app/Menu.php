<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';
    protected $fillable = [
      'title', 'url', 'position', 'description', 'type'
    ];
    protected $primaryKey = 'id_menu';
    
    /**
     * Mendapatkan url melalui aksesor
     */

    // public function getValAttribute()
    // {
    //     return $this->attributes['url'];
    // }

    // /**
    //  * Mendapatkan url yang sudah di format melalui aksesor
    //  */

    // public function getUrlAttribute() {
    //   switch($this->attributes['type']) {
    //     case 'genre':
    //       if (env('APP_TYPE') == 'master') {
    //         $cat = Category::find($this->attributes['url']);
    //         if ($cat === null) {
    //             return null;
    //         }
    //         return route('category.show', [ 'genre', $cat->slug ]);
    //       } else {
    //         $md = new MovieData;
    //         $cat = json_decode($md->getGenreData($this->attributes['url']));
    //         if ($cat == null)
    //           return null;
    //         return route('category.show', [ 'genre', $cat->slug ]);
    //       }
    //     break;

    //     case 'tag':
    //       if(env('APP_TYPE') == 'master') {
    //         $tag = Tag::find($this->attributes['url']);
    //         if($tag == null)
    //           return null;
    //       } else {
    //         $md = new MovieData;
    //         $tag = json_decode($md->getTagData($this->attributes['url']));
    //         if ($tag == null)
    //           return null;
    //       }
    //       return route('tag.show', [ $tag->slug ]);
    //     break;

    //     case 'keyword':
    //       $cat = URLGlobal::find($this->attributes['url']);
    //       if($cat === null)
    //         return null;
    //       return route('urlglobal', $cat->slug);
    //     break;

    //     case 'external' :
    //       return $this->attributes['url'];
    //     break;

    //     case 'page' :
    //       $page = Page::find($this->attributes['url']);
    //       if($page === null)
    //         return null;
    //       return route('urlglobal', $page->slug);
    //     break;
    //   }
    // }
}
