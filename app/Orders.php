<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //
    protected $fillable = ['user_id','total','payment_id','delivered', 'payment_status, payment_type'];

    public function orderItems()
    {
        return $this->belongsToMany(Products::class,'order_product','product_id','order_id')->withPivot('id','qty','total', 'delivery_status', 'delivery_data', 'price', 'discount');
    }
        public function user() {
            return $this->belongsTo('App\User');
        }


}
