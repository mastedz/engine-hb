<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table='shop';
    protected $fillable=['name', 'user_id', 'status', 'description', 'is_flagship', 'shop_address', 'shop_city', 'shop_slug', 'image', 'delivery', 'province', 'district', 'note'];
    public function user() {

        return $this->belongsTo('App\User');

    }
}
