<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = [
      'title', 'slug', 'description', 'schema', 'meta_title', 'meta_description', 'meta_keyword', 'id_user'
    ];
    protected $primaryKey = 'id_page';

    public function user() {
      return $this->hasOne('App\User', 'id', 'id_user');
    }
}
