<?php
namespace App\Http\Controllers;

use App\Helper;
use App\Orders;
use App\Products;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware(['auth', 'admin']);
    }

    public function orders($type = '') {
        //
        if ($type == 'pending') {
            $orders = Orders::where('delivered', '0')->where('user_id', Auth::user())->get();

        }
        elseif ($type == 'delivered') {
            $orders = Orders::where('delivered', '1')->where('user_id', Auth::user())->get();

        }
        else {

            $orders = Orders::all();
        }



        return view('backend.order', compact('orders'));
    }

    public function toggledeliver(Request $request, $orderId) {
        $order = Orders::find($orderId);
        if ($request->has('delivered')) {
            $order->delivered = $request->delivered;
        }
        else {
            $order->delivered = "0";
        }

        $order->save();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //

    }

    public function trxSales(Request $request) {
    // $confirmation = Orders::wherePaymentStatus('done')->whereDelivered('0')
    //                   ->whereVendorId(Auth::user()->id)
    //                   ->get()->count();

            $confirmation = Orders::whereHas('orderItems', function ($query) {
                      $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 0);

                    })->with('orderItems')->where('payment_status', '=', 'settlement')->count();

            $received = Orders::whereHas('orderItems', function ($query) {
                      $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 3);

                    })->with('orderItems')->where('payment_status', '=', 'settlement')->count();

      return response()->json(['title' => 'data trx','status' => 'success', 'confirmation' => $confirmation, 'received' => $received], 201);
    }

    public function orderDetail($id) {
      $user = User::where('id', Auth::user()->id)
          ->with('address')
          ->first();
      $address = $user->address()
          ->where('is_primary', 1)
          ->first();

      $cities = Helper::non()->getCity();
      $city = $cities[$address->city - 1]['type'] . ' ' . $cities[$address->city - 1]['city_name'];
      $province = $cities[$address->city - 1]['province'];

      $order = Orders::whereId($id)->with('orderItems')->first();
      $trx = $order->orderItems;
      foreach ($trx as $key => $value) {
          $idproduct[] = $value->id;
          $status[] = $value->pivot->delivery_status;
          $qty[] = $value->pivot->qty;
      }

      $numbers = $status;
      $count = array_count_values($numbers);
      arsort($count);
      $status = key($count);



      $cartItems = Products::whereIn('id', $idproduct)->with('shop')->get();

      foreach ($cartItems as $key => $value) {
          $shops[] = Shop::whereId($value->shop_id)
              ->first();
          $qtyShop[] = $value->shop_id;
      }

      $qtyShop = array_unique($qtyShop);
      // dd($cartItems);
      return view('frontend.transaction.order_detail', compact('order', 'qty', 'cartItems', 'shops', 'qtyShop','order', 'status', 'address', 'city','province'));
    }

}
