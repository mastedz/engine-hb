<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;
use Auth;
use App\Veritrans\Midtrans;
use App\Products;
use App\User;
use Cache;
use Session;
class SnapController extends Controller {
    public function __construct() {
        Midtrans::$serverKey = env('MIDTRANS_SERVER_KEY');
        //set is production to true for production mode
        Midtrans::$isProduction =  env('MIDTRANS_PROD');
    }

    public function snap() {
        return view('snap_checkout');
    }

    public function token(Request $r) {

        error_log('masuk ke snap token dri ajax');
        $midtrans = new Midtrans;
        $prod = Products::whereIn('id', $r->product)
            ->get();
        $gross_amount = 0;
        $summary_cost = 0;
        foreach ($r->cost as $key => $value) {
            $summary_cost += explode(",", $value) [2];
            $service[] = explode(",", $value) [0].','.explode(",", $value) [1];
        }
        // dd($summary_cost);
        $service = json_encode($service);
        foreach ($prod as $key => $value) {
            if (strlen($value->name) > 10) {
                $value->name = substr($value->name, 0, 20) . '...';
            }
            $items[] = ['id' => $value->id, 'price' => $value->price - ($value->price * $value->discount / 100) , 'quantity' => 1, 'name' => $value->name];
            $gross_amount += $value->price - ($value->price * $value->discount / 100);

        }
        $items[count($items) ] = ['id' => 99, 'price' => $summary_cost, 'quantity' => 1, 'name' => 'Ongkir'];
        $transaction_details = array(
            'order_id' => uniqid() ,
            'gross_amount' => $gross_amount + $summary_cost
        );
        Cache::put($transaction_details['order_id'], $items, 1660);
        Cache::put($transaction_details['order_id'] . "cart", $r->product, 1660);
        $user = User::where('id', Auth::user()->id)
            ->with('address')
            ->first();

        // Populate items
        $address = $user->address()
            ->where('is_primary', 1)
            ->first();
        // Populate customer's billing address
        $billing_address = array(
            'first_name' => $address->receiver,
            'last_name' => " ",
            'address' => $address->address,
            'city' => $address->city,
            'postal_code' => " ",
            'phone' => $address->phoneno,
            'country_code' => 'IDN'
        );

        // Populate customer's shipping address
        $shipping_address = array(
            'first_name' => $address->receiver,
            'last_name' => " ",
            'address' => $address->address,
            'city' => $address->city,
            'postal_code' => " ",
            'phone' => $address->phoneno,
            'country_code' => 'IDN'
        );

        // $user = Auth::user();
        $customer_details = array(
            'first_name' => $user->name,
            'last_name' => " ",
            'email' => $user->email,
            'phone' => $address->phoneno,
            'billing_address' => $billing_address,
            'shipping_address' => $shipping_address
        );

        // Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;
        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O", $time) ,
            'unit' => 'hour',
            'duration' => 2
        );

        $transaction_data = array(
            'transaction_details' => $transaction_details,
            'item_details' => $items,
            'customer_details' => $customer_details,
            'credit_card' => $credit_card,
            'expiry' => $custom_expiry
        );

        try {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            //return redirect($vtweb_url);
          return ["token" => $snap_token, "service" =>$service];
        }
        catch(Exception $e) {
            return $e->getMessage;
        }
    }

    public function finish(Request $request) {
        $res = json_decode($request->result_data);
        Cache::put($res->transaction_id, $res, 86400);
        $user = Auth::user();
        $vendor = Products::all();
        $order = $user->orders()
            ->create(['total' => $res->gross_amount,  'payment_id' => $res->transaction_id, 'vendor_id' => 0, 'payment_status' => $res->transaction_status, 'payment_type'=>$res->payment_type]);

        $cartItems = Cart::content();
        // dd($cartItems);
        $i=0;
        foreach ($cartItems as $key => $cartItem) {
            $service = json_decode($request->service)[$i];
            $i++;
            $order->orderItems()
                ->attach($cartItem->id, ['delivery_status' => 0, 'delivery_data' => json_encode(['provider' => explode(',',$service)[0], 'service' => explode(',',$service)[1]]) , 'qty' => $cartItem->qty, 'price'=>$cartItem->price,'total' => $cartItem->qty * $cartItem->price]);
        }
        Session::forget('cart');
        return response()
            ->json(['title' => 'Sukses', 'message' => 'Menunggu Pembayaran Valid', 'status' => 'success'], 201);

    }

    public function notification() {
        $midtrans = new Midtrans;
        echo 'test notification handler';
        $json_result = file_get_contents('php://input');
        $result = json_decode($json_result);

        if ($result) {
            $notif = $midtrans->status($result->order_id);
        }

        error_log(print_r($result, true));

        /*
        $transaction = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud = $notif->fraud_status;

        if ($transaction == 'capture') {
          // For credit card transaction, we need to check whether transaction is challenge by FDS or not
          if ($type == 'credit_card'){
            if($fraud == 'challenge'){
              // TODO set payment status in merchant's database to 'Challenge by FDS'
              // TODO merchant should decide whether this transaction is authorized or not in MAP
              echo "Transaction order_id: " . $order_id ." is challenged by FDS";
              }
              else {
              // TODO set payment status in merchant's database to 'Success'
              echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
              }
            }
          }
        else if ($transaction == 'settlement'){
          // TODO set payment status in merchant's database to 'Settlement'
          echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
          }
          else if($transaction == 'pending'){
          // TODO set payment status in merchant's database to 'Pending'
          echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
          }
          else if ($transaction == 'deny') {
          // TODO set payment status in merchant's database to 'Denied'
          echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
        }*/

    }
}
