<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Veritrans\Veritrans;

use App\Address;
use App\Helper;
use App\Orders;
use App\Shop;
use App\User;
use Auth;
use DB;
use Cache;
class TransactionController extends Controller {

    public function pembelian(Request $request) {
      // dd($request->mode);

        $user = Auth::user();

        $orders['waiting_payment'] = Orders::whereHas('orderItems', function ($query) {
                  $query->where('delivery_status', 0);

                })->with('orderItems')->where('payment_status', '=', 'pending')->where('user_id', Auth::user()->id)->get();

        $orders['success_payment'] = Orders::whereHas('orderItems', function ($query) {
                 $query->where('delivery_status', 0);

               })->with('orderItems')->where('payment_status', '=', 'settlement')->where('user_id', Auth::user()->id)->get();


        $orders['packed'] = Orders::whereHas('orderItems', function ($query) {
                  $query->where('delivery_status', 1);

                })->with('orderItems')->where('user_id', Auth::user()->id)->get();

        $orders['on_delivery'] = Orders::whereHas('orderItems', function ($query) {
                  $query->where('delivery_status', 2);

                })->with('orderItems')->where('user_id', Auth::user()->id)->get();

        $orders['delivered'] = Orders::whereHas('orderItems', function ($query) {
                  $query->where('delivery_status', 3);

                })->with('orderItems')->where('user_id', Auth::user()->id)->get();
        $orders['canceled'] = Orders::whereHas('orderItems', function ($query) {
                  $query->where('delivery_status', 4);

                })->with('orderItems')->where('user_id', Auth::user()->id)->get();
        $orders['all'] = Orders::whereUserId($user->id)
            ->get();

        // dd($orders);
        if ($request->mode ==null) {
          return view('frontend.transaction.order2', compact('orders'));
        }else{
          switch ($request->mode) {
            case 'waiting_payment':
            // dd($orders['waiting_payment']);
              $data=[];
              $order = $orders['waiting_payment'];
              foreach ($order as $key => $value) {
                $data[]= Cache::get($value->payment_id);
              }

              return view('frontend.transaction.invoice', compact('data'));
              // dd($data);
              break;
              case 'on_delivery':
              $orders= $orders['on_delivery'];
              return view('frontend.transaction.order', compact('orders'));
                break;
              case 'packed':
              $orders= $orders['packed'];
              return view('frontend.transaction.order', compact('orders'));
                break;
              case 'delivered':
              $orders= $orders['delivered'];
              return view('frontend.transaction.order', compact('orders'));
                break;
              case 'canceled':
              $canceled = true;
              // dd($orders['canceled']);
              $orders= $orders['canceled'];
              return view('frontend.transaction.order', compact('orders', 'canceled'));
                break;

            default:
              $orders= $orders['success_payment'];
              return view('frontend.transaction.order', compact('orders'));
              break;
          }
        }


    }
    public function transactionUpdate(Request $r)
    {
      if ($r->denyOrder == 1) {
        $update = DB::table('order_product')->where('id', '=', $r->id)->update(['delivery_status' => 4]);
        return redirect('/my/sales');
      }
      $trxs =  DB::table('order_product')->where('id', '=', $r->id)->first();
      $delivery_data = (array)json_decode($trxs->delivery_data);
      $delivery_data['resi'] = $r->resi;

      $update = DB::table('order_product')->where('id', '=', $r->id)->update(['delivery_status' => $r->status, 'delivery_data'=> json_encode($delivery_data)]);
      return redirect('/trx/sales');

    }
    public function sales($type){
      if($type == 'confirmation'){
        $trxs =  Orders::whereHas('orderItems', function ($query) {
                  $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 0);

                })->with('orderItems')->where('payment_status', '=', 'settlement')->get();

      }elseif($type == 'received'){
        $trxs =  Orders::whereHas('orderItems', function ($query) {
                  $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 3);

                })->with('orderItems')->where('payment_status', '=', 'settlement')->get();

      }elseif($type == 'done'){
        $trxs =  Orders::whereHas('orderItems', function ($query) {
                  $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 1);

                })->with('orderItems')->where('payment_status', '=', 'settlement')->get();

      }elseif($type == 'failed'){
        $trxs = Orders::whereHas('orderItems', function ($query) {
                  $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 4);

                })->with('orderItems')->where('payment_status', '=', 'settlement')->get();

      }

      return view('frontend.merchant.list', compact('trxs', 'type'));
    }
    public function penjualan(Request $r){
      $orders = Orders::with('orderItems')->get();
      dd($order);
      return view('frontend.transaction.sold', compact('orders'));
    }
    public function mySales(Request $request) {
        $user = Auth::user();
        $orders = Orders::whereHas('orderItems', function ($query) use ($user) {
                    $query->where('shop_id', '=', $user->id);
                })->with('orderItems')->get();
        // dd($orders);
        return view('frontend.merchant.my_sales', compact('user'));
    }


    public function transaction() {
        return view('transaction');
    }

    public function transaction_process(Request $request) {
        $vt = new Veritrans;
        $order_id = $request->input('order_id');
        $action = $request->input('action');
        switch ($action) {
            case 'status':
                $this->status($order_id);
            break;
            case 'approve':
                $this->approve($order_id);
            break;
            case 'expire':
                $this->expire($order_id);
            break;
            case 'cancel':
                $this->cancel($order_id);
            break;
        }
    }

    public function status($order_id) {
        $vt = new Veritrans;
        echo 'test get status </br>';
        print_r($vt->status($order_id));
    }

    public function cancel($order_id) {
        $vt = new Veritrans;
        echo 'test cancel trx </br>';
        echo $vt->cancel($order_id);
    }

    public function approve($order_id) {
        $vt = new Veritrans;
        echo 'test get approve </br>';
        print_r($vt->approve($order_id));
    }

    public function expire($order_id) {
        $vt = new Veritrans;
        echo 'test get expire </br>';
        print_r($vt->expire($order_id));
    }

    public function trxDetail(Request $request) {
        $trx = Orders::whereId($request->trxId)
            ->with('orderItems')
            ->first();
        $address = Address::whereUserId($trx->user_id)
            ->whereIsPrimary('1')
            ->first();
        $cities = Helper::non()->getCity();
        $trx->city = $cities[$address->city - 1]['type'] . ' ' . $cities[$address->city - 1]['city_name'];
        $trx->province = $cities[$address->city - 1]['province'];
        $trx->buyer_name = User::whereId($trx->user_id)
            ->first()->name;
        $trx->address_name = $address->name;
        $trx->address = $address->address;
        $trx->phone = $address->phoneno;
        $trx->postcode = $address->postcode;
        return json_encode($trx);
    }

    public function trxDetailProduct(Request $request) {
        $trx = Orders::whereId($request->trxId)
            ->with('orderItems')
            ->first()->orderItems;
        return json_encode($trx);
    }

    public function myOrder($type){
      // dd($type);
      if($type == 'orders'){
        $order = Orders::whereUserId(Auth::user()->id)->with('orderItems')->get();
        dd($order);
        $trx = $order->orderItems;
        foreach ($trx as $key => $value) {
            $idproduct[] = $value->id;
            $status[] = $value->pivot->delivery_status;
            $qty[] = $value->pivot->qty;
        }

        $orders =  Orders::whereHas('orderItems', function ($query) {
                  $query
                  ->where('shop_id', '=', Auth::user()->id)
                  ->where('delivery_status', 0);

                })->with('orderItems')->where('payment_status', '=', 'settlement')->get();
                dd($orders);

      }elseif($type == 'packed'){
        $orders =  Orders::whereHas('orderItems', function ($query) {
                  $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 3);

                })->with('orderItems')->where('payment_status', '=', 'settlement')->get();

      }elseif($type == 'delivered'){
        $orders =  Orders::whereHas('orderItems', function ($query) {
                  $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 1);

                })->with('orderItems')->where('payment_status', '=', 'settlement')->get();

      }elseif($type == 'completed'){
        $orders =  Orders::whereHas('orderItems', function ($query) {
                  $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 1);

                })->with('orderItems')->where('payment_status', '=', 'settlement')->get();

      }elseif($type == 'canceled'){
        $orders = Orders::whereHas('orderItems', function ($query) {
                  $query->where('shop_id', '=', Auth::user()->id)->where('delivery_status', 4);

                })->with('orderItems')->where('payment_status', '=', 'settlement')->get();

      }

      return view('frontend.transaction.order', compact('orders', 'type'));
    }

    public function trxMetode(Request $request) {
        $user = Auth::user();

        return view('frontend.transaction.invoice', compact('user'));
    }

    public function trxGuide(Request $request) {
        $user = Auth::user();

        return view('frontend.transaction.metode_bayar', compact('user'));
    }

}
