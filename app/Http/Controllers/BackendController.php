<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use DB;
use App\Orders;
use App\Shop;
class BackendController extends Controller {
    //
    public function __construct() {
        $this->middleware(['auth', 'admin']);
    }

    public function admin() {
      $stats['user'] = DB::select(DB::raw("SELECT count(*) as c, DATE(created_at) as d FROM users GROUP BY DATE(users.created_at)"));
      $stats['merchant'] = DB::select(DB::raw("SELECT count(*) as c, DATE(created_at) as d FROM shop GROUP BY DATE(shop.created_at)"));
      $stats['transaction'] = DB::select(DB::raw("SELECT count(*) as c, DATE(created_at) as d FROM orders GROUP BY DATE(orders.created_at)"));
    
        $data['user'] = User::count();
        $data['transaction'] = Orders::count();
        $data['shops'] = Shop::count();
        $data['pending_approve'] = Shop::whereStatus(0)->count();
        return view('backend.dashboard', compact('data'));
    }
    public function getStats() {

        return response()->json($stats);
    }
    public function categories() {
        return view('backend.categories');
    }
    public function products() {
        return view('backend.products');
    }
    public function brands() {
        return view('backend.brands');
    }
    public function order() {
        return view('backend.order');
    }
    public function getLogout() {
        Auth::logout();
        Session::flush();
        return redirect()->route('frontend.signin');
    }
}
