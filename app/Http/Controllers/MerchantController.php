<?php
namespace App\Http\Controllers;

use Log;
use Redirect;
use Session;
use Storage;
use Intervention\Image\Facades\Image;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Category;
use App\Brands;
use App\Helper;
use App\Orders;
use App\Products;
use App\Shop;
use App\ShopAssigment;

class MerchantController extends Controller {
    public function create(Request $request) {
        $name = $request->name;
        $domain = $request->domain;
        $id = Shop::create(['name' => $name, 'user_id' => Auth::user()->id, 'status' => '0', 'description' => '', 'is_flagship' => '0', 'shop_address' => '', 'shop_city' => 0, 'shop_slug' => $domain, 'image' => '', 'delivery' => ''])->id;
        Log::info('id : ' . $id);

        if ($id) {
            $key = 'id_' . Auth::user()->id;
            $session = Session::put($key, $id, 3600);
            return redirect()->route('frontend.step');
        }
        else {
            return Redirect::back()
                ->with('error', 'Data Gagal Insert!');
        }
    }
    public function approveShop(Request $r){
      $status = 1;
      $note = "";
      $shop = Shop::whereId($r->id)->with('user')->first()->toArray();

      if ($r->status == 'Deny') {
        $shop['note'] = $r->note;
        Mail::send('emails.denial', $shop, function ($message) use ($shop) {
            $message->to($shop['user']['email']);
            $message->subject('haribelanja.com - Merchant Request Denied');
        });
        $status = 2;
        $note = $r->note;
      }else{
        Mail::send('emails.approval', $shop, function ($message) use ($shop) {
            $message->to($shop['user']['email']);
            $message->subject('haribelanja.com - Merchant Request Approved');
        });
      }
      $update = Shop::whereId($r->id)->update(['status' => $status, 'note'=>$note]);
      // dd($r->id);
      if ($update) {
        return redirect('merchant');
      }
    }
    public function createStep(Request $request) {
        $validator = $this->validate($request, ['id' => 'required', 'description' => 'required', 'logo' => 'required', 'product' => 'required', 'ktp' => 'required', 'selfie' => 'required', 'npwp' => 'required', 'province' => 'required', 'city' => 'required', 'district' => 'required', 'zipcode' => 'required', 'address' => 'required', 'jasa' => 'required'

        ]);

        $description = $request->description;
        $logo = $request->file('logo');
        $product = $request->file('product');
        $ktp = $request->file('ktp');
        $selfie = $request->file('selfie');
        $npwp = $request->file('npwp');
        $province = $request->province;
        $city = $request->city;
        $district = $request->district;
        // $zipcode     = $request->zipcode;
        $address = $request->address;
        $jasa = json_encode($request->jasa);
        $id = $request->id;

        //logo
        $nameLogo = 'logo-' . Auth::user()->id;
        $resizeLogo = Image::make($logo)->fit(300);
        $extension = '.' . $logo->getClientOriginalExtension();
        $store = Storage::put('public/images/' . $nameLogo . $extension, $resizeLogo->encode());

        try {
            $update = Shop::whereId($id)->update(['description' => $description, 'shop_address' => $address, 'shop_city' => $city, 'image' => $nameLogo . $extension, 'delivery' => $jasa, 'province' => $province, 'district' => $district]);
            Log::info('Shop berhasil di tambahkan status 1');
        }
        catch(\Exception $e) {
            Log::info('Shop gagal disimpan' . print_r($e->getMessage() , true));
            return redirect()
                ->back()
                ->withErrors(['msg', 'Merchant Gagal Disimpan!']);
        }

        //product
        $nameproduct = 'product-' . Auth::user()->id;
        $resizeLogo = Image::make($product)->fit(500);
        $extension = '.' . $product->getClientOriginalExtension();
        $store = Storage::put('public/images/' . $nameproduct . $extension, $resizeLogo->encode());

        //ktp
        $namektp = 'ktp-' . Auth::user()->id;
        $resizeLogo = Image::make($ktp)->fit(500);
        $extension = '.' . $ktp->getClientOriginalExtension();
        $store = Storage::put('public/images/' . $namektp . $extension, $resizeLogo->encode());

        //ktp
        $namenpwp = 'npwp-' . Auth::user()->id;
        $resizeLogo = Image::make($npwp)->fit(500);
        $extension = '.' . $npwp->getClientOriginalExtension();
        $store = Storage::put('public/images/' . $namenpwp . $extension, $resizeLogo->encode());

        $attach_file = array(
            'product' => $nameproduct.".". $product->getClientOriginalExtension(),
            'ktp' => $namektp.".". $ktp->getClientOriginalExtension(),
            'npwp' => $namenpwp.".". $npwp->getClientOriginalExtension(),
            'logo' => $nameLogo.".". $logo->getClientOriginalExtension()
        );

        try {
            $assigment = ShopAssigment::insert(['shop_id' => $id, 'user_id' => Auth::user()->id, 'attach_file' => json_encode($attach_file) ]);
            Log::info('Shop Assigment has been create');
            return redirect()->route('frontend.biodata');
        }
        catch(\Exception $e) {
            Log::info('Shop failed created' . print_r($e->getMessage() , true));
            return redirect()
                ->back()
                ->withErrors(['msg', 'Merchant Gagal Disimpan!']);
        }
        return '00';
    }
    public function detailAssigment($shop) {
        // dd($shop);

        $data = ShopAssigment::whereId($shop)->first();

        $attachment = json_decode($data['attach_file']);
        // dd($data);
        $files['npwp'] = $attachment->npwp ;

        $files['ktp'] =  $attachment->ktp ;

        $files['product'] =  $attachment->product ;

        $files['logo'] =  $attachment->logo;
        $merchant = ShopAssigment::with(['shop', 'user'])->where('id', $shop)->first();

        return view('backend.merchant.detail-merchant', compact('files', 'merchant'));

    }

    public function getMerchant() {
        $merchants = ShopAssigment::with('shop')->whereHas('shop', function ($query) {
                  $query->where('status', '=', 0);

                })->get();
        return view('backend.merchant.index-merchant', compact('merchants'));
    }

    public function detail($slug) {
        $id = Shop::whereShopSlug($slug)->first();
        // dd($id);
        if ($id) {
            $user = Auth::user();
            $categories = Category::find($id->user_id);
            $brands = Brands::find($id->user_id);
            $shop = Shop::whereUserId($id->user_id)
                ->first();
            // dd($shop);
            $city = Helper::getCity() [$id->shop_city]['city_name'];
            $province = Helper::getCity() [$id->shop_city]['province'];
            if ($brands) {
                $products = Products::where('brand_id', $brands->id)
                    ->where('category_id', $brands->category_id)
                    ->paginate(32);
            }
            else {
                $products = null;
            }
        }
        else {
            abort(404);
        }

        return view('frontend.detail_merchant', compact('province', 'city', 'categories', 'user', 'brands', 'products', 'shop'));
    }

    public function edit(Request $request) {
        $input = $request->all();
        $shop = Shop::findOrFail($request->id);
        // $this->authorize('modify', $shop);
        $data['name'] = $request->input('name');
        $data['shop_address'] = $request->input('address');
        $data['shop_city'] = $request->input('city');
        $data['delivery'] = json_encode($request->input('jasa'));
        $data['province'] = $request->input('province');
        $data['district'] = $request->input('district');
        // $data['slug'] = str_slug($request->input('nama')."-".rand(0,2));


        $data['description'] = $request->input('description');

        if ($request->hasfile('image')) {
            $name = $request->file('image')
                ->getClientOriginalName();
            $request->file('image')
                ->move(public_path() . '/images/', $name);
            $img = $name;
            // }
            $data['image'] = $img;
        }
        $shop->update($data);
        return back()->with('msg', 'Product updated successfully');;

    }



}
