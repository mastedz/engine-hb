<?php
namespace App\Http\Controllers;

use Log;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Address;
use App\Category;
use App\Helper;
use App\Products;

class AddressController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all() , ['addressName' => 'required', 'receiverName' => 'required', 'province' => 'required', 'district' => 'required', 'phone' => 'required', 'city' => 'required', 'zipcode' => 'required|integer', 'address' => 'required']);

        if ($validator->fails()) {
            return json_encode(['resultDesc' => 'data belum lengkap!', 'resultCode' => '99']);
        }
        else {
            $address_name = Address::whereName($request->addressName)
                ->whereUserId(Auth::user()
                ->id)
                ->first();
            $is_primary = Address::whereUserId(Auth::user()->id)
                ->whereIsPrimary('1')
                ->get()
                ->count();

            if ($is_primary == '1') {
                $is_primary = '0';
            }
            else {
                $is_primary = '1';
            }

            // dd($address_name);
            if ($address_name) {
                return response()->json(['title' => 'gagal!', 'message' => 'alamat sudah ada...', 'status' => 'success'], 503);
            }

            try {

                $address = Address::create(['name' => $request->addressName, 'receiver' => $request->receiverName, 'address' => $request->address, 'city' => $request->city, 'province' => $request->province, 'district' => $request->district, 'postcode' => $request->zipcode, 'phoneno' => $request->phone, 'user_id' => Auth::user()->id, 'is_primary' => $is_primary]);
                return response()->json(['title' => 'Sukses!', 'message' => 'Registrasi Alamat Berhasil...', 'status' => 'success'], 201);

            }
            catch(\Exception $e) {

                Log::info('alamat gagal disimpan' . print_r($e->getMessage() , true));
                // return json_encode(['resultDesc' => 'alamat gagal disimpan!', 'resultCode' => '99']);

            }
        }
        Auth::user()
            ->address()
            ->create($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $validator = Validator::make($request->all() , ['addressName' => 'required', 'receiverName' => 'required', 'phone' => 'required', 'city' => 'required', 'zipcode' => 'required|integer', 'address' => 'required', 'id' => 'required']);

        if ($validator->fails()) {
            return json_encode(NULL);
        }
        else {
            // dd($request->all());
            // $address_name = Address::whereName($request->addressName)->first();
            // dd($address_name);
            // if($address_name){
            //   return json_encode(['resultDesc' => 'data nama '.$request->addressName.' sudah ada!', 'resultCode' => '99']);
            // }
            // $procity = explode("&",$request->city);
            try {
                $address = Address::whereId($request->id)
                    ->update(['name' => $request->addressName, 'receiver' => $request->receiverName, 'address' => $request->address, 'city' => $request->city, 'province' => $request->province, 'district' => $request->district, 'postcode' => $request->zipcode, 'phoneno' => $request->phone, 'user_id' => Auth::user()->id, 'updated_at' => date('YmdHis') ]);
                return response()
                    ->json(['title' => 'Sukses!', 'message' => 'Update Alamat Berhasil...', 'status' => 'success'], 201);
            }
            catch(\Exception $e) {
                Log::info('alamat gagal disimpan' . print_r($e->getMessage() , true));
                return json_encode(null);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $delete = Address::whereId($id)->first();
        try {
            $delete->delete();

            return json_encode(['resultDesc' => 'alamat berhasil didelete', 'resultCode' => '00']);
        }
        catch(\Exception $e) {
            Log::error('alamat gagal didelete : ' . $e->getMessage());

            return json_encode(['resultDesc' => 'alamat gagal didelete!', 'resultCode' => '99']);
        }
    }

    public function alamat(Request $request) {
        // $user = Auth::user();
        // $categories = Category::orderBy('name')->get();
        // $products = Products::all();
        $provinces = Helper::non()->getProvinces();
        $cities = Helper::non()->getCity();

        $addresses = Address::whereUserId(Auth::user()->id)
            ->get();

        return view('frontend.profile.alamat', compact('products', 'categories', 'user', 'provinces', 'addresses', 'cities'));
    }

    public function alamatDetail(Request $request) {
        $id = $request->id;
        $address = Address::whereId($id)->first();
        $subdistrict = Helper::non()->getDistrict($address->city, $address->district);
        $address['province_name'] = $subdistrict['province'];
        $address['province_id'] = $subdistrict['province_id'];
        $address['city_name'] = $subdistrict['city'];
        $address['city_id'] = $subdistrict['city_id'];

        $address['all_province'] = Helper::non()->getProvinces();
        $address['subdistrict_name'] = $subdistrict['subdistrict_name'];
        $address['subdistrict_id'] = $subdistrict['subdistrict_id'];

        echo json_encode($address);
        return;
    }

    public function delete($id) {

        try {
            $delete = Address::whereId($id)->first();
            if ($delete->count() < 1) {
                Log::info('alamat tidak ditemukan!');
                Log::info('alamat tidak ditemukan ' . print_r($e->getMessage() , true));

                return json_encode(['resultDesc' => 'alamat tidak ditemukan!', 'resultCode' => '99']);
            }
            $delete->delete();

            return json_encode(['resultDesc' => 'alamat berhasil disimpan!', 'resultCode' => '00']);
        }
        catch(\Exception $e) {
            Log::error('Alamat Gagal Terhapus : ' . $e->getMessage());
            return json_encode(['resultDesc' => 'alamat gagal disimpan ke db!', 'resultCode' => '99']);
        }

    }

    public function addressChange(Request $request) {
        $old_address = Address::whereUserId(Auth::user()->id)
            ->whereIsPrimary('1')
            ->first()->id;
        $new_address = $request->id;

        $old_update = Address::whereId($old_address)->update(['is_primary' => '0']);
        $new_update = Address::whereId($new_address)->update(['is_primary' => '1']);

        if ($old_update == 1 && $new_update == 1) {
            return '00';
        }
        else {
            return '88';
        }

    }
}
