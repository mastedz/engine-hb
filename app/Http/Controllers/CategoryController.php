<?php
namespace App\Http\Controllers;

use App\Brands;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller {
    use \App\Fungsi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware(['auth', 'admin']);
    }

    /*Start CRUD*/
    public function index($parent = '', $query = '') {
        //
        if ($parent == '') {
            $categories = Category::where('id_parent', '')->paginate(20);
            $parent = null;
        }
        else {
            $categories = Category::where('id_parent', $parent)->where('title', 'like', '%' . $query . '%')->paginate(20);
            $parent = Category::findOrFail($parent);

        }

        return view('backend.category.categories', compact('categories', 'parent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = Category::where('id_parent', '')->get();
        return view('backend.category.create-category', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->merge(['slug' => $this->generateAlias($request->post('title')) ,

        ]);
        $request->validate(['title' => 'required|max:255', 'description' => 'required', 'schema' => 'required', 'meta_title' => 'required', 'meta_description' => 'required', 'meta_keyword' => 'required',

        ]);

        $category = Category::create($request->all());
        return response()
            ->json(['title' => 'Sukses!', 'message' => 'Category berhasil dibuat...', 'status' => 'success'], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category) {
        //
        $categories = Category::where('id_parent', '')->get();

        return view('backend.category.edit-category', compact('categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, Category $category)
    {
        $request->merge([
          'slug' => $this->generateAlias($request->post('title')),

        ]);
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            'schema' => 'required',
            'meta_title' => 'required',
            'meta_description' => 'required',
            'meta_keyword' => 'required',
            'id_parent' => 'nullable|int'
        ]);
        if($request->post('id_parent') !== "") {
          $parent = Category::where([
              'id_category' => $request->post('id_parent')
          ])->where('id_parent', '')->first();
          if($parent === NULL) {
            return response()->json([
              'message' => 'Parent category tidak valid',
              'errors' => [
                'id_parent' => ['']
              ]
            ], 422);
          }
        }
        $category->update($request->all());
        return response()->json([
          'title' => 'Sukses!',
          'message' => 'Category berhasil diupdate...',
          'status' => 'success'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Category $category)
     {
       $category->delete();
       return response()->json([
         'title' => 'Sukses!',
         'message' => 'Category berhasil dihapus...',
         'status' => 'success'
       ], 200);
     }

    /*End CRUD*/

    public function publish($id) {
        Category::where('id', $id)->update(['publication_status' => 1]);
        return back();
    }

    public function unpublish($id) {
        Category::where('id', $id)->update(['publication_status' => 2]);
        return back();
    }

}
