<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Socialite;
use App\User;
use Session;
class AuthController extends Controller
{
   public function redirectToProvider($provider)
    {

        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
    	try {
    		$user = Socialite::with($provider)->stateless()->user();
    	} catch (Exception $e) {
    		return redirect()->back();
    	}


    	// dd($user);

        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            // log them in
            Session::put('success','Login successfully.');
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->provider           = $provider;
            $newUser->provider_id       = $user->id;
            $newUser->image          = $user->avatar;
            $newUser->save();
            auth()->login($newUser, true);
        }
        dd($user);
        Session::put('success','Account created successfully.');
        return redirect()->to('/');
    }
}
