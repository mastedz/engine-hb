<?php
namespace App\Http\Controllers;

use App\Brands;
use App\Category;
use App\Products;
use App\User;
use App\Shop;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller {
    use \App\Fungsi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        // $this->middleware(['auth','admin']);

    }

    /*Start CRUD*/
    public function index() {

        //


        $brands = Brands::all();
        $user = Auth::User()->id;

        $products = Products::whereHas('shop', function ($query) use ($user) {
            $query->where('user_id', '=', $user);
        })->with('category')
            ->paginate(10);

        $edit = 0;
        // dd($products);
        return view('frontend.product.products', compact('products', 'user', 'edit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $category = Category::where('id_parent', NULL)->get();
        $brands = Brands::all();
        // dd();
        return view('frontend.product.pilih_add_product', compact('category', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r) {

        // dd($r->all());
        // $rules = [
        //     'name' => ['required', 'min:3'],
        //     'category' => ['required'],
        //     'harga' => ['required'],
        //      'kondisi' => ['required'],
        //      'asuransi' => ['required'],
        // ];
        // $this->validate($r, $rules);
        $data['name'] = $r->input('nama');
        $data['category_id'] = $r->input('subcategory');
        $data['price'] = $r->input('harga');
        $data['condition'] = $r->input('kondisi');
        $data['brand_id'] = 0;
        $data['insurance'] = $r->input('asuransi');
        $data['discount'] = $r->input('discount');
        $data['qty'] = $r->input('qty');
        $data['weight'] = $r->input('weight');
        $data['slug'] = str_slug($r->input('nama') . "-" . rand(0, 2));

        $data['description'] = $r->input('description');

        if ($r->hasfile('foto')) {

            foreach ($r->file('foto') as $image) {
                $name = $image->getClientOriginalName();
                $image->move(public_path() . '/images/', $name);
                $img[] = $name;
            }
        }
        else {
            $img[] = ["default.jpg"];
        }

        $data['image'] = json_encode($img);
        $data['shop_id'] = Shop::where('user_id', Auth::user()->id)
            ->first()->id;
        // dd($data);
        Products::create($data);
        return response()->json(['title' => 'Sukses!', 'message' => 'Barang berhasil diInsert...', 'status' => 'success'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug) {

        $product = Products::whereSlug($slug)->first(['id','is_artist']);
        $id = $product->id;
        //
        $user = Auth::user();
        $users = User::all();
        $categories = Category::all();
        $brands = Brands::all();
        if ($user->id == 1) {
            $products = Products::all();
        }
        else {
            $shop_id = Shop::whereUserId($user->id)
                ->first()->id;
            $products = Products::whereShopId($shop_id)->get();
        }
        $edit = 1;
        $product_edit = Products::findOrfail($id);
        $images = json_decode($product_edit->image);
        // $this->authorize('modify', $product_edit);
        if($product->is_artist == 1)
        {
          return view('frontend.product.edit_product_video', compact('products', 'edit', 'product_edit', 'categories', 'brands', 'users', 'user', 'images'));
        }
        return view('frontend.product.edit_product', compact('products', 'edit', 'product_edit', 'categories', 'brands', 'users', 'user', 'images'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        //
        $input = $request->all();
        // dd($input['url']);
        $products = Products::findOrFail($request->id);
        // $this->authorize('modify', $products);
        $data['name'] = $request->input('nama');
        if(@$input['url']){
          $data['category_id'] = $request->input('category_id');
        }else{
          $data['category_id'] = $request->input('subcategory');
        }
        $data['price'] = $request->input('harga');
        $data['condition'] = $request->input('kondisi');
        $data['brand_id'] = 0;
        $data['insurance'] = $request->input('asuransi');
        $data['discount'] = $request->input('discount');
        $data['qty'] = $request->input('qty');
        $data['weight'] = $request->input('weight');
        // $data['slug'] = str_slug($request->input('nama')."-".rand(0,2));


        $data['description'] = $request->input('description');

        if ($request->hasfile('foto')) {
            $db_img = json_decode($products->image);
            // foreach($request->file('foto') as $image)
            // {
            $img[] = "";
            if (@$request->file('foto') [0]) {
                $name = $request->file('foto') [0]
                    ->getClientOriginalName();
                $request->file('foto') [0]
                    ->move(public_path() . '/images/', $name);
                $img[0] = $name;
            }
            else {
                $img[0] = @$db_img[0];
            }

            if (@$request->file('foto') [1]) {
                $name = $request->file('foto') [1]
                    ->getClientOriginalName();
                $request->file('foto') [1]
                    ->move(public_path() . '/images/', $name);
                $img[1] = $name;
            }
            else {
                $img[1] = @$db_img[1];
            }

            if (@$request->file('foto') [2]) {
                $name = $request->file('foto') [2]
                    ->getClientOriginalName();
                $request->file('foto') [2]
                    ->move(public_path() . '/images/', $name);
                $img[2] = $name;
            }
            else {
                $img[2] = @$db_img[2];
            }
            // }
            $data['image'] = json_encode($img);
        }
        $products->update($data);
        return back()->with('msg', 'Product updated successfully');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $products = Products::findOrFail($id);
        // $this->authorize('modify', $products);
        $products->delete();
        return back()
            ->with('message', 'Product deleted successfully');;
    }

    public function uploadVideo(Request $r) {
      // dd($r);
      parse_str(parse_url($r->input('url') , PHP_URL_QUERY) , $vId);

      $data['name'] = $r->input('nama');
      $data['category_id'] = $r->input('category');
      $data['price'] = $r->input('harga');
      $data['description'] = $r->input('description');
      $data['discount'] = $r->input('discount');
      $data['slug'] = str_slug($r->input('nama') . "-" . rand(0, 2));
      $data['is_artist'] = 1;
      $data['shop_id'] = Shop::where('user_id', Auth::user()->id)
          ->first()->id;
      $data['url'] = $vId['v'];
      $data['image'] = 'https://img.youtube.com/vi/' . $vId['v'] . '/maxresdefault.jpg';
      Products::create($data);

      return redirect('/products/add/video');

  }
    /*End CRUD*/

    //    public function publish($id) {
    //        Products::where('id', $id)->update(['publication_status'=>1]);
    //        return back();
    //    }
    //
    //    public function unpublish($id) {
    //        Products::where('id', $id)->update(['publication_status'=>2]);
    //        return back();
    //    }



}
