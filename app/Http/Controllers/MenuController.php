<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;

use App\Category;

use App\Settings as Option;
use Illuminate\Validation\Rule;

/**
 * Data order menu disimpan di dalam tabel options
 * dengan key menu_'posisi'
 */

class MenuController extends Controller {
    public function __construct() {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $menus = ['top' => [], 'bottom' => [], 'footer' => []];
        foreach ($menus as $key => $value) {
            $opt = Option::where('option', 'menu_' . $key)->first();
            $menus[$key] = ($opt === NULL) ? [] : json_decode($opt->value, true);
        }
        return view('backend.menu.index-menu', compact('menus'));
    }

    public function parent_menu(Request $request) {
        $options = Option::where('option', 'menu_' . $request->get('position'))
            ->first();
        if ($options === NULL) {
            return response()->json(['data' => [], 'status' => 'success']);
        }
        $ret = array(
            'data' => [],
            'status' => 'success'
        );
        $sort = json_decode($options->value, true);
        foreach ($sort as $key => $value) {
            $ret['data'] = array_merge($ret['data'], array(
                Menu::find($value['id'])
            ));
        }
        return response()->json($ret);
    }

    public function child_menu(Request $request) {
        $id = $request->get('id');
        $menu = Menu::find($id);
        $options = Option::where('option', 'menu_' . $menu->position)
            ->first();
        if ($options === NULL) {
            return response()->json(['data' => [], 'status' => 'success']);
        }
        $ret = array(
            'data' => [],
            'status' => 'success'
        );
        $sort = json_decode($options->value, true);
        foreach ($sort as $key => $value) {
            if ($value['id'] == $id) {
                foreach (@$value['children'] ? : array() as $key2 => $value2) {
                    $ret['data'] = array_merge($ret['data'], array(
                        Menu::find($value2['id'])
                    ));
                }
                break;
            }
        }
        return response()->json($ret);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.menu.create-menu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->merge(['type' => $request->post('menu_type') , 'url' => $request->post('menu_value') ]);
        $request->validate(['title' => 'required|max:255', 'position' => 'required|in:top,bottom,footer', 'description' => 'required', 'type' => 'required|in:genre,tag,page,keyword,external', 'parent_id' => 'nullable|int']);
        $menu = Menu::create($request->all());
        $option = Option::where('option', 'menu_' . strtolower($request->post('position')))
            ->first();
        if ($option === NULL) {
            $option = new Option;
            $option->option = 'menu_' . strtolower($request->post('position'));
            $option->value = "[]";
        }
        if ($request->post('parent_id') === NULL) {
            $option->value = json_encode(array_merge(json_decode($option->value, true) , array(
                ['id' => $menu->id_menu]
            )));
        }
        else {
            $array = json_decode($option->value, true);
            foreach ($array as $key => $value) {
                if ($value['id'] == $request->post('parent_id')) {
                    $arr = json_decode($option->value, true);
                    if (!is_array(@json_decode($option->value, true) [$key]['children'])) {
                        $arr[$key]['children'] = array();
                        $option->value = json_encode($arr);
                    }
                    $arr[$key]['children'] = array_merge($arr[$key]['children'], array(
                        ['id' => $menu->id_menu]
                    ));
                    $option->value = json_encode($arr);
                    break;
                }
                foreach (@$value['children'] ? : array() as $key2 => $value2) {
                    if ($value2['id'] == $request->post('parent_id')) {
                        $arr = json_decode($option->value, true);
                        if (!is_array(@json_decode($option->value, true) [$key]['children'][$key2]['children'])) {
                            $arr[$key]['children'][$key2]['children'] = array();
                            $option->value = json_encode($arr);
                        }
                        $arr[$key]['children'][$key2]['children'] = array_merge($arr[$key]['children'][$key2]['children'], array(
                            ['id' => $menu->id_menu]
                        ));
                        $option->value = json_encode($arr);
                        break;
                    }
                }
            }
        }
        $option->save();
        return response()
            ->json(['title' => 'Sukses!', 'message' => 'Menu berhasil dibuat...', 'status' => 'success'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu) {
        return view('backend.menu.edit-menu', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu) {
        $request->merge(['type' => $request->post('menu_type') , 'url' => $request->post('menu_value') ]);
        $request->validate(['title' => 'required|max:255', 'type' => 'required|in:genre,tag,page,keyword,external', 'description' => 'required']);
        $menu->update($request->all());
        return response()
            ->json(['title' => 'Sukses!', 'message' => 'Menu berhasil diupdate...', 'status' => 'success'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu) {
        $opt = Option::where('option', 'menu_' . $menu->position)
            ->first();
        $array = json_decode($opt->value, true);
        foreach ($array as $key => $value) {
            if ($value['id'] == $menu->id_menu) {
                if (is_array(@$value['children']) && count($value['children']) > 0) {
                    return response()->json(['message' => 'Tidak dapat menghapus menu yang memiliki submenu...'], 422);
                }
                unset($array[$key]);
                break;
            }
            foreach (@$value['children'] ? : array() as $key2 => $value2) {
                if ($value2['id'] == $menu->id_menu) {
                    if (is_array(@$value2['children']) && count($value2['children']) > 0) {
                        return response()->json(['message' => 'Tidak dapat menghapus menu yang memiliki submenu...'], 422);
                    }
                    unset($array[$key]['children'][$key2]);
                    break;
                }
                foreach (@$value2['children'] ? : array() as $key3 => $value3) {
                    if ($value3['id'] == $menu->id_menu) {
                        unset($array[$key]['children'][$key2]['children'][$key3]);
                        break;
                    }
                }
            }
        }
        $opt->value = json_encode($array);
        $opt->save();
        $menu->delete();
        return response()
            ->json(['title' => 'Sukses!', 'message' => 'Menu berhasil dihapus...', 'status' => 'success'], 200);
    }

    public function sort_form($type) {
        $types = ['top', 'bottom', 'footer'];
        if (!in_array(strtolower($type) , $types)) abort(404);
        $opt = Option::where('option', 'menu_' . $type)->first();
        $menus = ($opt === NULL) ? [] : json_decode($opt->value, true);
        return view('backend.menu.sort-menu', compact('menus', 'type', 'types'));
    }

    public function type(Request $request) {
        $type = strtolower($request->get('type'));
        $ret = [];
        switch ($type) {
            case 'genre':
                if (env('APP_TYPE') == 'master') {
                    $genre = Category::where('title', 'Genre')->first();
                    $ret = Category::select('id_category as id', 'title')->where('id_parent', $genre['id_category'])->orderBy('title', 'asc')
                        ->get();
                }
                else {
                    $md = new MovieData;
                    $ret = json_decode($md->getGenre());
                    foreach ($ret as $key => $value) {
                        $value->id = $value->id_category;
                    }
                }
            break;
            case 'tag':
                if (env('APP_TYPE') == 'master') {
                    $ret = Tag::select('id_tag as id', 'title')->orderBy('title', 'asc')
                        ->get();
                }
                else {
                    $md = new MovieData;
                    $ret = json_decode($md->getTags());
                    foreach ($ret as $key => $value) {
                        $value->id = $value->id_tag;
                    }
                }
            break;
            case 'page':
                $ret = Page::select('id_page as id', 'title')->orderBy('title', 'asc')
                    ->get();
            break;
            case 'keyword':
                $ret = URLGlobal::select('id_url_global as id', 'name as title')->orderBy('title', 'asc')
                    ->get();
            break;
            default:
            break;
        }
        return response()
            ->json($ret);
    }

    public function sort(Request $request, $type) {
        /**
         * data yang dikirim sudah berupa json
         */
        $request->validate(['json' => 'required']);
        switch (strtolower($type)) {
            case 'top':
            case 'bottom':
            case 'footer': {
                    break;
                }

            default: {
                    abort(404);
                }
            }
            $option = Option::where('option', 'menu_' . $type)->first();
            if ($option === NULL) $option = new Option;
            $option->option = 'menu_' . $type;
            $option->value = $request->post('json');
            $option->save();
            return response()
                ->json(['title' => 'Sukses!', 'message' => 'Urutan menu berhasil disimpan;...', 'status' => 'success'], 200);
        }

    }
