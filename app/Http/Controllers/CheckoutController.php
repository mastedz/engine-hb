<?php
namespace App\Http\Controllers;

use App\Category;
use App\Products;
use App\Orders;
use App\Shop;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Stripe;
use App\User;
use App\Helper;
class CheckoutController extends Controller {
    //
    //    public function step1()
    //    {
    //        $user = Auth::user();
    //        $categories = Category::all();
    //        if(Auth::check())
    //        {
    //            return view('frontend.shipping-info',compact('user','categories'));
    //        }
    //        return redirect('/signin');
    //    }
    public function shipping() {

        $products = Products::all();
        if (Cart::count() >= 1) {
            return view('frontend.shipping-info', compact('user', 'categories'));
        }
        else {
            return view('frontend.cart.index', compact('user', 'categories'));
        }

    }
    public function payment() {
        if (Cart::count() >= 1) {
            $product = Products::all();
            $user = Auth::user();
            $categories = Category::all();
            return view('frontend.payment', compact('user', 'categories', 'product'));
        }
        else {
            return redirect()->route('cart.index');
        }
    }
    public function storePayment(Request $request) {
        //       $carts = Cart::content();


        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        Stripe::setApiKey("sk_test_CAlUJdboGG5NPcnXYTg2tWpI");

        // Token is created using Stripe.js or Checkout!
        // Get the payment token submitted by the form:
        // Charge the user's card:
        try {
            $charge = Charge::create(array(
                "amount" => cart::subTotal() * 100,
                "currency" => "usd",
                "source" => $request->input('stripeToken') ,
                "description" => "Pako.com.ng charge"
            ));
            //        create the order
            $user = Auth::user();
            $vendor = Products::all();
            $order = $user->orders()
                ->create(['total' => $charge->amount / 100, 'delivered' => 0, 'payment_id' => $charge->id, 'vendor_id' => 0]);

            $cartItems = Cart::content();
            foreach ($cartItems as $cartItem) {
                $order->orderItems()
                    ->attach($cartItem->id, ['qty' => $cartItem->qty, 'price' => $cartItem->price, 'discount' => $cartItem->discount, 'total' => $cartItem->qty * ($cartItem->price * $cartItem->discount / 100) ]);
            }

            //            $order = new Orders();
            //            $order->cart = serialize($item);
            //            $order->name = $request->input('cardName');
            //            $order->payment_id = $charge->id;
            //            Auth::user()->orders()->save($order);

        }
        catch(\Exception $e) {

            return redirect()->route('checkout.payment')
                ->with('error', $e->getMessage());
        }

        Session::forget('cart');

        return redirect()
            ->route('frontend.collection')
            ->with('success', 'Successfully purchased item!');

    }

    public function checkout(Request $r) {
        // dd($r->cart);
        $user = User::where('id', Auth::user()->id)
            ->with('address')
            ->first();
        $address = $user->address()
            ->where('is_primary', 1)
            ->first();
        if ($address == null) {
            return redirect()->route('frontend.alamat');
        }
        // $cartItems = Cart::content();
        $categories = Category::all();

        foreach ($r->cart as $cart) {

            // dd(Cart::get($cart));
            $id[] = Cart::get($cart)->id;
            $qty[] = Cart::get($cart)->qty;
        }
        // dd($ongkir);
        $cartItems = Products::whereIn('id', $id)->with('shop')
            ->get();
        // dd($cartItems);
        foreach ($cartItems as $key => $value) {
            $data = ['origin' => $value->shop()
                ->first()->shop_city, 'originType' => 'city',

            'destination' => $address->district, 'destinationType' => 'subdistrict', 'weight' => $value->weight, 'courier' => json_decode($value->shop()
                ->first()
                ->delivery) [0]];
            // dd($data);
            $ongkir[] = json_decode(Helper::non()->getOngkir($data));
            $shops[] = Shop::whereId($value->shop_id)
                ->first();
            $qtyShop[] = $value->shop_id;
        }
        $qtyShop = array_unique($qtyShop);
        // dd($cartItems->first());
        foreach ($cartItems as $key => $value) {
          if ($value->is_artist == 1) {
            return view('frontend.cart.reservasi', compact('products', 'categories', 'cartItems', 'user', 'address', 'ongkir', 'shops', 'qtyShop', 'qty'));
          }
        }

        return view('frontend.cart.checkout', compact('products', 'categories', 'cartItems', 'user', 'address', 'ongkir', 'shops', 'qtyShop', 'qty'));
    }
}
