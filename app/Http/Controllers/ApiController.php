<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use Mail;
use App\Orders;
use Youtube;
use App\Products;
use App\Shop;
use Auth;
class ApiController extends Controller {

    public function finish(Request $r) {
        // Mail::to('fransbismania@gmail.com')->send($r->all());
        Log::info(json_encode($r->all()));
    }
    public function unfinish(Request $r) {
        // Mail::to('fransbismania@gmail.com')->send($r->all());
        Log::info($r->all());
    }
    public function error(Request $r) {
        // Mail::to('fransbismania@gmail.com')->send($r->all());
        Log::info($r->all());
    }
    public function handler(Request $r) {
        if ($r->transaction_status == "settlement" && $r->status_code == 200) {
            $data = Orders::where('payment_id', $r->transaction_id)
                ->update(['payment_status' => 'settlement']);
        }elseif ($r->transaction_status == "expire" ) {
          $data = Orders::where('payment_id', $r->transaction_id)
              ->update(['payment_status' => 'expired']);
        }

        // Mail::to('fransbismania@gmail.com')->send($r->all());
        return 200;
    }

}
