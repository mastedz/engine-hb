<?php
namespace App\Http\Controllers;

use App\Category;
use App\Products;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $user = Auth::user();
        // $cartItems1 = Cart::content();
        $categories = Category::all();
        $index = 0;
        $cartItems = [];

        foreach (Cart::content() as $key => $value) {
            $i = $index++;
            $cartItems[] = Products::whereId($value->id)
                ->first()
                ->toArray();
            $cartItems[$i]['rowId'] = $value->rowId;
            $cartItems[$i]['qty'] = Cart::content() [$value
                ->rowId]->qty;

        }
        if (empty($cartItems)) {
            return redirect('/');
        }
        // dd($cartItems);


        return view('frontend.cart.index', compact('products', 'categories', 'cartItems', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $product = Products::find($id);
        Cart::add($id, $product->name, 1, $product->price);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $product = Products::find($id);
        Cart::add($id, $product->name, 1, $product->price - ($product->price * ($product->discount / 100)));
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        $update = Cart::update($id, $request->qty);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        Cart::remove($id);
        return back();
    }

    public function buynow($id) {
        //
        $product = Products::find($id);
        Cart::add($id, $product->name, 1, $product->price - ($product->price * ($product->discount / 100)));
        return Redirect()
            ->back();
    }

    public function updateQty(Request $request) {
        $qty = $request->qty;
        $rowId = $request->rowId;

        Cart::update($rowId, $qty);
        return Redirect()->back();
    }

}
