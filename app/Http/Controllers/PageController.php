<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Page;

class PageController extends Controller {
    use \App\Fungsi;
    public function __construct() {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $pages = Page::paginate(20);
        return view('backend.page.index-page', compact('pages'));
    }

    public function search($query = '') {
        if (!strlen($query)) {
            return redirect(route('admin.page.index'));
        }
        $pages = Page::where('title', 'like', '%' . $query . '%')->paginate(20);
        return view('backend.page.index-page', compact('pages', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.page.create-page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->merge(['slug' => $this->generateAlias($request->post('title')) , 'id_user' => Auth::user()->id]);
        $request->validate(['title' => 'required|max:255', 'description' => 'required', 'schema' => 'required', 'meta_title' => 'required', 'meta_description' => 'required', 'meta_keyword' => 'required']);
        Page::create($request->all());
        return response()
            ->json(['title' => 'Sukses!', 'message' => 'Page berhasil dibuat...', 'status' => 'success'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page) {
        return view('backend.page.edit-page', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page) {
        $request->merge(['slug' => $this->generateAlias($request->post('title')) , 'id_user' => Auth::user()->id]);
        $request->validate(['title' => 'required|max:255', 'description' => 'required', 'schema' => 'required', 'meta_title' => 'required', 'meta_description' => 'required', 'meta_keyword' => 'required']);
        $page->update($request->all());
        return response()
            ->json(['title' => 'Sukses!', 'message' => 'Page berhasil diupdate...', 'status' => 'success'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page) {
        $page->delete();
        return response()
            ->json(['title' => 'Sukses!', 'message' => 'Page berhasil dihapus...', 'status' => 'success'], 200);
    }

    public function bulkdelete(Request $request) {
        $records = $request->post('table_records');
        $count = 0;
        foreach ($records as $key => $record) {
            $page = Page::find($record);
            if ($page !== null) {
                $count++;
                $page->delete();
            }
        }
        return response()
            ->json(['title' => 'Sukses!', 'message' => $count . ' page berhasil dihapus...', 'status' => 'success'], 200);
    }
}
