<?php
namespace App\Http\Controllers;

use App\Address;
use App\Brands;
use App\Category;
use App\Products;
use App\Role;
use App\Thread;
use App\Reply;
use App\User;
use App\Helper;
use App\Shop;

// use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Mail;
use DB;
use Validator;
use Storage;
use Log;
use App\Orders;
use Cart;

class FrontendController extends Controller {
    //
    public function home() {

        // dd(Cart::tax());
        $user = Auth::user();


        return view('frontend.layouts.landing_page', compact( 'user'));
    }
    public function attempt(Request $request) {
        $credentials = $request->only('email', 'password');
        // dd($credentials);
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return response()->json(['title' => 'Sukses!', 'message' => 'Login Berhasil...', 'status' => 'success'], 201);

        }
    }
    public function filter(Request $r) {
        $products = Products::whereBetween('price', [$r->start, $r
            ->end])
            ->paginate(32);
        return view('frontend.collection', compact('products'));
    }
    public function getCity(Request $r) {
        return Helper::non()->getCity($r->province);

    }
    public function getDistrict(Request $r) {

        return Helper::non()->getDistrict($r->city);

    }
    public function collection() {

        $products = Products::paginate(32);
        return view('frontend.collection', compact('products', 'categories', 'user'));
    }
    public function show($id) {

        $user = Auth::user();
        //        $brands = Brands::all();
        $categories = Category::find($id);

        //        $brands = Brands::find($id);
        $brands = Brands::find($id);

        $products = Products::where('category_id', $brands->category_id)
            ->paginate(32);

        // dd($products);
        return view('frontend.collection', compact('categories', 'products', 'user', 'brands'));
    }
    public function storeDiscuss(Request $r) {
        Reply::create(['thread_id' => $r->TID, 'user_id' => $r->UID, 'body' => $r->comment]);

        return json_encode(['success' => true, 'user' => Auth::user()->name]);

    }
    public function storeQuestion(Request $r) {
        Thread::create(['user_id' => $r->UID, 'product_id' => $r->PID, 'body' => $r->question]);

        return json_encode(['success' => true, 'user' => Auth::user()->name]);

    }
    public function subproduct($id) {

        $categories = Category::where('slug', $id)->first();

        $products = Products::where('category_id', $categories->id_category)
            ->paginate(32);

        // dd($products);
        return view('frontend.collection', compact('categories', 'products'));
    }

    public function topbrands() {
        return view('frontend.topbrands');
    }
    public function contact() {

        return view('frontend.contact');
    }
    public function signin() {

        $products = Products::all();
        if (!(Auth::check())) {

            return view('frontend.account.signin', compact('products', 'categories', 'user'));
        }
        return back();
    }
    public function signup(Request $request) {

        $products = Products::all();

        if (!(Auth::check())) {
            return view('frontend.account.signup', compact('categories', 'user'));
        }
        return view('frontend.account.signin', compact('user', 'categories'));
    }
    public function vendorSignup(Request $request) {

        $products = Products::all();

        if (!(Auth::check())) {
            return view('frontend.account.signup', compact('categories', 'user'));
        }
        return view('frontend.account.signup-vendor', compact('user', 'categories'));
    }

    public function process_vendorSignup(Request $request) {
        $this->validate($request, ['name' => 'required|max:255', 'email' => 'required|email|max:255|unique:users', 'password' => 'required|min:6', ]);
        $data['name'] = $request->input('name');
        $data['email'] = $request->input('email');
        $data['password'] = bcrypt($request->input('password'));

        $user = User::create($data);
        // $data->roles()->attach(1);


        $user = $user->toArray();
        $user['link'] = str_random(30);

        DB::table('user_activations')->insert(['id_user' => $user['id'], 'token' => $user['link']]);

        Mail::send('emails.activation', $user, function ($message) use ($user) {
            $message->to($user['email']);
            $message->subject('haribelanja.com - Activation Code');
        });
        return response()
            ->json(['title' => 'Sukses!', 'message' => 'Registrasi Berhasil, silakan cek email untuk aktivasi', 'status' => 'success'], 201);
    }
    public function userActivation($token) {
        $check = DB::table('user_activations')->where('token', $token)->first();
        if (!is_null($check)) {
            $user = User::find($check->id_user);
            if ($user->is_activated == 1) {
                return redirect()
                    ->to('signin')
                    ->with('success', "user is already activated.");
            }
            $user->is_activated = 1;
            $user->save();
            //            $user->update(['is_activated' => 1]);
            DB::table('user_activations')
                ->where('token', $token)->delete();
            return redirect()
                ->to('signin')
                ->with('success', "user active successfully.");
        }
        return redirect()
            ->to('signin')
            ->with('Warning', "your token is invalid");
    }
    //
    //    public function process_signin(Request $request)
    //    {
    //
    //        $this->validate($request, [
    //            'name' => 'required|max:255',
    //            'email' => 'required|email|max:255|unique:users',
    //            'password' => 'required|min:6|confirmed',
    //        ]);
    ////
    //            $data['name'] = $request->input('name');
    //            $data['email'] = $request->input('email');
    //            $data['password'] = bcrypt($request->input('password'));
    ////        'admin' => $data['admin'],
    //             User::create($data);
    //        return back()->with('msg','Thanks for signing up');
    //    }
    public function search(Request $r) {
        $products = Products::where('name', 'like', '%' . $r->q . '%')
            ->paginate(32);
        // dd($r->q);
      $categories = new \stdClass();
      $categories->title = $r->q;
      $categories->meta_title = $r->q;
      $categories->description = "barang dalam pencarian '". $r->q."'";
        return view('frontend.collection', compact('products', 'categories'));
    }

    public function details($id) {
        $user = Auth::user();

        $product_edit = Products::where('slug', $id)->with('category')
            ->first();
        $shopDistrict = Shop::whereId($product_edit->shop_id)
            ->first()->district;
        $delivery = json_decode(Shop::whereId($product_edit->shop_id)
            ->first()
            ->delivery);
        if (!empty(Auth::user()
            ->id)) {
            $districts = Address::whereUserId(Auth::user()->id)
                ->get(['district']);
            $provinces = '';
        }
        else {
            $districts = null;
            $provinces = Helper::non()->getProvinces();
        }
        return view('frontend.product.product-details', compact('product_edit', 'user', 'delivery', 'districts', 'shopDistrict', 'provinces'));
    }

    public function biodata(Request $request) {
        $user = Auth::user();

        return view('frontend.profile.biodata', compact('user'));
    }

    public function create_merchant() {
        if (Auth::check()) {
            return view('frontend.merchant.create_merchant', compact('user'));
        }
        return back();
    }

    public function cretae_step() {
        if (Auth::check()) {
            $provinces = Helper::non()->getProvinces();

            $id = Shop::whereUserId(Auth::user()->id)
                ->first()->id;

            return view('frontend.merchant.create_merchant_step', compact('user', 'cities', 'provinces', 'id'));
        }
        return back();
    }

    public function profile(Request $request) {
        $validator = Validator::make($request->all() , ['name' => 'required', 'phone' => 'required',
        // 'image'           => 'required'
        ]);
        $name = $request->name;
        $phone = $request->phone;
        $image = $request->file('image');
        if ($validator->fails()) {
            Log::info('Validasi Profile Gagal :' . print_r($request->all() , true));
            return redirect()
                ->back()
                ->withErrors(['msg', 'Merchant Gagal Disimpan!']);
        }else {
            if (!empty($image)) {
                //image
                $nameImage = 'profile-' . Auth::user()->id;
                $resizeImage = Image::make($image)->fit(300);
                $extension = '.' . $image->getClientOriginalExtension();
                $store = Storage::put('public/images/' . $nameImage . $extension, $resizeImage->encode());

                try {
                    $update = User::whereId(Auth::user()->id)
                        ->update(['name' => $name, 'image' => $nameImage . $extension, 'phone' => $phone]);
                    Log::info('User Has Been Updated');
                    return redirect()->back()
                        ->with('flash_success', 'Thank you,!');
                }
                catch(\Exception $e) {
                    Log::info('User Failed Updated' . print_r($e->getMessage() , true));
                    return redirect()
                        ->back()
                        ->withErrors(['msg', 'Profile Gagal Disimpan!']);
                }
            }else {
                try {
                    $update = User::whereId(Auth::user()->id)
                        ->update(['name' => $name, 'phone' => $phone]);
                    Log::info('User Has Been Updated');
                    return redirect()->back()
                        ->with('flash_success', 'Thank you,!');
                }
                catch(\Exception $e) {
                    Log::info('User Failed Updated' . print_r($e->getMessage() , true));
                    return redirect()
                        ->back()
                        ->withErrors(['msg', 'Profile Gagal Disimpan!']);
                }
            }
        }
    }

    public function checkout(Request $request) {
        $user = Auth::user();

        $products = Products::all();
        return view('cart.billing', compact('products', 'categories', 'user'));
    }

    public function ongkir(Request $request) {
        $ongkir = Helper::non()->getOngkir($request->all());
        return $ongkir;
    }

    public function productAddImage(Request $request) {
        $user = Auth::user();

        return view('frontend.product.add_product', compact('user'));
    }
    public function productAddVid(Request $request) {
        $user = Auth::user();

        return view('frontend.product.add_product_video', compact('user'));
    }

    public function productEdit(Request $request) {
        $user = Auth::user();

        return view('frontend.product.edit_product_video', compact('user'));
    }

    public function chooseMethod(Request $request) {
        $user = Auth::user();

        return view('frontend.product.pilih_add_product', compact('user'));
    }

    public function myMerchant($slug) {
        $user = Auth::user();
        $shop = Shop::whereShopSlug($slug)->first();
        $provinces = Helper::non()->getProvinces();
        $cities = Helper::non()->getCity();
        $districts = Helper::non()->getDistrict($shop['shop_city']);

        $delivery = json_decode($shop['delivery']);

        return view('frontend.merchant.merchant_update', compact('shop', 'user', 'provinces', 'cities', 'districts', 'delivery'));
    }

    public function resetPassword(Request $request) {
        $old = $request->lama;
        $new = $request->baru;
        $db = Auth::user()->password;
        $hashed = Hash::make($new);
        $check = Hash::check($old, $db);
        if ($check == true) {
            $update = User::whereId(Auth::user()->id)
                ->update(['password' => $hashed]);
            if ($update) {
                return '00';
            }
            else {
                return '201';
            }
        }
        else {
            return '88';
        }
    }

    public function merchantConfirmationResi(Request $request) {
        $user = Auth::user();
        $shopId = Shop::whereUserId(Auth::user()->id)
            ->first()->id;
        $delivery =   $trxs =  DB::table('order_product')->where('id', '=', $request->id)->first()->delivery_data;
        $delivery = (array)json_decode($delivery);

        $delivery['id'] = $request->id;
  // dd($delivery);
        return view('frontend.merchant.merchant_confirmation_resi', compact('user', 'delivery'));
    }


    public function salesConfirmation(Request $request) {
        $user = Auth::user();

        return view('frontend.merchant.list_sales_confirmation', compact('user'));
    }

    public function getGuide($id) {
      $guide = Helper::non()->getGuide($id);
      return response(json_encode($guide));
    }

    public function discussion(Request $request) {
        $user = Auth::user();

        return view('frontend.profile.diskusi', compact('user'));
    }

    public function discussionAkun($type) {
        $user = Auth::user();

        return view('frontend.profile.diskusi_detail', compact('user'));
    }

    public function ivoucher(Request $request) {

        return view('frontend.ivoucher');
    }

    public function tracking(Request $request) {

        return view('frontend.tracking');
    }

    public function reservasi(Request $request) {

        return view('frontend.cart.reservasi');
    }

    public function merchantDetail(Request $request) {
        $user = Auth::user();

        return view('frontend.merchant.detail_merchantproduct', compact('user'));
    }

}
