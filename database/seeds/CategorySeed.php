<?php
use App\Category;
use Illuminate\Database\Seeder;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        Category::truncate();
        $yn = date('Y');
        for ($i = 1970; $i <= $yn; $i++){
            $y[] = $i;
        }

        $datas =[
        'Genre' => ['Comedy','Action','Horror','Documentary'],
        'Year' => $y,
        'Country' => ['Amerika','Australia','Cina','Perancis','Jerman','Hongkong','Indonesia','India','Inggris','Israel'
            ,'Itali','Jepang','Kanada','Korea', 'Malaysia', 'Meksiko', 'Pilipina', 'Romania', 'Saudi Arabia', 'Taiwan'
            , 'Tailand'],
            'Actor' => null,
            'Writer' => null,
            'Director' => null,
            ];

        foreach ($datas as $key => $data){
            $pc = new Category();
            $pc->title = $key;
            $pc->slug = $key;
            $pc->description = "Category $key";
            $pc->schema = $key;
            $pc->meta_title = $key;
            $pc->meta_description = "Category $key";
            $pc->meta_keyword = $key;
            $pc->id_parent= 1;
            $pc->save();
            if ($data != null){
                foreach ($data as $value){
                    $c = new Category();
                    $c->id_parent = $pc->id_category;
                    $c->title = $value;
                    $c->slug = $value;
                    $c->description = "Category $key $value";
                    $c->schema = $key;
                    $c->meta_title = $key;
                    $c->meta_description = "$key $value";
                    $c->meta_keyword = $value;
                    $pc->id_parent= 1;
                    $c->save();
                }
            }
        }
       
    }
}
