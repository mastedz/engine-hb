<?php
use App\Products;
use Illuminate\Database\Seeder;
use App\Settings;
use App\Brands;
use App\Shop;

class ProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Shop::create(['name' => 'Rani Shop', 
            'user_id' => 1, 
            'status' => 1, 
            'description' => 'jual apapun', 
            'is_flagship' => 1, 
            'shop_address' => 'Jalan jln', 
            'shop_city' => 12, 
            'shop_slug' => 'rani-shops'
        ]);
        Settings::create(['option'=>'footer_contact', 'value' => json_encode(['contact'=>'085854079798', 'address'=>'Philipines'])]);
        Brands::create(['category_id'=>'1', 'name'=>'Mac']);
        for ($i=0; $i < 50 ; $i++) { 
        	$product = new Products;
        	$product->name = "product".$i;
        	$product->price= $i;
        	$product->qty= $i;
            $product->description= "barang".$i."sangat good";
        	$product->category_id = "1";
        	$product->brand_id = "1";
        	$product->weight = "1000";
            $product->insurance = 0;
        	$product->shop_id=2;
            $product->condition=1;
        	$product->save();
        }
        
    }
}
