<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->string('image')->nullable();
          $table->string('phone')->nullable();
          $table->string('provider')->nullable();
          $table->string('provider_id')->nullable();
          $table->string('password')->nullable()->change();
         
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('users', function (Blueprint $table) {
        $table->dropColumn('image');
          $table->dropColumn('phone');
          $table->dropColumn('provider');
          $table->dropColumn('provider_id');
           });
    }
}
