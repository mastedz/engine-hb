<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id_permission');
            $table->integer('banner');
            $table->integer('movie');
            $table->integer('blog');
            $table->integer('movie_cast');
            $table->integer('movie_category');
            $table->integer('movie_country');
            $table->integer('movie_episode');
            $table->integer('movie_language');
            $table->integer('page');
            $table->integer('site_menu');
            $table->integer('site_parameter');
            $table->integer('site_slideshow');
            $table->integer('site_statistic');
            $table->integer('fornt_page');
            $table->integer('site_chache');
            $table->integer('site_media');
            $table->integer('user_management');
            $table->integer('id_user')->unsigned();
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
