<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageShop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('shop', function (Blueprint $table) {
          $table->string('image', 255)->nullable();
          $table->string('delivery', 255)->nullable();
         
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('shop', function (Blueprint $table) {
          $table->dropColumn('image');
          $table->dropColumn('delivery');
      });
    }
}
