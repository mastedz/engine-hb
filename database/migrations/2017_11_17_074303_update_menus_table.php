<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('menus', function (Blueprint $table) {
          $table->string('position', 50)->change();
          $table->string('url', 225)->after('slug');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('menus', function (Blueprint $table) {
          $table->integer('position')->change();
          $table->dropColumn('url');
      });
    }
}
